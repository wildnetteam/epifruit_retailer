//
//  UIView+BorderAndDesign.m
//  PODRetailerApp
//
//  Created by Arpana on 29/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "UIView+BorderAndDesign.h"

@implementation UIView (BorderAndDesign)

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}

@end

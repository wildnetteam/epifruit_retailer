//
//  UIView+BorderAndDesign.h
//  PODRetailerApp
//
//  Created by Arpana on 29/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BorderAndDesign)

@property (nonatomic,readwrite) IBInspectable UIColor *borderColor;
@property (nonatomic, readwrite) IBInspectable CGFloat borderWidth;
@property (nonatomic, readwrite) IBInspectable CGFloat cornerRadius;

@end

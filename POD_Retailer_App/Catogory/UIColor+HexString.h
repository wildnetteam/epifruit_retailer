//
//  UIColor+HexString.h
//  PODRetailerApp
//
//  Created by Arpana on 01/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)

+ (UIColor *)colorWithHexString:(NSString *)hexString;
+ (id)colorWithHex:(unsigned int)hex;

@end

/*
 Copyright (C) 2010 Saxo Bank. All rights reserved.
 */

/*!
 @brief UIImage Category
 
 This is a category over UIImage class object to get the image for specified scale
 
 @ingroup models
 
 @author Satyadev Sain
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIImage (Scale)
-(UIImage*)scaleToSize:(CGSize)size;
-(UIImage *)crop:(CGRect) cropRect;
- (UIImage*) scaledToSize:(CGSize)newSize;
- (UIImage *)imageWithCornerWidth:(int)cornerWidth   height:(int)cornerHeight;
- (UIImage *)fixOrientation;
- (UIImage *)scaleToWidth:(CGFloat)width;

@end

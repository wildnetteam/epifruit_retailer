//
//  AppDelegate.m
//  POD_Retailer_App
//
//  Created by Arpana on 10/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "DashboardViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "ApplyThemesForViewClass.h"
#import "UserManager.h"
#import "BraintreeCore.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "PushView.h"
#import "UIView+XIB.h"
#import "NotificationVC.h"

@import UserNotifications;

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (AppDelegate*) getAppDelegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //Set up crash analytics
    [Fabric with:@[[Crashlytics class]]];

    // Check if a pathway to a random host exists
    _hostReachable = [Reachability reachabilityWithHostName: @"www.google.com"];
    
    [_hostReachable startNotifier];
    
    BOOL internetActive =  ([_internetReachability currentReachabilityStatus] != NotReachable);
    BOOL hostActive =  ([_hostReachable currentReachabilityStatus] != NotReachable);
    
    if (internetActive || hostActive)
    {
        [LSUtils updateNetworkStatus:YES];
    }else
    {
        [LSUtils updateNetworkStatus:NO];
    }
    [self RegisterNotification];

    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [BTAppSwitch setReturnURLScheme:@"com.downsweep.payments"];
    
    //Create a shared instance of facebook Manager
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    _fbManager = [[FBSDKLoginManager alloc]init];
    
    //Create a shared instance for google SignIn
    
    _googleSignInInstance = [GIDSignIn sharedInstance];
    
    _googleSignInInstance.clientID = @"525366193389-383tb8hro1q5l45adinc6bpb9epkfrto.apps.googleusercontent.com"; //@"63006533716-2ql8o59olm435tjidr1jfsh5qng3je1c.apps.googleusercontent.com";//
    
   sharedUtils.locationManager= [[CLLocationManager alloc]init];

    //Provide key for google place api
    //map related credential
    
    [GMSServices provideAPIKey:@"AIzaSyAx1j2P1zt6aRcDnHtsfvwnpMBfpSN-SpY"];
    
    [GMSPlacesClient provideAPIKey:@"AIzaSyAx1j2P1zt6aRcDnHtsfvwnpMBfpSN-SpY"];
    
    _leftMenu = (LeftMenuViewController*)[sharedUtils.mainStoryboard
                                          instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    LoginViewController *login = (LoginViewController*)[sharedUtils.mainStoryboard
                                                        instantiateViewControllerWithIdentifier: @"LoginViewController"];
    DashboardViewController *dashboard = (DashboardViewController*)[sharedUtils.mainStoryboard
                                                                    instantiateViewControllerWithIdentifier: @"DashboardViewController"];
    
    _SwiftySideMenu = (SwiftySideMenuViewController*)[sharedUtils.mainStoryboard
                                                      instantiateViewControllerWithIdentifier: @"SwiftySideMenuViewController"];
    
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (localNotif) {

        //open notification screen
        UIViewController*  vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NotificationVC"];
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        
    } else {
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ISLOGGEDIN"] == true) {
            self.navigationController = [[UINavigationController alloc] initWithRootViewController:dashboard];
        }
        else
        {
            self.navigationController = [[UINavigationController alloc] initWithRootViewController:login];
            
        }
    }
    _SwiftySideMenu.centerViewController = self.navigationController;
    _SwiftySideMenu.leftViewController = _leftMenu;
    
    self.window.rootViewController = _SwiftySideMenu;
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    
    [sharedUtils getCurrentLocationWithCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
    }];
    
    [ApplyThemesForViewClass applyThemeToNavigationBar];
    [FIRApp configure];
    [self authToFirebase];

    return YES;
}

//check login type to return correct url
- (BOOL)application:(UIApplication *)app
     openURL:(NSURL *)url
            options:(NSDictionary *)options {
    
    if ([[LSUtils getFromUserDefaultsForKeyString:@"logintype"] isEqualToString:@"google"]) {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    
    else if([[LSUtils getFromUserDefaultsForKeyString:@"logintype"] isEqualToString:@"facebook"]){
        return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                              openURL:url
                                                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
        
    }
//    else if ([url.scheme localizedCaseInsensitiveCompare:@"com.downsweep.payments"] == NSOrderedSame) {
//        return [BTAppSwitch handleOpenURL:url options:options];
//    }
    else
        return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    [self LoadNotificationCount];
}
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
   // [self saveContext];
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    NSLog(@"Application db URL %@:", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Network Change Notification Function

- (void)handleNetworkChange:(NSNotification *)note
{
    BOOL internetActive = NO;
    BOOL hostActive = NO;
    // Called after network status changes
    NetworkStatus internetStatus = [_internetReachability currentReachabilityStatus];
    
    switch (internetStatus)
    {
            // Case: No internet
        case NotReachable:
        {
            internetActive = NO;
            //NSLog(@"The internet is down.");
            break;
        }
        case ReachableViaWiFi:
        {
            internetActive = YES;
            //NSLog(@"The internet is working via WIFI.");
            break;
        }
        case ReachableViaWWAN:
        {
            internetActive = YES;
            //NSLog(@"The internet is working via WWAN.");
            break;
        }
    }
    // Check if the host site is online
    NetworkStatus hostStatus = [_hostReachable currentReachabilityStatus];
    //NSLog(@"HostStatus--> %d",hostStatus);
    switch (hostStatus)
    {
        case NotReachable:
        {
            hostActive = NO;
            //NSLog(@"A gateway to the host server is down.");
            break;
        }
        case ReachableViaWiFi:
        {
            hostActive = YES;
            //NSLog(@"A gateway to the host server is working via WIFI.");
            break;
        }
        case ReachableViaWWAN:
        {
            hostActive = YES;
            //NSLog(@"A gateway to the host server is working via WWAN.");
            break;
        }
    }
    
    if (internetActive && hostActive)
    {
        [LSUtils updateNetworkStatus:YES];
    }
    else
    {
        [LSUtils updateNetworkStatus:NO];
    }
}

- (void)authToFirebase {
    
    // Note that your Database URL is automatically determined from your GoogleService-Info.plist file so you don't need to specify it.
    _rootRef= [[FIRDatabase database] reference];
    _database = [_rootRef database];
    [[FIRAuth auth] signInAnonymouslyWithCompletion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error on login %@", error);
        }else{
            NSLog(@"AnonymousUserID :%@", user.uid);
            self.displayName_ = [NSString stringWithFormat:@"%@_id%@",user.uid,[UserManager getUserID]];//user.uid;
        }
    }];
    
}

#pragma  mark - Notification

-(void)RegisterNotification{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound +UNAuthorizationOptionBadge;
    [center requestAuthorizationWithOptions:options
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              if (!granted) {
                                  NSLog(@"Something went wrong");
                              }
                          }];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", deviceToken);
    NSString * token = [[[[deviceToken description]
                          stringByReplacingOccurrencesOfString: @"<" withString: @""]
                         stringByReplacingOccurrencesOfString: @">" withString: @""]
                        stringByReplacingOccurrencesOfString: @" " withString: @""];
    [UserManager saveUserCredentials:token withvar:PushToken];
    NSLog(@"The generated device token string is : %@",token);
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
    
}

- (void)application:(UIApplication* )application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    if(application.applicationState == UIApplicationStateActive){
     //create view and send to notification screen
    NSString *nibName =  @"PushView";
    PushView *viewPop  = [PushView initWithNib:nibName bundle:nil];
    
    viewPop.frame = CGRectMake(0,-60, [[UIScreen mainScreen] bounds].size.width, viewPop.frame.size.height);
    
    [UIView animateWithDuration:.5
                          delay:0.0
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         viewPop.frame = CGRectMake(10,20, [[UIScreen mainScreen] bounds].size.width-20, 60);
                     }
                     completion:^(BOOL finished){
                     }];
    [UIView commitAnimations];
    [viewPop setData:userInfo];
    [viewPop.redirectButton addTarget:self action:@selector(redirectScreen:) forControlEvents:UIControlEventTouchUpInside];

    [self.window addSubview:viewPop];
    
    [NSTimer scheduledTimerWithTimeInterval:7.0
                                         target:self
                                       selector:@selector(doSomethingWhenTimeIsUp:)
                                       userInfo:nil
                                        repeats:NO];
    
    }
    
    if ([UserManager getNotificationCount]){
        int k=[[UserManager getNotificationCount] intValue]+1;
        
        [UserManager saveNotificationCount:[NSString stringWithFormat:@"%d",k]];
        [UIApplication sharedApplication].applicationIconBadgeNumber =[[UserManager getNotificationCount] integerValue];
    }
    else{
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        [UserManager saveNotificationCount:[NSString stringWithFormat:@"%d",1]];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NSdidGetUnReadNotificationCount object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:NSdidRecieveNotification object:userInfo];

}

- (void) doSomethingWhenTimeIsUp:(NSTimer*)t {
    
    // YES! Do something here!!
        //Sleep for 5 seconds, Then Remove
        for (UIView *view in [self.window subviews])
        {
            if([view isKindOfClass:[PushView class]])  {
    
                [UIView animateWithDuration:1.0   delay:4.0
                                    options:UIViewAnimationOptionTransitionFlipFromTop
                                 animations:^{
    
                                     view.frame = CGRectMake(10,-70, [[UIScreen mainScreen] bounds].size.width, view.frame.size.height);
                                     [view removeFromSuperview];
                                 }
                                 completion:^(BOOL finished){
                                     if(finished){
                                     }
                                 }];
            }
        }
        [UIView commitAnimations];

}

-(void)redirectScreen:(id)sender{
    
    for (UIView *view in [self.window subviews])
    {
        if([view isKindOfClass:[PushView class]])  {
            
            [UIView animateWithDuration:1.0   delay:0.0
                                options:UIViewAnimationOptionTransitionFlipFromTop
                             animations:^{
                                 
                                 view.frame = CGRectMake(10,-70, [[UIScreen mainScreen] bounds].size.width, view.frame.size.height);
                             }
                             completion:^(BOOL finished){
                                 if(finished){
                                     [view removeFromSuperview];
                                 }
                             }];
        }
    }

    [UIView commitAnimations];
    //Go to particular view
    UIViewController*  vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NotificationVC"];
    [self.navigationController pushViewController:vc animated:YES];

}

-(void)LoadNotificationCount{
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",getNotificationCount_Service,[UserManager getUserID]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{ if (data.length > 0 && error == nil)
                                      {
                                          NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                   options:0
                                                                                                     error:NULL];
                                          NSLog(@"Response:%@",Response);
                                          if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                              if (![[Response objectForKey:@"count"] isEqualToString:@"0"]) {
                                                  [UserManager saveNotificationCount:[Response objectForKey:@"count"]];
                                                  [UIApplication sharedApplication].applicationIconBadgeNumber =[[UserManager getNotificationCount] integerValue];
                                              }
                                              else{
                                                  [UserManager saveNotificationCount:[NSString stringWithFormat:@"%d",0]];
                                                  [UIApplication sharedApplication].applicationIconBadgeNumber =0;
                                              }
                                          }
                                          [[NSNotificationCenter defaultCenter] postNotificationName:NSdidGetUnReadNotificationCount object:nil];
                                      }
                                      else{
                                      }
                                      });
                                  }];
    [task resume];
}

@end

//
//  TransportModel.m
//  PODRetailerApp
//
//  Created by Arpana on 18/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "TransportModel.h"

@implementation TransportModel
-(id)initWithModeName:(NSString*)mode ModeID:(NSString*)modeId{
    
    self =[super init];
    
    if(self){
        
        _tMode = mode;
        _tId = modeId;
        return self;
    }
    return self;
}
@end

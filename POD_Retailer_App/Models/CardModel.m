//
//  CardModel.m
//  PODRetailerApp
//
//  Created by Arpana on 09/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "CardModel.h"

@implementation CardModel

-(id)initWithCardDetails:(NSString*)cardNo :(NSString*)month :(NSString*)year :(NSString*)type{
    
    self = [super init];
    if(self){
    _cardNumber = [NSString stringWithString:cardNo];
    _expYear = year;
    _expMonth = month;
    _cardType = type;
    }
    return self;
}

@end

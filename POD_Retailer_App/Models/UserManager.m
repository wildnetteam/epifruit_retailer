//  UserManager.h
//  PODApplication
//
//  Created by Arpana on 10/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager

+(NSString*)getTokenID {
    
     return [[NSUserDefaults standardUserDefaults] objectForKey:k_LoginTokenID];
//    return [dict objectForKey:k_LoginTokenID];
}

+(NSString*)getEmail {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:k_UserEmail];
}

+(NSString*)getUserImageURL {

    return [[NSUserDefaults standardUserDefaults] objectForKey:k_UserImageURL];
}


+(NSString*)getUserName {

    return [[NSUserDefaults standardUserDefaults] objectForKey:k_UserName];
}

+(NSString*)getFirstName {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:k_FirstName];
}
+(NSString*)getLastName {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:k_LastName];
}

+(NSString*)getUserPhoneNumber{
    
  return [[NSUserDefaults standardUserDefaults] objectForKey:k_UserPhoneNumber];

}

+(NSString*)getUserID {

   return [[NSUserDefaults standardUserDefaults] objectForKey:k_LoggedInUserID];
}

+(NSString*)getUserReferenceCode {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:k_UserReferenceCode];
}

+(NSString*)getLoginType {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:k_LoginType];
}

+(void)saveUserCredentials:(NSString*)value withvar:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setValue:value forKey:key];
        [standardUserDefaults synchronize];
    }
}

+(void)saveToBoolDefaults:(BOOL)value withvar:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setBool:value forKey:key];
        [standardUserDefaults synchronize];
    }
}

+(BOOL)BoolDefaultsForKey:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL val = NO;
    
    if (standardUserDefaults)
        val = [standardUserDefaults boolForKey:key];
    
    return val;
}

//+(void)saveUserName:(NSMutableDictionary*)userInfo{
//    
//    for (NSString* key in userInfo)
//    {
//        id value = [userInfo objectForKey:key];
//        [userCredInfo setObject:[NSString stringWithFormat:@"%@",value] forKey:key];
//    }
//}

+(void)saveUserCredential:(NSMutableDictionary*)userInfo
{
    NSMutableDictionary *userCredInfo=[[NSMutableDictionary alloc]init];
    if(userInfo.count>0)
    {
        for (NSString* key in userInfo)
        {
            id value = [userInfo objectForKey:key];
            [userCredInfo setObject:[NSString stringWithFormat:@"%@",value] forKey:key];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:userCredInfo forKey:K_USERCREDENTIAL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)removeUserCredential
{
   [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
}


+(void)saveNotificationCount:(NSString*)count
{
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:k_NotificationCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString*)getNotificationCount
{
    NSString *count=[[NSUserDefaults standardUserDefaults] objectForKey:k_NotificationCount];
    return count;
}
+(NSString*)getDeviceToken{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:PushToken];
}


@end



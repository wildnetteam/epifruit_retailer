//
//  OrderHistoryModel.h
//  PODRetailerApp
//
//  Created by Arpana on 24/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderHistoryModel : NSObject
@property (nonatomic, retain) NSString* productId;
@property (nonatomic, retain) NSString* productTitle;
@property (nonatomic, retain) NSString* productDescription;
@property (nonatomic, retain) NSString* productdateAndTime;
@property (nonatomic, retain) NSString* productOrderNo;
@property (nonatomic, retain) NSString* imagebaseURL;
@property (nonatomic, retain) NSString* pickAdress;
@property (nonatomic, retain) NSDictionary* pickUpCordinate;
@property (nonatomic, retain) NSString* delAdress;
@property (nonatomic, retain) NSString* price;
@property (nonatomic) int orderStatus;
@property (nonatomic) int imageCount;
@property (nonatomic, retain) NSString* randomImageString;
//For Bid orders
@property (nonatomic, retain) NSString* minRate;
@property (nonatomic, retain) NSString* maxrate;
@property (nonatomic, retain) NSString* proposalCount;
@property (nonatomic) int driverCount;
////////////////////////
@property (nonatomic) int jumpPrice;
@property (nonatomic) int productstatus;
@property (nonatomic, retain) NSMutableArray* productImagesArray;

-(id)initWithTitle:(NSString*)title Description:(NSString*)description OrderNo:(NSString*)orderNumber Status:(int)status DateAndTime:(NSString*)dateAndTime;
@end

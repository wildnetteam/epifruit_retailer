//  UserManager.h
//  PODApplication
//
//  Created by Arpana on 10/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface UserManager : NSObject

@property (nonatomic, retain) NSMutableDictionary *userCompleteInfo;

+(void)saveUserCredential:(NSMutableDictionary*)userInfo;
+(void)saveUserCredentials:(NSString*)value withvar:(NSString*)key;

+(void)removeUserCredential;
+(NSString*)getUserName;
+(NSString*)getUserImageURL;
+(NSString*)getTokenID;
+(NSString*)getUserID;
+(NSString*)getLoginType;
+(NSString*)getEmail;
+(NSString*)getFirstName;
+(NSString*)getLastName;
+(NSString*)getUserPhoneNumber;
+(NSString*)getDeviceToken;
+(NSString*)getUserReferenceCode;

+(void)saveNotificationCount:(NSString*)count;

+(NSString*)getNotificationCount;

+(BOOL)BoolDefaultsForKey:(NSString*)key;
@end

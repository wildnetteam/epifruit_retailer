//
//  TransportModel.h
//  PODRetailerApp
//
//  Created by Arpana on 18/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransportModel : NSObject
@property(nonatomic ,retain)NSString* tMode;
@property(nonatomic ,retain)NSString* tId;

-(id)initWithModeName:(NSString*)mode ModeID:(NSString*)modeId;

@end

//
//  OrderHistoryModel.m
//  PODRetailerApp
//
//  Created by Arpana on 24/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "OrderHistoryModel.h"

@implementation OrderHistoryModel

-(id)initWithTitle:(NSString*)title Description:(NSString*)description OrderNo:(NSString*)orderNumber Status:(int)status DateAndTime:(NSString*)dateAndTime{
    
    self = [super init];
    if(self){
        
        _productTitle = title;
        _productDescription = description;
        _productOrderNo = orderNumber;
        _orderStatus = status;
        _productdateAndTime = dateAndTime;
        
    }return self;
}
@end

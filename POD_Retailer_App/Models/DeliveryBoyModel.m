//
//  DeliveryBoyModel.m
//  PODRetailerApp
//
//  Created by Arpana on 10/02/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "DeliveryBoyModel.h"

@implementation DeliveryBoyModel




-(id)initWithModeName:(NSString*)name Email:(NSString*)email Mobile:(NSString*)mobile Rate:(NSString*)rate AddressLine1:(NSString*)addressLine1  AddressLine2:(NSString*)addressLine2 City:(NSString*)city State:(NSString*)state Lat:(NSString*)lat Lon:(NSString*)lon TransportMode:(NSString*)transportMode  Profile:(NSString*)profile Distane:(NSString*)distane delId:(NSString*)deliveryBoyID profilBaseURL:(NSString*)baseURL{
    
    self = [super init];
    if(self){
        _dName = name;
        _dEmail = email;
        _dMobile = mobile;
        _dRate = rate;
        _daddressfield1 = addressLine1;
        _daddressfield2 = addressLine2;
        _dcity = city;
        _dstate = state;
        _dlat = lat;
        _dlon = lon;
        _dtransportMode = transportMode;
        _imageName = profile;
        _dDistane = distane;
        _dId = deliveryBoyID;
        _dProfileURL = baseURL;
    }
    return self;
}
@end

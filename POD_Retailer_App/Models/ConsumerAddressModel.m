//
//  ConsumerAddressModel.m
//  PODRetailerApp
//
//  Created by Arpana on 28/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "ConsumerAddressModel.h"

@implementation ConsumerAddressModel
-(id)initWithAddressParamsAsAddressTitle:(NSString*)addressTitle AddressLine1:(NSString*)addressLine1 AddressLine2:(NSString*)addressLine2 ZipCode:(NSString*)zipCode  City:(NSString*)city State:(NSString*)state  Country:(NSString*)country PhoneNumber:(NSString*)phoneNumber Comment:(NSString*)comment userName:(NSString*)userName  userEmail:(NSString*)userEmail Longitute:(float)longi Latitute:(float)lati AddressId:(NSString*)addressId{
    
    self =[super init];
    
    if(self){
        _consumerName = [LSUtils removeLeadingSpacesfromString:userName];
        _consumerEmail = [LSUtils removeLeadingSpacesfromString:userEmail];
        _addressTitle = [LSUtils removeLeadingSpacesfromString:addressTitle];
        _addressLine1 = [LSUtils removeLeadingSpacesfromString:addressLine1];
        _addressLine2 = [LSUtils removeLeadingSpacesfromString:addressLine2];
        _zipCode = [LSUtils removeLeadingSpacesfromString:zipCode];
        _city = [LSUtils removeLeadingSpacesfromString:city];
        _state = [LSUtils removeLeadingSpacesfromString:state];
        _country = [LSUtils removeLeadingSpacesfromString:country];
        _phoneNumber = [LSUtils removeLeadingSpacesfromString:phoneNumber];
        _comment = [LSUtils removeLeadingSpacesfromString:comment];
        _longitute = longi;
        _latitue = lati;
        _addressId =addressId;
        return self;
    }
    return self;
}
@end

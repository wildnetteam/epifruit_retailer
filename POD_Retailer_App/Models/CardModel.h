//
//  CardModel.h
//  PODRetailerApp
//
//  Created by Arpana on 09/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardModel : NSObject

@property (nonatomic, retain) NSString *cardNumber;
@property (nonatomic, retain) NSString *expMonth;
@property (nonatomic, retain) NSString *expYear;
@property (nonatomic, retain) NSString* cardType;

-(id)initWithCardDetails:(NSString*)cardNo :(NSString*)month :(NSString*)year :(NSString*)type;
@end

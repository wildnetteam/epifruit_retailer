//
//  DeliveryBoyModel.h
//  PODRetailerApp
//
//  Created by Arpana on 10/02/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeliveryBoyModel : NSObject

@property(nonatomic ,retain)NSString* dName;
@property(nonatomic ,retain)NSString* dEmail;
@property(nonatomic ,retain)NSString* dMobile;
@property (nonatomic, retain) NSString* imageName;
@property(nonatomic ,retain)NSString* dRate;
@property(nonatomic ,retain)NSString* daddressfield1;
@property(nonatomic ,retain)NSString* daddressfield2 ;
@property(nonatomic ,retain)NSString* dcity;
@property(nonatomic ,retain)NSString* dstate;
@property(nonatomic ,retain)NSString* dlat;
@property(nonatomic ,retain)NSString* dlon;
@property(nonatomic ,retain)NSString* dtransportMode;
@property (nonatomic, retain) NSString* currentOrderStatus;
@property (nonatomic) int isBooked;
@property(nonatomic ,retain)NSString* dProfileURL;
@property(nonatomic ,retain)NSString* dDistane;
@property(nonatomic ,retain)NSString* dId;
@property(nonatomic ,retain)NSDictionary* dModeSelcted;
@property(nonatomic ,retain)NSString* dBidAmount;
@property(nonatomic ,retain)NSString* dOrderBidId;
@property(nonatomic ,retain) NSString *delBoyImageBaseUrl;

@property(nonatomic )int isfavouite;


@property (nonatomic, retain) NSString* deliveryCostPerDeliveryBoy;

//Used when bid screen comes


-(id)initWithModeName:(NSString*)name Email:(NSString*)email Mobile:(NSString*)mobile Rate:(NSString*)rate AddressLine1:(NSString*)addressLine1  AddressLine2:(NSString*)addressLine2 City:(NSString*)city State:(NSString*)state Lat:(NSString*)lat Lon:(NSString*)lon TransportMode:(NSString*)transportMode  Profile:(NSString*)profile Distane:(NSString*)distane delId:(NSString*)deliveryBoyID profilBaseURL:(NSString*)baseURL;

@end


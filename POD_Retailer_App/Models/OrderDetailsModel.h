//
//  OrderDetailsModel.h
//  PODRetailerApp
//
//  Created by Arpana on 20/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"
#import "ConsumerAddressModel.h"
@interface OrderDetailsModel : NSObject
@property (nonatomic, retain) NSString* productTitle;
@property (nonatomic, retain) NSString* productDescription;
@property (nonatomic, retain) NSString* deliveryCost;
@property (nonatomic, retain) NSString* deliveryCostPerDeliveryBoy;
@property (nonatomic, retain)AddressModel* productPickUp;
@property (nonatomic, retain)ConsumerAddressModel* productDelivery;
@property (nonatomic) NSArray *transport;
@property (nonatomic) NSMutableArray *driverCopleteDetails;
@property (nonatomic, retain) NSString* maxAmount;
@property (nonatomic, retain) NSString* minAmount;
@property (nonatomic, retain) NSString* redeemPrice;
@property (nonatomic, retain) NSString* packageType;
@property (nonatomic, retain) NSString* productWeight;
@property (nonatomic, retain) NSString* productQuantity;
@property (nonatomic, retain) NSString* productCost;
@property (nonatomic) BOOL isJumpedPrice;
@property (nonatomic) int isRedeemPrice;
@property (nonatomic, retain) NSString* productdateAndTime;
@property (nonatomic, retain) NSString* productOrderNo;

@property (nonatomic, retain) NSString* imagebaseURL;
@property (nonatomic, retain) NSMutableArray* productImagesArray;





@end

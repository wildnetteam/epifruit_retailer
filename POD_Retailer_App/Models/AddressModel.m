//
//  AddressModel.m
//  PODRetailerApp
//
//  Created by Arpana on 16/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "AddressModel.h"

@implementation AddressModel

-(id)initWithAddressParamsAsAddressTitle:(NSString*)addressTitle AddressLine1:(NSString*)addressLine1 AddressLine2:(NSString*)addressLine2 ZipCode:(NSString*)zipCode  City:(NSString*)city State:(NSString*)state  Country:(NSString*)country PhoneNumber:(NSString*)phoneNumber Comment:(NSString*)comment BussinessName:(NSString*)bussinessName Longitute:(float)longi Latitute:(float)lati AddressId:(NSString*)addressId;{
    
    self =[super init];
    
    if(self){
        
        _addressTitle = [LSUtils removeLeadingSpacesfromString:addressTitle];
        _addressLine1 = [LSUtils removeLeadingSpacesfromString:addressLine1];
        _addressLine2 = [LSUtils removeLeadingSpacesfromString:addressLine2];
        _zipCode = [LSUtils removeLeadingSpacesfromString:zipCode];
        _city = [LSUtils removeLeadingSpacesfromString:city];
        _state = [LSUtils removeLeadingSpacesfromString:state];
        _country = [LSUtils removeLeadingSpacesfromString:country];
        _phoneNumber = [LSUtils removeLeadingSpacesfromString:phoneNumber];
        _comment = [LSUtils removeLeadingSpacesfromString:comment];
        _bussinessname = [LSUtils removeLeadingSpacesfromString:bussinessName];
        _longitute = longi;
        _latitue = lati;
        _addressId =addressId;
        return self;
    }
    return self;
}

@end

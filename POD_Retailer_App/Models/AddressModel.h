//
//  AddressModel.h
//  PODRetailerApp
//
//  Created by Arpana on 16/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressModel : NSObject

@property(nonatomic ,retain)NSString* addressTitle;
@property(nonatomic ,retain)NSString* addressLine1;
@property(nonatomic ,retain)NSString* addressLine2;
@property(nonatomic ,retain)NSString* zipCode;
@property(nonatomic ,retain)NSString* city;
@property(nonatomic ,retain)NSString* state;
@property(nonatomic ,retain)NSString* country;
@property(nonatomic ,retain)NSString* comment;
@property(nonatomic ,retain)NSString* bussinessname;
@property(nonatomic ,retain)NSString* phoneNumber;
@property(nonatomic ,retain)NSString* addressId;
@property(nonatomic) double longitute;
@property(nonatomic)double latitue;



-(id)initWithAddressParamsAsAddressTitle:(NSString*)addressTitle AddressLine1:(NSString*)addressLine1 AddressLine2:(NSString*)addressLine2 ZipCode:(NSString*)zipCode  City:(NSString*)city State:(NSString*)state  Country:(NSString*)country PhoneNumber:(NSString*)phoneNumber Comment:(NSString*)comment BussinessName:(NSString*)bussinessName Longitute:(float)longi Latitute:(float)lati AddressId:(NSString*)addressId;
@end

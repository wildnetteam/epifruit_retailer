
//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Arpana on 07/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "MenuVC.h"

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setFrames];
    [self setFonts];
   
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
 
    
    //MenuItems
   
-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setFrames
{
    self.viewHeader.frame=CGRectMake(0, 0, SCREEN_WIDTH, [ConstantsSingleton getValueForDeviceFrame:85]);
    self.userImg.frame=CGRectMake([ConstantsSingleton getValueForDeviceFrame:14],self.viewHeader.frame.size.height/2-[ConstantsSingleton getValueForDeviceFrame:20] , [ConstantsSingleton getValueForDeviceFrame:48], [ConstantsSingleton getValueForDeviceFrame:48]);
    self.userNameLbl.frame=CGRectMake(CGRectGetMaxX(self.userImg.frame)+[ConstantsSingleton getValueForDeviceFrame:15], self.viewHeader.frame.size.height/2-[ConstantsSingleton getValueForDeviceFrame:6], [ConstantsSingleton getValueForDeviceFrame:130], [ConstantsSingleton getValueForDeviceFrame:20]);
    self.menuTable.frame=CGRectMake(0, CGRectGetMaxY(self.viewHeader.frame), SCREEN_WIDTH, SCREEN_HEIGHT-self.viewHeader.frame.size.height);
}

-(void)setFonts
{
    self.userNameLbl.font=FONT_Heading_Label;
}


#pragma -mark TableViewdelegates/Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            return self.aboutDropwashItems.count;
        }
        return 1;
    }
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (![UserManager getEmail])
    if (section==3) return YES;
    
    if ([UserManager getEmail])
        if (section==6) return YES;
    
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (![UserManager getEmail])
    return self.beforeLoginMenu.count;
    else  return self.afterLoginMenu.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"menuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    
    UILabel *lineLabel=[[UILabel alloc]initWithFrame:CGRectMake([ConstantsSingleton getValueForDeviceFrame:47], [ConstantsSingleton getValueForDeviceFrame:39], SCREEN_WIDTH-[ConstantsSingleton getValueForDeviceFrame:39], 1)];
    lineLabel.backgroundColor=COLOR_MenuSeperator;
    
    arrowImg=[[UIImageView alloc]initWithFrame:CGRectMake([ConstantsSingleton getValueForDeviceFrame:190],[ConstantsSingleton getValueForDeviceFrame:19],[ConstantsSingleton getValueForDeviceFrame:9],[ConstantsSingleton getValueForDeviceFrame:5])];
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake([ConstantsSingleton getValueForDeviceFrame:15], [ConstantsSingleton getValueForDeviceFrame:13], [ConstantsSingleton getValueForDeviceFrame:16], [ConstantsSingleton getValueForDeviceFrame:16])];
    imageView.contentMode=UIViewContentModeScaleAspectFit;
    
    UILabel *textLabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame)+[ConstantsSingleton getValueForDeviceFrame:17], [ConstantsSingleton getValueForDeviceFrame:11], [ConstantsSingleton getValueForDeviceFrame:150], [ConstantsSingleton getValueForDeviceFrame:20])];
    cell.backgroundColor=COLOR_Bg_NavBar;
    textLabel.font = FONT_Common_Label;
    textLabel.textColor=COLOR_White;
    textLabel.textAlignment=NSTextAlignmentLeft;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if ([UserManager getEmail]) {
        textLabel.text=[self.afterLoginMenu objectAtIndex:indexPath.section];
        imageView.image=[UIImage imageNamed:[self.afterLoginMenuImages objectAtIndex:indexPath.section]];
    }
    else{
        textLabel.text=[self.beforeLoginMenu objectAtIndex:indexPath.section];
        imageView.image=[UIImage imageNamed:[self.beforeLoginMenuImages objectAtIndex:indexPath.section]];
    }
    [cell addSubview:textLabel];
    [cell addSubview:imageView];
    [cell addSubview:lineLabel];
    if ([UserManager getEmail]) {
        self.userNameLbl.text=[[NSString stringWithFormat:@"%@ %@",[[UserManager getLoginUser]objectForKey:@"f_name"],[[UserManager getLoginUser]objectForKey:@"l_name"]]uppercaseString];
        self.userImg.image=[UIImage imageNamed:@"user_pic"];
        
        NSString *userImagePath =[[UserManager getUserImage] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        if (userImagePath) {
            [self.userImg.layer setCornerRadius:self.userImg.frame.size.width / 2];
            self.userImg.clipsToBounds = YES;
            [self.userImg setImage:[UIImage imageWithData:[UserManager getUserImage]]];
        }
        
        if (indexPath.section==6) {
            arrowImg.image=[UIImage imageNamed:@"arrow_down"];
            [cell addSubview:arrowImg];
        }
    }
    else{
        if (indexPath.section==3) {
            self.userNameLbl.text=@"WELCOME";
            self.userImg.image=[UIImage imageNamed:@"Dropwash-Logo"];
            arrowImg.image=[UIImage imageNamed:@"arrow_down"];
            [cell addSubview:arrowImg];
        }
    }
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (indexPath.row)
        {
            // all other rows except first
            cell.bounds = CGRectMake(cell.bounds.origin.x-[ConstantsSingleton getValueForDeviceFrame:31],
                                     cell.bounds.origin.y,
                                     cell.bounds.size.width,
                                     ceilf(cell.bounds.size.height));
            [arrowImg removeFromSuperview];
        }
        
        imageView.image=[UIImage imageNamed:[self.aboutDropwashImages objectAtIndex:indexPath.row]];
        textLabel.text = [self.aboutDropwashItems objectAtIndex:indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UINavigationController* loginPageNavController;
    
    if (indexPath.section==0) {
        BookOrderVC *bookOrder= [[BookOrderVC alloc]initWithNibName:@"BookOrderVC" bundle:nil];
        loginPageNavController = [[UINavigationController alloc] initWithRootViewController: bookOrder];
        self.revealController.frontViewController=loginPageNavController;
        [self.revealController showViewController:loginPageNavController];
    }
    
    if ([UserManager getEmail]) {
        if (indexPath.section==1) {
                OrderHistoryVC *OrderHistory= [[OrderHistoryVC alloc]initWithNibName:@"OrderHistoryVC" bundle:nil];
                loginPageNavController = [[UINavigationController alloc] initWithRootViewController: OrderHistory];
                self.revealController.frontViewController=loginPageNavController;
                [self.revealController showViewController:loginPageNavController];
            }
        if (indexPath.section==2) {
                PriceListVC *PriceList= [[PriceListVC alloc]initWithNibName:@"PriceListVC" bundle:nil];
                loginPageNavController = [[UINavigationController alloc] initWithRootViewController: PriceList];
                self.revealController.frontViewController=loginPageNavController;
                [self.revealController showViewController:loginPageNavController];
            }
        if (indexPath.section==3) {
                FaqVC *faqVC= [[FaqVC alloc]initWithNibName:@"FaqVC" bundle:nil];
                loginPageNavController = [[UINavigationController alloc] initWithRootViewController: faqVC];
                self.revealController.frontViewController=loginPageNavController;
                [self.revealController showViewController:loginPageNavController];
            }
        if (indexPath.section==4) {
            NSMutableArray *sharingItems = [NSMutableArray new];
            [sharingItems addObject:strInviteText];
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
            [activityController setValue:@"Try Dropwash App" forKey:@"subject"];
            [self presentViewController:activityController animated:YES completion:nil];
        }
        if (indexPath.section==5) {
                MyProfileViewVC *profileVC= [[MyProfileViewVC alloc]initWithNibName:@"MyProfileViewVC" bundle:nil];
                loginPageNavController = [[UINavigationController alloc] initWithRootViewController: profileVC];
                self.revealController.frontViewController=loginPageNavController;
                [self.revealController showViewController:loginPageNavController];
        }
    }
    else{
        if (indexPath.section==1) {
        PriceListVC *PriceList= [[PriceListVC alloc]initWithNibName:@"PriceListVC" bundle:nil];
        loginPageNavController = [[UINavigationController alloc] initWithRootViewController: PriceList];
        self.revealController.frontViewController=loginPageNavController;
        [self.revealController showViewController:loginPageNavController];
        }
        if (indexPath.section==2) {
        FaqVC *faqVC= [[FaqVC alloc]initWithNibName:@"FaqVC" bundle:nil];
        loginPageNavController = [[UINavigationController alloc] initWithRootViewController: faqVC];
        self.revealController.frontViewController=loginPageNavController;
        [self.revealController showViewController:loginPageNavController];
        }
    }
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                 arrowImg.image=[UIImage imageNamed:@"arrow_down"];
             //   [tableView reloadData];
                [expandedSections removeIndex:section];
            }
            else
            {
                [expandedSections addIndex:section];
                 arrowImg.image=[UIImage imageNamed:@"arrow_up"];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            if (currentlyExpanded)
            {
                arrowImg.image=[UIImage imageNamed:@"arrow_down"];
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationFade];
                [tableView reloadData];
            }
            else
            {
                arrowImg.image=[UIImage imageNamed:@"arrow_up"];
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationFade];
            }
        }
    }
    
    if (indexPath.row==1) {
        ContactUsVC *ContactUs= [[ContactUsVC alloc]initWithNibName:@"ContactUsVC" bundle:nil];
        loginPageNavController = [[UINavigationController alloc] initWithRootViewController: ContactUs];
        self.revealController.frontViewController=loginPageNavController;
        [self.revealController showViewController:loginPageNavController];
    }
    
    if (indexPath.row==2){
        NSString * appId = @"1121132473";
        NSString * theUrl = [NSString  stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",appId];
        if ([[UIDevice currentDevice].systemVersion integerValue] > 6) theUrl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",appId];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    }
    
    if (indexPath.row==3){
        NSString * fbUrl = [NSString  stringWithFormat:@"https://facebook.com/DropwashApp/"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbUrl]];
    }
    
    if (indexPath.row==4) {
        LegalVC *Legal= [[LegalVC alloc]initWithNibName:@"LegalVC" bundle:nil];
        loginPageNavController = [[UINavigationController alloc] initWithRootViewController: Legal];
        self.revealController.frontViewController=loginPageNavController;
        [self.revealController showViewController:loginPageNavController];
    }

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [ConstantsSingleton getValueForDeviceFrame:40];
}


@end

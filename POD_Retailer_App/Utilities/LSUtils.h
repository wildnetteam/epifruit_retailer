//
//  LSUtils.h
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import <GooglePlaces/GooglePlaces.h>
#import <GooglePlacePicker/GooglePlacePicker.h>
#import "UserManager.h"
#import "AddressModel.h"
#import "CardModel.h"
#define sharedUtils [LSUtils sharedUtitlities]
#define shareUtilesDelegate [LSUtils sharedUtitlities].delegate

@interface LSUtils : NSObject{
    MBProgressHUD *progressHUD;
}
///0 => pending, 1=>accepted ,  2 => ongoing , 3 =>compleated , 4 =>order canceled , 5=>order rejected
typedef enum {
    PENDING = 0,
    ACCEPTED,
    ONGOING,
    DROPOFF,
    CANCELED,
    REJECTED,
    COMPLETED
    
}OrderStatusType;

//For screen Size
@property float screenWidth;
@property float screenHeight;
@property float leftMenuWidth;
@property float statusBarHeight;
@property float navigationBarHeight;
@property BOOL shouldDisplayBackButtonWithLeftMenu;
+(instancetype)sharedUtitlities;
-(UIAlertController*)showAlert:(NSString *)title withMessage:(NSString *)message;
-(UIFont *)setFontWithSize:(int)size;

@property (nonatomic,strong) UIStoryboard *mainStoryboard ;
@property (nonatomic,strong) NSArray *mainMenu;
@property (nonatomic,strong) AppDelegate* appdelegateInstance;
-(void)startAnimator:(UIViewController *)controller;
-(void)stopAnimator:(UIViewController *)controller;
+(NSString*)getShortTitle:(NSString*)YourString length:(int)length;
-(void)startAnimatorForCustomview:(UIView *)view;
-(void)stopAnimatorForCustomview:(UIView *)view;
@property(nonatomic,strong) CLLocationManager *locationManager;
// Save valuse to UserDefaults
+(void)saveToUserDefaults:(NSString*)string_to_store keys:(NSString *)key_for_the_String;
+(NSString*)getFromUserDefaultsForKeyString:(NSString*)key_String;
+(NSMutableDictionary *) removeNullValues:(NSMutableDictionary *)dictionary;
+(CGSize)textString:(NSString*)text sizeWithFont:(UIFont*)font constrainedToSize:(CGSize)size;

+(void)updateNetworkStatus:(BOOL)status;

+(BOOL)isNetworkConnected;

@property (nonatomic,retain) GMSPlacesClient *placesClient;

-(void)getCurrentLocationWithCompletion:(void (^) (BOOL success,NSDictionary* result,NSError* error))completion;
+(CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr ;
+(NSString*)getFormatedAddressString:(NSString*)strAddress1 addressLine2:(NSString*)strAddress2 townValue:(NSString*)strTown countyValue:(NSString*)strCountry postCodeValue:(NSString*)strPostCode inLineChangeFormat:(BOOL)boolValue;
-(void)strikeOutText:(NSMutableAttributedString*)originalText :(NSString*)dummyText :(NSRange)range :(UIFont*)font;


- (UIButton*)setNavigationBarLeftButton ;

@property (nonatomic) CLLocationCoordinate2D  CURRENTLOC;
//@property (retain, nonatomic) UIImage *profileImagefromCamera;
+(void)addPaddingToTextField :(UITextField *)textField;

+ (BOOL) validateEmail: (NSString *) candidate;
+(BOOL)checkNilandEmptyString:(NSString*)stringToCheck;
+(NSString*)removeLeadingSpacesfromString :(NSString*)originalString;
+ (BOOL)validatePhoneWithString:(NSString *)phoneString;
+ (BOOL)validatePostalCodeWithString:(NSString *)postalCodeString;
-(void)placeholderSize:(UITextField*)textfield :(NSString*)text;
-(void)addAddressToDataBase :(AddressModel*)place Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion;

-(void)updatePickUpAddress :(AddressModel*)place Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion;
-(void)getAllPickUpAddress:(void (^) (BOOL success,NSDictionary* result,NSError* error))completion;
-(void)updateConsumerAddress :(ConsumerAddressModel*)place Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion;
-(void)addConsumerAddressToDataBase:(ConsumerAddressModel*)place Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion;

-(void)getAllConsumerAddress:(NSString*)searchString :(void (^) (BOOL success,NSDictionary* result,NSError* error))completion;
@property (nonatomic,strong) NSMutableArray *pickUpAddressArray;
@property (nonatomic,strong) NSMutableArray *consumerAddressArray;

//Testing
@property(nonatomic,retain) AddressModel *pickupAddressModel;
@property(nonatomic,retain) ConsumerAddressModel* consumerAddressModel;
@property(nonatomic,retain) CardModel *cardDetails;
-(void)getYourCard:(void (^) (BOOL success, NSDictionary* result,NSError* error))completion;

-(NSMutableAttributedString*)setPickUpFieldsForDeliveryAddress:(ConsumerAddressModel*)obj;
-(NSMutableAttributedString*)setPickUpFields:(AddressModel*)obj;
-(NSMutableString*)getDeliveryAddress:(ConsumerAddressModel*)obj;
-(NSMutableString*)getPickUpAddress:(AddressModel*)obj;

-(NSAttributedString*)getOrderStatus :(int)acceptStatus;

+(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat;
+(NSDate*)getDateFromDateString:(NSString*)dateString byFormat:(NSString*)strFormat;

@property (nonatomic , strong) NSMutableDictionary  *dictOfOrderTosRate;

-(void)getRatingStatusCompletion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion;
//for converting no to ordinal  value
-(NSString*)getDateWithOrdinalValue:(NSString*)dateString;

@property (strong, nonatomic) UIView *noResultsView;
@property (strong, nonatomic) UILabel *noResultsIcon;
@property (strong, nonatomic) UILabel *noResultsLabel;
-(void)showNoResultsViewWithOptionalText:(NSString *)textToDisplay xPosition:(NSInteger)x yPosition:(NSInteger)y screenWidth:(NSInteger)strScreenWidth screenHeight:(NSInteger)strScreenHeight;
+ (BOOL) deleteMyImagesDirectory;

@end

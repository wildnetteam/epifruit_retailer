//
//  MenuViewController.h
//  SlideMenu
//
//  Created by Arpana on 07/10/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardViewController.h"
#import "DeliveryAddressViewController.h"
#import "SwiftySideMenuViewController.h"
#import "UIViewController+SwiftySideMenu.h"
#import "sideMenuCell.h"
@interface LeftMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableIndexSet *expandedSections;
    BOOL currentlyExpanded;
}
@property (strong) NSArray *menuItem;
@property (strong) NSArray *menuSubItem;
@property(strong) sideMenuCell *cell;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@end

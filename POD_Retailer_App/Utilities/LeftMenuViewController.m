//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Arpana on 07/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "sideMenuCell.h"
#import "AddNewCardViewController.h"

@implementation LeftMenuViewController

typedef enum {
    PLACEORDER = 0,
    MYBIDS,
    MYORDERS,
    EXPIREDJOB,
    PICKUPADDRESS,
    DROPOFFADDRESS,
    ACCOUNTSETTING,
    PAYMENTDETAILS,
    MYREWARDS,
    CONTACTUS,
    HELPANDFAQ,
    LEGAL,
    LOGOUT
    
}MenuItemType;

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = NO;
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftMenu.jpg"]];
    
    self.tableView.backgroundView = imageView;
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    currentlyExpanded = true;
    UINib *nib = [UINib nibWithNibName:@"sideMenuCell" bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:@"sideMenuCell"];
    
      sharedUtils.mainMenu = [[NSArray alloc]initWithObjects:@"Place Orders",@"Pending Orders", @"My Orders",@"Expired Orders",@"PickUp Address",@"Delivery Address",@"Account Settings",@"Payment Details", @"My Rewards", @"Contact Us", @"Help & Faq", @"Rate Us",@"Logout",nil];
    
      _menuSubItem = [[NSArray alloc]initWithObjects:@"Legal" , @"        privacy policy", @"        Terms & Conditions",nil];
    
   [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                     FONT_NavigationBar, NSFontAttributeName, nil]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

#pragma -mark TableViewdelegates/Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE5){
        return 41;
    }else return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            return _menuSubItem.count;
        }
        return 1;
    }
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    
    if (section==LEGAL)
        return YES;
    
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sharedUtils.mainMenu.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _cell =  [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
    
    _cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if (indexPath.section==8) {
        
    }
    
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (currentlyExpanded) {
            _cell.arrowImage.image = [UIImage imageNamed:@"down_arrow"];
            _cell.imageWidth.constant = 12;
            _cell.imageHeight.constant = 9;
        }
        else
        {
            _cell.arrowImage.image = [UIImage imageNamed:@"right_arrow"];
            _cell.imageWidth.constant = 9;
            _cell.imageHeight.constant = 12;
            
        }
        
        if (indexPath.row)
        {
            
            [_cell.arrowImage removeFromSuperview];
        }
        
        _cell.titleLabel.text = [self.menuSubItem objectAtIndex:indexPath.row];
    }
    else{
        _cell.titleLabel.text = [sharedUtils.mainMenu objectAtIndex:indexPath.section];
        _cell.arrowImage.hidden = true;
    }
    _cell.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:16];
    _cell.titleLabel.textColor= [UIColor blackColor];
    _cell.backgroundColor = [UIColor clearColor];
  //  IS_IPHONE5?[UIFont fontWithName:@"Lato-Regular" size:16]:[UIFont fontWithName:@"Lato-Regular" size:17];
    
    return _cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *vc ;
    
    vc= nil;
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            NSInteger section = indexPath.section;
            currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                
                rows = [self tableView:tableView numberOfRowsInSection:section];
                _cell.arrowImage.image = [UIImage imageNamed:@"right_arrow"];
                [expandedSections removeIndex:section];
            }
            else
            {
                [expandedSections addIndex:section];
                _cell.arrowImage.image = [UIImage imageNamed:@"down_arrow"];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            if (currentlyExpanded)
            {
                 _cell.arrowImage.image = [UIImage imageNamed:@"down_arrow"];
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationFade];
               // [tableView reloadData];
                NSArray *index = [[NSArray alloc]initWithObjects:indexPath, nil];
                [tableView reloadRowsAtIndexPaths:index withRowAnimation:UITableViewRowAnimationFade];
                
            }
            else
            {
                _cell.arrowImage.image = [UIImage imageNamed:@"down_arrow"];
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationFade];
                NSArray *index = [[NSArray alloc]initWithObjects:indexPath, nil];
                [tableView reloadRowsAtIndexPaths:index withRowAnimation:UITableViewRowAnimationFade];

            }
        }
        else{
            
            if (indexPath.row == 1) {
                
                vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"PrivacyPoliciesViewController"];
            }else  if (indexPath.row == 2){
                vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"TermsAndConditionsViewController"];
            }
        }
    }
  
    switch (indexPath.section)
    {
        case PLACEORDER:
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardViewController"];
            break;
            
           case MYBIDS:
              vc = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"MyBidsViewController"];
            break;
            
        case MYORDERS:
              vc = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier:@"OrderHistoryViewController"];
            break;
            
        case EXPIREDJOB:
            vc = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier:@"ExpiredJobViewController"];
            break;
            
        case PICKUPADDRESS:{
            NSString* controllerName = sharedUtils.pickUpAddressArray.count==0?@"NoAddressViewController":@"PickUpAddressViewController";
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: controllerName];
         }
            break;
            
        case DROPOFFADDRESS:{
            NSString* controllerName = sharedUtils.consumerAddressArray.count==0?@"NoConsumerAddressViewController":@"DeliveryAddressViewController";
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: controllerName];
        }
            break;
            
        case ACCOUNTSETTING:{
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                  @"SettingsViewController"];
        }
            break;
        case PAYMENTDETAILS:{
            NSString* controllerName = sharedUtils.cardDetails == nil?@"NoCardViewController":@"PaymentDtaSourceViewController";
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                  controllerName];
        }
            break;
        case MYREWARDS:{
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                  @"MyRewardListViewController"];
        }
            break;
        case CONTACTUS:{
        
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                  @"ContactUSViewController"];
        }
            break;
        case HELPANDFAQ:{
            
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                  @"AboutUsViewController"];
        }
            break;
         
        case LOGOUT: {
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Logout"
                                         message:@"Are you sure, you want to log out?"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* noButton = [UIAlertAction
                                        actionWithTitle:@"No"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                           
                                        }];
            
            UIAlertAction* yesButton = [UIAlertAction
                                       actionWithTitle:@"Yes"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"ISLOGGEDIN"];
                                           [UserManager removeUserCredential];
                                           
                                          /////////free memory from address and card/////////////////
                                           sharedUtils.cardDetails = nil;
                                           sharedUtils.pickUpAddressArray = nil;
                                           sharedUtils.consumerAddressArray = nil;
                                           sharedUtils.pickupAddressModel = nil;
                                           sharedUtils.consumerAddressModel = nil;
                                         ////////////////////////////////////////////////////////////////
                                           LoginViewController *login = (LoginViewController*)[sharedUtils.mainStoryboard
                                                                                               instantiateViewControllerWithIdentifier: @"LoginViewController"];
                                           
                                           NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:[AppDelegate getAppDelegate].navigationController.viewControllers];
                                           
                                            [navigationArray removeAllObjects];    // This is just for remove all view controller from navigation stack.
                                          
                                           self.navigationController.viewControllers = navigationArray;
                                           
                                       //  [AppDelegate getAppDelegate].navigationController.viewControllers = NULL;
                                           [[AppDelegate getAppDelegate].navigationController pushViewController:login animated:YES];
                                           
                                            self.swiftySideMenu.centerViewController = [AppDelegate getAppDelegate].navigationController;
                                           
                                            [self.swiftySideMenu toggleSideMenu];

                                       }];
            
            [alert addAction:noButton];
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
            break;
    }
    if(vc!=nil) {
        
        [[AppDelegate getAppDelegate].navigationController pushViewController:vc animated:YES];
        [self.swiftySideMenu toggleSideMenu];
    }

}
- (IBAction)crossButtonTapped:(id)sender {
    
    [self.swiftySideMenu toggleSideMenu ];
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserUnteractionChangedNotification object:nil];
}



@end

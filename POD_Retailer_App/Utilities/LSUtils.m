//
//  LSUtils.m
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "LSUtils.h"
#import "TTTOrdinalNumberFormatter.h"
@implementation LSUtils

static BOOL _isInternetConnected;

+(instancetype)sharedUtitlities
{
    static LSUtils *utils;
    static dispatch_once_t token;
    
    dispatch_once(&token,^{
        utils = [[LSUtils alloc]init];
    });
    return utils;
    
}


-(id)init {
    if (self = [super init]) {
        
        //initiate Story board
        _mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                   bundle: nil];
        
        //initialize left menu items
       // _mainMenu = [[NSArray alloc]initWithObjects:@"Place Order", @"My Orders",@"Pick Address",@"Deliver Address", @"Account Setting",@"Contact Us",@"Help and FAQs",@"Rate Us", @"Privacy policies", @"Terms & Conditions",@"Log out" ,nil];
        
        _appdelegateInstance = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [self getNavigationBarHeight];
        // On init set the different values
        self.screenWidth = [[UIScreen mainScreen] bounds].size.width;
        self.screenHeight = [[UIScreen mainScreen] bounds].size.height;
        self.statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
        
        _noResultsView = [[UIView alloc] init];
        _noResultsIcon = [[UILabel alloc] init];
        _noResultsLabel = [[UILabel alloc] init];
        
        _shouldDisplayBackButtonWithLeftMenu = false;
        
    }
    
    return self;
}


- (float) getNavigationBarHeight{
    UIViewController *rootViewController = [[[UIApplication sharedApplication] windows][0] rootViewController];
    self.navigationBarHeight = rootViewController.navigationController.navigationBar.frame.size.height;
    return self.navigationBarHeight;
}

-(UIAlertController*)showAlert:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""  message: message   preferredStyle:    UIAlertControllerStyleAlert];
    
    UIAlertAction *closeAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Ok", @"")
                                  style:UIAlertActionStyleCancel
                                  handler:^(UIAlertAction *action)
                                  {
                                  }];
    
    [alertController addAction:closeAction];
    
    return alertController;
    

}

+ (UILabel*)addLabelForNavigationBarHead
{
    UILabel *headingForNavigationController = [[UILabel alloc] init];
    headingForNavigationController.text =@"";
    headingForNavigationController.frame = CGRectMake(80, 12, 160, 24);
    headingForNavigationController.textColor = [UIColor whiteColor];
    [headingForNavigationController setFont:[UIFont fontWithName:@"segoeui" size:19]];
    headingForNavigationController.textAlignment = NSTextAlignmentCenter;
    return headingForNavigationController;
}


+(UIButton*)setNavigationBarRightButton{

    UIButton*  notificationbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    notificationbutton.frame = CGRectMake(0,0,33,37);
    [notificationbutton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    return notificationbutton;
}

- (UIButton*)setNavigationBarLeftButton {
    
    UIButton *_btnbackButton = [UIButton buttonWithType:UIButtonTypeCustom];
   // [_btnbackButton setBackgroundImage:[UIImage imageNamed:@"back_arrow_register"] forState:UIControlStateNormal];
      _btnbackButton.frame = CGRectMake(0,0,33,37);
    [ _btnbackButton setImage:[UIImage imageNamed:@"back_arrow_register"] forState:UIControlStateNormal];
    return _btnbackButton;
}

-(UIFont *)setFontWithSize:(int)size
{
    UIFont *font = [UIFont fontWithName:@"AvenirNext-Medium" size:size];
    return font;
}


+(NSString*)getShortTitle:(NSString*)YourString length:(int)length{
    
    NSString *string=YourString;
    
    int size= (int)[YourString length];
    
    if (size>length)
    {
        NSMutableString *string1 = [[NSMutableString alloc]init];
        char c;
        for(int index = 0;index <length ;index++)
        {
            c =[string characterAtIndex:index];
            
            [string1 appendFormat:@"%c",c];
        }
        [string1 appendFormat:@"..."];
        string=string1;
        
    }
    return string;
}

+(CGSize)textString:(NSString*)text sizeWithFont:(UIFont*)font constrainedToSize:(CGSize)size
{
   // if(IS_OS_7_OR_LATER){
        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                              font, NSFontAttributeName,
                                              nil];
        
        CGRect frame = [text boundingRectWithSize:size
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:attributesDictionary
                                          context:nil];
        
        return frame.size;
//    }else{
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wdeprecated-declarations"
//        return [text sizeWithFont:font constrainedToSize:size];
//#pragma clang diagnostic pop
//        //return [text sizeWithAttributes:@{NSFontAttributeName:font}];//[text sizeWithFont:font constrainedToSize:size];
//    }
}

#pragma mark- activity indicator method

-(void)startAnimator:(UIViewController *)controller
{
    progressHUD=[MBProgressHUD showHUDAddedTo:controller.view animated:YES];
    progressHUD.labelText=@"Please wait...";
    
}

-(void)startAnimatorForCustomview:(UIView *)view
{
    progressHUD=[MBProgressHUD showHUDAddedTo:view animated:YES];
    progressHUD.labelText=@"Please wait...";
}

-(void)stopAnimatorForCustomview:(UIView *)view
{
   [MBProgressHUD hideHUDForView:view animated:YES];
    progressHUD=nil;
}

-(void)stopAnimator:(UIViewController *)controller
{
    [MBProgressHUD hideHUDForView:controller.view animated:YES];
    progressHUD=nil;
    
}

#pragma marks -UserDefaults Methods

+(void)saveToUserDefaults:(NSString*)string_to_store keys:(NSString *)key_for_the_String
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults) {
        [standardUserDefaults setObject:string_to_store forKey:key_for_the_String];
        [standardUserDefaults synchronize];
    }
}

+(NSString*)getFromUserDefaultsForKeyString:(NSString*)key_String
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:key_String];
}

// Remove the null values from a dictionary retrieved from a CBL document
+(NSMutableDictionary *) removeNullValues:(NSMutableDictionary *)dictionary{
    
    // Remove all the null values
    NSArray *keysForNullValues = [dictionary allKeysForObject:[NSNull null]];
    [dictionary removeObjectsForKeys:keysForNullValues];
    
    // Remove the null values of sub dictionaries recursively
    for (NSString *key in [dictionary allKeys]){
        if ([dictionary[key] isKindOfClass:[NSDictionary class]]){
            dictionary[key] = [LSUtils removeNullValues:[dictionary[key] mutableCopy]];
        }
        else if ([dictionary[key] isKindOfClass:[NSArray class]]){
            dictionary[key] = [LSUtils removeNullValuesFromArray:[dictionary[key] mutableCopy]];
        }
    }
    
    return dictionary;
}


+(NSMutableArray *) removeNullValuesFromArray:(NSMutableArray *)array{
    
    NSMutableArray *returnedArray = [[NSMutableArray alloc] init];
    
    for (NSObject *object in array){
        if ([object isKindOfClass:[NSDictionary class]]){
            [returnedArray addObject:[LSUtils removeNullValues:[object mutableCopy]]];
        }
        else if ([object isKindOfClass:[NSArray class]]){
            [returnedArray addObject:[LSUtils removeNullValuesFromArray:[object mutableCopy]]];
        }
        else if (![object isKindOfClass:[NSNull class]]){
            [returnedArray addObject:object];
        }
    }
    
    return returnedArray;
}
#pragma mark- Network methods

+(void)updateNetworkStatus:(BOOL)status {
    
    _isInternetConnected = status;
    //NSLog(@"_isInternetConnected-->%d",_isInternetConnected);
}

+(BOOL)isNetworkConnected {
    return _isInternetConnected;
}

#pragma mark - Get location of user
//as soon as app launches get the location of user
-(void)getCurrentLocationWithCompletion:(void (^) (BOOL success,NSDictionary* result,NSError* error))completion
{
    
    if([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])

      [_locationManager requestAlwaysAuthorization];
    
        // go do something asynchronous...
        _placesClient = [GMSPlacesClient sharedClient];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *likelihoodList, NSError *error) {
                if (error != nil) {
                    //NSLog(@"Current Place error %@", [error localizedDescription]);
                   return  completion(false, nil,error);
                }
                int cnt =0;
                for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
                    cnt++;
                    if(cnt==1)
                    {
                        
                        GMSPlace* place = likelihood.place;
                        _CURRENTLOC.latitude =  place.coordinate.latitude;
                        _CURRENTLOC.longitude = place.coordinate.longitude;
                        
                    }
                    
                }
                completion(true, nil,nil);
 
            }];
        });
   
}

+(NSString*)getFormatedAddressString:(NSString*)strAddress1 addressLine2:(NSString*)strAddress2 townValue:(NSString*)strTown countyValue:(NSString*)strCountry postCodeValue:(NSString*)strPostCode inLineChangeFormat:(BOOL)boolValue
{
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    if ([self checkNilandEmptyString:strAddress1])
    {
        [tempArray addObject:strAddress1];
    }
    if ([self checkNilandEmptyString:strAddress2])
    {
        [tempArray addObject:strAddress2];
    }
    if ([self checkNilandEmptyString:strTown])
    {
        [tempArray addObject:strTown];
    }
    if ([self checkNilandEmptyString:strCountry])
    {
        [tempArray addObject:strCountry];
    }
    if ([self checkNilandEmptyString:strPostCode])
    {
        [tempArray addObject:strPostCode];
    }
    
    NSString *formatedAddressString = @"";
    
    int tempLoopCount = 1;
    for (NSString *tempString in tempArray)
    {
        if (tempLoopCount == [tempArray count])
        {
            if (tempString.length != k_EmptyLength)
            {
                formatedAddressString = [formatedAddressString stringByAppendingString:[NSString stringWithFormat:@"%@",tempString]];
            }
        }else
        {
            if (boolValue) {
                if (tempString.length != k_EmptyLength)
                {
                    formatedAddressString = [formatedAddressString stringByAppendingString:[NSString stringWithFormat:@"%@\n",tempString]];
                }
            }else
            {
                formatedAddressString = [formatedAddressString stringByAppendingString:[NSString stringWithFormat:@"%@ ",tempString]];
            }
        }
        
        tempLoopCount++;
    }
    tempArray = nil;
    
    return formatedAddressString;
}

+(CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr {
    
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSMutableCharacterSet
                                                                                          alphanumericCharacterSet]];
    
    [esc_addr stringByRemovingPercentEncoding];
    
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
    
}


#pragma mark-check location service access
//while app gets launched check if location services are on.
//if not notify user to on the services
-(BOOL)checkIfLocationServicesAreEnabledOrNot {
    
BOOL accesspermited = false;
    
   BOOL isServiceEnabled =  [CLLocationManager locationServicesEnabled];
    
    if(isServiceEnabled){
        
        switch ([CLLocationManager authorizationStatus]) {
                
            case kCLAuthorizationStatusAuthorizedAlways:
                
                accesspermited= true;
                break;
                
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                
                accesspermited= true;
                break;
                
            case kCLAuthorizationStatusDenied:
                
                accesspermited= false;
                break;
                
            case kCLAuthorizationStatusRestricted:
                
                accesspermited= false;
                break;
                
            case kCLAuthorizationStatusNotDetermined:
                
                accesspermited= false;
                break;
                
        }
    }
    return accesspermited;
}

-(void)strikeOutText:(NSMutableAttributedString*)originalText :(NSString*)dummyText :(NSRange)range :(UIFont*)font{
    
    //Just added a dummy text to display strike out line
    //By default it displays only till text
    //So added dummy text of clear color and added strike line
    NSAttributedString *dummyString = [[NSAttributedString alloc]
                                       initWithString:dummyText
                                       attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor clearColor],NSForegroundColorAttributeName,font, NSFontAttributeName,
                                                   nil]];
    [originalText appendAttributedString:dummyString];
    
    [originalText addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    
     
    [originalText  addAttribute:NSStrikethroughColorAttributeName value:[UIColor colorWithHexString:@"#232323"] range:range];

}

+(void)addPaddingToTextField :(UITextField *)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark - Validate Methods

#pragma mark - Validate Methods

+(BOOL)checkNilandEmptyString:(NSString*)stringToCheck{
    
    // Now that we know it's an NSString, perform different tests
    if ((stringToCheck == nil) || ([stringToCheck isKindOfClass:[NSNull class]]) || (stringToCheck == (NSString *)[NSNull null]) || [stringToCheck isEqual:[NSNull null]] ){
        return false;
    }
    
    NSString *withoutWhiteSpace =  [stringToCheck stringByTrimmingCharactersInSet:[NSCharacterSet  whitespaceAndNewlineCharacterSet]];
    
    // Check that we are dealing with an NSString, or we will get a crash when trying to check the length
    if(![stringToCheck isKindOfClass:[NSString class]]){
        return false;
    }
    
    if( withoutWhiteSpace.length==0 ) return  false;
    
    // If they all passed, then this is non-nil, non-empty string
    return true;
}
+(NSString*)removeLeadingSpacesfromString :(NSString*)originalString {
    
    NSRange range = [originalString rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
    originalString = [originalString stringByReplacingCharactersInRange:range withString:@""];
    return originalString;
}

+ (BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL)validatePhoneWithString:(NSString *)phoneString
{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *stringValueOfTextField = [[phoneString componentsSeparatedByCharactersInSet:charactersToRemove]
                                        componentsJoinedByString:@""];
    NSPredicate *no = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",stringValueOfTextField];
    return [no evaluateWithObject:phoneString];
}

+ (BOOL)validatePostalCodeWithString:(NSString *)postalCodeString
{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *stringValueOfTextField = [[postalCodeString componentsSeparatedByCharactersInSet:charactersToRemove]
                                        componentsJoinedByString:@""];
    
    if ((postalCodeString.length != stringValueOfTextField.length)  || ([postalCodeString length]!= 5) )
    {
        return false;
    }
    return true;
}



-(void)placeholderSize:(UITextField*)textfield :(NSString*)text{
    UIColor *color = [UIColor lightGrayColor];
    textfield.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Light" size:15.0]
                                                 }
     ];
}

#pragma mark - Server Fetch needed information

-(void)getAllPickUpAddress:(void (^) (BOOL success,NSDictionary* result,NSError* error))completion{
    
    _pickUpAddressArray= nil;
    
    _pickUpAddressArray = [[NSMutableArray alloc]init];
    
    NSString*  params = [NSString stringWithFormat:@"%@&vendor_id=%@",GetPickUpAddress_Service,[UserManager getUserID]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if ([[responseDictionary objectForKey:@"code"] isEqualToString:@"200"]) {
                                                  
                                                  NSMutableArray *arrayOfAddress;
                                                  arrayOfAddress = [responseDictionary valueForKey:@"data"];
                                                  
                                                  for(int index = 0 ; index<arrayOfAddress.count ; index++) {
                                                      
                                                      NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                      
                                                      //Set address Model
                                                      AddressModel *addressObject = [[AddressModel alloc]initWithAddressParamsAsAddressTitle:[arrayOfdata valueForKey:@"address_label"]
                                                                                                                                AddressLine1:[arrayOfdata valueForKey:@"address_field_1"]
                                                                                                                                AddressLine2:[arrayOfdata valueForKey:@"address_field_2"]
                                                                                                                                     ZipCode:[arrayOfdata valueForKey:@"postcode"]
                                                                                                                                        City:[arrayOfdata valueForKey:@"city"]
                                                                                                                                       State:[arrayOfdata valueForKey:@"state"] Country:[arrayOfdata valueForKey:@"country"] PhoneNumber:[arrayOfdata valueForKey:@"contact_no"] Comment:[arrayOfdata valueForKey:@"comments"] BussinessName:[arrayOfdata valueForKey:@"bussiness_name"] Longitute:[[arrayOfdata valueForKey:@"address_long"]floatValue]
                                                                                                                                    Latitute:[[arrayOfdata valueForKey:@"address_lat"]floatValue] AddressId:[arrayOfdata valueForKey:@"id"]];
                                                      
                                                      [_pickUpAddressArray addObject:addressObject];
                                                      
                                                  }
                                                  completion(true,responseDictionary,nil);
                                              }
                                             else if([[responseDictionary objectForKey:@"code"] isEqualToString:@"300"]) {
                                                  
                                                  completion(true,responseDictionary,nil);

                                              }
                                             else if(responseDictionary == nil){
                                                  
                                                  completion(false,responseDictionary,nil);
                                              }
                                          }else{
                                              completion(false,nil,error);

                                          }
                                      });
                                  }];
    [task resume];
    
}

-(void)addAddressToDataBase :(AddressModel*)place Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion{
  
    NSString* params = [NSString stringWithFormat:@"%@&vendor_id=%@&address_field_1=%@&address_field_2=%@&address_lat=%f&address_long=%f&address_label=%@&bussiness_name=%@&city=%@&state=%@&country=%@&postcode=%@&contact_no=%@&comments=%@",AddPicUpAddress_Service,[UserManager getUserID],place.addressLine1,place.addressLine2,place.latitue,place.longitute,place.addressTitle,place.bussinessname,place.city,place.state,place.country,place.zipCode,place.phoneNumber,place.comment];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary !=nil){
                                                  completion(true, responseDictionary , nil);
                                              }else
                                                  completion(false, responseDictionary , nil);
                                          }else{
                                              completion(false, nil , error);
 
                                          }

                                      });
                                  }];
    [task resume];
}

-(void)updatePickUpAddress :(AddressModel*)place Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion{
    
    NSString* params = [NSString stringWithFormat:@"%@&id=%@&vendor_id=%@&address_field_1=%@&address_field_2=%@&address_lat=%f&address_long=%f&address_label=%@&bussiness_name=%@&city=%@&state=%@&country=%@&postcode=%@&contact_no=%@&comments=%@",UpdatePicUpAddress_Service,place.addressId,[UserManager getUserID],place.addressLine1,place.addressLine2,place.latitue,place.longitute,place.addressTitle,place.bussinessname,place.city,place.state,place.country,place.zipCode,place.phoneNumber,place.comment];

    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              
                                              if ([[responseDictionary objectForKey:@"code"] isEqualToString:@"200"]) {
                                                  NSLog(@"address update");
                                                  ;
                                                  completion(true, responseDictionary , nil);
                                                  
                                              }
                                              else  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"211"]) {
                                                  
                                                  NSLog(@"Invalid Address");
                                                  completion(true, responseDictionary , nil);
                                              }
                                              else  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"300"]) {
                                                  completion(true, responseDictionary , nil);
                                              }
                                              if(responseDictionary ==nil){
                                                  
                                                  completion(false, responseDictionary , nil);
                                              }
                                          }
                                      });
                                  }];
    [task resume];
    
}

-(void)addConsumerAddressToDataBase:(ConsumerAddressModel*)place Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion{
    
    NSString* params = [NSString stringWithFormat:@"%@&vendor_id=%@&address_field_1=%@&address_field_2=%@&address_lat=%f&address_long=%f&address_label=%@&username=%@&usermobileno=%@&useremail=%@&city=%@&state=%@&country=%@&postcode=%@&comments=%@",AddConsumerAddress_Service,[UserManager getUserID],place.addressLine1,place.addressLine2,place.latitue,place.longitute,place.addressTitle,place.consumerName,place.phoneNumber,place.consumerEmail,place.city,place.state,place.country,place.zipCode,place.comment];

    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary == nil)
                                                  completion(false, responseDictionary,nil);

                                             else completion(true,responseDictionary,nil);
                                              
                                          }else{
                                              completion(true,nil,error);
                                          }
                                      });
                                  }];
    [task resume];
    
}

-(void)updateConsumerAddress :(ConsumerAddressModel*)place Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion{
    
    NSString* params = [NSString stringWithFormat:@"%@&id=%@&vendor_id=%@&address_field_1=%@&address_field_2=%@&address_lat=%f&address_long=%f&address_label=%@&username=%@&usermobileno=%@&useremail=%@&city=%@&state=%@&country=%@&postcode=%@&comments=%@",UpdateConsumerAddress_Service,place.addressId,[UserManager getUserID],place.addressLine1,place.addressLine2,place.latitue,place.longitute,place.addressTitle,place.consumerName,place.phoneNumber,place.consumerEmail,place.city,place.state,place.country,place.zipCode,place.comment];

    
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  completion(true, responseDictionary,nil);
                                              }
                                              else  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"211"]) {
                                                  
                                                  NSLog(@"Invalid Address");
                                                  completion(true,responseDictionary,nil);
                                                  
                                              }
                                              if(responseDictionary ==nil){

                                              completion(false,responseDictionary,nil);
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

-(void)getAllConsumerAddress :(NSString*)searchString :(void (^) (BOOL success,NSDictionary* result,NSError* error))completion{
    
    _consumerAddressArray= nil;
    
    _consumerAddressArray = [[NSMutableArray alloc]init];
    
    NSString*  params = [NSString stringWithFormat:@"%@&vendor_id=%@&search_string=%@",GetConsumerAddress_Service,[UserManager getUserID],searchString];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSMutableArray *arrayOfAddress;
                                                  arrayOfAddress = [responseDictionary valueForKey:@"data"];
                                                  
                                                  for(int index = 0 ; index<arrayOfAddress.count ; index++){
                                                      
                                                      NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                      
                                                      
                                                      //Set address Model
                                                      ConsumerAddressModel *addressObject = [[ConsumerAddressModel alloc]initWithAddressParamsAsAddressTitle:[arrayOfdata valueForKey:@"address_label"]
                                                                                                                                                AddressLine1:[arrayOfdata valueForKey:@"address_field_1"]
                                                                                                                                                AddressLine2:[arrayOfdata valueForKey:@"address_field_2"]
                                                                                                                                                     ZipCode:[arrayOfdata valueForKey:@"postcode"]
                                                                                                                                                        City:[arrayOfdata valueForKey:@"city"]
                                                                                                                                                       State:[arrayOfdata valueForKey:@"state"] Country:[arrayOfdata valueForKey:@"country"]
                                                                                                                                                 PhoneNumber:[arrayOfdata valueForKey:@"usermobileno"] Comment:[arrayOfdata valueForKey:@"comments"]
                                                                                                                                                    userName:[arrayOfdata valueForKey:@"username"]
                                                                                                                                                   userEmail:[arrayOfdata valueForKey:@"useremail"]
                                                                                                                                                   Longitute:[[arrayOfdata valueForKey:@"address_long"]floatValue]
                                                                                                                                                    Latitute:[[arrayOfdata valueForKey:@"address_lat"]floatValue] AddressId:[arrayOfdata valueForKey:@"id"]];
                                                      
                                                      [_consumerAddressArray addObject:addressObject];
                                                  }
                                                  completion(true,responseDictionary,nil);
                                              }
                                              if([[responseDictionary objectForKey:@"code"]isEqualToString:@"300"]) {
                                                  
                                                  completion(true,responseDictionary,nil);
                                                  
                                              }
                                              if(responseDictionary ==nil){
                                                  completion(false,responseDictionary,nil);
                                              }
                                          }else{
                                              completion(false,nil,error);
                                              
                                          }
                                      });
                                  }];
    [task resume];
    
}

#pragma mark - Get Added Card Detail
-(void)getYourCard:(void (^) (BOOL success,NSDictionary* result,NSError* error))completion{
    
        NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",GetCardDetail_Service ,[UserManager getUserID]];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  
                                                  if([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]){
                                                      
                                                      NSArray *arrayOfdata;
                                                      arrayOfdata = [responseDictionary valueForKey:@"data"];
                                                      
                                                      _cardDetails = [[CardModel alloc]initWithCardDetails:[[arrayOfdata valueForKey:@"last4"]componentsJoinedByString:@""] :[[arrayOfdata valueForKey:@"expirationMonth"]componentsJoinedByString:@""]  :[[arrayOfdata valueForKey:@"expirationYear"]componentsJoinedByString:@""] :[[arrayOfdata valueForKey:@"cardType"]componentsJoinedByString:@""]];
                                                      
                                                  completion(true,responseDictionary,nil);
                                                      
                                                  }else {
                                                      
                                                      completion(true,responseDictionary,nil);
                                                  }
                                                  if(responseDictionary == nil){
                                                  completion(false,nil,nil);
                                                  }
                                              }
                                              
                                          });
                                      }];
        [task resume];
        
}

#pragma mark - get string from address object
-(NSMutableAttributedString*)setPickUpFields:(AddressModel*)obj{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3.0f;
    
    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    if([LSUtils checkNilandEmptyString:obj.addressTitle]){
        
        NSAttributedString *label = [[NSMutableAttributedString alloc]initWithString:obj.addressTitle                                                                            attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:label];
    }
    
    
    if([LSUtils checkNilandEmptyString:obj.bussinessname]){
        
        NSAttributedString *bussinessName = [[NSMutableAttributedString alloc] initWithString:obj.bussinessname                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        
        [completeTitlestring appendAttributedString:bussinessName];
        
    }
    if([LSUtils checkNilandEmptyString:obj.addressLine1]){
        
        NSAttributedString *addressLine1 = [[NSMutableAttributedString alloc] initWithString:obj.addressLine1                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [completeTitlestring appendAttributedString:addressLine1];
        
    }
    
    if([LSUtils checkNilandEmptyString:obj.addressLine2]){
        NSAttributedString *addressLine2 = [[NSMutableAttributedString alloc] initWithString:obj.addressLine2                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        if([LSUtils checkNilandEmptyString:obj.bussinessname]){
            
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
            
        }else
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        
        [completeTitlestring appendAttributedString:addressLine2];
    }
    
    if([LSUtils checkNilandEmptyString:obj.city]){
        NSAttributedString *city = [[NSMutableAttributedString alloc] initWithString:obj.city                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
        [completeTitlestring appendAttributedString:city];
    }
    
    if([LSUtils checkNilandEmptyString:obj.state]){
        NSAttributedString *state = [[NSMutableAttributedString alloc] initWithString:obj.state                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [completeTitlestring appendAttributedString:state];
    }
    if([LSUtils checkNilandEmptyString:obj.zipCode]){
        NSAttributedString *zip = [[NSMutableAttributedString alloc] initWithString:obj.zipCode                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@" - "]];
        [completeTitlestring appendAttributedString:zip];
    }
    return completeTitlestring;
}

-(NSMutableAttributedString*)setPickUpFieldsForDeliveryAddress:(ConsumerAddressModel*)obj{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3.0f;
    
    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    if([LSUtils checkNilandEmptyString:obj.addressTitle]){
        
        NSAttributedString *label = [[NSMutableAttributedString alloc]initWithString:obj.addressTitle                                                                            attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:label];
    }
    
    
    if([LSUtils checkNilandEmptyString:obj.consumerName]){
        
        NSAttributedString *bussinessName = [[NSMutableAttributedString alloc] initWithString:obj.consumerName                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        
        [completeTitlestring appendAttributedString:bussinessName];
        
    }
    if([LSUtils checkNilandEmptyString:obj.addressLine1]){
        
        NSAttributedString *addressLine1 = [[NSMutableAttributedString alloc] initWithString:obj.addressLine1                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [completeTitlestring appendAttributedString:addressLine1];
        
    }
    
    if([LSUtils checkNilandEmptyString:obj.addressLine2]){
        NSAttributedString *addressLine2 = [[NSMutableAttributedString alloc] initWithString:obj.addressLine2                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
        
        if([LSUtils checkNilandEmptyString:obj.consumerName]){
            
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
            
        }else
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        
        [completeTitlestring appendAttributedString:addressLine2];
    }
    
    if([LSUtils checkNilandEmptyString:obj.city]){
        NSAttributedString *city = [[NSMutableAttributedString alloc] initWithString:obj.city                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
        [completeTitlestring appendAttributedString:city];
    }
    
    if([LSUtils checkNilandEmptyString:obj.state]){
        NSAttributedString *state = [[NSMutableAttributedString alloc] initWithString:obj.state                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [completeTitlestring appendAttributedString:state];
    }
    if([LSUtils checkNilandEmptyString:obj.zipCode]){
        NSAttributedString *zip = [[NSMutableAttributedString alloc] initWithString:obj.zipCode                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@" - "]];
        [completeTitlestring appendAttributedString:zip];
    }
    return completeTitlestring;
}

-(NSMutableString*)getPickUpAddress:(AddressModel*)obj{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3.0f;
    
    NSMutableString * completeTitlestring =  [[NSMutableString alloc] initWithString:@""];
    if([LSUtils checkNilandEmptyString:obj.addressTitle]){
        
        [completeTitlestring appendString:obj.addressTitle];
    }
//    if([LSUtils checkNilandEmptyString:obj.bussinessname]){
//        
//        [completeTitlestring appendString:@","];
//        [completeTitlestring appendString:obj.bussinessname];
//    }
    if([LSUtils checkNilandEmptyString:obj.addressLine1]){
        
        [completeTitlestring appendString:@","];
        [completeTitlestring appendString:obj.addressLine1];
    }
    if([LSUtils checkNilandEmptyString:obj.addressLine2]){
        
        [completeTitlestring appendString:@","];
        [completeTitlestring appendString:obj.addressLine2];
    }
    if([LSUtils checkNilandEmptyString:obj.city]){
        
        [completeTitlestring appendString:@","];
        [completeTitlestring appendString:obj.city];
    }
    if([LSUtils checkNilandEmptyString:obj.state]){
        
        [completeTitlestring appendString:@","];
        [completeTitlestring appendString:obj.state];
    }
    if([LSUtils checkNilandEmptyString:obj.zipCode]){
        
        [completeTitlestring appendString:@"-"];
        [completeTitlestring appendString:obj.zipCode];
    }
    return completeTitlestring;
}

-(NSMutableString*)getDeliveryAddress:(ConsumerAddressModel*)obj{
    
    NSMutableString * completeTitlestring =  [[NSMutableString alloc] initWithString:@""];
    if([LSUtils checkNilandEmptyString:obj.addressTitle]){
        
        [completeTitlestring appendString:obj.addressTitle];
    }
//    if([LSUtils checkNilandEmptyString:obj.consumerName]){
//        
//        [completeTitlestring appendString:@","];
//        [completeTitlestring appendString:obj.consumerName];
//        
//    }
    if([LSUtils checkNilandEmptyString:obj.addressLine1]){
        
        [completeTitlestring appendString:@","];
        [completeTitlestring appendString:obj.addressLine1];
    }
    if([LSUtils checkNilandEmptyString:obj.addressLine2]){
        
        [completeTitlestring appendString:@","];
        [completeTitlestring appendString:obj.addressLine2];
    }
    if([LSUtils checkNilandEmptyString:obj.city]){
        [completeTitlestring appendString:@","];
        [completeTitlestring appendString:obj.city];
    }
    if([LSUtils checkNilandEmptyString:obj.state]){
        
         [completeTitlestring appendString:@","];
        [completeTitlestring appendString:obj.city];
    }
    if([LSUtils checkNilandEmptyString:obj.zipCode]){
        [completeTitlestring appendString:@"-"];
        [completeTitlestring appendString:obj.zipCode];
    }
    return completeTitlestring;
}


#define COLOR_AcceptButtonGreen                [UIColor colorWithHex:0x60A369]
#define COLOR_RejectButtonRed                  [UIColor colorWithHex:0xB95858]
#define COLOR_NotesLabelGreen                  [UIColor colorWithHex:0x059811]
#define COLOR_AbortButtonYellow                [UIColor colorWithHex:0xD4A93C]
#define COLOR_TravelButtonBlue                 [UIColor colorWithHex:0x6393B5]

-(NSAttributedString*)getOrderStatus :(int)acceptStatus  {
    
    NSAttributedString *newString;
///0 => pending, 1=>accepted ,  2 => ongoing , 3 =>compleated , 6 =>order canceled , 7=>order rejected
        switch (acceptStatus) {
            case PENDING:{
                newString =  [[NSAttributedString alloc] initWithString:@"Pending"
                                                             attributes:@{
                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0f green:145/255.0f blue:148/255.0f alpha:1],
                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                                          }];
            }
                break;
            case ACCEPTED:{
                newString =  [[NSAttributedString alloc] initWithString:@"Accepted"
                                                             attributes:@{
                                                                          NSForegroundColorAttributeName: [UIColor colorWithHex:0x60A369],
                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                                          }];
            }
                break;
            case ONGOING:{
                newString =  [[NSAttributedString alloc] initWithString:@"Ongoing"
                                                             attributes:@{
                                                                          NSForegroundColorAttributeName:[UIColor colorWithHex:0xD4A93C] ,
                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                                          }];
            }
                break;
            case REJECTED:
            {
                newString =  [[NSAttributedString alloc] initWithString:@"Rejected"
                                                             attributes:@{
                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0f green:145/255.0f blue:148/255.0f alpha:1],
                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                                          }];
            }
                break;
                
            case DROPOFF:
            {
                newString =  [[NSAttributedString alloc] initWithString:@"Drop-Off"
                                                             attributes:@{
                                                                          NSForegroundColorAttributeName: [UIColor colorWithHex:0xD4A93C],
                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                                          }];
            }
                break;
            case COMPLETED:
            {
                newString =  [[NSAttributedString alloc] initWithString:@"Completed"
                                                             attributes:@{
                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:36/255.0f green:211/255.0f blue:128/255.0f alpha:1],
                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                                          }];
            }
                break;
            case CANCELED:
            {
                newString =  [[NSAttributedString alloc] initWithString:@"Canceled"
                                                             attributes:@{
                                                                          NSForegroundColorAttributeName: [UIColor colorWithHex:0xB95858],
                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                                          }];
            }
            default:
                break;
        }
  
    return newString;
}
//
//-(NSAttributedString*)getOrderStatus :(int)acceptStatus :(int)deliveryStatus {
//    
//    NSAttributedString *newString;
//    if(acceptStatus == 0 ){
//        
//        newString =  [[NSAttributedString alloc] initWithString:@"Rejected"
//                                                     attributes:@{
//                                                                  NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0f green:145/255.0f blue:148/255.0f alpha:1],
//                                                                  NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
//                                                                  }];
//
//    }  else  if(acceptStatus == 6 ){
//        
//        newString =  [[NSAttributedString alloc] initWithString:@"Canceled"
//                                                     attributes:@{
//                                                                  NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0f green:145/255.0f blue:148/255.0f alpha:1],
//                                                                  NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
//                                                                  }];
//        
//    }else{
//        switch (deliveryStatus) {
//            case 0:{
//                newString =  [[NSAttributedString alloc] initWithString:@"Rejected"
//                                                             attributes:@{
//                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0f green:145/255.0f blue:148/255.0f alpha:1],
//                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
//                                                                          }];
//            }
//                break;
//            case 1:{
//                newString =  [[NSAttributedString alloc] initWithString:@"Accepted"
//                                                             attributes:@{
//                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:36/255.0f green:211/255.0f blue:128/255.0f alpha:1],
//                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
//                                                                          }];
//            }
//                break;
//            case 2:{
//                newString =  [[NSAttributedString alloc] initWithString:@"Rejected"
//                                                             attributes:@{
//                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0f green:145/255.0f blue:148/255.0f alpha:1],
//                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
//                                                                          }];
//            }
//                break;
//            case 4:
//            {
//                newString =  [[NSAttributedString alloc] initWithString:@"Rejected"
//                                                             attributes:@{
//                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0f green:145/255.0f blue:148/255.0f alpha:1],
//                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
//                                                                          }];
//            }
//                break;
//            case 3:
//            {
//                newString =  [[NSAttributedString alloc] initWithString:@"Completed"
//                                                             attributes:@{
//                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:36/255.0f green:211/255.0f blue:128/255.0f alpha:1],
//                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
//                                                                          }];
//            }
//                break;
//            case 6:
//            {
//                newString =  [[NSAttributedString alloc] initWithString:@"Canceled"
//                                                             attributes:@{
//                                                                          NSForegroundColorAttributeName: [UIColor colorWithRed:36/255.0f green:211/255.0f blue:128/255.0f alpha:1],
//                                                                          NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
//                                                                          }];
//            }
//            default:
//                break;
//        }
//    }
//    return newString;
//}

+(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    //[df setLocale:[NSLocale currentLocale]];
    [df setDateFormat:strFormat];
    //[df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [df stringFromDate:date];
}

+(NSDate*)getDateFromDateString:(NSString*)dateString byFormat:(NSString*)strFormat{
    
    // Determine if we are in 12 or 24hr format
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // 12hr format! Change HH to hh
    if (hasAMPM){
        //strFormat = [strFormat stringByReplacingOccurrencesOfString:@"HH" withString:@"hh"];
        //NSLog(@"strFormat: %@", strFormat);
    }
    
    [df setDateFormat:strFormat];
    return [df dateFromString:dateString];
}

//get all orders for which rating is not done

-(void)getRatingStatusCompletion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion{
    
    NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@", cheeckratingstatus,[UserManager getUserID]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary == nil){

                                                  completion(true, nil , nil);

                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSMutableArray *arrayOfData;
                                                  
                                                  arrayOfData = [[responseDictionary valueForKey:@"data"]objectAtIndex:0] ;
                                                  
                                                 _dictOfOrderTosRate = [[NSMutableDictionary alloc]init];
                                                  
                                                  [_dictOfOrderTosRate setObject:[arrayOfData valueForKey:@"orderid"] forKey:@"orderid"];
                                                  [_dictOfOrderTosRate setObject:[responseDictionary valueForKey:@"deliveryboyimgurl"] forKey:@"deliveryboyimgurl"];
                                                  
                                                  NSArray *driverArray = [arrayOfData valueForKey:@"deliveryboydetail"];
                                                  
                                                  NSMutableArray * driversArry = [[NSMutableArray alloc]init];
                                                  
                                                  for(int index =0 ; index<driverArray.count ; index++){
                                                      
                                                      NSDictionary * driverDict = [driverArray objectAtIndex:index];
                                                      
                                                      [driversArry addObject:driverDict];
                                                  }
                                                  [_dictOfOrderTosRate setObject:driversArry forKey:@"DriverArray"];
                                                  completion(true, responseDictionary , nil);
                                                  
                                              }
                                              else  if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                               
                                                  completion(true, responseDictionary , nil);
                                             //no date
                                              }
                                              
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                 //error
                                                  completion(false, nil , error);

                                              }else{
                                                 //somting wrong
                                                  completion(false, nil , nil);

                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
    
}

//for converting no to ordinal  value
-(NSString*)getDateWithOrdinalValue:(NSString*)dateString {
    
    TTTOrdinalNumberFormatter *ordinalNumberFormatter = [[TTTOrdinalNumberFormatter alloc] init];
    
    NSNumber *number = [NSNumber numberWithInt:[dateString intValue]];
    return [NSString stringWithFormat:@"%@", [ordinalNumberFormatter stringFromNumber:number]];
}
// This method sets the NoResultsView UIView
-(void)showNoResultsViewWithOptionalText:(NSString *)textToDisplay xPosition:(NSInteger)x yPosition:(NSInteger)y screenWidth:(NSInteger)strScreenWidth screenHeight:(NSInteger)strScreenHeight{
    
    _noResultsView.frame = CGRectMake(x, y, self.screenWidth-strScreenWidth, self.screenHeight-strScreenHeight);
    
    [_noResultsIcon setFont:[UIFont fontWithName:@"SSSTANDARD" size:40.0f]];
    _noResultsIcon.textColor = [UIColor colorWithRed:0/255.0f green:208/255.0f blue:230/255.0f alpha:1];
    [_noResultsIcon setText:@"info"];
    [_noResultsIcon sizeToFit];
    
    _noResultsIcon.frame = CGRectMake((_screenWidth -strScreenWidth- CGRectGetWidth(_noResultsIcon.frame))/2,
                                      (_noResultsView.frame.size.height - CGRectGetHeight(_noResultsIcon.frame))/2-32, CGRectGetWidth(_noResultsIcon.frame),
                                      CGRectGetHeight(_noResultsIcon.frame));
    
    [_noResultsLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
    _noResultsLabel.textColor =  [UIColor colorWithRed:0/255.0f green:208/255.0f blue:230/255.0f alpha:1];;
    if (textToDisplay) _noResultsLabel.text = textToDisplay;
    else
        _noResultsLabel.text = NSLocalizedString(@"No records to display", @"No records to display");// NSLocalizedString(@"No results found", @"No results found");
    
    [_noResultsLabel sizeToFit];
    _noResultsLabel.frame = CGRectMake((_screenWidth -strScreenWidth - CGRectGetWidth(_noResultsLabel.frame))/2, (_noResultsView.frame.size.height + CGRectGetHeight(_noResultsIcon.frame))/2-32, CGRectGetWidth(_noResultsLabel.frame), CGRectGetHeight(_noResultsLabel.frame));
    // Add subviews
    [_noResultsView addSubview:_noResultsLabel];
    [_noResultsView addSubview:_noResultsIcon];
}

//Delete images stored during placing an order
+ (BOOL) deleteMyImagesDirectory
{
    // Get the Document directory path
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    // Create a new path for the new images folder
    NSString *imagesPath = [documentsPath stringByAppendingPathComponent:directoryImage];
    return [[NSFileManager defaultManager] removeItemAtPath:imagesPath error:nil];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}
@end

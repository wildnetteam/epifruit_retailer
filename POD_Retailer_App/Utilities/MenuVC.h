//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Arpana on 07/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableIndexSet *expandedSections;
    UIImageView *arrowImg;
    NSString *strInviteText;
}
@property (strong) NSArray *itemArray;
@property (strong) NSArray *subItemArray;


//IBOutlets-UIImageView
@property (weak, nonatomic) IBOutlet UIImageView *userImg;

//IBOutlets-UIView
@property (weak, nonatomic) IBOutlet UIView *viewHeader;

//IBOutlets-UITableView
@property (weak, nonatomic) IBOutlet UITableView *menuTable;

//IBOutlets-UILabel
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;


@end

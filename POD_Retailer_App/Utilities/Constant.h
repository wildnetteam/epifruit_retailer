//
//  Created by Arpana on 09/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define directoryImage @"ImageReferenceDirectory"

#define kUserUnteractionChangedNotification  @"kUserUnteractionChangedNotification"

#define Base_URL         @"http://site4demo.com/deliveryapp/vendorapi/"

#define  SignUp_Service  @"methodname=signUp&token=ecbcd7eaee29848978134beeecdfbc7c"

#define  otpcodeverificationForRegister   @"methodname=otpcodeverificationForRegister&token=ecbcd7eaee29848978134beeecdfbc7c"

#define validateRefrenceCode  @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=validateRefrenceCode"

#define  resendotp @"methodname=resendotp&token=ecbcd7eaee29848978134beeecdfbc7c"

#define  Login_Service           @"methodname=login&token=ecbcd7eaee29848978134beeecdfbc7c&mode=1"

#define ForgotPassword_Service @"methodname=forgotPassword&token=ecbcd7eaee29848978134beeecdfbc7c"

#define RemovePickUpAddress_Service @"methodname=removepickupaddress&token=ecbcd7eaee29848978134beeecdfbc7c"

#define GetPickUpAddress_Service  @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getpickupaddresslist"

#define AddPicUpAddress_Service     @"methodname=addpickupaddress&token=ecbcd7eaee29848978134beeecdfbc7c"

#define  UpdatePicUpAddress_Service @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=updatepickupaddresslist"

#define GetConsumerAddress_Service  @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=listconsumeraddress"

#define AddConsumerAddress_Service     @"methodname=saveconsumeraddress&token=ecbcd7eaee29848978134beeecdfbc7c"

#define  UpdateConsumerAddress_Service @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=updateconsumeraddress"

#define RemoveConsumerAddress_Service @"methodname=removeconsumeraddress&token=ecbcd7eaee29848978134beeecdfbc7c"

#define GetCardDetail_Service @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getpaymentcardinfo"

#define PostCode_Validation_Link  @"http://maps.googleapis.com/maps/api/geocode/json?"
#define privacyLink  @"http://site4demo.com/deliveryapp/terms-and-conditions"
#define FAQLink  @"http://site4demo.com/deliveryapp/FAQs"
#define TermsAndConditionLink  @"http://site4demo.com/deliveryapp/about-us"

#define GetModeOfTransport @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=deliveryboytranspotationmode"

#define getordercost @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getordercost"

#define PlaceOrder @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=vendorPlaceOrder"

#define SaveProductImages @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=addimagestoorder"

#define GetDeliveryBoyList @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getnewdeliveryboylist"

#define getProductType @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getProductType"

#define vendorTotalPointEarning @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=vendorTotalPointEarning"

#define getUserPoints @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getUserPoints"

#define getdeliveryboyrating @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getdeliveryboyrating"

#define cheeckratingstatus @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=cheeckratingstatus"

#define orderdetailforrating @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=orderdetailforrating"

#define setorderrating @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=setorderrating"

#define GetEstimateCost @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getordercost"

#define UpdateProfile @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=changeaccountsetting"

#define UpdatePassword @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=changeaccountpassword"

#define GetTodayOrder @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=gettodayorder"

#define getBidsorderlist @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=vendorNewOrderList"

#define vendorExpiredOrderList @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=vendorExpiredOrderList"

#define getorderDetail @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getorderdetail"

#define completedOrderList @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=completedOrderList"

#define myPreviousOrderList @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=myPreviousOrderList"

#define ongoingOrderList @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=ongoingOrderList"

#define vendorOrderDetail @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=vendorOrderDetail"

#define MyOrderDetail @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=orderDetail"

#define getDriverOrderDistance @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getDriverOrderDistance"

#define toggleFavouriteDriver @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=toggleFavouriteDriver"

#define cancelOrder @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=VendorCancelOrder"

#define vendorUpdatePrice @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=vendorUpdatePrice"

#define acceptsBid @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=acceptBid"

#define sortDriver @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=sortDriver"

#define repostOrder @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=repostOrder"

#define  contactUs_Service @"methodname=contactus&token=ecbcd7eaee29848978134beeecdfbc7c"

#define  getNotificationList_Service   @"methodname=getvendornoification&token=ecbcd7eaee29848978134beeecdfbc7c"
#define  clearAllNotification   @"methodname=clearAllNotification&token=ecbcd7eaee29848978134beeecdfbc7c"
#define  getNotificationCount_Service  @"methodname=unreadnotificationcount&token=ecbcd7eaee29848978134beeecdfbc7c"
#define  setReadNotification_Service @"methodname=setnotificationread&token=ecbcd7eaee29848978134beeecdfbc7c"

// Constants for User Login Credentials Info.
#define K_USERCREDENTIAL   [NSString stringWithFormat:@"USER_CREDENTIALS"]
#define k_UserName         [NSString stringWithFormat:@"UserName"]
#define k_FirstName         [NSString stringWithFormat:@"FirstName"]
#define k_LastName         [NSString stringWithFormat:@"LastName"]
#define k_LoggedInUserID   [NSString stringWithFormat:@"id"] //LoggedInUserID
#define k_LoginType        [NSString stringWithFormat:@"LoginType"]
#define k_LoginTokenID     [NSString stringWithFormat:@"LoginTokenID"]
#define k_UserImageURL     [NSString stringWithFormat:@"userImageURL"]
#define k_UserEmail        [NSString stringWithFormat:@"UserEmail"]
#define k_UserPhoneNumber  [NSString stringWithFormat:@"UserPhoneNumber"]
#define k_UserReferenceCode  [NSString stringWithFormat:@"UserReferenceCode"]
#define k_NotificationCount             [NSString stringWithFormat:@"UNC"]
#define  NSdidGetUnReadNotificationCount @"didGetUnReadNotificationCount"
#define  NSdidRecieveNotification        @"didRecieveNotification"
#define PushToken  [NSString stringWithFormat:@"PushToken"]

//#define k_Token            [NSString stringWithFormat:@"UserEmail"]

//------- Macro's - Check String -------
#define k_EmptyLength      0
#define k_EmptyString      @""
#define ProfileImageResizeScale CGSizeMake(250, 250)

#define HEIGHT_NAVIGATIONBAR 64.0f
#define SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT               @"yyyy-MM-dd HH:mm:ss" // 2015-06-03 for 24 hour format
#define SHOW_YEAR_MONTH_DATE_FORMAT                         @"yyyy-MM-dd" // "2015-06-03"
#define SHOWING_HOURS_MINUTES_TIME_FORMAT                   @"HH:mm" // for 24 hour format
#define DATE_TIME_ZONE_FORMAT                               @"yyyy-MM-dd HH:mm:ss.000000" // "2015-06-03 13:31:56.000000"
#define DATE_TIME_ZONE_FORMAT_LOCAL                         @"yyyy-MM-dd HH:mm:ss +0000" //// "2015-06-03 13:31:56 +0000
#define SHOWING_MONTH_AND_DATE_FORMAT                       @"dd MMM"
#define SHOWING_MONTH_AND_DATE_FORMAT_DRIVER_COMMENT_DATE   @"dd MMM YY"



typedef void (^completionHandler)(); // A completion handler that returns nothing

#define  k_LOCATIONSERVICEENABLED

//Constant for Colors
#define APP_BACKGROUND_COLOR [UIColor whiteColor]
#define AppDefaultBlueColor [UIColor colorWithRed:119/255.0f green:209/255.0f blue:226/255.0f alpha:1]

//[UIColor colorWithRed:236/255.0f green:236/255.0f blue:236/255.0f alpha:1.0f]

#define REGISTERBUTTON_BACKGROUND_COLOR [UIColor colorWithRed:63/255.0f green:203/255.0f blue:228/255.0f alpha:1.0f]

#define IS_IPHONE5     (([[UIScreen mainScreen] bounds].size.height-568)?NO:true)
#define IS_IPHONE_6 (([[UIScreen mainScreen] bounds].size.height-667.0)?NO:true)
#define IS_IPHONE_6P (([[UIScreen mainScreen] bounds].size.height-736.0)?NO:true)

#define IS_OS_6_OR_LATER    (([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)?true:false)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_9_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)

#define LEFTMENU_BACKGROUND_COLOR [UIColor colorWithRed:159/255.0f green:159/255.0f blue:159/255.0f alpha:1.0f]

#define  NAVIGATIONBAR_COLOR [UIColor whiteColor]
//[UIColor colorWithRed:223/255.0f green:223/255.0f blue:223/255.0f alpha:1.0f]
#define ColourConstants_LightBackground  [UIColor colorWithHex:0xf7faff] // The default background colour, blueish white



#define FONT_Label_PlaceHolder  [UIFont fontWithName:@"Lato-Light" size:6];
#define FONT_TextField [UIFont fontWithName:@"Lato-Regular" size:16];
#define FONT_Title_Label [UIFont fontWithName:@"Lato-Regular" size:17];

#define FONT_TextField_6P [UIFont fontWithName:@"Lato-Light" size:17];
#define FONT_Title_Label_6P [UIFont fontWithName:@"Lato-Regular" size:19];


#define COLOR_Title_Label [UIColor colorWithHexString:@"#7D7D7D"]
#define COLOR_Border [UIColor colorWithHexString:@"#C4C4C4"]

#define FONT_NavigationBar                   [UIFont systemFontOfSize:16.0f]
#define FONT_TOP_HEADING                     [UIFont boldSystemFontOfSize:16.0f]
#define FONT_LABEL_REGULAR                   [UIFont systemFontOfSize:15.0f]
#define FONT_LABEL_BOLD                       [UIFont boldSystemFontOfSize:15.0f]

#endif /* Constant_h */




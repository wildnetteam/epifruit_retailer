//
//  AppDelegate.h
//  POD_Retailer_App
//
//  Created by Arpana on 10/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Reachability.h>
#import "SwiftySideMenuViewController.h"
#import "UIViewController+SwiftySideMenu.h"
#import "LeftMenuViewController.h"
#import <firebase/Firebase.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)  FBSDKLoginManager *fbManager;
@property (strong, nonatomic) GIDSignIn *googleSignInInstance ;
@property (nonatomic ,strong) Reachability * internetReachability;
@property (nonatomic ,strong) Reachability *hostReachable;
@property(nonatomic) UINavigationController *navigationController;
@property(nonatomic) SwiftySideMenuViewController *SwiftySideMenu;
@property(nonatomic) LeftMenuViewController *leftMenu;
+ (AppDelegate*) getAppDelegate;
@property(nonatomic , strong )FIRDatabaseReference *rootRef ;
@property(nonatomic , strong ) FIRDatabase *database;
@property(nonatomic , strong ) FIRAuth *FAuth;
@property (strong, nonatomic) NSString *displayName_;
@end


//
//  ApplyThemesForViewClass.m
//

#import "ApplyThemesForViewClass.h"

@implementation ApplyThemesForViewClass


+ (void)applyThemeToNavigationBar
{
    // Apply theme to UINavigationBar
    
    
    // Navigation bar appearance setup throughout the application.
    
    //  Set Nav Bar BG Color OR Image Globaly.
    [[UINavigationBar appearance] setBarTintColor:NAVIGATIONBAR_COLOR];
    
    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
//    // Set Nav Bar Appearance Globaly, iOS7 Supported.
//    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
//                                                           [UIColor blackColor], NSForegroundColorAttributeName,
//                                                           FONT_NavigationBar, NSFontAttributeName, nil]];
    
    
    //  Set Nav Bar Item Appearance Globaly
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                               [UIColor colorWithHexString:@"#40a4b9"],NSForegroundColorAttributeName,
                                FONT_NavigationBar, NSFontAttributeName,nil];
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes
                                                forState:UIControlStateNormal];
    
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UINavigationController class]]] setTitleTextAttributes:attributes
                                                                                                                   forState:UIControlStateNormal];
    
}






@end

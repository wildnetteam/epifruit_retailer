//
//  PickUpAddressTableViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 20/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMapsBase/GoogleMapsBase.h>
#import "ABMenuTableViewCell.h"

@interface PickUpAddressTableViewCell : ABMenuTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageviewBox;
@property (weak, nonatomic) IBOutlet UILabel *LabelHeading;
@property (weak, nonatomic) IBOutlet UILabel *bussinessLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UIButton *selectOrAddButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContentViewToEditviewSpacingContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *EditToBackContentViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *customecellLeftConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *customecellRightConstraint;
@property (nonatomic, retain) GMSPlace *completePlaceDetails;
-(void)setAddressFields:(GMSPlace*)placeInstance :(NSString*)searchtext ;
@end

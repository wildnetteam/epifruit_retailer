//
//  RatingCellTableViewCell.h
//  DownSweepDriverApp
//
//  Created by Arpana on 07/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"



@protocol CommentTextViewDelegatedDelegate <NSObject>
@optional
//Called to the delegate whenever return is hit when a user is typing into the rightTextField of an ELCTextFieldCell
- (BOOL)textFieldCell:(UITextView *)inCell shouldReturnForIndexPath:(NSIndexPath*)inIndexPath withValue:(NSString *)inValue;
//Called to the delegate whenever the text in the rightTextField is changed
- (void)textFieldCell:(UITextView *)inCell updateTextLabelAtIndexPath:(NSIndexPath *)inIndexPath string:(NSString *)inValue;

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView;
- (BOOL)textViewShouldEndEditing:(UITextView *)textView;
- (void)textViewDidBeginEditing:(UITextView *)textView;
- (void)textViewDidEndEditing:(UITextView *)textView;
- (void)saveTextFieldText:(NSIndexPath *)indexPath string:(NSString *)inValue;

//- (BOOL)textView:(UITextView *)textView
//shouldChangeTextInRange:(NSRange)range
// replacementText:(NSString *)text;
@end

@interface RatingCellTableViewCell : UITableViewCell <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *namelabel;
@property (weak, nonatomic) IBOutlet ASStarRatingView *starView;

@property (weak, nonatomic) IBOutlet UILabel *emaillabel;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UITextView *ratingTextView;
@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, weak) id<CommentTextViewDelegatedDelegate> delegate;


@end

//
//  RatingCellTableViewCell.m
//  DownSweepDriverApp
//
//  Created by Arpana on 07/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "RatingCellTableViewCell.h"

@implementation RatingCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//-(BOOL)textFieldShouldReturn:(UITextField *)textField {
//    
//    BOOL ret = YES;
//    if([_delegate respondsToSelector:@selector(textFieldCell:shouldReturnForIndexPath:withValue:)]) {
//        ret = [_delegate textFieldCell:self.ratingTextView shouldReturnForIndexPath:_indexPath withValue:self.licenseTextField.text];
//    }
//    if(ret) {
//        [textField resignFirstResponder];
//    }
//    return ret;
//}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *textString = self.ratingTextView.text;
    textString = [textString stringByReplacingCharactersInRange:range withString:text];
    
    if([_delegate respondsToSelector:@selector(textFieldCell:updateTextLabelAtIndexPath:string:)]) {
        [_delegate textFieldCell:self.ratingTextView updateTextLabelAtIndexPath:_indexPath string:textString];
    }
    
    return YES;
}

//- (BOOL)textFieldShouldClear:(UITextField *)textField
//{
//    if([_delegate respondsToSelector:@selector(textFieldCell:updateTextLabelAtIndexPath:string:)]) {
//        [_delegate textFieldCell:self.licenseTextField updateTextLabelAtIndexPath:_indexPath string:nil];
//    }
//    return YES;
//}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView;
{
    if([_delegate respondsToSelector:@selector(textViewShouldBeginEditing:)]) {
        return [_delegate textViewShouldBeginEditing:(UITextView *)textView];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if([_delegate respondsToSelector:@selector(textViewShouldEndEditing:)]) {
        return [_delegate textViewShouldEndEditing:self.ratingTextView];
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    if([_delegate respondsToSelector:@selector(textViewDidBeginEditing:)]) {
        return [_delegate textViewDidBeginEditing:(UITextView *)textView];
    }
    
}

- (void)textViewDidEndEditing:(UITextView *)textView;
{
    if([_delegate respondsToSelector:@selector(saveTextFieldText: string:)]) {
        [_delegate saveTextFieldText:_indexPath string:textView.text];
        
    }
}
@end

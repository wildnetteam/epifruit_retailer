//
//  DriverDetailsTableViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 09/03/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
@interface DriverDetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *noLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentlabel;
@property (weak, nonatomic) IBOutlet ASStarRatingView *starView;

@end

//
//  OrderDetailViewDriverTableViewCell.m
//  PODRetailerApp
//
//  Created by Arpana on 04/04/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "OrderDetailViewDriverTableViewCell.h"

@implementation OrderDetailViewDriverTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)trackButtonClicked:(id)sender {
    OrderDetailViewDriverTableViewCell *cell = (OrderDetailViewDriverTableViewCell *)self;
    
    if ([self.cellDelagte respondsToSelector:@selector(track:)]) {
        [self.cellDelagte track:cell.trackButton];
    }
}
- (IBAction)callButtonClicked:(id)sender {
    OrderDetailViewDriverTableViewCell *cell = (OrderDetailViewDriverTableViewCell *)self;
    
    if ([self.cellDelagte respondsToSelector:@selector(call:)]) {
        [self.cellDelagte call:cell.callButton];
    }
}
- (IBAction)messageButtonClicked:(id)sender {
    OrderDetailViewDriverTableViewCell *cell = (OrderDetailViewDriverTableViewCell *)self;
    
    if ([self.cellDelagte respondsToSelector:@selector(message:)]) {
        [self.cellDelagte message:cell.messageButton];
    }
}

-(IBAction)favButtonClicked:(id)sender {
    
    OrderDetailViewDriverTableViewCell *cell = (OrderDetailViewDriverTableViewCell *)self;
    
    if ([self.cellDelagte respondsToSelector:@selector(message:)]) {
        [self.cellDelagte fav:cell.favTopButton];
    }
  
}
-(void)setDeliverBoyDetails:(DeliveryBoyModel*)delBoyInstance  {
    
    _nameLabel.text = delBoyInstance.dName;
    //_phoneLabel.text = delBoyInstance.dMobile;
    _vehicleLabel.text = delBoyInstance.dtransportMode;

    //1 fav  2 = unfav
    NSString* imageName = delBoyInstance.isfavouite == 2?@"heart":@"filledHeart";
    [_favImageButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
   // if(delBoyInstance.dModeSelcted!=nil)
      //  _vehicleLabel.text = [delBoyInstance.dModeSelcted valueForKey:@"transport_mode"];
    
}
@end

//
//  ProductImageCollectionViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 05/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProduct;
@property (weak, nonatomic) IBOutlet UIButton *buttonProductdelete;

@end

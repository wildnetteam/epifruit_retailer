//
//  OrderDetailViewDriverTableViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 04/04/17.
//  Copyright © 2017 Arpana. All rights reserved.


#import <UIKit/UIKit.h>
#import "DeliveryBoyModel.h"

@protocol TrackMessageCallDelegate <NSObject>
@optional
-(void)track:(UIButton*)tracBt;
-(void)message:(UIButton*)messgcBt;
-(void)call:(UIButton*)callBt;
-(void)fav:(UIButton*)favButton;
@end

@interface OrderDetailViewDriverTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *vehicleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIButton *trackButton;
@property (weak, nonatomic) IBOutlet UIButton *messageButton;
@property (weak, nonatomic) IBOutlet UIImageView *driverimageView;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* distanceHeihgtConstraint;
@property (weak, nonatomic) IBOutlet UIButton *favImageButton;
@property (weak, nonatomic) IBOutlet UIButton *favTopButton;
@property (nonatomic, assign) id <TrackMessageCallDelegate> cellDelagte;
-(void)setDeliverBoyDetails:(DeliveryBoyModel*)delBoyInstance ;

@end

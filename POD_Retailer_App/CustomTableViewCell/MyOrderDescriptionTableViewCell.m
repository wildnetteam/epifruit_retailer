//
//  MyOrderDescriptionTableViewCell.m
//  PODRetailerApp
//
//  Created by Arpana on 28/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "MyOrderDescriptionTableViewCell.h"

@implementation MyOrderDescriptionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    [self.repostButton.layer setShadowOffset:CGSizeMake(0, 4)];
    [self.repostButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.repostButton.layer setShadowOpacity:0.2];
    self.repostButton.layer.cornerRadius = self.repostButton.frame.size.width/20;
    self.repostButton.layer.masksToBounds= true;
}


- (IBAction)repostButtonClicked:(id)sender {
    
    MyOrderDescriptionTableViewCell *cell = (MyOrderDescriptionTableViewCell *)self;
    
    if ([self.repostDelagte respondsToSelector:@selector(repostButtonClicked:)]) {
        
        [self.repostDelagte repostButtonClicked:cell.repostButton];
    }
}


-(void)setOrderDetails:(OrderHistoryModel*)orderObject {
    
    _titleLable.text = orderObject.productTitle;
    _descriptionLabel.text = orderObject.productDescription;
    
    [_progressLabel setAttributedText:[sharedUtils getOrderStatus:orderObject.orderStatus]];

    _oredrNoLabel.text = [NSString stringWithFormat:@"Order no.%@",orderObject.productOrderNo];
    if(orderObject.productImagesArray.count > 1){
        
        _moreImagesLabel.text = [NSString stringWithFormat:@"+%lu", (unsigned long)orderObject.productImagesArray.count -1 ];
        _moreView.hidden = false;
    }else
        _moreView.hidden = true;
}

-(void)setBIdOrderDetails:(OrderHistoryModel*)orderObject {
    
    _titleLable.text = orderObject.productTitle;
    _descriptionLabel.text = orderObject.productDescription;
    _driverCountlabel.text  = [NSString stringWithFormat:@"%d", orderObject.driverCount];
    _oredrNoLabel.text = [NSString stringWithFormat:@"Order no. %@",orderObject.productOrderNo];
    
    _priceTitleLabel.hidden = false;
    _priceView.hidden = false;
    _maxPriceLabel.hidden = false;
    _minPriceLabel.hidden = false;
    _hyphenLabel.hidden = false;
    if(([orderObject.minRate isEqualToString:@""] && [orderObject.maxrate isEqualToString:@""])){
        //If both values are unspecified
        _priceView.hidden = true;
        _priceTitleLabel.hidden = true;

    }
    else if((![orderObject.minRate isEqualToString:@""] && ![orderObject.maxrate isEqualToString:@""])){
        
        //If both values are specified
        _minPriceLabel.text = [orderObject.minRate isEqualToString:@""]?@"NA":[NSString stringWithFormat:@"$%@",orderObject.minRate];
        _maxPriceLabel.text = [orderObject.maxrate isEqualToString:@""]?@"NA": [NSString stringWithFormat:@"$%@",orderObject.maxrate];
        
    }else{
        //Any one is specified , can be max or min
        _hyphenLabel.hidden = true;
        _minPriceLabel.hidden = true;
        _maxPriceLabel.text = [orderObject.maxrate isEqualToString:@""]?[NSString stringWithFormat:@"$%@",orderObject.minRate]: [NSString stringWithFormat:@"$%@",orderObject.maxrate];
        
    }
    if(orderObject.imageCount > 1){
        
        _moreImagesLabel.text = [NSString stringWithFormat:@"+%lu", (unsigned long)orderObject.imageCount -1 ];
        _moreView.hidden = false;
    }else
        _moreView.hidden = true;
    
    if(orderObject.jumpPrice){
        _jumpPriceLabel.hidden = false;
    }else
        _jumpPriceLabel.hidden = true;
    
}


-(void)setPreviousOrderDetails:(OrderDetailsModel*)orderObject {
    
    _titleLable.text = orderObject.productTitle;
    _descriptionLabel.text = [NSString stringWithFormat:@"%@ %@  (%@)",[LSUtils checkNilandEmptyString:orderObject.productQuantity]?orderObject.productQuantity:@"" ,[LSUtils checkNilandEmptyString:orderObject.packageType]?orderObject.packageType:@"",[LSUtils checkNilandEmptyString:orderObject.productWeight]?orderObject.productWeight:@"" ];
    
    if([_descriptionLabel.text containsString:@"( )"]||![LSUtils checkNilandEmptyString:orderObject.productWeight]){
        _descriptionLabel.text = [[_descriptionLabel.text stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    }
    _oredrNoLabel.text = [NSString stringWithFormat:@"Order no. %@",orderObject.productOrderNo];
    _progressLabel.text =@"";
    if(orderObject.productImagesArray.count > 1){
        
        _moreImagesLabel.text = [NSString stringWithFormat:@"+%lu", (unsigned long)orderObject.productImagesArray.count -1 ];
        _moreView.hidden = false;
    }else
        _moreView.hidden = true;
}

@end

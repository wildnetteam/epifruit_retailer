//
//  DeliveryAddressTableViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 26/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABMenuTableViewCell.h"
#import <GoogleMapsBase/GoogleMapsBase.h>

@interface DeliveryAddressTableViewCell :ABMenuTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bussinessLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottomSpace;
@property (retain, nonatomic) IBOutlet UIImageView *imageViewBox;

@property (weak, nonatomic) IBOutlet UIButton *selectOrAddButton;

@property (nonatomic, retain) GMSPlace *completePlaceDetails;
-(void)setAddressFields:(GMSPlace*)placeInstance :(NSString*)searchtext;
@end

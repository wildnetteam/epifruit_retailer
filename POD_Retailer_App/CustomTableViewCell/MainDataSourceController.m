//
//  MainDataSourceController.m
//  SampleProject
//
//  Created by Alex Bumbu on 13/07/15.
//  Copyright (c) 2015 Alex Bumbu. All rights reserved.
//

#import "MainDataSourceController.h"
#import "ABMenuTableViewCell.h"
#import "UserManager.h"

#import "PickUpAddressViewController.h"
#import "ABCellMenuView.h"
#import "PickUpAddressTableViewCell.h"


static NSString *menuCellIdentifier = @"Default Cell";
static NSString *customCellIdentifier = @"Custom Cell";

@interface MainDataSourceController () <ABCellMenuViewDelegate>

@end

@implementation MainDataSourceController {
    NSMutableArray *_dataSource;
}

@synthesize tableView = _tableView;
@synthesize viewController = _viewController;

- (void)refreshDataSourceWithCompletionHandler:(void (^)())completion {
    [self loadDataSource];
    if (completion)
        completion();
}

#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  sharedUtils.pickUpAddressArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ABMenuTableViewCell *cell = nil;
    
    cell = [self customCellAtIndexPath:indexPath];

    // custom menu view
    NSString *nibName =  @"ABCellCustomStyleMenuView";
    ABCellMenuView *menuView = [ABCellMenuView initWithNib:nibName bundle:nil];
    menuView.delegate = self;
    menuView.indexPath = indexPath;
    cell.rightMenuView = menuView;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (ABMenuTableViewCell*)menuCellAtIndexPath:(NSIndexPath*)indexPath {
    ABMenuTableViewCell *cell = (ABMenuTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:menuCellIdentifier];
    
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PickUpAddressViewController* v = (PickUpAddressViewController*)_viewController;
     AddressModel *addressObject = [v.pickUpAddressArray objectAtIndex:indexPath.row];

    sharedUtils.pickupAddressModel= nil;
    
    NSLog(@"Pickup Edit mode: %d", v.saveOrEditMode);

    if( v.saveOrEditMode ==3 ){
        
        sharedUtils.pickupAddressModel = addressObject;
        
        [v.navigationController popViewControllerAnimated:true];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PickUpAddressTableViewCell *cell = (PickUpAddressTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
    int height;
    
    height = [self getLabelDynamicHeight:cell.bussinessLabel.text andLabel:cell.bussinessLabel];
    int height1 = [self getLabelDynamicHeight:cell.stateLabel.text andLabel:cell.stateLabel];
    int totalHeight = height +height1+ cell.titleLabel.frame.size.height +31 +2.5+4 + 16;
    //+cell.stateLabel.frame.size.height
    return totalHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

//return dynamic label size
-(int)getLabelDynamicHeight:(NSString *)yourString andLabel:(UILabel *)yourLabel
{
    //Calculate the expected size based on the font and linebreak mode of your label
    
    CGSize constraint = CGSizeMake(yourLabel.frame.size.width,9999);
    
    NSDictionary *attributes = @{NSFontAttributeName: yourLabel.font};
    
    CGRect rect = [yourString boundingRectWithSize:constraint
                                       options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                    attributes:attributes
                                       context:nil];
    return rect.size.height;
}

#pragma mark ABCellMenuViewDelegate Methods

- (void)cellMenuViewFlagBtnTapped:(ABCellMenuView *)menuView {
  }

//Edit Address
- (void)cellMenuViewMoreBtnTapped:(ABCellMenuView *)menuView {
    
    ABMenuTableViewCell *cell = (ABMenuTableViewCell*)[self.tableView cellForRowAtIndexPath:menuView.indexPath];
    
    [cell updateMenuView:ABMenuUpdateHideAction animated:YES];

     PickUpAddressViewController* v = (PickUpAddressViewController*)_viewController;
    UIView *dimView;
    [dimView removeFromSuperview];
    dimView = nil;
    dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, v.view.frame.size.width, v.view.frame.size.height )];
    
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    
   PickUpAddressPopoverView*  viewPop= [[PickUpAddressPopoverView alloc] init];
    viewPop.pickUPAddressDel = (id)v;
    viewPop.addressObjectDetail = [sharedUtils.pickUpAddressArray objectAtIndex:menuView.indexPath.row];
    viewPop.isInEditMode = true;
    viewPop.parentControllerTypePickUP = EditController;
     viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, dimView.frame.size.width, v.view.frame.size.height);
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewPop.frame = CGRectMake(0,CGRectGetHeight(v.view.frame)*.20, v.view.frame.size.width, v.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];
    viewPop.backgroundColor=[UIColor whiteColor];
    //1 for save 2 for edit
    [dimView addSubview:viewPop];
    
   if(v.saveOrEditMode !=3)
    v.saveOrEditMode = 2;
    
    [v.view addSubview:dimView];

}

- (void)cellMenuViewDeleteBtnTapped:(ABCellMenuView *)menuView {
    
    PickUpAddressViewController* v = (PickUpAddressViewController*)_viewController;
    
    if([LSUtils isNetworkConnected]){
        
        NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                               initWithString:@"Do you want to delete address from address book?"
                                               attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@""
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           ABMenuTableViewCell *cell = (ABMenuTableViewCell*)[self.tableView cellForRowAtIndexPath:menuView.indexPath];
                                           
                                           [cell updateMenuView:ABMenuUpdateHideAction animated:YES];
                                       }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       // update data source
                                       AddressModel *object = [sharedUtils.pickUpAddressArray objectAtIndex:menuView.indexPath.row];
                                       
                                       [sharedUtils startAnimator:v];
                                       
                                       //To restrict user to click back button while deleting procees is goin on.
                                       v.isprocessing = true;
                                       [self deletePickUpAddress:object CompletionHandler:^(BOOL success, NSError *error) {
                                           [sharedUtils stopAnimator:v];
                                           
                                           v.isprocessing = false;

                                           if(error!= nil){
                                               //Thre is some error occured
                                               UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                               [v presentViewController:controller animated:YES completion:nil];
                                               return ;
                                           }if(success){
                                               
                                               [sharedUtils.pickUpAddressArray removeObjectAtIndex:menuView.indexPath.row];
                                               
                                               if( v.saveOrEditMode == 3 ){
                                                   
                                                   if([v.PickupID intValue] == [object.addressId intValue]){
                                                       
                                                       sharedUtils.pickupAddressModel = nil;
                                                       v.modelWhenNotChanged = nil;
                                                   }
                                               }
                                               if(sharedUtils.pickUpAddressArray.count == 0){//to change mantish Bug
                                                   
                                                   NoAddressViewController* vc = (NoAddressViewController*)[sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NoAddressViewController"];
                                                   vc.saveOrEditMode = v.saveOrEditMode;
                                                   [v.navigationController pushViewController:vc animated:YES];
                                               }
                                               
                                               // update UI
                                               [self.tableView deleteRowsAtIndexPaths: @[menuView.indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                                               
                                               // make sure to reload in order to update the custom menu index path for each row
                                               NSMutableArray *rowsToReload = [NSMutableArray array];
                                               for (int i = 0; i < sharedUtils.pickUpAddressArray.count - menuView.indexPath.row; i++) {
                                                   [rowsToReload addObject:[NSIndexPath indexPathForRow:menuView.indexPath.row + i inSection:0]];
                                               }
                                               
                                               [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationAutomatic];
                                               
                                               if(sharedUtils.pickUpAddressArray.count > 3){
                                                   v.navigationItem.rightBarButtonItem.enabled = false;
                                                   v.navigationItem.rightBarButtonItem.customView.alpha = 0.1;                                       }
                                               else{
                                                   v.navigationItem.rightBarButtonItem.enabled = true;
                                                   v.navigationItem.rightBarButtonItem.customView.alpha = 1;
                                               }
                                           }else{
                                               UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Address could not be deleted"];
                                               [v presentViewController:controller animated:YES completion:nil];
                                           }
                                       }];
                                   }];
        
        [alertController setValue:alertmessage forKey:@"attributedTitle"];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        UIViewController *currentTopVC = [self currentTopViewController];
        
        if(currentTopVC!=nil)
            [currentTopVC  presentViewController:alertController animated:NO completion:Nil];
    }else{
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [v presentViewController:controller animated:YES completion:nil];
    }
}

-(void)deletePickUpAddress:(AddressModel*)ad  CompletionHandler:(void (^) (BOOL success, NSError* error))completion  {
    
        NSString*  params = [NSString stringWithFormat:@"%@&id=%@",RemovePickUpAddress_Service,ad.addressId];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      completion(true,nil);
                                                  }else
                                                      completion(false,nil);
                                              }
                                              if(error!=nil)
                                                  completion(false,error);
                                          });
                                      }];
        [task resume];    
}

- (UIViewController *)currentTopViewController {
    
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

#pragma mark Private Methods

- (void)loadDataSource {
    
    PickUpAddressViewController* v = (PickUpAddressViewController*)_viewController;
    v.pickUpAddressArray = nil;
//    NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:sharedUtils.pickUpAddressArray];
//    v.pickUpAddressArray = [[NSMutableArray alloc] initWithArray:[mySet array]];
    v.pickUpAddressArray =sharedUtils.pickUpAddressArray;
     [self .tableView reloadData];
}

- (PickUpAddressTableViewCell*)customCellAtIndexPath:(NSIndexPath*)indexPath {
    
    // setup the cell
    PickUpAddressTableViewCell *cell = (PickUpAddressTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
    PickUpAddressViewController* v = (PickUpAddressViewController*)_viewController;
    if( v.saveOrEditMode == 3 ){
        
        cell.selectOrAddButton.hidden = false;
        
        AddressModel* model = [v.pickUpAddressArray objectAtIndex:indexPath.row];
        if([v.PickupID intValue] == [model.addressId intValue]){
            
            [cell.selectOrAddButton setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
            v.modelWhenNotChanged = model;
        }
        
    }else
            cell.selectOrAddButton.hidden = true;

    [cell setAddressFields:[v.pickUpAddressArray objectAtIndex:indexPath.row] :@"Addresslabel"];
    cell.backgroundColor =[UIColor clearColor];
    return cell;
}

-(void)saveData:(id)sender{
    [self loadDataSource];
}





@end

//
//  PickUpAddressTableViewCell.m
//  PODRetailerApp
//
//  Created by Arpana on 20/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "PickUpAddressTableViewCell.h"
#import "AddressModel.h"


@implementation PickUpAddressTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setAddressFields:(AddressModel*)placeInstance :(NSString*)searchtext {

   // [_LabelHeading layoutIfNeeded];

    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    
    if([LSUtils checkNilandEmptyString:placeInstance.addressTitle]){

        _titleLabel.text = placeInstance.addressTitle;
    }
    
    if([LSUtils checkNilandEmptyString:placeInstance.bussinessname]){
        
        NSAttributedString *bussiness = [[NSMutableAttributedString alloc]  initWithString:placeInstance.bussinessname ];
    
        [completeTitlestring appendAttributedString:bussiness];
    }
    
    if([LSUtils checkNilandEmptyString:placeInstance.addressLine1]){
        
        NSAttributedString *AddressLine1 = [[NSMutableAttributedString alloc] initWithString:placeInstance.addressLine1];
        
        if([LSUtils checkNilandEmptyString:placeInstance.bussinessname]){
            
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];

        }
        [completeTitlestring appendAttributedString:AddressLine1];
        
    }
    

    if([LSUtils checkNilandEmptyString:placeInstance.addressLine2]){
        
        NSAttributedString *AddressLine2 = [[NSMutableAttributedString alloc] initWithString:placeInstance.addressLine2];
        
        if([LSUtils checkNilandEmptyString:placeInstance.bussinessname]){
            
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
            
        }else
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];

        [completeTitlestring appendAttributedString:AddressLine2];
    }

    [_bussinessLabel setAttributedText:completeTitlestring];
    [_bussinessLabel sizeToFit];

    NSMutableAttributedString * completeAddressstring =  [[NSMutableAttributedString alloc] initWithString:@""];

    if([LSUtils checkNilandEmptyString:placeInstance.state]){
        
        NSAttributedString *state = [[NSMutableAttributedString alloc] initWithString:placeInstance.state];
        [completeAddressstring appendAttributedString:state];
    }
    
    if([LSUtils checkNilandEmptyString:placeInstance.city]){
        
        NSAttributedString *city = [[NSMutableAttributedString alloc]initWithString:placeInstance.city ];
        [completeAddressstring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [completeAddressstring appendAttributedString:city];
    }

     if([LSUtils checkNilandEmptyString:placeInstance.zipCode]){
        
        NSAttributedString *Zip = [[NSMutableAttributedString alloc] initWithString:placeInstance.zipCode];
        [completeAddressstring appendAttributedString:[[NSAttributedString alloc] initWithString:@" - "]];
        [completeAddressstring appendAttributedString:Zip];
    }
    [_stateLabel setAttributedText:completeAddressstring];

}
@end

//
//  DeliveryAddressTableViewCell.m
//  PODRetailerApp
//
//  Created by Arpana on 26/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "DeliveryAddressTableViewCell.h"

@implementation DeliveryAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setAddressFields:(ConsumerAddressModel*)placeInstance :(NSString*)searchtext {
        
    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    
    if([LSUtils checkNilandEmptyString:placeInstance.addressTitle]){

        _titleLabel.text = placeInstance.addressTitle;
    }
    if([LSUtils checkNilandEmptyString:placeInstance.consumerName]){
 
    NSAttributedString *consumername = [[NSMutableAttributedString alloc]  initWithString:placeInstance.consumerName
                                                                        attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    

    [completeTitlestring appendAttributedString:consumername];

    }
    if([LSUtils checkNilandEmptyString:placeInstance.consumerEmail]){

    NSAttributedString *consumerEmail = [[NSMutableAttributedString alloc]  initWithString:placeInstance.consumerEmail
                                                                               attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [completeTitlestring appendAttributedString:consumerEmail];
    }
    [_bussinessLabel setAttributedText:completeTitlestring];
  //  [_bussinessLabel sizeToFit];

        NSMutableAttributedString * completeAddresstring =  [[NSMutableAttributedString alloc] initWithString:@""];
    
    if([LSUtils checkNilandEmptyString:placeInstance.addressLine1]){
        
        NSAttributedString *AddressLine1 = [[NSMutableAttributedString alloc] initWithString:placeInstance.addressLine1                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
        
        [completeAddresstring appendAttributedString:AddressLine1];
    }
    
    if([LSUtils checkNilandEmptyString:placeInstance.addressLine2]){
        
        NSAttributedString *AddressLine2 = [[NSMutableAttributedString alloc] initWithString:placeInstance.addressLine2                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
        
        [completeAddresstring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
        [completeAddresstring appendAttributedString:AddressLine2];
    }
    

    if([LSUtils checkNilandEmptyString:placeInstance.state]){

        NSAttributedString *state = [[NSMutableAttributedString alloc] initWithString:placeInstance.state]  ;
        [completeAddresstring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [completeAddresstring appendAttributedString:state];
    }
    
    if([LSUtils checkNilandEmptyString:placeInstance.city]){
        
        NSAttributedString *city = [[NSMutableAttributedString alloc]initWithString:placeInstance.city];
        [completeAddresstring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
        [completeAddresstring appendAttributedString:city];
    }
    if([LSUtils checkNilandEmptyString:placeInstance.country]){
        
        NSAttributedString *country = [[NSMutableAttributedString alloc] initWithString:placeInstance.country];
        
        [completeAddresstring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
        [completeAddresstring appendAttributedString:country];
    }
    if([LSUtils checkNilandEmptyString:placeInstance.zipCode]){

    NSAttributedString *Zip = [[NSMutableAttributedString alloc] initWithString:placeInstance.zipCode];
        [completeAddresstring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
        [completeAddresstring appendAttributedString:Zip];
    }
   
    [_stateLabel setAttributedText:completeAddresstring];
   // [_stateLabel sizeToFit];
    
}
    

@end

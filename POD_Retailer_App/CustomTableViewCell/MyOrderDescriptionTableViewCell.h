//
//  MyOrderDescriptionTableViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 28/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderHistoryModel.h"
#import "OrderDetailsModel.h"

@protocol RepostDelegate <NSObject>

@optional

- (void)repostButtonClicked:(UIButton*)repostButton;

@end

@interface MyOrderDescriptionTableViewCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UILabel* titleLable;
@property(nonatomic,weak) IBOutlet UILabel* timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property(nonatomic,weak) IBOutlet UILabel* progressLabel;
@property(nonatomic,strong) IBOutlet UIImageView* imageViewForProduct;
@property (weak, nonatomic) IBOutlet UILabel *oredrNoLabel;
@property(nonatomic,strong) IBOutlet UIImageView* imageViewForProductBackground;
@property (weak, nonatomic) IBOutlet UIView *moreView;
@property(nonatomic,weak) IBOutlet UILabel* moreImagesLabel;
-(void)setOrderDetails:(OrderHistoryModel*)orderObject ;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellShadowSeparartor;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleAndDescDistance;

//Used for biding orders
@property (weak, nonatomic) IBOutlet UILabel *driverCountlabel;
@property (weak, nonatomic) IBOutlet UILabel *jumpPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *minPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *hyphenLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTitleLabel;


@property (weak, nonatomic) IBOutlet UIView *priceView;

-(void)setBIdOrderDetails:(OrderHistoryModel*)orderObject ;
-(void)setPreviousOrderDetails:(OrderDetailsModel*)orderObject;



@property (weak, nonatomic) IBOutlet UIButton *repostButton;
@property (nonatomic, weak) id <RepostDelegate> repostDelagte;

@end

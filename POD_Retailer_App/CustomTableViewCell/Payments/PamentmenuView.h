//
//  PamentmenuView.h
//  PODRetailerApp
//
//  Created by Arpana on 10/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+XIB.h"

@protocol ABCellMenuViewDelegate;

@interface PamentmenuView : UIView

@property (nonatomic, assign) id<ABCellMenuViewDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end


@protocol ABCellMenuViewDelegate <NSObject>

@optional
- (void)cellMenuViewDeleteBtnTapped:(PamentmenuView *)menuView;

@end

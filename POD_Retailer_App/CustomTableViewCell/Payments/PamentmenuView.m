//
//  PamentmenuView.m
//  PODRetailerApp
//
//  Created by Arpana on 10/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "PamentmenuView.h"

@implementation PamentmenuView {
    IBOutlet UIButton *deleteButton;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}


#pragma mark Actions

- (IBAction)deleteBtnPressed:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(cellMenuViewDeleteBtnTapped:)])
        [_delegate cellMenuViewDeleteBtnTapped:self];
}



@end

//
//  PaymentTableViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 10/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABMenuTableViewCell.h"

@interface PaymentTableViewCell :ABMenuTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageBox;
@property (weak, nonatomic) IBOutlet UILabel *cardTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *expiryLabel;

@property (weak, nonatomic) IBOutlet UILabel *cvvLabel;
@end

//
//  NotificationManager.h
//  PODApplication
//
//  Created by Arpana on 18/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationView.h"
#import "Enum.h"

@interface NotificationManager : NSObject

+ (id)notificationManager; // Manages the single instance

@property (strong, nonatomic) UIViewController *rootViewController;
@property (strong, nonatomic) NSMutableArray *notificationsToShowArray;
@property BOOL isExecuting;

- (void) displayMessage:(NSString *)message withType:(NotificationType)type;
- (void) displayMessage:(NSString *)message withType:(NotificationType)type andDuration:(double)duration;
- (completionHandler) displayLoadingMessage:(NSString *)message;

@end

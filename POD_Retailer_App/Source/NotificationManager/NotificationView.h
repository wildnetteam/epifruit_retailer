//
//  NotificationView.h
//  PODApplication
//
//  Created by Arpana on 18/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationManager.h"
#import "Enum.h"

@interface NotificationView : UIView

@property (nonatomic, strong) LSUtils *constantsSingleton;

@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property NotificationType type;

- (instancetype)initWithMessage:(NSString *)message andType:(NSNumber *)type;

@end

//
//  NotificationManager.m
//  PODApplication
//
//  Created by Arpana on 18/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "NotificationManager.h"
#import <UIKit/UIKit.h>

@implementation NotificationManager

#pragma mark Singleton Method
// This returns only one instance of this class (hence why it's a singleton)
+ (id)notificationManager {
    static NotificationManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

#pragma mark Init methods
- (id)init {
    if (self = [super init]) {
        
        // Add any initialisation logic here
        _rootViewController = [UIApplication sharedApplication].windows[0].rootViewController;
        _notificationsToShowArray = [[NSMutableArray alloc] init];
        _isExecuting = false;
    }
    return self;
}

// By default show notifications for 0.8 seconds
- (void) displayMessage:(NSString *)message withType:(NotificationType)type{
    [self displayMessage:message withType:type andDuration:.6];
}

- (void) displayMessage:(NSString *)message withType:(NotificationType)type andDuration:(double)duration{
    
    // Loading notifications have their own method, displayLoadingMessage
    if (type == Loading){
        return;
    }
    
    // If we are already showing a notification, log this one in the notificationsToShowArray array
    if (_isExecuting){
        // Log this operation to be executed when done
        NSMutableDictionary *operationDictionary = [[NSMutableDictionary alloc] init];
        operationDictionary[@"message"] = message;
        operationDictionary[@"typeHash"] = [NSNumber numberWithInt:type];
        operationDictionary[@"duration"] = [NSNumber numberWithDouble:duration];
        [_notificationsToShowArray addObject:operationDictionary];
        return;
    }
    
    _isExecuting = true;
    NotificationView *notificationView = [[NotificationView alloc] initWithMessage:message andType:[NSNumber numberWithInt:type]];
    
    //Added on window sometimes notification banner is not showing
    AppDelegate *appdel=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appdel.window addSubview:notificationView];
    
    //[_rootViewController.view addSubview:notificationView];
    
    // UI updates happen on the main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // Bring the notification down
            CGRect notificationFrame = notificationView.frame;
            notificationFrame.origin.y = 0;
            notificationView.frame = notificationFrame;
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.4 delay:duration options:UIViewAnimationOptionCurveEaseOut animations:^{
                // Bring the notification up
                CGRect notificationFrame = notificationView.frame;
                notificationFrame.origin.y =  - CGRectGetHeight(notificationFrame);
                notificationView.frame = notificationFrame;
                
            } completion:^(BOOL finished) {
                // Remove the notification and declare oneself as free to show another notification
                [notificationView removeFromSuperview];
                _isExecuting = false;
                
                // Check if we have any notifications to show in the array
                if ([_notificationsToShowArray count] > 0){
                    
                    NSMutableDictionary *retrievedDictionary = [[_notificationsToShowArray firstObject] mutableCopy];
                    NotificationType retrievedType = [retrievedDictionary[@"typeHash"] intValue];
                    [_notificationsToShowArray removeObject:retrievedDictionary];
                    [self displayMessage:retrievedDictionary[@"message"] withType:retrievedType andDuration:[retrievedDictionary[@"duration"] doubleValue]];
                }
                
            }];
            
        }];
         
    });
    
}

// Return a block to the caller so that the loading view can be removed whenever
- (completionHandler) displayLoadingMessage:(NSString *)message{
    
    // If we are already showing a notification, log this one in the notificationsToShowArray array
    if (_isExecuting){
        // Log this operation to be executed when done
        NSMutableDictionary *operationDictionary = [[NSMutableDictionary alloc] init];
        operationDictionary[@"message"] = message;
        operationDictionary[@"typeHash"] = [NSNumber numberWithInt:Loading];
        return nil;
    }
    
    _isExecuting = true;
    
    NotificationView *notificationView = [[NotificationView alloc] initWithMessage:message andType:[NSNumber numberWithInt:Loading]];
    [_rootViewController.view addSubview:notificationView];
    
    // UI updates happen on the main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // Bring the notification down
            CGRect notificationFrame = notificationView.frame;
            notificationFrame.origin.y = 0;
            notificationView.frame = notificationFrame;
            
        } completion:^(BOOL finished) {
            
        }];
    });
    
    // Prepare the block to return to the caller
    completionHandler blockToReturn = ^(){
        
        // UI updates happen on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                // Bring the notification up
                CGRect notificationFrame = notificationView.frame;
                notificationFrame.origin.y = - CGRectGetHeight(notificationFrame);
                notificationView.frame = notificationFrame;
                
            } completion:^(BOOL finished) {
                
                // Remove the notification and declare oneself as free to show another notification
                [notificationView removeFromSuperview];
                _isExecuting = false;
                
                // Check if we have any notifications to show in the array
                if ([_notificationsToShowArray count] > 0){
                    
                    NSMutableDictionary *retrievedDictionary = [[_notificationsToShowArray firstObject] mutableCopy];
                    
                    NotificationType retrievedType = [[retrievedDictionary valueForKey:@"type"] intValue];
                    [_notificationsToShowArray removeObject:retrievedDictionary];
                    [self displayMessage:retrievedDictionary[@"message"] withType:retrievedType andDuration:[retrievedDictionary[@"duration"] doubleValue]];
                }
                
            }];
        });
        
    };
    
    return blockToReturn;
}

@end

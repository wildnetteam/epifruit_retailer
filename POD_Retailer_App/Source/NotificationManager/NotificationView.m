//
//  NotificationView.m
//  PODApplication
//
//  Created by Arpana on 18/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "NotificationView.h"

@implementation NotificationView

- (instancetype)initWithMessage:(NSString *)message andType:(NSNumber *)type{
    
    self = [super init];
    if (self) {
        _constantsSingleton = [LSUtils sharedUtitlities];
        self.frame = CGRectMake(0, -HEIGHT_NAVIGATIONBAR, _constantsSingleton.screenWidth, HEIGHT_NAVIGATIONBAR);
        
        _type = [type intValue];
        
        // Change the background colour depending on the message
        if (_type == Positive){
            [self setBackgroundColor:[UIColor greenColor]];
        }
        else if (_type == Negative){
            [self setBackgroundColor:[UIColor colorWithHex:0xB95858]];
        }
        else if (_type == Normal){
            [self setBackgroundColor:[UIColor orangeColor]];
        }
        
        // Adapt the label
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.frame = CGRectMake(10, 20, CGRectGetWidth(self.frame) - 20, CGRectGetHeight(self.frame) - 20);
        _messageLabel.text = message;
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.numberOfLines = 0;
        _messageLabel.textColor = [UIColor whiteColor];
        _messageLabel.font = [UIFont systemFontOfSize:12.0f];
        
        // Special case! The loading notification!
        if (_type == Loading) {
            [self setBackgroundColor:[UIColor orangeColor]];
            
            // The small activity indicator is 21pts high, the big one is 36pts high
            // We want to keep 25pts to display the text
            BOOL isBigIndicator = CGRectGetHeight(self.frame) >= 20 + 36 + 25 ? true : false; // 20 = status bar, 36 = big activity indicator, 25 = label
            _activityIndicatorView = [[UIActivityIndicatorView alloc] init];
            _activityIndicatorView.frame = CGRectMake(10, 20, CGRectGetWidth(self.frame) - 20, isBigIndicator == true ? 36 : 21);
            _activityIndicatorView.hidesWhenStopped = true;
            _activityIndicatorView.activityIndicatorViewStyle = isBigIndicator ? UIActivityIndicatorViewStyleWhiteLarge : UIActivityIndicatorViewStyleWhite;
            
            _messageLabel.frame = CGRectMake(10, CGRectGetMaxY(_activityIndicatorView.frame), CGRectGetWidth(self.frame) - 20, CGRectGetHeight(self.frame) - CGRectGetMaxY(_activityIndicatorView.frame));
            _messageLabel.numberOfLines = 1;
            
            [self addSubview:_activityIndicatorView];
            [_activityIndicatorView startAnimating];
        }
        
        // Add subviews
        [self addSubview:_messageLabel];
        
        // Register to know when the device is rotated (the notifications are sent through UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications() in AppDelegate)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetLayout) name:UIDeviceOrientationDidChangeNotification object:nil];
        
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) resetLayout{
    
    self.frame = CGRectMake(0, CGRectGetMinY(self.frame), _constantsSingleton.screenWidth, HEIGHT_NAVIGATIONBAR);
    
    _messageLabel.frame = CGRectMake(10, 20, CGRectGetWidth(self.frame) - 20, CGRectGetHeight(self.frame) - 20);
    
    if (_type == Loading){
        _activityIndicatorView.frame = CGRectMake(10, 20, CGRectGetWidth(self.frame) - 20, CGRectGetHeight(_activityIndicatorView.frame));
        _messageLabel.frame = CGRectMake(10, CGRectGetMaxY(_activityIndicatorView.frame), CGRectGetWidth(self.frame) - 20, CGRectGetHeight(self.frame) - CGRectGetMaxY(_activityIndicatorView.frame));
    }
    
}

@end

//
//  BidAmountViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 24/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailsModel.h"

@interface BidAmountViewController : UIViewController < UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *minimumAmountTextField;
@property (weak, nonatomic) IBOutlet UITextField *maximumAmountTextField;
@property (weak, nonatomic) IBOutlet UILabel *minimumAmountLable;
@property (weak, nonatomic) IBOutlet UILabel *maximumAmountLable;
@property (weak, nonatomic) IBOutlet UITextField *redeemAmountTextField;
@property (weak, nonatomic) IBOutlet UILabel *rewardAmountlabel;
@property (weak, nonatomic) IBOutlet UIButton *viewSuggestionButton;
@property (weak, nonatomic) IBOutlet UIButton *JumbPriceButton;
@property (weak, nonatomic) IBOutlet UIButton *redeemButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (retain, nonatomic) OrderDetailsModel *confirmationDetails;

@end

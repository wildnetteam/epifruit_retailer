//
//  BidAmountViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 24/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "BidAmountViewController.h"
#import "LeftMenuViewController.h"
#import "ConfirmOrderViewController.h"
#import "NotificationManager.h"
@interface BidAmountViewController ()
{
    float rewardPoint;
}
@end

@implementation BidAmountViewController{
    
    BOOL isJumpedPrice;
    BOOL isRedeemPrice;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    isJumpedPrice = false;
    isRedeemPrice = false;
    [self setUIElement];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self getYourTotalRewardPoints];
    self.view.userInteractionEnabled = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:true];
}

-(void)setUIElement{
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;
    self.title = @"";
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    //Add padding to text field
    [LSUtils addPaddingToTextField:_minimumAmountTextField];
    [LSUtils addPaddingToTextField:_maximumAmountTextField];
    [LSUtils addPaddingToTextField:_redeemAmountTextField];
    [sharedUtils placeholderSize:_maximumAmountTextField :@"Enter maximum amount..."];
    [sharedUtils placeholderSize:_minimumAmountTextField :@"Enter minimum amount..."];
    [sharedUtils placeholderSize:_redeemAmountTextField :@"Enter amount..."];

    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 8;
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;

}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)backButtonClicked:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - TextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    NSRange spaceRange = [string rangeOfString:@" "];
    
    if([textField.text length] == 0){
        if (spaceRange.location != NSNotFound)
        {
            return NO;
        }
    }
    
    NSCharacterSet *validCharSet;
    if (range.location == 0)
        validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    else
        validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    
    if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
        [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
        return false;  //not allowable char
    }
    
    return  true;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // Disable emoji input
    if (![text canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    NSRange spaceRange = [text rangeOfString:@" "];
    if([textView.text length] == 0){
        if (spaceRange.location != NSNotFound)
        {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_minimumAmountTextField)
    {
        if(!([_minimumAmountTextField.text length]>0))
        {
            [sharedUtils placeholderSize:_minimumAmountTextField :@"Enter minimum amount..."];
            
        }
    }
    
    if (textField==_maximumAmountTextField)
    {
        if(!([_maximumAmountTextField.text length]>0))
        {
            [sharedUtils placeholderSize:_maximumAmountTextField :@"Enter maximum amount..."];
            
        }
    }
    if (textField==_redeemAmountTextField)
    {
        if(!([_redeemAmountTextField.text length]>0))
        {
            [sharedUtils placeholderSize:_redeemAmountTextField :@"Enter amount..."];
            
        }
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIView *textFieldSuperView = textField.superview;
    UIResponder* nextResponder = [textFieldSuperView.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}
-(void)cancelKeyboard {
    
    [[self view] endEditing:true];
}

-(BOOL)validateTextField {
  
//    if(![LSUtils checkNilandEmptyString:_minimumAmountTextField.text]){
//        
//        [[NotificationManager notificationManager] displayMessage:@"Minimum amount can not be blank." withType:Negative];
//        
//        return false;
//    }
//    
//    if(![LSUtils checkNilandEmptyString:_maximumAmountTextField.text]){
//        
//        [[NotificationManager notificationManager] displayMessage:@"Maximum amount can not be blank." withType:Negative];
//        
//        return false;
//    }
    if(isRedeemPrice){
        //You have check to redeem price to compulsory to put amount to be redeem
        if(![LSUtils checkNilandEmptyString:_redeemAmountTextField.text]){
            
            [[NotificationManager notificationManager] displayMessage:@"Please enter amount to be redeemed." withType:Negative];
            
            return false;
        }
    }
    if([LSUtils checkNilandEmptyString:_maximumAmountTextField.text] && [LSUtils checkNilandEmptyString:_minimumAmountTextField.text]){
        if([_minimumAmountTextField.text floatValue]  > [_maximumAmountTextField.text floatValue]){
            //amount quoted for reddemption is greater than available amount in account
            [[NotificationManager notificationManager] displayMessage:@"Minimum amount should be less than maximum amount." withType:Negative];
            
            return false;
        }
    }
    if(rewardPoint < [_redeemAmountTextField.text floatValue]){
        //amount quoted for reddemption is greater than available amount in account
        [[NotificationManager notificationManager] displayMessage:@"Redemption amount can not more than total points." withType:Negative];
        
        return false;
    }
    return true;
}

- (IBAction)jumpPriceButtonClicked:(id)sender {
   
    if(isJumpedPrice){
        [_JumbPriceButton setTitle:@"" forState:UIControlStateNormal];
        [_JumbPriceButton setBackgroundColor: [UIColor clearColor]];
        isJumpedPrice = false;
        
    }else{
        [_JumbPriceButton setTitle:@"check" forState:UIControlStateNormal];
        [_JumbPriceButton setBackgroundColor: [UIColor darkGrayColor]];
        isJumpedPrice = true;
        
    }
}
- (IBAction)redeemButtonClicked:(id)sender {
    
    if(isRedeemPrice){
        [_redeemButton setTitle:@"" forState:UIControlStateNormal];
        [_redeemButton setBackgroundColor: [UIColor clearColor]];
        isRedeemPrice = false;
        _redeemAmountTextField.enabled = false;
        _redeemAmountTextField.text =@"";
        
        
    }else{
        [_redeemButton setTitle:@"check" forState:UIControlStateNormal];
        [_redeemButton setBackgroundColor: [UIColor darkGrayColor]];
        isRedeemPrice = true;
        _redeemAmountTextField.enabled = true;
        
    }
}
//- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
//    return UIModalPresentationNone;
//}

- (IBAction)viewSuggestionsButtonClicked:(id)sender {
    
    [[self view] endEditing:true];
    [self getSuggestedCost:^(BOOL success, NSDictionary *result, NSError *error) {
        if (error != nil) {
            // handel error part here
            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            [self presentViewController:controller animated:YES completion:nil];
        }
        if(success){
            if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                
                NSDictionary * amountDictionary = [[result valueForKey:@"data"] objectAtIndex:0];
                _maximumAmountTextField.text = [NSString stringWithFormat:@"%.02f",[[amountDictionary objectForKey:@"maxcost"] floatValue]];
                _minimumAmountTextField.text = [NSString stringWithFormat:@"%.02f",[[amountDictionary objectForKey:@"mincost"] floatValue]];
                
            }else{
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:[result objectForKey:@"message"]];
                [self presentViewController:controller animated:YES completion:nil];
                
            }
        }else{
            UIViewController*  controller;
            
            if(result ==nil){
                
                controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
            }else{
                NSLog(@"%@",result);
                controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
            }
            [self presentViewController:controller animated:YES completion:nil];
        }
    }];
    
}
/*
 "token"                 :   "ecbcd7eaee29848978134beeecdfbc7c"
 "methodname"            :   "getordercost"
 "origin"                :   "28.631020,77.381273"
 "destination"           :   "28.584933,77.311776"
 */
-(void)getSuggestedCost:(void (^) (BOOL success,NSDictionary* result,NSError* error))completion {
    
    NSString *params = [NSString stringWithFormat:@"%@&origin=%@,%@&destination=%@,%@",getordercost, [NSString stringWithFormat:@"%f",_confirmationDetails.productPickUp.latitue],[NSString stringWithFormat:@"%f",_confirmationDetails.productPickUp.longitute], [NSString stringWithFormat:@"%f",_confirmationDetails.productDelivery.latitue],[NSString stringWithFormat:@"%f", _confirmationDetails.productDelivery.longitute]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data  options:0 error:NULL];
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  completion(false, nil, nil);
                                                  
                                                  
                                              }else
                                                  completion(true,responseDictionary,nil);
                                              
                                              
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  completion(true,nil,error);
                                                  
                                              }else{
                                                  
                                                  completion(false,nil,nil);
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
    
}

-(void)getYourTotalRewardPoints {
    //Get it done in background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",vendorTotalPointEarning,[UserManager getUserID]];
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [_spinner stopAnimating];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data  options:0 error:NULL];
                                                  
                                                  if(responseDictionary == nil){
                                                      UIAlertController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:false completion:nil];
                                                      
                                                  }else{
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          _rewardAmountlabel.text = [NSString stringWithFormat:@"( $%.02f )",[[[responseDictionary objectForKey:@"data"] objectForKey:@"total_amount"] floatValue]];
                                                          rewardPoint = [[[responseDictionary objectForKey:@"data"] objectForKey:@"total_amount"] floatValue];
                                                      }else{
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"Sucess" withMessage:[responseDictionary objectForKey:@"message"]];
                                                          [self presentViewController:controller animated:false completion:nil];
                                                          
                                                      }
                                                  }
                                              }else{
                                                  UIViewController *controller;
                                                  if(error != nil){
                                                      controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                                                  }else{
                                                      controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                                                  }
                                                  if(controller!=nil)
                                                  [self presentViewController:controller animated:false completion:nil];

                                              }
                                          });
                                      }];
        [task resume];

    });
}
- (IBAction)proceeedButtonClicked:(id)sender {
    
    if([self validateTextField]){
        
        ConfirmOrderViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"ConfirmOrderViewController"];
        vc.orderSummary = _confirmationDetails;
        vc.orderSummary.maxAmount =  [_maximumAmountTextField.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        vc.orderSummary.minAmount =  [_minimumAmountTextField.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        vc.orderSummary.isJumpedPrice = isJumpedPrice;
        vc.orderSummary.isRedeemPrice = isRedeemPrice;
        if(isRedeemPrice) vc.orderSummary.redeemPrice = _redeemAmountTextField.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

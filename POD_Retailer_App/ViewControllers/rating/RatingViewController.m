//
//  RatingViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 07/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "RatingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface RatingViewController ()
{
    NSString *orderId;
    NSMutableArray *driversArry;
    NSString *imageBaseURL;
    int isfav;
    //  is_favourite 1 => favourite, 2=> No
}
@end

@implementation RatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElementsValues];
    imageBaseURL = [sharedUtils.dictOfOrderTosRate objectForKey:@"deliveryboyimgurl"];
    _orderNoLabel.text = [NSString stringWithFormat:@"Order NO.%@",[sharedUtils.dictOfOrderTosRate objectForKey:@"orderid"]];
}

-(void )viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    driversArry = [sharedUtils.dictOfOrderTosRate objectForKey:@"DriverArray"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpUIElementsValues{
    
    self.title = @"";
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    _tableView.delegate = (id)self;
    _tableView.dataSource =(id) self;
}

- (IBAction)sentRatings:(id)sender {
  //  __weak typeof(self) weakSelf = self;
    
    if([self validate]){
      [self setRating:[sharedUtils.dictOfOrderTosRate objectForKey:@"orderid"] Completion:^(BOOL success, NSDictionary *result, NSError *error) {
          if(success){

          }
      }];
        
    }else{
        UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Please give rating to driver"];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return 5;
    return driversArry.count;
}

static NSString *cellIdentifier = @"CellIdentifier";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @try
    {
        RatingCellTableViewCell *cell = (RatingCellTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
        cell.indexPath = indexPath;
        cell.delegate = self;
        cell.starView.indexPathForRating = indexPath;
        cell.starView.delegateRate = self;
        NSDictionary *dict = [driversArry objectAtIndex:indexPath.row];
        cell.namelabel.text = [dict objectForKey:@"deliveryboy_name"];
        cell.emaillabel.text = [dict objectForKey:@"deliveryboy_email"];//deliveryboy_image
        cell.starView.canEdit = YES;
        cell.starView.maxRating = 5;
        [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",imageBaseURL, [dict objectForKey:@"deliveryboy_image"]]]  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        cell.profileImageView.layer.cornerRadius = 10;
        cell.profileImageView.layer.masksToBounds = true;
        return cell;
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exeption Description:%@",exception.description);
    }
}


-(void)setRating:(NSString*)orderIds  Completion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion{
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:driversArry options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data
                                              encoding:NSUTF8StringEncoding];
    
    NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@&order_id=%@&fromios=1&rating_detail=%@&is_favorite=%@",setorderrating,[UserManager getUserID],orderIds, jsonStr,[NSString stringWithFormat:@"%d",isfav]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                 // completion(true ,responseDictionary,nil);
                                                  NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                                                  for (UIViewController *aViewController in allViewControllers) {
                                                      if ([aViewController isKindOfClass:[DashboardViewController class]]) {
                                                          [self.navigationController popToViewController:aViewController animated:NO];
                                                      }
                                                  }
                                              }
                                              else  if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Could not update on server"];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];

}

/*
-(void)getRatingStatus {

    NSString *params = [NSString stringWithFormat:@"%@&vendor_id=1",cheeckratingstatus];//,[UserManager getUserID]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                    
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSMutableArray *arrayOfData;
                                                  
                                                  arrayOfData = [[responseDictionary valueForKey:@"data"] objectAtIndex:0];
                                                  
                                                  orderId = [arrayOfData valueForKey:@"orderid"] ;
                                                  imageBaseURL = [responseDictionary valueForKey:@"deliveryboyimgurl"];
                                                  
                                                  NSArray *driverArray = [arrayOfData valueForKey:@"deliveryboydetail"];

                                                  driversArry = [[NSMutableArray alloc]init];
                                                  
                                                  for(int index =0 ; index<driverArray.count ; index++){
                                                      
                                                      NSDictionary * driverDict = [driverArray objectAtIndex:index];
                                                      
                                                      [driversArry addObject:driverDict];
                                                  }
                                                  
                                                  [_tableView reloadData];
                                                  
                                              }
                                              else  if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There are no drivers available for given location."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
    
}
*/
- (void)textFieldCell:(UITextView *)inCell updateTextLabelAtIndexPath:(NSIndexPath *)inIndexPath string:(NSString *)inValue {
    
    NSLog(@"See input: %@ from section: %ld row: %ld, should update models appropriately", inValue, (long)inIndexPath.section, (long)inIndexPath.row);
}

- (void)saveTextFieldText:(NSIndexPath *)indexPath string:(NSString *)inValue{

    NSMutableDictionary *dict = [[driversArry objectAtIndex:indexPath.row]  mutableCopy];
    [dict setObject:inValue forKey:@"order_review"];
    [driversArry replaceObjectAtIndex:indexPath.row withObject:dict];

}

- (void)saveRate:(NSIndexPath *)indexPath rateValue:(float )rating{
    NSMutableDictionary *dict = [[driversArry objectAtIndex:indexPath.row] mutableCopy];

    if(rating == 0){
        
        UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Please rate for driver"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    NSLog(@"%ld" ,(long)indexPath.row);
    [dict setObject:[NSString stringWithFormat:@"%d",(int)rating] forKey:@"order_rating"];
    [driversArry replaceObjectAtIndex:indexPath.row withObject:dict];

}

-(BOOL)validate{
    for(NSDictionary * dict in driversArry){
        
        if([[dict valueForKey:@"order_rating"] floatValue] == 0){
            
            return false;
        }
    }
    return true;
}
@end

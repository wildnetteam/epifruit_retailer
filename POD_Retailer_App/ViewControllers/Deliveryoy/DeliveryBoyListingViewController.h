//
//  DeliveryBoyListingViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 10/02/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormPickerView.h"
#import "OrderDetailsModel.h"
@interface DeliveryBoyListingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource ,CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightSeparator;

@property (nonatomic, strong) FormPickerView *transportModePickerView;

@property (nonatomic, strong) NSMutableArray *selectedIndexPathArray;
@property (nonatomic, strong) NSMutableArray *deliveryBoyArray;
@property (nonatomic, retain) NSArray *modeIds;
@property (nonatomic, retain) NSMutableArray* pickerArray;
//@property (nonatomic, retain) NSMutableArray* selectedDriversArray;
@property (retain, nonatomic) OrderDetailsModel *orderSummary;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;

@end

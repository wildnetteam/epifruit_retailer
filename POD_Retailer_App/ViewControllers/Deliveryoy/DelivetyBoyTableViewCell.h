//
//  DelivetyBoyTableViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 10/02/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "DeliveryBoyModel.h"

@interface DelivetyBoyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ratingViewWidthConstant;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *driverProfileBackgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellShadowSeparartor;
@property (weak, nonatomic) IBOutlet UIImageView *driverProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet ASStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIButton *selectionButton;
@property (weak, nonatomic) IBOutlet UILabel *transportTextLabel;

-(void)setDeliverBoyDetails:(DeliveryBoyModel*)delBoyInstance  ;
@end

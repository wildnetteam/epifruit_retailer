//
//  DeliveryBoyListingViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 10/02/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "DeliveryBoyListingViewController.h"
#import "DelivetyBoyTableViewCell.h"
#import "AddressViewController.h"
#import "IQActionSheetPickerView.h"
#import "DeliveryBoyModel.h"
#import "TransportModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>

@interface DeliveryBoyListingViewController () <IQActionSheetPickerViewDelegate>

@end

@implementation DeliveryBoyListingViewController
{
    CLLocationManager *locationManager;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    _deliveryBoyArray = [[NSMutableArray alloc]init];
    _selectedIndexPathArray = [[NSMutableArray alloc]init];
    [self setUIElement];
}

-(void)test{
    
    NSString *alertmessage ;
    if (![CLLocationManager locationServicesEnabled])
    {
        alertmessage = @"Please go to Settings and turn on Location Service for this app." ;
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Location Services Disabled!"
                                              message:alertmessage
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           //go back
                                       }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self  presentViewController:alertController animated:NO completion:Nil];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    locationManager = [[CLLocationManager alloc]init];
    [locationManager requestWhenInUseAuthorization];
    if(_orderSummary.driverCopleteDetails .count > 0){
        _selectedIndexPathArray = [_orderSummary.driverCopleteDetails copy];
        
    }
    [ self test];
    
    if(_deliveryBoyArray.count ==0){

        if([LSUtils isNetworkConnected]){
            
            [self getDeliveryBoyList];
        }
        else{
            UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}
-(void)ShowMessageAlert :(NSString*)message{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Location Services Disabled!"
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       //go back
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self  presentViewController:alertController animated:NO completion:Nil];
}
-(void)setUIElement{
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;
    self.title = @"";
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"map"] style:UIBarButtonItemStylePlain target:self action:@selector(mapBClicked:)];
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;
}

#pragma mark - SideMenu Methods

-(void)backButtonClicked:(id)sender{
    _orderSummary.driverCopleteDetails = _selectedIndexPathArray;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.deliveryBoyArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DelivetyBoyTableViewCell *cell = (DelivetyBoyTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    DeliveryBoyModel *object = [_deliveryBoyArray objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[DelivetyBoyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:@"CellIdentifier"];
    }
    [cell setDeliverBoyDetails:object];
    if(IS_IPHONE_6P)
    cell.cellShadowSeparartor.constant = -5;
    if(_selectedIndexPathArray.count == 0){
        
        [cell.selectionButton setImage:[UIImage imageNamed:@"add_Driver"] forState:UIControlStateNormal];
        cell.transportTextLabel.text = @"";
        
    }else{
    
    for (DeliveryBoyModel *obj1 in _selectedIndexPathArray) {
        
        if(obj1.dId == object.dId){
            
            [cell.selectionButton setImage:[UIImage imageNamed:@"added"] forState:UIControlStateNormal];
            cell.transportTextLabel.text= [obj1.dModeSelcted valueForKey:@"transport_mode"];
            break;
        }
        else{
         [cell.selectionButton setImage:[UIImage imageNamed:@"add_Driver"] forState:UIControlStateNormal];
            cell.transportTextLabel.text =@"";
        }
    }
}
    [cell.driverProfileImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", object.dProfileURL, object.imageName]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.driverProfileImage.layer.cornerRadius = 10;
    cell.driverProfileImage.layer.masksToBounds = true;
 
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DeliveryBoyModel* object = [_deliveryBoyArray objectAtIndex:indexPath.row ];
    
    NSMutableArray *selectedIndexPathArrayCopy = [_selectedIndexPathArray copy];
    
    __block BOOL deleteObjec = false;
    __block  NSUInteger index ;
    if(selectedIndexPathArrayCopy.count == 0){
        
        [self initializePickerView:indexPath];

    }else{
        
        [selectedIndexPathArrayCopy enumerateObjectsUsingBlock:^(DeliveryBoyModel* obj, NSUInteger idx, BOOL *stop) {
            if ([object.dId isEqualToString:obj.dId]) {
                //Assigning YES to the stop variable is the equivalent of calling "break" during fast enumeration
                *stop = YES;
                index = idx;
                deleteObjec = true;
            }
        }];
        
        _selectedIndexPathArray = [NSMutableArray arrayWithArray:selectedIndexPathArrayCopy];
        
        if(deleteObjec){
            
            [_selectedIndexPathArray removeObjectAtIndex:index];
        }else{
            [self initializePickerView:indexPath];
        }
        
    }
    [_tableView deselectRowAtIndexPath:indexPath animated:true];
    
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

-(void)initializePickerView :(NSIndexPath*)index {
    
   NSArray *aray = [self getDriversModeOfTransport:index];

    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select Transport" delegate:self];
    picker.titleFont = [UIFont fontWithName:@"Lato-Semibold" size:20.0];
    picker.titleColor = [UIColor blackColor];
    picker.pickerComponentsFont = [UIFont fontWithName:@"Lato-Regular" size:20.0];
    
    //TabaleView IndexPath Row
    picker.index = (int) index.row;
    [picker setTag:1];
    [picker setTitlesForComponents: @[aray]];//@[@[@"Bus", @"Car", @"Subway", @"Foot"]]];
    [picker show];

}

-(NSMutableArray*)getDriversModeOfTransport :(NSIndexPath*)index {
    DeliveryBoyModel *object = [_deliveryBoyArray objectAtIndex:index.row];

    NSString* modID = object.dtransportMode;
    NSArray *ar = [modID componentsSeparatedByString:@","];
    _pickerArray = [[NSMutableArray alloc]init];
    for (id mID in ar){
        
        NSPredicate *pred=[NSPredicate predicateWithFormat:@"tId==%@",mID];
        NSArray *resultArray=[_modeIds filteredArrayUsingPredicate:pred];
        if (resultArray.count != 0){
            TransportModel *m = [resultArray firstObject];
            [_pickerArray addObject:m];
        }
    }
    return _pickerArray;
    
}
-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles :(int)arrayNo
{
    //indexNo is picker array index
    [self getTransportIdFromText:pickerView.index pickerArrayIndex:arrayNo];
    
}

-(void)getTransportIdFromText :(int)indexNo pickerArrayIndex:(int)arrayNumber{
  
    TransportModel *selectedDriverMode = [_pickerArray objectAtIndex:arrayNumber];
    
    if (_deliveryBoyArray != nil && [_deliveryBoyArray count] > 0){
        
        DeliveryBoyModel* object = [_deliveryBoyArray objectAtIndex:indexNo];
        
        if(![_selectedIndexPathArray containsObject:object]){
            
            [_selectedIndexPathArray addObject:object];

            
            object.dModeSelcted = [NSDictionary
                                   dictionaryWithObjectsAndKeys: selectedDriverMode.tId,@"transport_id", selectedDriverMode.tMode,@"transport_mode",nil];
            [_orderSummary.driverCopleteDetails addObject:object];
            
    }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexNo inSection:0];
        
        [_tableView deselectRowAtIndexPath:indexPath animated:true];
        
        [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
    }
}
//-(IBAction)backButtonClicked:(id)sender{
//    
//    _orderSummary.driverCopleteDetails = _selectedIndexPathArray;
//    [self dismissViewControllerAnimated:true completion:nil];
//}

//-(IBAction)mapClicked:(id)sender{
//    
//    AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
//    vc.locationArray = self.deliveryBoyArray;
//    [self.navigationController pushViewController:vc animated:true];
//}

-(void)mapBClicked:(id)sender{
    AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
    vc.locationArray = self.deliveryBoyArray;
    [self.navigationController pushViewController:vc animated:true];
}
-(void)getDeliveryBoyList {

    NSMutableString *idString = [[NSMutableString alloc]init];
    for ( TransportModel *model in _modeIds ) {
        [idString appendFormat:@"%@,", model.tId];
    }
    
    
            NSString *params = [NSString stringWithFormat:@"%@&vendor_lat=%@&vendor_long=%@&vendor_radius=20&mode_id=%@",GetDeliveryBoyList,[NSString stringWithFormat:@"%f",_orderSummary.productPickUp.latitue],[NSString stringWithFormat:@"%f",_orderSummary.productPickUp.longitute],[idString substringToIndex:idString.length-(idString.length>0)]];
            
            NSURL *url = [NSURL URLWithString:Base_URL];
            [sharedUtils startAnimator:self];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [sharedUtils stopAnimator:self];
                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data  options:0 error:NULL];
                                                      
                                                      if(responseDictionary == nil){
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                          
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          return ;

                                                      }
                                                      
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          NSMutableArray *arrayOfAddress;
                                                          arrayOfAddress = [responseDictionary valueForKey:@"data"];
                                                          
                                                          for(int index =0 ; index<arrayOfAddress.count ; index++){
                                                              
                                                              NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                              
                                                              //Set address Model
                                                              DeliveryBoyModel *deliveryBoy = [[DeliveryBoyModel alloc] initWithModeName:[arrayOfdata valueForKey:@"deliveryboy_name"] Email:[arrayOfdata valueForKey:@"deliveryboy_email"] Mobile:[arrayOfdata valueForKey:@"deliveryboy_mobile"] Rate:[arrayOfdata valueForKey:@"del_rating"] AddressLine1:[arrayOfdata valueForKey:@"address"] AddressLine2:[arrayOfdata valueForKey:@"addressfield2"] City:[arrayOfdata valueForKey:@"city"] State:[arrayOfdata valueForKey:@"state"] Lat:[arrayOfdata valueForKey:@"lat"] Lon:[arrayOfdata valueForKey:@"long"] TransportMode:[arrayOfdata valueForKey:@"transport_mode"] Profile:[arrayOfdata valueForKey:@"deliveryboy_image"] Distane:[arrayOfdata valueForKey:@"delboy_distance"] delId:[arrayOfdata valueForKey:@"id"] profilBaseURL:[responseDictionary objectForKey:@"deliveryboyimgurl"]] ;
                                                              
                                                              [self.deliveryBoyArray addObject:deliveryBoy];
                                                          }
                                                       
                                                          [_tableView reloadData];
                                                          
                                                      }
                                                      else  if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
    
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There are no drivers available for given location."];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                      
                                                  }else{
                                                      //data is nil
                                                      if(error != nil){
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                          [self presentViewController:controller animated:YES completion:nil];

                                                      }else{
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                          [self presentViewController:controller animated:YES completion:nil];

                                                      }
                                                  }
                                              });
                                          }];
            [task resume];
            
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}

@end

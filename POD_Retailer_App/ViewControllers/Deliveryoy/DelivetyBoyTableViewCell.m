//
//  DelivetyBoyTableViewCell.m
//  PODRetailerApp
//
//  Created by Arpana on 10/02/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "DelivetyBoyTableViewCell.h"
@implementation DelivetyBoyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if(IS_IPHONE5){

        _ratingViewWidthConstant.constant = 100;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)addDeliveryBoy:(id)sender {
    NSLog(@"Add button pressed");
}
-(void)setDeliverBoyDetails:(DeliveryBoyModel*)delBoyInstance  {
  
    _driverNameLabel.text = delBoyInstance.dName;
    _distanceLabel.text = [NSString stringWithFormat:@"%.02f km away...",[delBoyInstance.dDistane floatValue]];
    _ratingView.rating = [delBoyInstance.dRate floatValue];
    if(delBoyInstance.dModeSelcted!=nil)
   _transportTextLabel.text = [delBoyInstance.dModeSelcted valueForKey:@"transport_mode"];

}

@end

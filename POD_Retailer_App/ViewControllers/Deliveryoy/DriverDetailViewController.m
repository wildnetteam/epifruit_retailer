//
//  DriverDetailViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 09/03/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "DriverDetailViewController.h"
#import "DriverDetailsTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DriverDetailViewController ()
{
    NSDictionary *deliveryBoyProfileDict;
    NSMutableArray *reviewArray;
}
@end

@implementation DriverDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIElement];
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    _driverNameLabel.text = _driverDetail.dName;
    _driverEmailLabel.text = _driverDetail.dEmail;
    _averageRatingView.rating = [_driverDetail.dRate floatValue];
    [self loadImage:[NSString stringWithFormat:@"%@/%@", _driverDetail.delBoyImageBaseUrl, _driverDetail.imageName]];
    [self getDriverDetail];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    _ratingReviewLabelTopCOnstraint.constant =  _driverInformationViewTopConstraint.constant  + 20 ;
//    
//    [UIView animateWithDuration:0.5 animations:^{
//        [self.view layoutIfNeeded];
//    } completion:^(BOOL finished) {
//    }];
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y == 0){
        
        _ratingReviewLabelTopCOnstraint.constant =  0 ;
        
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
        }];
    }
}
-(void)setUIElement{
    
    self.title = @"";
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem ];
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;
    
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0;
    
    self.driverSmallProfileView.layer.cornerRadius = self.driverSmallProfileView.frame.size.width /4;
    self.driverSmallProfileView.layer.masksToBounds = YES;
    _driverSmallProfileView.clipsToBounds = YES;
}
-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return reviewArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    DriverDetailsTableViewCell *cell = (DriverDetailsTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    cell.noLabel.text = [NSString stringWithFormat:@"%d.",(int)indexPath.row +1];
    cell.commentlabel.text =  [[reviewArray objectAtIndex:indexPath.row] objectForKey:@"order_review"];
    cell.starView.canEdit = NO;
    cell.starView.maxRating = 5;
    cell.starView.rating = [[[reviewArray objectAtIndex:indexPath.row] objectForKey:@"order_rating"] floatValue];
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT];
    
    //Create the date assuming the given string is in GMT
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [df dateFromString:[[reviewArray objectAtIndex:indexPath.row] valueForKey:@"created_on"]];
    
    //Create a date string in the local timezone
    [df setDateFormat:SHOWING_MONTH_AND_DATE_FORMAT_DRIVER_COMMENT_DATE];
    
    cell.dateLabel.text= [NSString stringWithFormat:@"%@ - ", [df stringFromDate:date]];
    
    if(indexPath.row ==1){
       cell.commentlabel.text = @"u5uty  tuy545 8ty5 45ty4 45 4958 45ty  u5uty  tuy545 8ty5 45ty4 45 4958 45ty";
    }
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (IBAction)callButtonPressed:(id)sender {
    
    NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",_driverDetail.dMobile];
    NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
    [[UIApplication sharedApplication] openURL:phoneURL];
    
  }
- (IBAction)messageButtonPresseed:(id)sender {
    
    NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",_driverDetail.dMobile];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:%@",phoneStr]]];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)getDriverDetail{
    
    if([LSUtils isNetworkConnected]){
        
        [sharedUtils startAnimator:self];
        
        NSString *params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@",getdeliveryboyrating, _driverDetail.dId];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data       options:0  error:NULL];
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      reviewArray = [[NSMutableArray alloc]init];
                                                      [reviewArray addObjectsFromArray:[responseDictionary objectForKey:@"data"]];
                                                      [_tableView reloadData];
                                                  }
                                              }else{
                                              }
                                          });
                                      }];
        [task resume];
    }else{
        
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void) loadImage:(NSString *)imageLink{
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    [manager loadImageWithURL:[NSURL URLWithString:imageLink] options:SDWebImageDelayPlaceholder progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        _driverBigProfileView.image = image;
        _driverSmallProfileView.image = image;
    }];
}

@end

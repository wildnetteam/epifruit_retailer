//
//  DriverDetailViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 09/03/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSUtils.h"
#import "DeliveryBoyModel.h"
#import "ASStarRatingView.h"
@interface DriverDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource ,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverInformationViewTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ratingReviewLabelTopCOnstraint;
@property (weak, nonatomic) IBOutlet UIImageView *driverSmallProfileView;
@property (weak, nonatomic) IBOutlet UIView *driverInformationView;
@property (weak, nonatomic) IBOutlet UIImageView *driverBigProfileView;
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverEmailLabel;
@property (nonatomic , strong) DeliveryBoyModel* driverDetail;
@property (weak, nonatomic) IBOutlet ASStarRatingView *averageRatingView;

@end

//
//  TermsAndConditionsViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 19/04/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

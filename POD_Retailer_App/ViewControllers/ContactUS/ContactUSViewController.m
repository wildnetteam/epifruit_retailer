//
//  ContactUSViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 10/04/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "ContactUSViewController.h"
#import "NotificationManager.h"
#import "UITextView+Placeholder.h"
@interface ContactUSViewController ()

@end

@implementation ContactUSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElementsValues];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
    }
}

-(BOOL)validatetextField {
    
    if(![LSUtils checkNilandEmptyString:_subjecttextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Subject can not be blank." withType:Negative];
        
        return false;
    }

    if(![LSUtils checkNilandEmptyString:_descriptionTextView.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Question can not be blank." withType:Negative];
        
        return false;
    }
    
    return true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    if(!([_subjecttextField.text length]>0))
    {
        [sharedUtils placeholderSize:_subjecttextField :@"Enter your subject..."];
    }

    [_descriptionTextView.placeholderLabel setValue:[UIFont fontWithName:@"Lato-Light" size:15.0] forKeyPath:@"font"];
      _descriptionTextView.placeholder =@"Add  question...";
    _descriptionTextView.placeholderLabel.textColor = [UIColor grayColor];

    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
}
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIView *textFieldSuperView = textField.superview;
    UIResponder* nextResponder = [textFieldSuperView.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    return true;
}

-(void)sendMail{
    
    NSString* params = [NSString stringWithFormat:@"%@&vendor_id=%@&email_subject=%@&email_desc=%@",contactUs_Service,[UserManager getUserID],_subjecttextField.text,_descriptionTextView.text];
    
    if([LSUtils isNetworkConnected])
    {
        self.view.userInteractionEnabled = NO;
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              self.view.userInteractionEnabled = YES;
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  
#ifdef DEBUG
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  
#endif
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      [self.navigationController popViewControllerAnimated:NO];
                                                      
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Query Submitted successfully."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:^{
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                          });
                                                      }];
                                                      
                                                  }
                                                  else {
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"could not be submit your query"];
                                                      [self presentViewController:controller animated:YES completion:nil];                                                  }
                                              }
                                              if(error!= nil){
                                                  UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    
}

- (IBAction)submitButtonPressed:(id)sender {
    if([self validatetextField]){
        
        [self sendMail];
        
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // Disable emoji input
    if (![text canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    NSRange spaceRange = [text rangeOfString:@" "];
    if([textView.text length] == 0){
        if (spaceRange.location != NSNotFound)
        {
            return NO;
        }
    }
    return YES;
}


-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if (textView == _descriptionTextView)
    {
        if([_descriptionTextView.text length] == 0){
            UIColor *color = [UIColor grayColor];
            _descriptionTextView.placeholderLabel.textColor = color;
            
            [_descriptionTextView.placeholderLabel setValue:[UIFont fontWithName:@"Lato-Light" size:15.0] forKeyPath:@"font"];
            
            _descriptionTextView.placeholder =@"Add  question...";
        }
    }
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    _descriptionTextView.placeholder =@"";
    [_descriptionTextView setValue:[UIFont fontWithName:@"Lato-Regular" size:16.0] forKeyPath:@"font"];
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if(!([_subjecttextField.text length]>0))
    {
        [sharedUtils placeholderSize:_subjecttextField :@"Enter your subject..."];
    }

}
@end

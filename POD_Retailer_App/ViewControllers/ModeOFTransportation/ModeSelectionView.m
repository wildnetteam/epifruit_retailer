//
//  ModeSelectionView.m
//  PODRetailerApp
//
//  Created by Arpana on 16/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "ModeSelectionView.h"
#import "TransportModel.h"
#import "NotificationManager.h"
@implementation ModeSelectionView
{
    
    int selectedIndexPath;
}
-(id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _popViewTopBar = [[UIView alloc] init];
        _popViewTopBar.backgroundColor = [UIColor colorWithHexString:@"#3fcbe4"];
        
        _cancelButton = [[UIButton alloc] init];
        [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = FONT_LABEL_BOLD;
        _cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        _doneButton = [[UIButton alloc] init];
        [_doneButton setTitle:@"Save" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _doneButton.titleLabel.font = FONT_LABEL_BOLD;
       // _doneButton.backgroundColor = [UIColor redColor];

        _popViewTopBarTitle =[[UILabel alloc]initWithFrame:CGRectZero];
        [_popViewTopBarTitle setTextColor:[UIColor whiteColor]];
        _popViewTopBarTitle.text = @"Select Transportation";
        _popViewTopBarTitle.font = IS_IPHONE5?[UIFont fontWithName:@"Lato-Regular" size:16]:[UIFont fontWithName:@"Lato-Regular" size:18];
        _popViewTopBarTitle.textAlignment = NSTextAlignmentCenter;
        
        // Add the subviews to the popBar
        [_popViewTopBar addSubview:_popViewTopBarTitle];
        [_popViewTopBar addSubview:_cancelButton];
        [_popViewTopBar addSubview:_doneButton];
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero];
        [_cancelButton addTarget:self action:@selector(popDown) forControlEvents:UIControlEventTouchUpInside];
        [_doneButton addTarget:self action:@selector(saveAndPopDown) forControlEvents:UIControlEventTouchUpInside];
       // _cancelButton.backgroundColor = [UIColor redColor];

        _tableView.delegate = (id)self;
        _tableView.dataSource= (id)self;
        
        [self addSubview:_tableView];
        [self addSubview:_popViewTopBar];
        [self setFrame:frame];
        
    }
    return self;
}

-(void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    int pad = 20;
    
    _tableView.frame = CGRectMake(pad, CGRectGetMaxY(_popViewTopBar.frame), self.frame.size.width - 2*pad, CGRectGetHeight(self.frame) - (CGRectGetHeight(_popViewTopBar.frame)+CGRectGetHeight(self.frame)*.20) +20);
    _tableView.backgroundColor =  [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    _popViewTopBar.frame = CGRectMake(0,0, self.frame.size.width, 50);
    
    _cancelButton.frame = CGRectMake(_tableView.frame.origin.x, 0, 100, 50);
    
    _doneButton.frame = CGRectMake(CGRectGetWidth(_tableView.frame) * 3/4 + _tableView.frame.origin.x, 0,100, 50);
    
    _popViewTopBarTitle.frame = CGRectMake(0, 0, self.frame.size.width, 50);
    NSLog(@"Selected array: %lu", (unsigned long)_selectedIndexPathArray.count);

    [_tableView reloadData];

}

// Called by Cancel
- (void) popDown{
    
    [self endEditing:true];
    
    [UIView animateWithDuration:1.0   delay:0.0
                        options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         
                         self.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + self.frame.size.height, self.frame.size.width, self.frame.size.height);
                         self.superview.backgroundColor = [UIColor clearColor];
                         
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             [self.superview removeFromSuperview];
                             
                         }
                     }];
}

-(void)saveAndPopDown{
    
    if(_selectedIndexPathArray.count == 0){
        
    [[NotificationManager notificationManager] displayMessage:@"You need to selected atleast one mode of transportation" withType:Negative];
        return;
    }
    else{
        if([self.selectionsDel respondsToSelector:@selector(saveTransportMode:)]){
            
            
            [self.selectionsDel saveTransportMode:_selectedIndexPathArray];
        }
        [self popDown];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return _modeOfTransportArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
       return 54;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier"];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *modeLabel;
    UILabel *checkUncheckLabel;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - 0.5, tableView.frame.size.width, 0.5)];
        separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        separatorView.layer.borderWidth = 0.5;
        [cell.contentView addSubview:separatorView];
        
        modeLabel = [[UILabel alloc] init];
        modeLabel.tag = 100;
        modeLabel.frame = CGRectMake(0, 0, CGRectGetWidth(cell.contentView.frame) - 52 - 20, CGRectGetHeight(cell.contentView.frame));
        modeLabel.font = FONT_TextField;
        modeLabel.backgroundColor = [UIColor clearColor];
        modeLabel.text = @"";
        [cell.contentView addSubview:modeLabel];
        
        checkUncheckLabel = [[UILabel alloc] init];
        checkUncheckLabel.tag = 101;
        checkUncheckLabel.frame = CGRectMake(tableView.frame.size.width - 25, 0, 30, CGRectGetHeight(cell.contentView.frame));
        checkUncheckLabel.textColor = [UIColor colorWithHexString:@"#23aac2"];
        checkUncheckLabel.font = [UIFont fontWithName:@"SSSTANDARD" size:10.0f];
        checkUncheckLabel.text = @"check";
        
        checkUncheckLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:checkUncheckLabel];
    }
    else{
        modeLabel=(UILabel*)[cell.contentView viewWithTag:100];
        checkUncheckLabel=(UILabel*)[cell.contentView viewWithTag:101];
    }
    
    
    if (_modeOfTransportArray
        == nil || [_modeOfTransportArray count] <= 0) {
        //do something
    }
    else{
        
        
        TransportModel* object = [_modeOfTransportArray objectAtIndex:indexPath.row];
        
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        
        modeLabel.text = [object.tMode capitalizedString];
        
        if(_selectedIndexPathArray.count == 0){
            
            checkUncheckLabel.text = @"";
            modeLabel.textColor = [UIColor colorWithHexString:@"#7d7d7d"];
            
        }else{
            
            for (TransportModel*obj1 in _selectedIndexPathArray) {
                
                if(obj1.tId == object.tId){
                    checkUncheckLabel.text = @"check";
                    modeLabel.textColor = [UIColor colorWithHexString:@"#23aac2"];
                    break;
                }
                else{
                    checkUncheckLabel.text = @"";
                    modeLabel.textColor = [UIColor colorWithHexString:@"#7d7d7d"];
                    
                }
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // Navigation logic may go here. Create and push another view controller.
    
    TransportModel* object = [_modeOfTransportArray objectAtIndex:indexPath.row];
    
    if([_selectedIndexPathArray containsObject:object]){
        
        [_selectedIndexPathArray removeObject:object];
        
    }else
        [_selectedIndexPathArray addObject:object];
    
    [_tableView deselectRowAtIndexPath:indexPath animated:true];
    
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
}


@end

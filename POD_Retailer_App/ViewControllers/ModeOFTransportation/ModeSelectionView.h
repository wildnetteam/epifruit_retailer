//
//  ModeSelectionView.h
//  PODRetailerApp
//
//  Created by Arpana on 16/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol modeSelectionDelegate <NSObject>
@optional
-(void)saveTransportMode:(NSMutableArray *)selectedValues;
-(void)cancel:(id)sender;
-(void)removePopedView;
@end

@interface ModeSelectionView : UIView

@property (strong, nonatomic) UITableView* tableView;

@property (strong, nonatomic) UIView *popViewTopBar; // The bar at the top of the picker view
@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) UILabel *popViewTopBarTitle;
//@property (strong, nonatomic)NSString * modeString;


@property (assign , nonatomic) id<modeSelectionDelegate> selectionsDel;
@property (nonatomic, strong) NSMutableArray *selectedIndexPathArray;
@property (nonatomic, strong) NSArray *modeOfTransportArray;

@end

//
//  ConfirmOrderViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 18/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"
#import "ConsumerAddressModel.h"
#import "ASStarRatingView.h"
#import "OrderDetailsModel.h"
@interface ConfirmOrderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliverAddressLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *modeOfTransportCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *prodTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (retain, nonatomic) NSMutableArray *imageArray;
@property (weak, nonatomic) IBOutlet UIButton*confirmButton;
@property (weak, nonatomic) IBOutlet UIButton* jumpBtn;
@property (retain, nonatomic) OrderDetailsModel *orderSummary;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionViewHeightConstant;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UILabel *minLable;
@property (weak, nonatomic) IBOutlet UILabel *maxLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageCOllectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel* productimageLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productimageHeightConstraint;
@end

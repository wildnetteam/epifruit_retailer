//
//  ConfirmOrderViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 18/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "ConfirmOrderViewController.h"
#import "SelectedCell.h"
#import "ProductImageCollectionViewCell.h"
#import "TransportModel.h"
#import "DelivetyBoyTableViewCell.h"
#import "NoCardViewController.h"
#import "PaymentDtaSourceViewController.h"
#import "UIImage+Scale.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "CompletOrderViewController.h"

@interface ConfirmOrderViewController ()

@end

@implementation ConfirmOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIElement];
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self setTextValues];
}

-(void)setTextValues {
   
    [_pickUpAddressLabel setAttributedText:  [sharedUtils setPickUpFields:_orderSummary.productPickUp]];
    [_deliverAddressLabel setAttributedText:[sharedUtils setPickUpFieldsForDeliveryAddress:_orderSummary.productDelivery]];
    [_quantityLabel setText:_orderSummary.productQuantity];
    [_prodTitleLabel setText:_orderSummary.productTitle];
    [_weightLabel setText:_orderSummary.productWeight];
    [_costLabel setText: [LSUtils checkNilandEmptyString: _orderSummary.productCost]?_orderSummary.productCost:@"NA"];
    [_typeLabel setText:_orderSummary.packageType];
    _minLable.text = [_orderSummary.minAmount isEqualToString:@""]?@"NA":[NSString stringWithFormat:@"$%@",_orderSummary.minAmount];
    _maxLabel.text = [_orderSummary.maxAmount isEqualToString:@""]?@"NA":[NSString stringWithFormat:@"$%@",_orderSummary.maxAmount];
    _jumpBtn.hidden = _orderSummary.isJumpedPrice == true? false:true;
    [self imagesCountOfProduct];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    if(_imageArray.count == 0){
        _imageCOllectionViewHeightConstraint.constant = 0;
       _productimageHeightConstraint.constant = 0;
    }
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillLayoutSubviews{
    [super updateViewConstraints];
    self.descriptionViewHeightConstant.constant = self.tableView.contentSize.height;
}

-(void)setUIElement {
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;
    self.title = @"";
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 8;
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;
    
    [_imageCollectionView registerNib:[UINib nibWithNibName:@"ProductImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [_prodTitleLabel sizeToFit];
    
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (collectionView.tag) {
        case 229:
            return  _imageArray.count;
            break;
        default:
            break;
    }
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell;
    switch (collectionView.tag) {
        case 229:
        {
            ProductImageCollectionViewCell* tableCell =(ProductImageCollectionViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
            UIImage *retrivedImage = [UIImage imageWithContentsOfFile:[_imageArray objectAtIndex:indexPath.row]];
            tableCell.imageViewProduct.image = [retrivedImage scaleToSize:CGSizeMake(750, 750)] ;
            ;
            return tableCell;
        }
            break;
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
       return CGSizeMake(50,50 );
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
   // [_tableView removeObserver:self forKeyPath:@"contentSize"];
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(NSArray *)imagesCountOfProduct
{
    // Get the Document directory path
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    // Create a new path for the new images folder
    NSString *documentsDirectory = [documentsPath stringByAppendingPathComponent:directoryImage];
    
    int count;
    _imageArray  = [[NSMutableArray alloc]init];
    
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
        NSLog(@"File %d: %@", (count + 1), [directoryContent objectAtIndex:count]);
        
        if ([[[directoryContent objectAtIndex:count] pathExtension] isEqualToString:@"png"]) {
            //This is Image File with .png Extension
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[directoryContent objectAtIndex:count]];
            [_imageArray addObject: filePath];
        }
    }
    
    return _imageArray;
}

- (IBAction)proceedToPaymentClicked:(id)sender {
    
    if([LSUtils isNetworkConnected]){
        _confirmButton.enabled = false;
        [self validateCard];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)validateCard{
    
    [sharedUtils getYourCard:^(BOOL success, NSDictionary *result, NSError *error) {
        
        [sharedUtils stopAnimator:self];
        if (error != nil) {
            _confirmButton.enabled = true;
            // handel error part here
            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            if(success){
                if([[result valueForKey:@"code"]intValue] == 200){
                    
                    NSArray * arrayOfdata = [result valueForKey:@"data"];
                    NSDictionary* cardDetailDict = [arrayOfdata objectAtIndex:0];
                    NSMutableString* dt= [[NSMutableString alloc]init];
                    [dt appendString:[cardDetailDict objectForKey:@"expirationMonth"]] ;
                    [dt appendString:@"/"];
                    [dt appendString:[cardDetailDict objectForKey:@"expirationYear"]] ;
                    
                    BOOL isCardNotExpired =[self ValidateExpiryDate:dt];
                    if(!isCardNotExpired){
                        
                        _confirmButton.enabled = true;

                        PaymentDtaSourceViewController* vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"PaymentDtaSourceViewController"];
                        vc.priviousScreen = 1;
                        vc.model = _orderSummary;
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                    else{
                        //  now make payment from this screen itself
                        [self saveOrderDetails];
                        
                    }
                }
                else  if([[result valueForKey:@"code"]intValue] == 300){
                    
                    //No card added screen
                    //if(sharedUtils.cardDetails == nil) {
                    
                    NoCardViewController* vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NoCardViewController"];
                    vc.priviousScreen = 1;
                    vc.orderModel =_orderSummary;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }else {
                UIViewController*  controller;
                //Try later card information can not be  fecthed
                if(result == nil){
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                }else{
                    NSLog(@"%@",result);
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                }
                [self presentViewController:controller animated:YES completion:nil];
            }
        }
    }];
}

-(NSString*)getFormatedDateStringFromDateUTC:(NSDate*)date inFormat:(NSString*)strFormat{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [df stringFromDate:date];
}

-(BOOL)ValidateExpiryDate:(NSMutableString*)cardExpiryDate{
    
    
    NSDateFormatter* dateFormatterHuman = [[NSDateFormatter alloc] init];
    [dateFormatterHuman setDateFormat:@"MM/yyyy"];
    [dateFormatterHuman setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatterHuman setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate* enteredDate = [dateFormatterHuman dateFromString:cardExpiryDate ];
    if(enteredDate == nil){
        return false;
    }
    NSString* todayString = [self getFormatedDateStringFromDateUTC:[NSDate date] inFormat:@"MM/yyyy"];
    NSDate *todayDate = [dateFormatterHuman dateFromString:todayString];
    
    NSComparisonResult result = [todayDate compare:enteredDate];
    switch (result)
    {
        case NSOrderedAscending:
            return true;
            
            break;
        case NSOrderedDescending:
            return false;
            break;
        case NSOrderedSame:
            return true;
            break;
    }
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showPriceJumpedMessage {
    
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:[NSString stringWithFormat:@"By selecting this option your job will be posted at the top. You will be charged 1.5X than your usual order."]
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   // update data sourc
                               }];
    
    [alertController setValue:alertmessage forKey:@"attributedTitle"];
    
   // [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self  presentViewController:alertController animated:NO completion:Nil];
}

#pragma mark - Methods related to send data to 



- (NSString*)createParamForImage{
    
    NSMutableString *imageNames =[[NSMutableString alloc]init];
    
    //Get All image base 64 string
    for(int j=1;j <= _imageArray.count;j++)
    {
        NSString *imageString = [self convertImageToBase:[_imageArray objectAtIndex:j-1]];
        
        [imageNames appendString:[NSString stringWithFormat:@"&product_image_%d=%@",j,imageString]];
    }
    return [NSString stringWithString: imageNames];
}
-(void)saveOrderDetails{
    
    NSString * images = [self createParamForImage];
    
    NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@&product_title=%@&pickup_address=%@&dropoff_address=%@&pickup_address_id=%@&dropoff_address_id=%@&min_rate=%@&max_rate=%@&jump_price=%@&redeemed_point=%@&redeemed_amount=%@&product_type=%@&product_quantity=%@&product_weight=%@&product_price=%@%@",PlaceOrder,[UserManager getUserID],_orderSummary.productTitle,[sharedUtils getPickUpAddress:_orderSummary.productPickUp],[sharedUtils getDeliveryAddress:_orderSummary.productDelivery],_orderSummary.productPickUp.addressId,_orderSummary.productDelivery.addressId,_orderSummary.minAmount,_orderSummary.maxAmount,[NSString stringWithFormat:@"%d",_orderSummary.isJumpedPrice],[NSString stringWithFormat:@"%d",_orderSummary.isRedeemPrice],_orderSummary.redeemPrice,_orderSummary.packageType,_orderSummary.productQuantity,_orderSummary.productWeight,_orderSummary.productCost ,images];
    
    MBProgressHUD* progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText=@"Uploading details...";
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          _confirmButton.enabled = true;
                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                          _confirmButton.enabled = true;

                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]){
                                                  
                                                  [LSUtils deleteMyImagesDirectory];
                                                  CompletOrderViewController* vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                                                                                    @"CompletOrderViewController"];
                                                  [self.navigationController pushViewController:vc animated:true];
                                                  
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              
                                          }else{
                                              UIViewController*  controller;
                                              
                                              if(error!= nil){
                                                  controller  =   [sharedUtils showAlert:@"Failed" withMessage:error.localizedDescription];
                                              }else
                                              controller  =   [sharedUtils showAlert:@"Failed" withMessage:@"Server error, please try again."];
                                              
                                              [self presentViewController:controller animated:YES completion:nil];
                                          }
                                      });
                                  }];
    [task resume];
    
}
/*
-(NSArray *)imagesCountOfProduct
{
    // Get the Document directory path
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    // Create a new path for the new images folder
    NSString *documentsDirectory = [documentsPath stringByAppendingPathComponent:directoryImage];
    
    int count;
    _imageArray  = [[NSMutableArray alloc]init];
    
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
        
        if ([[[directoryContent objectAtIndex:count] pathExtension] isEqualToString:@"png"]) {
            //This is Image File with .png Extension
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[directoryContent objectAtIndex:count]];
            [_imageArray addObject: filePath];
        }
    }
    
    return _imageArray;
}
*/

-(NSString*)convertImageToBase:(NSString*)imageStr{
    
    UIImage *img = [UIImage imageWithContentsOfFile:imageStr] ;
    
    NSString *imgURL =  [UIImagePNGRepresentation(img) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return imgURL;
}

@end

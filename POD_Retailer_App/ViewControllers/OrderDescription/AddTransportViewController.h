//
//  AddTransportViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 05/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModeSelectionView.h"
#import "OrderDetailsModel.h"
@interface AddTransportViewController : UIViewController <UITextFieldDelegate , modeSelectionDelegate , UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *ModeOfTransportLabel;
@property (weak, nonatomic) IBOutlet UIView *modeOfTransportView;
@property (weak, nonatomic) IBOutlet UITextField *modeOfTransportTextField;
@property (weak, nonatomic) IBOutlet UIImageView *modeOftransportimageView;
@property (weak, nonatomic) IBOutlet UILabel *selectDeliveryBoylabel;
@property (weak, nonatomic) IBOutlet UIView *deliveryBoyView;
@property (weak, nonatomic) IBOutlet UITextField *deliveryBoyTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeOfTransportLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *DeliveryBoyViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeOfTransportViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;
@property (weak, nonatomic) IBOutlet UIImageView *deliveryBoyImageView;

@property (weak, nonatomic) NSString* transportString;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedCollectionHeightConstrain;
@property (weak, nonatomic) IBOutlet UICollectionView *selectedCollectionView;
@property (retain, nonatomic) NSMutableArray *selectedValues;
@property (retain, nonatomic) OrderDetailsModel *confirmationDetails;
@property (retain, nonatomic)IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray* selectedDriversArray;




@end

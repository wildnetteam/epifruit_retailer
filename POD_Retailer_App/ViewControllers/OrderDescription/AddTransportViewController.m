//
//  AddTransportViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 05/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "AddTransportViewController.h"
#import "SelectedCell.h"
#import "TransportModel.h"
#import "ConfirmOrderViewController.h"
#import "NotificationManager.h"
#import "DeliveryBoyListingViewController.h"
#import "DelivetyBoyTableViewCell.h"
#import "DriverDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface AddTransportViewController ()
{
    NSMutableArray *availableTransportMode;
}
@end

@implementation AddTransportViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUIElement];
    _selectedValues = [[NSMutableArray alloc]init];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [sharedUtils placeholderSize:_modeOfTransportTextField :@"Select mode of transportation..."];
    [sharedUtils placeholderSize:_deliveryBoyTextField :@"Select delivery boy..."];
    
    if(_confirmationDetails.driverCopleteDetails.count > 0 )
    {
        [sharedUtils placeholderSize:_deliveryBoyTextField :@"Change..."];
    }
    [_tableView reloadData];
  
    if(_confirmationDetails.transport.count > 0){
        _selectedValues = [_confirmationDetails.transport mutableCopy];
        [_selectedCollectionView reloadData];
    }
    
    [self animateSelectedCollectionView];
    
    if(availableTransportMode.count == 0){
        
        if([LSUtils isNetworkConnected]) {
           
            [sharedUtils startAnimator:self];
            availableTransportMode = [[NSMutableArray alloc]init];

            [self getTransportMode:^(BOOL success, NSDictionary *result, NSError *error) {
                [sharedUtils stopAnimator:self];
                if (error != nil) {
                    // handel error part here
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                    [self presentViewController:controller animated:YES completion:nil];
                }else{
                    if(success){
                        
                        if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                            
                            NSMutableArray *arrayOfAddress;
                            arrayOfAddress = [result valueForKey:@"data"];
                            
                            for(int index =0 ; index<arrayOfAddress.count ; index++){
                                
                                NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                
                                //Set address Model
                                TransportModel *modeOfTransport = [[TransportModel alloc]initWithModeName:[arrayOfdata valueForKey:@"transportation_mode"] ModeID:[arrayOfdata valueForKey:@"id"]] ;
                                
                                [availableTransportMode addObject:modeOfTransport];
                            }
                            
                        }else if([[result objectForKey:@"code"]isEqualToString:@"210"]){
                            
                            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There are no transport mode available now."];
                            [self presentViewController:controller animated:YES completion:nil];
                        }
                    }
                    else{
                        UIViewController*  controller;
                        
                        if(result ==nil){
                            controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                        }else{
                            NSLog(@"%@",result);
                            controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                        }
                        [self presentViewController:controller animated:YES completion:nil];
                    }
                }
            }];
        }else{
            UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-
(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}
- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUIElement{
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;
    self.title = @"";
    
    self.selectedCollectionHeightConstrain.constant = 0;
   // self.modeOfTransportLabelHeightConstraint.constant = -10;

    //Add padding to text field
    [LSUtils addPaddingToTextField:_deliveryBoyTextField];
    [LSUtils addPaddingToTextField:_modeOfTransportTextField];
    _modeOfTransportViewHeightConstraint.constant = IS_IPHONE5?5:10;
    _DeliveryBoyViewHeightConstraint.constant = IS_IPHONE5?5:10;

    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 8;
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == _modeOfTransportTextField){
        
        
        if(availableTransportMode.count == 0 ){
            
            [sharedUtils startAnimator:self];
            availableTransportMode = [[NSMutableArray alloc]init];
            
            [self getTransportMode:^(BOOL success, NSDictionary *result, NSError *error) {
                [sharedUtils stopAnimator:self];
                if (error != nil) {
                    // handel error part here
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                    [self presentViewController:controller animated:YES completion:nil];
                }else{
                    if(success){
                        
                        if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                            
                            NSMutableArray *arrayOfAddress;
                            arrayOfAddress = [result valueForKey:@"data"];
                            
                            for(int index =0 ; index<arrayOfAddress.count ; index++){
                                
                                NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                
                                //Set address Model
                                TransportModel *modeOfTransport = [[TransportModel alloc]initWithModeName:[arrayOfdata valueForKey:@"transportation_mode"] ModeID:[arrayOfdata valueForKey:@"id"]] ;
                                
                                [availableTransportMode addObject:modeOfTransport];
                            }
                            
                            UIButton *dimView;
                            [dimView removeFromSuperview];
                            dimView = nil;
                            dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                            dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
                            ModeSelectionView* viewPop= [[ModeSelectionView alloc] init];
                            viewPop.selectionsDel = self;
                            viewPop.modeOfTransportArray = nil;
                            viewPop.modeOfTransportArray = [NSArray arrayWithArray:availableTransportMode];
                            viewPop.selectedIndexPathArray = [[NSMutableArray arrayWithArray:_selectedValues] mutableCopy];
                            viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
                            
                            [UIView animateWithDuration:0.4
                                                  delay:0.0
                                                options: UIViewAnimationOptionCurveEaseIn
                                             animations:^{
                                                 viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                                                 
                                             }
                                             completion:^(BOOL finished){
                                             }];
                            [UIView commitAnimations];
                            viewPop.backgroundColor=[UIColor whiteColor];
                            [dimView addSubview:viewPop];
                            [self.view addSubview:dimView];
                            [self. view endEditing:true];
                            
                        }else if([[result objectForKey:@"code"]isEqualToString:@"210"]){
                            
                            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There are no transport mode available now. Please try later"];
                            [self presentViewController:controller animated:YES completion:nil];
                        }
                    }
                    else{
                        UIViewController*  controller;
                        
                        if(result ==nil){
                            controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                        }else{
                            NSLog(@"%@",result);
                            controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                        }
                        [self presentViewController:controller animated:YES completion:nil];
                    }
                }
            }];
        }
        else{
            
            UIButton *dimView;
            [dimView removeFromSuperview];
            dimView = nil;
            dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
            ModeSelectionView* viewPop= [[ModeSelectionView alloc] init];
            viewPop.selectionsDel = self;
            viewPop.modeOfTransportArray = nil;
            viewPop.modeOfTransportArray = [NSArray arrayWithArray:availableTransportMode];
            viewPop.selectedIndexPathArray = [[NSMutableArray arrayWithArray:_selectedValues] mutableCopy];
            [dimView addSubview:viewPop];
            [self.view addSubview:dimView];
            viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            
            [UIView animateWithDuration:0.4
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                                 
                             }
                             completion:^(BOOL finished){
                             }];
            [UIView commitAnimations];
            viewPop.backgroundColor=[UIColor whiteColor];
            [self. view endEditing:true];
        }
        return false;
    }
    if(textField ==_deliveryBoyTextField){
        
        if(_selectedValues.count == 0){
            
          UIViewController*  controller =  [sharedUtils showAlert:@"" withMessage:@"Please select atleast one mode of transport."];
        [self presentViewController:controller animated:YES completion:nil];
        }
        else{
            
        DeliveryBoyListingViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DeliveryBoyListingViewController"];
        vc.modeIds = _selectedValues;
        vc.orderSummary = _confirmationDetails;
            
            CATransition* transition = [CATransition animation];
            transition.duration = 0.6;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
            transition.type=kCATransitionPush;
            transition.subtype=kCATransitionFromTop;
            [self.navigationController.view.layer addAnimation:transition forKey:nil];
            [self.navigationController pushViewController:vc animated:false ];
            [UIView commitAnimations];
            
        }
            return false;
    }
    return true;
}

//-(void)cancelKeyboard {
//    
//    [[self view] endEditing:true];
//}

#pragma mark - Delegate to show seleected modeof transport

-(void)saveTransportMode:(NSMutableArray *)selectedNewValues{
    
    //   Make a local copy of the array, because an array cannot be mutated as it is enumerated
    @try {
        _confirmationDetails.transport = selectedNewValues;
        _selectedValues = selectedNewValues;
        [self.selectedCollectionView reloadData];
        [self animateSelectedCollectionView];
    }
     @catch (NSException *exception) {
         NSLog(@"%@",exception.description);
        ;
    } @finally {
        ;
    }
}
#pragma mark - Animations

-(void)animateSelectedCollectionView{
    if (_selectedValues.count > 0) {
        
        self.selectedCollectionHeightConstrain.constant = 55;
      //  self.modeOfTransportLabelHeightConstraint.constant = -10;
        
    }else{
        self.selectedCollectionHeightConstrain.constant = 0;
      //  self.modeOfTransportLabelHeightConstraint.constant = 0;

    }
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        if(_selectedValues.count > 0){
            [sharedUtils placeholderSize:_modeOfTransportTextField :@"Change..."];
        }else{
            [sharedUtils placeholderSize:_modeOfTransportTextField :@"Select mode of transportation..."];
        }
    }];
}

#pragma mark - CellButton Action
-(IBAction)cellRemoveButtonAction:(id)sender{

    UIButton *button = (UIButton *)sender;
    NSInteger index = button.tag-10000;
    [self.selectedCollectionView performBatchUpdates:^{
        [_selectedValues removeObjectAtIndex:index];
        [self.selectedCollectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:index inSection:0]]];
    } completion:^(BOOL finished) {
        [self.selectedCollectionView reloadData];
        [self animateSelectedCollectionView];
    }];
    
    _confirmationDetails.transport = _selectedValues;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  _selectedValues.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TransportModel* object = [_selectedValues objectAtIndex:indexPath.row];
    
    SelectedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SelectedCell" forIndexPath:indexPath];
    
    cell.devName.text = [object.tMode capitalizedString];
    [cell.devName sizeToFit];
    
    cell.removeButton.tag = 10000+indexPath.row;
    [cell.removeButton addTarget:self action:@selector(cellRemoveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    TransportModel* object = [_selectedValues objectAtIndex:indexPath.row];
    
    CGSize calCulateSizze =[object.tMode sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:18.0]}];
    calCulateSizze.width = calCulateSizze.width+ 40;
    calCulateSizze.height = calCulateSizze.height + 20;
    return calCulateSizze;

}


#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _confirmationDetails.driverCopleteDetails.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DelivetyBoyTableViewCell *cell = (DelivetyBoyTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];

    DeliveryBoyModel *object = [_confirmationDetails.driverCopleteDetails objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[DelivetyBoyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:@"CellIdentifier"];
    }
    cell.selectionButton.hidden = true;
    if(IS_IPHONE_6P)
        cell.cellShadowSeparartor.constant = -5;
    [cell setDeliverBoyDetails:object];
    [cell.driverProfileImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", object.dProfileURL, object.imageName]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.driverProfileImage.layer.cornerRadius = 10;
    cell.driverProfileImage.layer.masksToBounds = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DeliveryBoyModel *object = [_confirmationDetails.driverCopleteDetails objectAtIndex:indexPath.row];

    DriverDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DriverDetailViewController"];
    vc.driverDetail = object;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}


#pragma mark - Get Added Card Detail
-(void)getTransportMode:(void (^) (BOOL success, NSDictionary* result, NSError* error))completion {
    
        NSString *params = [NSString stringWithFormat:@"%@",GetModeOfTransport];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:30.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  
                                                            completion(true, responseDictionary ,nil);

                                                  }else
                                                  completion(false,nil,error);
                                              
                                          });
                                      }];
        [task resume];
}

-(BOOL)ValidateTextField{
    
    if(_selectedValues.count == 0){
         [[NotificationManager notificationManager] displayMessage:@"Please select mode of transportation." withType:Negative];
        return  false;
    }
    return true;
}

-(NSArray*)createDriverBoys{
    
    _selectedDriversArray = [[NSMutableArray alloc]init];
    
    for(DeliveryBoyModel *object in _confirmationDetails.driverCopleteDetails){
        
        NSDictionary *driverDict = [NSDictionary
                                    dictionaryWithObjectsAndKeys:object.dId,@"deliveryboy_id", [object.dModeSelcted valueForKey:@"transport_id"],@"deliveryboy_mode",@"10", @"mode_charge",nil];
        [_selectedDriversArray addObject:driverDict];
    }
    return _selectedDriversArray;
}

- (IBAction)proceedButtonClicked:(id)sender {
 
    if([self ValidateTextField]){

        ConfirmOrderViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"ConfirmOrderViewController"];
        vc.modeOftransportArray = [NSArray arrayWithArray:_selectedValues];
        _confirmationDetails.transport = _selectedValues;
        vc.orderSummary = _confirmationDetails;
        vc.orderSummary.transport = _selectedValues;
        vc.orderSummary.Drivers =[self createDriverBoys];
        [self.navigationController pushViewController:vc animated:YES];
    
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return true;
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:true];
}

@end

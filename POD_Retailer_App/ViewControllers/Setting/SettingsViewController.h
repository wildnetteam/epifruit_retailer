//
//  SettingsViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 25/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+Scale.h"

@interface SettingsViewController : UIViewController <UIImagePickerControllerDelegate , UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *changeButtonView;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSwitch;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *NameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *profileImagaeView;
@property (weak, nonatomic) IBOutlet UIImageView *nameImageView;
@property (weak, nonatomic) IBOutlet UIView *nameBackView;
@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UIView *emailBackView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImageView;
@property (weak, nonatomic) IBOutlet UIView *phoneBackView;
@property (weak, nonatomic) IBOutlet UIButton *profileImageButton;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImageView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *notificationView;

@property (weak, nonatomic) IBOutlet UIView *changepasswordView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notificationViewHeightConstraint;

@end

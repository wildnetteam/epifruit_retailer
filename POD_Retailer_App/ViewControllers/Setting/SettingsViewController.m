//
//  SettingsViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 25/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "SettingsViewController.h"
#import "ChangePasswordViewController.h"
#import "NotificationManager.h"
#import "OTPViewController.h"
@interface SettingsViewController ()
{
   BOOL ismobileNumberChanged;
    BOOL isviewAppearingThroughImagePicker;
    NSString *newUrlOfImage;
}
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [self setNeedsStatusBarAppearanceUpdate];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    if(!isviewAppearingThroughImagePicker){
        
        [self getUserCredentials];
        [self disableTextField];
    }
    isviewAppearingThroughImagePicker= false;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)getUserCredentials{
    
    _NameTextField.text = [UserManager getUserName] ;
    _emailTextField.text = [UserManager getEmail];
    _phoneTextField.text = [self formatNumber:[UserManager getUserPhoneNumber]];
    NSLog(@"getUserReferenceCode: %@", [UserManager getUserReferenceCode]);
    _profileImagaeView.tag = 0000; // when no profile image
    if([newUrlOfImage isEqualToString:[UserManager getUserImageURL]] || [LSUtils checkNilandEmptyString:newUrlOfImage]){
       
        //don't download
    }else{
        if([LSUtils checkNilandEmptyString:[UserManager getUserImageURL]]){
            
            [_spinner startAnimating];
            _profileImagaeView.tag = 1111;
            _profileImageButton.enabled = false;
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //Background Thread
                UIImage *image =  [self downloadProfileImage:[UserManager getUserImageURL]];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    NSLog(@"Image retrieved") ;
                    if(image!= nil)
                        _profileImagaeView.image = image;
                    [_spinner stopAnimating];
                    _profileImageButton.enabled = true;
                });
            });
        }
    }
}

-(void)enableTextField{
    
    _NameTextField.userInteractionEnabled =true;
    _phoneTextField.userInteractionEnabled = true;
    _profileImageButton.userInteractionEnabled = true;
    _notificationSwitch.userInteractionEnabled = true;
    _notificationView.hidden = true;
    _saveButton.hidden = false;
    _changepasswordView.hidden = true;
    _separatorImageView.hidden = true;
    //To Hide
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItems = nil;
    
    [_NameTextField becomeFirstResponder];
    //set cross grey for editable field
    [ self.nameImageView setImage:[UIImage imageNamed:@"cross_grey"]];
    _nameImageView.contentMode = UIViewContentModeCenter;
    [ self.phoneImageView setImage:[UIImage imageNamed:@"cross_grey"]];
    _phoneImageView.contentMode = UIViewContentModeCenter;
 
}

-(void)disableTextField{
    
    _NameTextField.userInteractionEnabled = false;
    _phoneTextField.userInteractionEnabled = false;
    _emailTextField.userInteractionEnabled =false;
    _profileImageButton.userInteractionEnabled = false;
    _notificationView.hidden = false;
    _changepasswordView.hidden = false;
    _separatorImageView.hidden = false;
    _saveButton.hidden = true;
    //To Show
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 5;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"editprofile"] style:UIBarButtonItemStylePlain target:self action:@selector(editProfile:)] , nil]];}

#pragma mark - TextFieldDelegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==_NameTextField) {
        
        UIButton *nameClearButton = [_NameTextField valueForKey:@"_clearButton"];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.NameTextField.text length]>0) {
            self.nameImageView.hidden=true;
            _nameBackView.hidden = true;
        }
    }
    
    if (textField==_emailTextField) {
        UIButton *emailClearButton = [_emailTextField valueForKey:@"_clearButton"];
        [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.emailTextField.text length]>0) {
            self.emailImageView.hidden=true;
            _emailBackView.hidden = true;
        }
    }
    
    if (textField==_phoneTextField) {
        
        UIButton *phoneClearButton = [_phoneTextField valueForKey:@"_clearButton"];
        [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.phoneTextField.text length]>0) {
            self.phoneImageView.hidden=true;
            _phoneBackView.hidden = true;
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.nameImageView.hidden=false;
    _nameBackView.hidden = false;
    self.emailImageView.hidden=false;
    _emailBackView.hidden = false;
    self.phoneImageView.hidden=false;
    _phoneBackView.hidden = false;

    [ self.nameImageView setImage:[UIImage imageNamed:@"name"]];
    [ self.phoneImageView setImage:[UIImage imageNamed:@"phone"]];
    
    if (textField==_NameTextField)
    {
        if(!([_NameTextField.text length]>0))
        {
            [sharedUtils placeholderSize:_NameTextField :@"Enter your full name..."];
        }
    }
    if (textField==_phoneTextField)
    {
        if(!([_phoneTextField.text length]>0))
        {
            [sharedUtils placeholderSize:_phoneTextField :@"Enter your number..."];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
      // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    if(textField == self.NameTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.nameImageView.hidden = true;
            self.nameBackView.hidden = true;
        }
        
        if (([self.NameTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.nameImageView.hidden = false;
            self.nameBackView.hidden = false;
            return false;
        }
        
        if(_NameTextField.text.length > 28 ){
            
            
            [[NotificationManager notificationManager] displayMessage:@"Name must be of length 2 to 28." withType:Negative];
            
            return false;
        }
        return true;
    }
    
    if(textField == self.emailTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.emailImageView.hidden = true;
            self.emailBackView.hidden = true;
        }
        
        if (([self.emailTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.emailImageView.hidden = false;
            self.emailBackView.hidden = false;
        }
        
    }
    
    if(textField == _phoneTextField ) {
        
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.phoneImageView.hidden = true;
            self.phoneBackView.hidden =true;
        }
        
        if (([self.phoneTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.phoneImageView.hidden = false;
            self.phoneBackView.hidden = false;

        }
        
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }
        
        
        int length = (int)[self getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [self formatNumber:textField.text];
            //NSLog(@"%@",[num  substringToIndex:3]);
            //NSLog(@"%@",[num substringFromIndex:3]);
            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
    }
    return YES;
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField == _NameTextField){
        [_phoneTextField becomeFirstResponder];
    }
    if(textField == _phoneTextField){
        [_notificationSwitch becomeFirstResponder];
        [_phoneTextField resignFirstResponder];
    }
    
    return true;
}
-(UIImage*)downloadProfileImage:(NSString*)picUrlString {
    
    NSLog(@"profile Image URL: %@", picUrlString);
   UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:picUrlString]]];
    return image;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:true];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}
-(void)setUpUIElements{
    
    self.title = @"";

    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 5;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"editprofile"] style:UIBarButtonItemStylePlain target:self action:@selector(editProfile:)] , nil]];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //Add padding to text field
    [LSUtils addPaddingToTextField:_NameTextField];
    [LSUtils addPaddingToTextField:_emailTextField];
    [LSUtils addPaddingToTextField:_phoneTextField];
    
    [sharedUtils placeholderSize:_NameTextField :@"Enter your full name..."];
    [sharedUtils placeholderSize:_phoneTextField :@"Enter your number..."];
    
    UITapGestureRecognizer  *changePasswordTappedGesture   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changePasswordTappedGesture:)];
    _changeButtonView.userInteractionEnabled = YES;
    [_changeButtonView addGestureRecognizer:changePasswordTappedGesture];
    [self setprofileImagefarme];
}

-(void)setprofileImagefarme{
    
    self.profileImagaeView.layer.cornerRadius = self.profileImagaeView.frame.size.width /2.0;
    self.profileImagaeView.layer.masksToBounds = YES;
    _profileImagaeView.clipsToBounds = YES;
}

- (void)changePasswordTappedGesture:(UITapGestureRecognizer *)gestureRecognizer {
    
    [[self view] endEditing:true];
    ChangePasswordViewController *forgotPasswordVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"ChangePasswordViewController"];
    [self.navigationController pushViewController:forgotPasswordVC animated:YES ];
}

-(void)menuButtonClicked{

    [self disableTextField];
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}


-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)editProfile:(id)sender{
    
    [self enableTextField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchOnOrOffNotifications:(id)sender {
    
    UISwitch *switchBtn = (UISwitch*)sender;
    [self updateNotificationStatus:[NSString stringWithFormat:@"%d",switchBtn.on]];
}

-(void)updateNotificationStatus:(NSString*)value{
    
    NSString* params = [NSString stringWithFormat:@"%@&vendor_id=%@&notification_status=%@",UpdateProfile,[UserManager getUserID],value];
    
    if([LSUtils isNetworkConnected])
    {
        _notificationSwitch.userInteractionEnabled = NO;
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              _notificationSwitch.userInteractionEnabled = true;
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                     options:0 error:NULL];
                                                  
#ifdef DEBUG
                                                  NSLog(@"Response:%@",responseDictionary);
#endif
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Notification status changed."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];

                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]) {
                                                      
                                                      NSLog(@"could not be updated");
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"218"]) {
                                                      
                                                    UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Mobile number already registered."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }

}
- (IBAction)updateProfilePic:(id)sender {
    
    [self openImageOption];
}

//Give user a option if he wants photo from gallery or take fro camera
-(void)openImageOption {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //open another viewcontroller
                                                       [self goToCameraView];
                                                       
                                                   }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self getPhotoLibrary];
                                                      }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert.popoverPresentationController setPermittedArrowDirections:0];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(sharedUtils.screenWidth / 2.0 + 20, sharedUtils.screenHeight / 2.0 , 1.0, 1.0);
    
    [self presentViewController:alert animated:YES completion:nil];
}

//take a snap from cemera
//go to another viewcontroller for clicking photo
-(void)goToCameraView {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIViewController *controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Device has no camera"];
        [self presentViewController:controller animated:NO completion:nil];
        
        
    } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = (id)self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}

//choose photo from gallery
-(void)getPhotoLibrary {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.allowsEditing = NO;
    imagePicker.delegate = self;
    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage ];
    imagePicker.navigationBar.translucent = false;
        [self presentViewController:imagePicker animated:YES completion:nil];;
}

#pragma mark UIImpagePicker Delegate

//this delegate is called when photo is selected from gallery
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ( [mediaType isEqualToString:@"public.image" ]) {
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [self setprofileImagefarme];
        isviewAppearingThroughImagePicker= true;
        _profileImagaeView.image = nil;
        UIImage *resizedImage =  [image scaleToSize:ProfileImageResizeScale];
        _profileImagaeView.image = [resizedImage fixOrientation];
        _profileImagaeView.tag = 1111;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)updateAccountSetting:(id)sender {
    
    if([self validatetextField]){
        
        NSString *params;
        NSString *profileBase64 = _profileImagaeView.tag == 0000?@"":[UIImagePNGRepresentation(_profileImagaeView.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        ismobileNumberChanged = false;
        
        if([[UserManager getUserPhoneNumber] isEqualToString:[self getPhoneNumber:_phoneTextField.text]]){
            
            params = [NSString stringWithFormat:@"%@&vendor_id=%@&fname=%@&vendor_mobile_no=""&vendor_image=%@",UpdateProfile,[UserManager getUserID],_NameTextField.text,profileBase64];
            [self updateSetting:params];
            
            
        }else{
            ismobileNumberChanged = true;
            
            
            params = [NSString stringWithFormat:@"%@&vendor_id=%@&fname=%@&vendor_mobile_no=%@&vendor_image=%@",UpdateProfile,[UserManager getUserID],_NameTextField.text,[self getPhoneNumber:_phoneTextField.text],profileBase64];
            
            params = [params stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"];

            NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                                   initWithString:[NSString stringWithFormat:@"Is this correct phone number?\n%@\n\nWe will send you a verification code by SMS",_phoneTextField.text]
                                                   attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@""
                                                  message:@""
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                           style:UIAlertActionStyleCancel
                                           handler:^(UIAlertAction *action)
                                           {
                                               
                                           }];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           // update data sourc
                                           [self updateSetting:params];
                                       }];
            
            [alertController setValue:alertmessage forKey:@"attributedTitle"];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self  presentViewController:alertController animated:NO completion:Nil];
            
        }
    }
}
-(BOOL)validatetextField{
    
    if(![LSUtils checkNilandEmptyString:_NameTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Name can not be blank." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_phoneTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Phone Number can not be blank." withType:Negative];
        return false;
        
    }
    
    if([self getLength:_phoneTextField.text]<10){
        [[NotificationManager notificationManager] displayMessage:@"Phone Number length can not be less than 10." withType:Negative];
        return false;
        
    }
    
    if(_NameTextField.text.length<2 || _NameTextField.text.length >28){
        
        [[NotificationManager notificationManager] displayMessage:@"Name  must be range in 2 to 28." withType:Negative];
        return false;
    }
       
    return true;
}

- (NSString*)getPhoneNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSString *mobileNumberWithCountryCode = [@"+01" stringByAppendingString:mobileNumber];
    return mobileNumberWithCountryCode;
}

-(void)updateSetting:(NSString*)params{
    
    if([LSUtils isNetworkConnected])
    {
        self.view.userInteractionEnabled = NO;
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              self.view.userInteractionEnabled = YES;
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                     options:0 error:NULL];
                                                  
#ifdef DEBUG
                                                  NSLog(@"Response:%@",responseDictionary);
#endif
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSMutableArray *arrayOfdata ;
                                                      
                                                      arrayOfdata = [responseDictionary valueForKey:@"data"];
                                                      
                                                      [UserManager saveUserCredentials:[[arrayOfdata objectAtIndex:0]valueForKey:@"fname"] withvar:k_UserName];
                                                      
                                                      
                                                      //compare newUrlOfImage with k_UserImageURL to prevent it loading unneccesarly every time
                                                      newUrlOfImage = [NSString stringWithFormat:@"%@/%@",[responseDictionary valueForKey:@"userimgurl"],[[arrayOfdata objectAtIndex:0] valueForKey:@"vendor_image"]];
                                                      
                                                      [UserManager saveUserCredentials:[NSString stringWithFormat:@"%@/%@",[responseDictionary valueForKey:@"userimgurl"],[[arrayOfdata objectAtIndex:0] valueForKey:@"vendor_image"]] withvar:k_UserImageURL];
                                                      
                                                      if([[[arrayOfdata objectAtIndex:0]valueForKey:@"mobileno_edit"] intValue] == 1){
                                                          
                                                          OTPViewController *otpVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"OTPViewController"];
                                                          otpVC.updatedPhonenumber = _phoneTextField.text;
                                                          otpVC.otp = [NSString stringWithFormat:@"%@",[[arrayOfdata objectAtIndex:0] valueForKey:@"otp_code"]];
                                                          [self.navigationController pushViewController:otpVC animated:YES ];
                                                      }
                                                      else{
                                                          
                                                          UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Profile updated successfully."];
                                                          
                                                          [self disableTextField];
                                                          
                                                          
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                  }
                                                  else{
                                                      
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}
@end

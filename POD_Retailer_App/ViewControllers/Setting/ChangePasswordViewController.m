//
//  ChangePasswordViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 25/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "NotificationManager.h"
@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:true];
}

-(void)setUpUIElements{
    
    self.title = @"";
    
  [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    
    //Add padding to text field
    [LSUtils addPaddingToTextField:_oldePasswordText];
    [LSUtils addPaddingToTextField:_PasswordText];
    [LSUtils addPaddingToTextField:_confirmPasswordText];
    
    [sharedUtils placeholderSize:_oldePasswordText :@"Enter old password..."];
    [sharedUtils placeholderSize:_PasswordText:@"Enter new password..."];
    [sharedUtils placeholderSize:_confirmPasswordText:@"Confirm new password..."];
    

}
#pragma mark - TextFieldDelegates


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==_oldePasswordText) {
        
        UIButton *nameClearButton = [_oldePasswordText valueForKey:@"_clearButton"];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
    }
    
    if (textField== _PasswordText) {
        UIButton *emailClearButton = [_PasswordText valueForKey:@"_clearButton"];
        [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
    }
    
    if (textField==_confirmPasswordText) {
        
        UIButton *phoneClearButton = [_confirmPasswordText valueForKey:@"_clearButton"];
        [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        int height =IS_IPHONE5 ?_confirmPasswordText.frame.size.height + 10:_confirmPasswordText.frame.size.height + 20;
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - height, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField==_oldePasswordText)
    {
        if(!([_oldePasswordText.text length]>0))
        {
            [sharedUtils placeholderSize:_oldePasswordText :@"Enter old password..."];
        }
    }
    
    if (textField==_PasswordText)
    {
        if(!([_PasswordText.text length]>0))
        {
            [sharedUtils placeholderSize:_PasswordText:@"Enter new password..."];
            
        }
    }
    if (textField==_confirmPasswordText)
    {
        if(!([_confirmPasswordText.text length]>0))
        {
            [sharedUtils placeholderSize:_confirmPasswordText:@"Confirm new password..."];
            
        }
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        }];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    
    if(textField == self.PasswordText)
    {
        
        if(_PasswordText.text.length > 21 ){
            
            
            [[NotificationManager notificationManager] displayMessage:@"Password must be of length 6 to 21." withType:Negative];
            
            return false;
        }
    }
    return true;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField == _oldePasswordText){
        [_PasswordText becomeFirstResponder];
    }
    if(textField == _PasswordText){
        [_confirmPasswordText becomeFirstResponder];
    }
    if(textField == _confirmPasswordText){
        [textField resignFirstResponder];
    }
    return true;
}
// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(BOOL)validatetextField {

    if(![LSUtils checkNilandEmptyString:_oldePasswordText.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Old password can not be blank." withType:Negative];
        
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_PasswordText.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Password can not be blank." withType:Negative];
        
        return false;
    }
    if(_PasswordText.text.length<6 || _PasswordText.text.length >21){
        
        [[NotificationManager notificationManager] displayMessage:@"Password must be range in 6 to 21." withType:Negative];
        
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_confirmPasswordText.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Confirm Password can not be blank." withType:Negative];
        
        return false;
    }
    if(![_PasswordText.text isEqualToString:_confirmPasswordText.text ]){
        
        [[NotificationManager notificationManager] displayMessage:@"Password mismatch." withType:Negative];
        
        return false;
    }
    
    return true;
}

-(void)changePass{
    
     NSString* params = [NSString stringWithFormat:@"%@&vendor_id=%@&old_password=%@&new_password=%@",UpdatePassword,[UserManager getUserID],_oldePasswordText.text,_PasswordText.text];
    
    if([LSUtils isNetworkConnected])
    {
        self.view.userInteractionEnabled = NO;
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              self.view.userInteractionEnabled = YES;
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  
#ifdef DEBUG
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  
#endif
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
//                                                        [self.navigationController popViewControllerAnimated:NO];
                                                      
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Password changed successfully."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:^{
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                          }); 
                                                      }];
                                                     
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]) {
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"could not be updated password"];
                                                      [self presentViewController:controller animated:YES completion:nil];                                                  }
                                              }
                                              if(error!= nil){
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }

}
-(void)backButtonClicked:(id)sender{
    
    [[self view] endEditing:true];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)updatePassword:(id)sender {
    if([self validatetextField]){
    [self changePass];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

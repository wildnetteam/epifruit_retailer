//
//  BidOrderDetailCell.h
//  PODRetailerApp
//
//  Created by Arpana on 29/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "DeliveryBoyModel.h"
@protocol AcceptDelegate <NSObject>
@optional
-(void)acceptBid:(UIButton*)acceptBt;
-(void)makeDriverFavourit:(UIButton*)favBt;
@end

@interface BidOrderDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ASStarRatingView *rateView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UIButton *favrouiteButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIImageView *driverimageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellShadowSeparartor;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleAndDescDistance;

@property (nonatomic, assign) id <AcceptDelegate> cellDelagte;
-(void)setDeliverBoyDetails:(DeliveryBoyModel*)delBoyInstance ;
@end

//
//  UpdateBidAmountView.m
//  PODRetailerApp
//
//  Created by Arpana on 30/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "UpdateBidAmountView.h"

@implementation UpdateBidAmountView


- (void)awakeFromNib {
    [super awakeFromNib];
    [LSUtils addPaddingToTextField:_mintextfield];
    [LSUtils addPaddingToTextField:_maxtextfield];
    
}

-(void)setPreviousAmount {
    
    _mintextfield.text = [_minAmount isEqualToString:@""]?@"":[NSString stringWithFormat:@"%.02f",[_minAmount floatValue]];
    _maxtextfield.text = [_maxAmount isEqualToString:@""]?@"":[NSString stringWithFormat:@"%.02f",[_maxAmount floatValue]];
}
- (IBAction)yesButtonClicked:(id)sender {
    
    [self updateBidAmount];
}

- (IBAction)noButtonClicked:(id)sender {
    [self popDown];
    if (_buttonDelegate && [_buttonDelegate respondsToSelector:@selector(editAddressObject:)])
        [_buttonDelegate cancelUpdate];
}


#pragma mark - UITextField Delegate Methods

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_mintextfield)
    {
        if(!([_mintextfield.text length]>0))
        {
            [sharedUtils placeholderSize:_mintextfield :@"Enter minimum amount..."];
        }
    }
    if (textField==_maxtextfield)
    {
        if(!([_maxtextfield.text length]>0))
        {
            [sharedUtils placeholderSize:_maxtextfield :@"Enter maximum amount..."];
        }
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _mintextfield){
        [_maxtextfield becomeFirstResponder];
    }
    if(textField == _maxtextfield){
        [_maxtextfield resignFirstResponder];
    }
    return true;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    NSRange spaceRange = [string rangeOfString:@" "];
    
    if([textField.text length] == 0){
        if (spaceRange.location != NSNotFound)
        {
            return NO;
        }
    }
    
    NSCharacterSet *validCharSet;
    if (range.location == 0)
        validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    else
        validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    
    if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
        
        return false;  //not allowable char
    }
    
    return  true;
}

// Called by Cancel
- (void) popDown{
    
    [self endEditing:true];
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    
    [UIView animateWithDuration:.5   delay:0.0
                        options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         
                         self.frame = CGRectMake(0, 0, 0, 0);
                         self.superview.backgroundColor = [UIColor clearColor];
                         
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             [self.superview removeFromSuperview];
                         }
                     }];
}

-(void)updateBidAmount {
    
    UIViewController *currentTopVC = [self currentTopViewController];
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&vendor_id=%@&min_price=%@&max_price=%@",vendorUpdatePrice,_orderId, [UserManager getUserID] ,_mintextfield.text,_maxtextfield.text ];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimatorForCustomview:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimatorForCustomview:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [currentTopVC presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"Sucess" withMessage:@"Updated successfully."];
                                                  _minAmount = [responseDictionary objectForKey:@"min_price"];
                                                  _maxAmount = [responseDictionary objectForKey:@"max_price"];
                                                  
                                                  [self setPreviousAmount];
                                                  
                                                  if (_buttonDelegate && [_buttonDelegate respondsToSelector:@selector(updateAmount::)])
                                                      [_buttonDelegate updateAmount:_minAmount :_maxAmount];

                                                     [currentTopVC presentViewController:controller animated:YES completion:^{
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self popDown];
                                                      });
                                                     }];
                                              }
                                              else {
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                           
                                                  [currentTopVC presentViewController:controller animated:YES completion:nil];
                                              }
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                               
                                                  [currentTopVC presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  
                                                  [currentTopVC presentViewController:controller animated:YES completion:nil];
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

- (UIViewController *)currentTopViewController {
    
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}
@end

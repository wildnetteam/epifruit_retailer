//
//  MyBidsViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 26/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyBidsViewController : UIViewController <UITableViewDelegate , UITableViewDataSource  >
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

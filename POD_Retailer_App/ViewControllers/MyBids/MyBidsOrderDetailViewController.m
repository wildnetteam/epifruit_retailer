//
//  MyBidsOrderDetailViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 29/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "MyBidsOrderDetailViewController.h"
#import "LSUtils.h"
#import "OrderHistoryModel.h"
#import "DeliveryBoyModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AddressViewController.h"
#import "UIView+XIB.h"
#import "OrderHistoryViewController.h"
#import "DriverDetailViewController.h"

@interface MyBidsOrderDetailViewController (){
    NSMutableArray *imagesArray;
    NSMutableArray *deliveryBoyArray;
    int center;
    OrderHistoryModel *orderDetail;
    UIRefreshControl *btmefreshControl;
}

@end

@implementation MyBidsOrderDetailViewController {

    __weak IBOutlet NSLayoutConstraint *progressTrailingConstraint;
    
    __weak IBOutlet NSLayoutConstraint *imageScrollViewLeadingConstraint;
    
    __weak IBOutlet NSLayoutConstraint *imageScrollViewHeightConstraint;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    orderDetail = [[OrderHistoryModel alloc]init];
    [self setUIElements];
    [self getOrderDetails];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    progressTrailingConstraint.constant = self.view.frame.size.width;
    [_tableView addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    self.navigationController.navigationItem.leftBarButtonItem.tintColor =[UIColor whiteColor];
    self.navigationController.navigationBar.translucent = YES;
  //  _pageControl.hidden = true;

    //Set default Image on scrollView
    UIImageView* imgView = [[UIImageView alloc] init];
    CGRect frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size = CGSizeMake(sharedUtils.screenWidth, _imageScrollView.frame.size.height + 64);
    imgView.frame = frame;
    imgView.alpha = .6;
    [imgView setBackgroundColor:[UIColor whiteColor]];
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    [_imageScrollView addSubview:imgView];
    imgView.image = [UIImage imageNamed:@"noimage"];
    
}

-(void)setTextFields {

    if(btmefreshControl.isRefreshing  == true){
        [btmefreshControl endRefreshing ];
    }
    
    _pickUpAddresslabel.text = orderDetail.pickAdress;
    _deliveryAddressLabel.text = orderDetail.delAdress;
    _productDescriptionLabel.text = orderDetail.productDescription;
    _productTitleLabel.text = orderDetail.productTitle;
    _priceLabel.text = [NSString stringWithFormat:@"%@$",orderDetail.price];
    _pickUpTimeLabel.text= [self getFormatedDate:orderDetail.productdateAndTime];
    _orderNoLabel.text = [NSString stringWithFormat:@"Order no. %@", orderDetail.productOrderNo];
    _minPriceLabel.text = [orderDetail.minRate isEqualToString:@""]?@"NA":[NSString stringWithFormat:@"$%@",orderDetail.minRate];
    _maxPriceLabel.text = [orderDetail.maxrate isEqualToString:@""]?@"NA": [NSString stringWithFormat:@"$%@",orderDetail.maxrate];
    _proposalCountLabel.text = orderDetail.proposalCount;

}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"placed on%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [_tableView removeObserver:self forKeyPath:@"contentSize"];
    [super viewWillDisappear:animated];
    
}

//-(void)viewWillLayoutSubviews{
//    [super updateViewConstraints];
//    CGFloat height = MIN(self.view.bounds.size.height, self.tableView.contentSize.height);
//
//    self.tableViewHeightConstant.constant = height;//self.tableView.contentSize.height;
//    [self.view layoutIfNeeded];
//}

-(void)GetProductImage {
    
    for (int index =0 ; index<orderDetail.productImagesArray.count; index++) {
        [self loadImage:[NSString stringWithFormat:@"%@/%@", orderDetail.imagebaseURL, [orderDetail.productImagesArray objectAtIndex:index]]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    orderDetail =nil;
    _arrayOFImages = nil;
}

-(void)setUIElements{
    
    self.title = @"";
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    _imageScrollView.pagingEnabled = true;
    
    UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(0, 0,80, 50)];
    [cancelButton addTarget:self action:@selector(cancelOrderr:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
        
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    self.view.backgroundColor = [UIColor colorWithRed:244.0f/254 green:244.0f/254 blue:244.0f/254 alpha:1];
    
    //Adding refresh controoler on op to referesh
    btmefreshControl = [[UIRefreshControl alloc]init];
    [self.scrollView insertSubview:btmefreshControl atIndex:0];
    self.scrollView.alwaysBounceVertical = true;
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
}

-(void)refresh:(id)sender{
    if([LSUtils isNetworkConnected]){
        [self getOrderDetails];
    }
    else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        } ];
    }
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)cancelOrderr:(id)sender{
    [self cancelOrderByID];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat x = _imageScrollView.contentOffset.x;
    float width  = _imageScrollView.frame.size.width;
    float value =x/width;
    self.pageControl.currentPage = round(value);
    
    [self.view layoutIfNeeded];
}

- (IBAction)changePage:(UIPageControl*)pageControll{
    [_imageScrollView setDelegate:nil];
    [_imageScrollView setContentOffset:CGPointMake(_imageScrollView.frame.size.width * self.pageControl.currentPage, 0) animated:YES];
    [_imageScrollView setDelegate:(id)self];
}

-(void)animateScrollView
{
    int x = _imageScrollView.contentOffset.x;
    int max = _imageScrollView.contentSize.width;
    if (max>(x+self.view.frame.size.width)) {
        [UIView animateWithDuration:0.7 animations:^{
            _imageScrollView.contentOffset = CGPointMake(x+self.view.frame.size.width,_imageScrollView.contentOffset.y);
        }];
    } else {
        _imageScrollView.contentOffset = CGPointMake(0,_imageScrollView.contentOffset.y);
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma  setUI for image on scroll View

-(void)setImages:(NSMutableArray*)imgArray{

    _arrayOFImages = [imgArray copy];
    _imageScrollView.contentSize = CGSizeMake(_imageScrollView.frame.size.width * imgArray.count, 0);
    //_pageControl.hidden = false;
    _pageControl.numberOfPages = [imgArray count];
    for(UIImageView *imgView in _imageScrollView.subviews){
        [imgView removeFromSuperview];
    }
    
    for (int i = 0; i < imgArray.count; i++) {
        CGRect frame;
        frame.origin.x = _imageScrollView.frame.size.width*i;
        frame.origin.y = 0;
        frame.size = CGSizeMake(_imageScrollView.frame.size.width, _imageScrollView.frame.size.height + 64);
        UIImageView* imgView = [[UIImageView alloc] init];
        imgView.frame = frame;
       // UIImage *newimage=  [self imageByScalingToSize:frame.size forImage: [imgArray objectAtIndex:i]];
        imgView.image = [imgArray objectAtIndex:i];
        imgView.backgroundColor = [UIColor whiteColor];
        [_imageScrollView addSubview:imgView];
    }
}

//to resize image
- (UIImage *) imageByScalingToSize:(CGSize)targetSize forImage:(UIImage *)sourceImage
{
    UIImage *generatedImage = nil;
    UIGraphicsBeginImageContextWithOptions(targetSize,NO,1.0);
    
    [sourceImage drawInRect:CGRectMake(0, 0,targetSize.width,targetSize.height-64)];
    generatedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return generatedImage;
}

- (UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    float ratio = newSize.width/image.size.width;
    [image drawInRect:CGRectMake(0, 0, newSize.width, ratio * image.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    NSLog(@"New Image Size : (%f, %f)", newImage.size.width, newImage.size.height);
    UIGraphicsEndImageContext();
    return newImage;
}
-(void) loadImage:(NSString *)imageLink{
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    [manager loadImageWithURL:[NSURL URLWithString:imageLink] options:SDWebImageDelayPlaceholder progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        [imagesArray addObject:image];
        [self setImages:imagesArray];
        
    }];
    
}

-(void)getOrderDetails{
    
    if([LSUtils isNetworkConnected]){
        
        NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&vendor_id=%@",vendorOrderDetail,_orderId,[UserManager getUserID]];
        imagesArray =[[NSMutableArray alloc]init];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        if(btmefreshControl.isRefreshing ==false)[sharedUtils startAnimator:self];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  if(responseDictionary == nil){
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      return ;
                                                      
                                                  }
                                                  
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSDictionary * arrayOforderDetail = [[responseDictionary valueForKey:@"orderdata"] objectAtIndex:0];
                                                      //status is dummy here, Of no use
                                                      //Set Order Model
                                                      orderDetail  =
                                                      [[OrderHistoryModel alloc]initWithTitle:[arrayOforderDetail objectForKey:@"product_title"] Description:[NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_quantity"]]?[arrayOforderDetail objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_type"]]?[arrayOforderDetail objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_weight"]]?[arrayOforderDetail objectForKey:@"product_weight"]:@"" ] OrderNo:[arrayOforderDetail objectForKey:@"order_number"] Status:0 DateAndTime:[arrayOforderDetail objectForKey:@"created_on"]];
                                                      if([orderDetail.productDescription containsString:@"( )"]||![LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_weight"]]){
                                                          orderDetail.productDescription = @"not mentioned";//[[orderDetail.productDescription stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                      }
                                                      orderDetail.pickUpCordinate =[NSDictionary dictionaryWithObjectsAndKeys:[arrayOforderDetail valueForKey:@"address_lat"], @"pi_lat",[arrayOforderDetail valueForKey:@"address_long"], @"pi_long",nil];
                                                      orderDetail.productId = [arrayOforderDetail objectForKey:@"order_id"];
                                                      orderDetail.productdateAndTime = [arrayOforderDetail objectForKey:@"created_on"];
                                                      orderDetail.pickAdress = [LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"pickup_address"]]?[arrayOforderDetail objectForKey:@"pickup_address"]:@"";
                                                      orderDetail.delAdress = [LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"dropoff_address"]]?[arrayOforderDetail objectForKey:@"dropoff_address"]:@"";
                                                      //for track View
                                                      orderDetail.orderStatus = [[arrayOforderDetail objectForKey:@"order_status"] intValue];
                                                      orderDetail.jumpPrice = [[arrayOforderDetail objectForKey:@"jump_price"] intValue];
                                                      orderDetail.minRate = [LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"min_rate"]]?[arrayOforderDetail objectForKey:@"min_rate"]:@"";
                                                      orderDetail.maxrate = [LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"max_rate"]]?[arrayOforderDetail objectForKey:@"max_rate"]:@"";
                                                       orderDetail.proposalCount = [arrayOforderDetail objectForKey:@"driverCount"];
                                                      ////////////////////////////////////////////////////////////////////////////
                                                      NSArray *imageArray =[responseDictionary valueForKey:@"imagedata"];
                                                      NSMutableArray* imageNameArray = [[NSMutableArray alloc]init];
                                                      
                                                      for(int index = 0; index < imageArray.count; index++){
                                                          
                                                          NSDictionary *imageDict =[imageArray objectAtIndex:index];
                                                          [imageNameArray addObject:[imageDict objectForKey:@"product_image"]];
                                                      }
                                                      if(btmefreshControl.isRefreshing == false){
                                                          orderDetail.productImagesArray = imageNameArray;
                                                          orderDetail.imagebaseURL= [responseDictionary valueForKey:@"productImagepath"];
                                                          [self GetProductImage ];
                                                      }
                                                      
                                                      deliveryBoyArray= [[NSMutableArray alloc]init];
                                                      /////////////////////////////////////////////////////////////////////////
                                                      NSArray *driverArray = [responseDictionary valueForKey:@"driverdata"];
                                                      if((driverArray == nil) || ([driverArray isKindOfClass:[NSNull class]])||driverArray.count == 0){
                                                          //dont set value
                                                          _appliedCouriersLabel.hidden = true;
                                                          _mapButton.hidden = true;
                                                          _sortButton.hidden = true;
                                                      }else{
                                                          
                                                          _appliedCouriersLabel.hidden = false;
                                                          _mapButton.hidden = false;
                                                          _sortButton.hidden = false;
                                                          
                                                          for(int index= 0 ; index < driverArray.count; index++){
                                                              
                                                              DeliveryBoyModel* deliveryBoy = [[DeliveryBoyModel alloc]init];
                                                              
                                                              NSDictionary * driverDictionary = [driverArray objectAtIndex:index];
                                                              if(driverDictionary!= nil){
                                                                  deliveryBoy.dName = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_name"]]?@"":[driverDictionary objectForKey:@"deliveryboy_name"];
                                                                  
                                                                  deliveryBoy.dProfileURL = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_image"]]?@"":[driverDictionary objectForKey:@"deliveryboy_image"];
                                                                  
                                                                  deliveryBoy.dMobile = [LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_mobile"]]?@"": [driverDictionary objectForKey:@"deliveryboy_mobile"];
                                                                  deliveryBoy.dEmail = [LSUtils checkNilandEmptyString:[driverDictionary valueForKey:@"deliveryboy_email"]]?[driverDictionary valueForKey:@"deliveryboy_email"]:@"";
                                                                  deliveryBoy.isBooked = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"isbooked"]]?0:[[driverDictionary objectForKey:@"isbooked"] intValue];
                                                                  
                                                                  deliveryBoy.imageName = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_image"]]?@"":[driverDictionary objectForKey:@"deliveryboy_image"];
                                                                  
                                                                  deliveryBoy.dtransportMode = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"transportation_mode"]]?@"":[driverDictionary objectForKey:@"transportation_mode"];
                                                                  
                                                                  deliveryBoy.dBidAmount = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"bid_amount"]]?@"":[driverDictionary objectForKey:@"bid_amount"];
                                                                  
                                                                  deliveryBoy.dId = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryBoy_id"]]?@"":[driverDictionary objectForKey:@"deliveryBoy_id"];
                                                                  
                                                                  deliveryBoy.dOrderBidId = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"orderBid_id"]]?@"":[driverDictionary objectForKey:@"orderBid_id"];
                                                                  
                                                                  deliveryBoy.dRate = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"total_rating"]]?@"":[driverDictionary objectForKey:@"total_rating"];
                                                                  
                                                                  deliveryBoy.isfavouite = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"is_favourite"]]?0:[[driverDictionary objectForKey:@"is_favourite"] intValue];
                                                                  
                                                                  deliveryBoy.delBoyImageBaseUrl = ![LSUtils checkNilandEmptyString:[responseDictionary objectForKey:@"driverImagepath"]]?@"":[responseDictionary objectForKey:@"driverImagepath"];
                                                              }
                                                              [deliveryBoyArray addObject:deliveryBoy];
                                                          }
                                                          
                                                      }
                                                      [self setTextFields];
                                                  }else  {
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                              else {
                                                  UIViewController*  controller ;
                                                  if(error != nil){
                                                      
                                                      controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  }else  controller =   [sharedUtils showAlert:@"Somthing went wrong." withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              if(btmefreshControl.isRefreshing  == true){
                                                  [btmefreshControl endRefreshing ];
                                              }
                                              [_tableView reloadData];

                                          });
                                          
                                      }];
        [task resume];
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil ];
 
    }
}

-(void)cancelOrderByID {
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&vendor_id=%@",cancelOrder,_orderId , [UserManager getUserID]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Order canceled successfully."];
                                                  [self presentViewController:controller animated:YES completion:^{
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                      });
                                                  }];
                                              }
                                              else {
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

-(void)sortDriversWithparam:(NSString*)param{
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {

                                                  deliveryBoyArray= [[NSMutableArray alloc]init];
                                                  /////////////////////////////////////////////////////////////////////////
                                                  NSArray *driverArray = [responseDictionary valueForKey:@"driverdata"];
                                                  if((driverArray == nil) || ([driverArray isKindOfClass:[NSNull class]])||driverArray.count == 0){
                                                      //dont set value
                                                      _appliedCouriersLabel.hidden = true;
                                                      _mapButton.hidden = true;
                                                      _sortButton.hidden = true;
                                                  }else{
                                                      
                                                      _appliedCouriersLabel.hidden = false;
                                                      _mapButton.hidden = false;
                                                      _sortButton.hidden = false;
                                                      
                                                      for(int index= 0 ; index < driverArray.count; index++){
                                                          
                                                          DeliveryBoyModel* deliveryBoy = [[DeliveryBoyModel alloc]init];
                                                          
                                                          NSDictionary * driverDictionary = [driverArray objectAtIndex:index];
                                                          if(driverDictionary!= nil){
                                                              
                                                              deliveryBoy.dName = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_name"]]?@"":[driverDictionary objectForKey:@"deliveryboy_name"];
                                                              
                                                              deliveryBoy.dProfileURL = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_image"]]?@"":[driverDictionary objectForKey:@"deliveryboy_image"];
                                                              
                                                              deliveryBoy.dMobile = [LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_mobile"]]?@"": [driverDictionary objectForKey:@"deliveryboy_mobile"];
                                                              
                                                              deliveryBoy.isBooked = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"isbooked"]]?0:[[driverDictionary objectForKey:@"isbooked"] intValue];
                                                              
                                                              deliveryBoy.imageName = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_image"]]?@"":[driverDictionary objectForKey:@"deliveryboy_image"];
                                                              
                                                              deliveryBoy.dtransportMode = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryboy_image"]]?@"":[driverDictionary objectForKey:@"transportation_mode"];
                                                              
                                                              deliveryBoy.dBidAmount = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"bid_amount"]]?@"":[driverDictionary objectForKey:@"bid_amount"];
                                                              
                                                              deliveryBoy.dId = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"deliveryBoy_id"]]?@"":[driverDictionary objectForKey:@"deliveryBoy_id"];
                                                              
                                                              deliveryBoy.dOrderBidId = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"orderBid_id"]]?@"":[driverDictionary objectForKey:@"orderBid_id"];
                                                              
                                                              deliveryBoy.dRate = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"total_rating"]]?@"":[driverDictionary objectForKey:@"total_rating"];
                                                              
                                                              deliveryBoy.isfavouite = ![LSUtils checkNilandEmptyString:[driverDictionary objectForKey:@"is_favourite"]]?0:[[driverDictionary objectForKey:@"is_favourite"] intValue];
                                                              
                                                              deliveryBoy.delBoyImageBaseUrl = ![LSUtils checkNilandEmptyString:[responseDictionary objectForKey:@"driverImagepath"]]?@"":[responseDictionary objectForKey:@"driverImagepath"];
                                                          }
                                                          [deliveryBoyArray addObject:deliveryBoy];
                                                      }
                                                      [_tableView reloadData];
                                                  }
                                              }
                                              else {
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    CGRect frame = _tableView.frame;
    frame.size = _tableView.contentSize;
    _tableViewHeightConstant.constant = frame.size.height;
}

//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//  
//    // then we are at the top
//    _pageControl.hidden = true;
//    imageScrollViewLeadingConstraint.constant = -209;
//    [self.view layoutIfNeeded];
// }

#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return deliveryBoyArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BidOrderDetailCell *cell = (BidOrderDetailCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    DeliveryBoyModel *object = [deliveryBoyArray objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[BidOrderDetailCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:@"CellIdentifier"];
    }
    if(IS_IPHONE_6P)
        cell.cellShadowSeparartor.constant = -2;
    
    cell.acceptButton.tag = (int)indexPath.row;
    cell.cellDelagte = self;
    [cell setDeliverBoyDetails:object];
    
    [self getDistanceForDriver:(int)indexPath.row :^(NSString *dis) {
       cell.modeLabel.text = [cell.modeLabel.text stringByAppendingString:dis];
    }];

    [cell.driverimageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",object.delBoyImageBaseUrl, object.imageName]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.driverimageView.layer.cornerRadius = 10;
    cell.driverimageView.layer.masksToBounds = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([LSUtils isNetworkConnected]){
        if (indexPath.row < [deliveryBoyArray count]){
            //To reduce the chances of crash
            DriverDetailViewController  *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DriverDetailViewController"];
            vc.driverDetail = [deliveryBoyArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:vc animated:true];
        }
    }
    else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}



-(void)getDistanceForDriver:(int)indexNumber :(void (^) (NSString* dis))completion{
    
    DeliveryBoyModel *delObject = [deliveryBoyArray objectAtIndex:indexNumber];
    
    AppDelegate *del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    __block NSString *lat;
    __block NSString *longi;
    __block int  count = 0 ;
    if([LSUtils checkNilandEmptyString:delObject.dId]){
        
        //Get driver's current location from fire base
        [[[del.rootRef child:@"User"] child:delObject.dId] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            
            if(snapshot.childrenCount == 0){
                //Either driver is not logged in or location could not be loaded right now
                completion(@"");
            }
            for (FIRDataSnapshot* child in snapshot.children) {
                count++;
                NSDictionary *dict = (NSDictionary*)snapshot.value;
                if([child.key isEqualToString:@"currentLat"]){
                    lat = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                if([child.key isEqualToString:@"currentLog"]){
                    longi = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                if(count == snapshot.childrenCount){
                    
                    //Get driver current latlong and pick up latlong to see driver distance from pick up location
                    __block NSArray *allLocation = [NSArray arrayWithObjects:lat, longi ,[ orderDetail.pickUpCordinate objectForKey:@"pi_lat"],[  orderDetail.pickUpCordinate objectForKey:@"pi_long"],nil];//@"28.535517", @"77.391029", nil];
                    
                    [self getDistance:allLocation :^(BOOL success, NSDictionary *result, NSError *error) {
                        
                        if(success && error == nil){
                            if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                                
                                completion([NSString stringWithFormat:@" ( %.02f miles )",[[[result objectForKey:@"data"] objectForKey:@"distance"] floatValue]]);
                            }
                        }
                    }];
                }
            }
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
    }
}

-(void)getDistance:(NSArray*)locationArray :(void (^) (BOOL success,NSDictionary* result,NSError* error))completion{
    
    NSString *params = [NSString stringWithFormat:@"%@&driver_lat=%@&driver_long=%@&order_lat=%@&order_long=%@",getDriverOrderDistance,[locationArray objectAtIndex:0],[locationArray objectAtIndex:1],[locationArray objectAtIndex:2],[locationArray objectAtIndex:3]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  completion(false, nil, nil);
                                                  
                                                  
                                              }else
                                                  completion(true,responseDictionary,nil);
                                              
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  completion(true,nil,error);
                                                  
                                              }else{
                                                  
                                                  completion(false,nil,nil);
                                                  
                                              }
                                          }
                                          
                                      });
                                  }];
    [task resume];
}

-(void)track:(UIButton*)tracBt{
    DeliveryBoyModel *object = [deliveryBoyArray objectAtIndex:tracBt.tag];
    //Read data from firebase DB
    [self readFromDB:object];
}

-(void)readFromDB :(DeliveryBoyModel*)delObject1{
    ;
    // NSString *userID =@"4";// [FIRAuth auth].currentUser.uid;
    AppDelegate *del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *tempArray =[[NSMutableArray alloc]init];
    
    for(DeliveryBoyModel *delObject in deliveryBoyArray){
        
        __block int  count = 0 ;
        [[[del.rootRef child:@"User"] child:delObject.dId] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            
            for (FIRDataSnapshot* child in snapshot.children) {
                NSDictionary *dict = (NSDictionary*)snapshot.value;
                if([child.key isEqualToString:@"currentLat"]){
                    delObject.dlat = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                if([child.key isEqualToString:@"currentLog"]){
                    delObject.dlon = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                count++;
                if(count == snapshot.childrenCount){

                    [tempArray addObject:delObject];

                    if(tempArray.count == deliveryBoyArray.count){
                        AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
                        vc.locationArray = tempArray ;
                        [self.navigationController pushViewController:vc animated:true];
                    }
                }
                
            }
            
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
    }
}

-(void)acceptBid:(UIButton*)acceptBt {
    
    DeliveryBoyModel *object = [deliveryBoyArray objectAtIndex:acceptBt.tag];
  
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:[NSString stringWithFormat:@"Do you want to accept driver %@?\n\nYou aggreed for bid amount. %@",object.dName , [NSString stringWithFormat:@"$%.02f",[object.dBidAmount floatValue]]]
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self acceptDriverBidAmount: object.dId:object.dOrderBidId ];
                               }];
    
    [alertController setValue:alertmessage forKey:@"attributedTitle"];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self  presentViewController:alertController animated:NO completion:Nil];

}


-(void)acceptDriverBidAmount :(NSString*)delId :(NSString*)bidId{
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&vendor_id=%@&orderBid_id=%@&deliveryBoy_id=%@",acceptsBid,_orderId, [UserManager getUserID] ,bidId,delId ];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"Sucess" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:^{
                                                      dispatch_async(dispatch_get_main_queue(), ^{

                                                     OrderHistoryViewController  *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderHistoryViewController"];
                                                          vc.isFromNotificationScreen = true;

                                                          [self.navigationController pushViewController:vc animated:true];
                                                      });
                                                  }];
                                              }
                                              else {
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

- (IBAction)sortButtonClicked:(id)sender {
    [self initializePickerView];
}

- (IBAction)mapButtonClicked:(id)sender {
    
    AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
    vc.locationArray = deliveryBoyArray;
    vc.pickUpLoctionCordinate = orderDetail.pickUpCordinate;
    
    //Since we are allowing to accept driver from address ui We need orderid for accepting driver
    vc.orderIDForAcceptingDriver = _orderId;
    
    //To show add driver option when bid screen
    vc.isFromBidDetailScreen = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)updateButtonClicked:(id)sender {
    UIButton *dimView;
    [dimView removeFromSuperview];
    dimView = nil;
    dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    
    NSString *nibName =  @"UpdateBidAmountView";
    UpdateBidAmountView *viewPop  = [UpdateBidAmountView initWithNib:nibName bundle:nil];
    viewPop.buttonDelegate = (id)self;
    viewPop.orderId = _orderId ;
    viewPop.minAmount = orderDetail.minRate;
    viewPop.maxAmount = orderDetail.maxrate;
    [UIView animateWithDuration:.5
                          delay:0.0
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         viewPop.frame = CGRectMake(40,CGRectGetMaxY(_imageScrollView.frame), self.view.frame.size.width - 80, viewPop.frame.size.height);
                         [viewPop setPreviousAmount];
                     }
                     completion:^(BOOL finished){
                     }];
    [UIView commitAnimations];
    viewPop.backgroundColor=[UIColor whiteColor];
    [dimView addSubview:viewPop];
    [self.view addSubview:dimView];
}

-(void)initializePickerView {
    
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Sort" delegate:(id)self];
    picker.titleFont = [UIFont fontWithName:@"Lato-Regular" size:18.0];
    picker.titleColor = [UIColor blackColor];
    picker.pickerComponentsFont = [UIFont fontWithName:@"Lato-Regular" size:16.0];
    [picker setTag:1];
    [picker setTitlesForComponents:@[@[@"Price-High", @"Price-Low", @"Most Rated", @"Favourite", @"Experience"]]]; // @[aray]];
    [picker show];
    
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles :(int)arrayNo
{
   // 1-high   2-low   3-rating   4-fav
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&vendor_id=%@&sort_by=%@",sortDriver,_orderId,
                        [UserManager getUserID], [NSString stringWithFormat:@"%d",arrayNo + 1]];
    [self sortDriversWithparam:params];
}
-(void)updateAmount:(NSString*)minAmount :(NSString*)maxxAmount{
    
    _minPriceLabel.text = minAmount;
    orderDetail.minRate = minAmount;
    _maxPriceLabel.text = maxxAmount;
    orderDetail.maxrate = maxxAmount;
}

-(void)cancelUpdate{
 
    NSLog((@"View removed"));
}

@end


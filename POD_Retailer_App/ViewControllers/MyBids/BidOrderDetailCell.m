//
//  BidOrderDetailCell.m
//  PODRetailerApp
//
//  Created by Arpana on 29/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "BidOrderDetailCell.h"

@implementation BidOrderDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)acceptButtonClicked:(id)sender {
    BidOrderDetailCell *cell = (BidOrderDetailCell *)self;
    
    if ([self.cellDelagte respondsToSelector:@selector(acceptBid:)]) {
        [self.cellDelagte acceptBid:cell.acceptButton];
    }
}

- (IBAction)favouritButtonClicked:(id)sender {
    BidOrderDetailCell *cell = (BidOrderDetailCell *)self;
    
    if ([self.cellDelagte respondsToSelector:@selector(makeDriverFavourit:)]) {
        [self.cellDelagte acceptBid:cell.favrouiteButton];
    }
}

-(void)setDeliverBoyDetails:(DeliveryBoyModel*)delBoyInstance  {
    
    _nameLabel.text = delBoyInstance.dName;
    _modeLabel.text = delBoyInstance.dtransportMode;
    _priceLabel.text = [NSString stringWithFormat:@"$%.02f",[delBoyInstance.dBidAmount floatValue]];
    UIImage *imageForfav = delBoyInstance.isfavouite == 1?[UIImage imageNamed:@"filledHeart"]:[UIImage imageNamed:@"heart"];
    [_favrouiteButton setImage:imageForfav forState:UIControlStateNormal];
    _rateView.rating = [delBoyInstance.dRate floatValue];
    
    UIColor *buttonColor = delBoyInstance.isBooked == 1?[UIColor colorWithRed:255/255.0f green:145/255.0f blue:148/255.0f alpha:1]:[UIColor blackColor];
    [_acceptButton setTitleColor:buttonColor forState:UIControlStateNormal];
}

@end

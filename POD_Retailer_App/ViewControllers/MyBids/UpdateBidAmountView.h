//
//  UpdateBidAmountView.h
//  PODRetailerApp
//
//  Created by Arpana on 30/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol yesAndNoActionDelegate <NSObject>

@required

-(void)updateAmount:(NSString*)minAmount :(NSString*)maxxAmount;
-(void)cancelUpdate;

@end
@interface UpdateBidAmountView : UIView <UITextFieldDelegate>
@property (nonatomic, assign) id<yesAndNoActionDelegate> buttonDelegate;
@property (nonatomic, strong)  IBOutlet UITextField *mintextfield;
@property (nonatomic, strong) IBOutlet UITextField *maxtextfield;
@property (nonatomic, strong)  NSString *orderId;
@property (nonatomic, strong)  NSString *minAmount;
@property (nonatomic, strong)  NSString *maxAmount;
-(void)setPreviousAmount ;

@end

//
//  MyBidsOrderDetailViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 29/05/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BidOrderDetailCell.h"
#import "AddressViewController.h"
#import "IQActionSheetPickerView.h"
#import "UpdateBidAmountView.h"

@interface MyBidsOrderDetailViewController : UIViewController <UIScrollViewDelegate ,UITableViewDelegate, UITableViewDataSource ,AcceptDelegate ,IQActionSheetPickerViewDelegate ,yesAndNoActionDelegate>


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *pickUpTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddresslabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *minPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *hyphenLabel;
@property (weak, nonatomic) IBOutlet UILabel *appliedCouriersLabel;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (weak, nonatomic) IBOutlet UILabel *proposalCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property(nonatomic, retain) NSString* imagebaseURL;
@property (retain, nonatomic) NSArray* arrayOFImages;
@property(nonatomic, retain) NSString* orderId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstant;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIStackView *priceStackView;

@end

//
//  SelectedMarkerDriver.h
//  PODRetailerApp
//
//  Created by Arpana on 27/04/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
@class DeliveryBoyModel;
@interface SelectedMarkerDriver : UIView
@property (weak, nonatomic) IBOutlet UILabel *noLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distancelabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileView;
@property (nonatomic, strong) DeliveryBoyModel* dBoy;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet ASStarRatingView *starView;
@end

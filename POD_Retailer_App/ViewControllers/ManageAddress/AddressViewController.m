//
//  AddressViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 14/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "AddressViewController.h"
#import "LeftMenuViewController.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "OrderHistoryViewController.h"
@interface AddressViewController (){
    
    NSMutableArray *allAddressesArray;
    NSDictionary *pickDict ;
}

/* these three will be used to guess the state of the map animation since there's no
 delegate method to track when the camera update ends */
@property BOOL markerTapped;
@property (strong, nonatomic) GMSMarker *currentlyTappedMarker;

@end

@implementation AddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _selectedView.hidden = true;
}

-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    if(_isFromBidDetailScreen){
        _selectedView.addButton.hidden = false;
    }else
        _selectedView.addButton.hidden = true;

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    allAddressesArray = [[NSMutableArray alloc] init];
    //Add pick up location to the array
    pickDict = [[NSDictionary alloc] initWithObjectsAndKeys:[_pickUpLoctionCordinate objectForKey:@"pi_lat"],@"lat" ,[_pickUpLoctionCordinate objectForKey:@"pi_long"],@"long",@"greenpin",@"marker",@"PICKUPLOCATION", @"title", @"", @"driverID",nil];
    [allAddressesArray addObject:pickDict];
    
    [self setUpUIElementsValues];
    _mapView.delegate = self;
    [self readAllDriversLocationFromDB];
}

-(void)getDistanceForDriver :(NSDictionary*)driverDict{
    
    [self getDistance:driverDict :^(BOOL success, NSDictionary *result, NSError *error) {
        
        if(success && error == nil){
            if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {

                _selectedView.distancelabel.text = [NSString stringWithFormat:@"%.02f miles",[[[result objectForKey:@"data"] objectForKey:@"distance"] floatValue]];
            }
        }
    }];
}


-(void)getDistance:(NSDictionary*)driverInfo :(void (^) (BOOL success,NSDictionary* result,NSError* error))completion{
    
    NSString *params = [NSString stringWithFormat:@"%@&driver_lat=%@&driver_long=%@&order_lat=%@&order_long=%@",getDriverOrderDistance,[driverInfo objectForKey:@"lat"],
                        [driverInfo objectForKey:@"long"],[pickDict objectForKey:@"lat"],[pickDict objectForKey:@"long"]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  completion(false, nil, nil);
                                                  
                                                  
                                              }else
                                                  completion(true,responseDictionary,nil);
                                              
                                              
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  completion(true,nil,error);
                                                  
                                              }else{
                                                  
                                                  completion(false,nil,nil);
                                                  
                                              }
                                          }
                                          
                                      });
                                  }];
    [task resume];
}

-(void )viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
}
-(void)viewWillDisappear:(BOOL)animated{
    allAddressesArray = nil;
    _markersArray =nil;
    [super viewWillDisappear:animated];
}

- (void)focusMapToShowAllMarkers
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (GMSMarker *marker in _markersArray){

        
        bounds = [bounds includingCoordinate:marker.position];
    }
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
}

// User pressed cancel button.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(void)readAllDriversLocationFromDB {
    
    AppDelegate *del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
   // __block int  DeliveryBoycount = 0 ;
    
    //Add pick up location to the array
     NSArray *pickUpArray = [NSArray arrayWithObjects:[_pickUpLoctionCordinate objectForKey:@"pi_lat"] , [_pickUpLoctionCordinate objectForKey:@"pi_long"] , @"greenpin", @"PickUP",  @"", nil];
    NSLog(@"Array %@", pickUpArray.description);

    [allAddressesArray addObject:pickUpArray];


    for(DeliveryBoyModel *delObject in _locationArray){
        
     //   DeliveryBoycount++;

       // __block int  Keycount = 0 ;
        __block NSString *lat;
        __block NSString *longi;
        
        [[[del.rootRef child:@"User"] child:delObject.dId] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            
            for(int index = 0; index< allAddressesArray.count;index++){
                //If the driver is already present in in array remove it first , then add with update location;
                NSPredicate *pred=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",delObject.dId];
                NSArray *resultArray=[[allAddressesArray objectAtIndex:index ] filteredArrayUsingPredicate:pred];
                
                [allAddressesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"ANY %K LIKE[cd] %@",@"tags",[partialkeyWord stringByAppendingString:@"*"]]]

                
                if(resultArray.count>0)
                    [allAddressesArray removeObject:[resultArray firstObject]];
            }
            NSMutableArray *tempArray =[[NSMutableArray alloc]init];
            
            NSLog(@"%@", snapshot.value);
            
            int  Keycount = 0;
            for (FIRDataSnapshot* child in snapshot.children) {
                
                Keycount++;
                NSDictionary *dict = (NSDictionary*)snapshot.value;
                if([child.key isEqualToString:@"currentLat"]){
                    lat = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                if([child.key isEqualToString:@"currentLog"]){
                    longi = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                
                if(Keycount == snapshot.childrenCount){
                    
                    //@"Driver" to identify its a  driver at index 3
                    __block NSArray *currentLocation = [NSArray arrayWithObjects:lat, longi ,@"pin", @"Driver",  delObject.dId, nil];
                    
                    [tempArray addObject:currentLocation];
                    [allAddressesArray addObject:currentLocation];
                }
                
            }
            NSLog(@"Array %@", tempArray.description);
            
            // if(DeliveryBoycount == _locationArray.count){
            
            //All delivery Boy address are added, Now add pickup address long lat
            // __block NSArray *pickUpArray = [NSArray arrayWithObjects:@"28.535517", @"77.391029" ,@"greenpin", @"PickUP",  @"", nil];
            
            //   allAddressesArray = tempArray ;
           // [allAddressesArray addObject:tempArray];
           
            [self plotMarkers];
            // }
            
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
        
    }
}*/

-(void)readAllDriversLocationFromDB {
    
    AppDelegate *del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    for(DeliveryBoyModel *delObject in _locationArray){
        
        //   DeliveryBoycount++;
        
        // __block int  Keycount = 0 ;
        __block NSString *lat;
        __block NSString *longi;
        
        [[[del.rootRef child:@"User"] child:delObject.dId] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            
            for(int index = 0; index< allAddressesArray.count;index++){
                //If the driver is already present in in array remove it first , then add with update location;
                NSPredicate *pred=[NSPredicate predicateWithFormat:@"driverID==%@",delObject.dId];
                NSArray *resultArray=[allAddressesArray filteredArrayUsingPredicate:pred];
                
                if(resultArray.count>0)
                    [allAddressesArray removeObject:[resultArray firstObject]];
            }
            
            NSLog(@"%@", snapshot.value);
            
            int  Keycount = 0;
            for (FIRDataSnapshot* child in snapshot.children) {
                
                Keycount++;
                NSDictionary *dict = (NSDictionary*)snapshot.value;
                if([child.key isEqualToString:@"currentLat"]){
                    lat = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                if([child.key isEqualToString:@"currentLog"]){
                    longi = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                
                if(Keycount == snapshot.childrenCount){
                    
                        NSDictionary *driverDict = [[NSDictionary alloc]initWithObjectsAndKeys:lat,@"lat" ,longi,@"long",@"pin",@"marker",@"Driver",@"title",delObject.dId , @"driverID",nil];
                    [allAddressesArray addObject:driverDict];
                }
            }
            [self plotMarkers];
            
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
    }
}

- (void)plotMarkers
{
    if (!_markersArray) {
        
        _markersArray = [NSMutableArray array];
    }
    //To remove all previous merkers
    [_markersArray removeAllObjects];
    [_mapView clear];
    for (int i=0; i<[allAddressesArray count]; i++){

        NSDictionary *dict =  [allAddressesArray objectAtIndex:i];
        NSString *lat = [dict objectForKey:@"lat"];
        NSString *lon = [dict objectForKey:@"long"];
        double lt=[lat doubleValue];
        double ln=[lon doubleValue];
        
        // Instantiate and set the GMSMarker properties
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.appearAnimation=YES;
        marker.position = CLLocationCoordinate2DMake(lt,ln);
        marker.map = self.mapView;
        marker.icon = [UIImage imageNamed:[dict objectForKey:@"marker"]];//@"pin"];
        marker.userData = [allAddressesArray objectAtIndex:i];
        [self.markersArray addObject:marker];
        GMSCameraUpdate *withinCurrentLocationView = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(lt,ln)];
        [_mapView animateWithCameraUpdate:withinCurrentLocationView];
    }
    [self focusMapToShowAllMarkers];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    // A marker has been tapped, so set that state flag
    self.markerTapped = YES;
    
    // If a marker has previously been tapped and stored in currentlyTappedMarker, then nil it out
    if(self.currentlyTappedMarker) {
        self.currentlyTappedMarker = nil;
    }
    
    // make this marker our currently tapped marker
    self.currentlyTappedMarker = marker;
    
    [mapView setSelectedMarker:marker];
    
    /* animate the camera to center on the currently tapped marker, which causes
     mapView:didChangeCameraPosition: to be called */
    GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:marker.position];
    [self.mapView animateWithCameraUpdate:cameraUpdate];
    
    NSDictionary *markerDict = marker.userData;
    _selectedView.dBoy = nil;
    if([[markerDict objectForKey:@"title"] isEqualToString:@"Driver"]){
        
        NSPredicate *pred=[NSPredicate predicateWithFormat:@"dId == %@",[markerDict objectForKey:@"driverID"] ];
        NSArray *resultArray=[_locationArray filteredArrayUsingPredicate:pred];
        if (resultArray.count != 0){
            DeliveryBoyModel *item = [resultArray firstObject];
            _selectedView.nameLabel.text = item.dName;
            _selectedView.starView.rating = [item.dRate floatValue];
            [_selectedView.profileView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", item.delBoyImageBaseUrl, item.imageName]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            _selectedView.dBoy = item;
            _selectedView.profileView.layer.cornerRadius = 10;
            _selectedView.profileView.layer.masksToBounds = true;
            _selectedView.hidden= false;
            [self getDistanceForDriver:markerDict];
        }
    }else{
       marker.title = @"Pick Up Location";
        _selectedView.hidden = true;

        [mapView setSelectedMarker:marker];
 
    }
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay{
    _selectedView.hidden = true;

}

/* If the map is tapped on any non-marker coordinate, reset the currentlyTappedMarker and remove our*/
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    if(self.currentlyTappedMarker) {
        self.currentlyTappedMarker = nil;
    }
    _selectedView.hidden = true;
}

/* When the button is clicked, verify that we've got access to the correct marker.
 You might use this button to push a new VC with detail about that marker onto the navigation stack. */
- (void)buttonClicked:(id)sender
{
    NSLog(@"button clicked for this marker: %@",self.currentlyTappedMarker);
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)backButtonClicked{
    
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)AcceptDriver:(id)sender {
    
    DeliveryBoyModel *object = _selectedView.dBoy;
    
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:[NSString stringWithFormat:@"Do you want to accept driver %@?\n\nYou aggreed for bid amount. %@",object.dName , [NSString stringWithFormat:@"$%.02f",[object.dBidAmount floatValue]]]
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self acceptDriverBidAmount: object.dId:object.dOrderBidId ];
                               }];
    
    [alertController setValue:alertmessage forKey:@"attributedTitle"];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self  presentViewController:alertController animated:NO completion:Nil];
    
    
}
-(void)acceptDriverBidAmount :(NSString*)delId :(NSString*)bidId{
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&vendor_id=%@&orderBid_id=%@&deliveryBoy_id=%@",acceptsBid,_orderIDForAcceptingDriver, [UserManager getUserID] ,bidId,delId ];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"Sucess" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:^{
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          
                                                          OrderHistoryViewController  *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderHistoryViewController"];
                                                          vc.isFromNotificationScreen = true;
                                                          
                                                          [self.navigationController pushViewController:vc animated:true];
                                                      });
                                                  }];
                                              }
                                              else {
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

@end



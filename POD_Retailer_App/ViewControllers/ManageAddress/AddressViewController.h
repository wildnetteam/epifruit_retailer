//
//  AddressViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 14/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "SelectedMarkerDriver.h"
#import "DeliveryBoyModel.h"

@interface AddressViewController : UIViewController <GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (retain, nonatomic) NSMutableArray* markersArray;
@property (retain, nonatomic) NSMutableArray* locationArray;
@property (retain, nonatomic) IBOutlet SelectedMarkerDriver* selectedView;
@property (retain, nonatomic) NSString* orderIDForAcceptingDriver;
@property (nonatomic) BOOL isFromBidDetailScreen;
@property (retain, nonatomic) NSDictionary *pickUpLoctionCordinate;
@end

//
//  PushView.m
//  EpiFruitApp
//
//  Created by Arpana on 29/08/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "PushView.h"
#import "OrderDetailViewController.h"
#import "OrderHistoryViewController.h"
#import "MyRewardListViewController.h"
@implementation PushView

- (void)awakeFromNib {
    
    [super awakeFromNib];

}

-(void)setData:(NSDictionary*)dict {
    
    _appName.text = @"EPIFRUITAPP";
    _alert.text = [[dict objectForKey:@"aps"] objectForKey:@"alert"];
    
}
@end

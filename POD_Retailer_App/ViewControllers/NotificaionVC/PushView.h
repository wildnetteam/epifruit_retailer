//
//  PushView.h
//  EpiFruitApp
//
//  Created by Arpana on 29/08/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PushView : UIView
@property (nonatomic, strong)  IBOutlet UILabel *alert;
@property (nonatomic, strong) IBOutlet UILabel *appName;
@property (nonatomic, strong) IBOutlet UIButton *redirectButton;
-(void)setData:(NSDictionary*)dict ;
@property (nonatomic, weak) UIPopoverController *currVisiblePopOver;

@end

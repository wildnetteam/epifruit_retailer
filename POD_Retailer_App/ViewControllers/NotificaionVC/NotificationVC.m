//
//  NotificationVC.m
//  Dropwash
//
//  Created by Yogita on 19/07/16.
//  Copyright © 2016 Mobrill. All rights reserved.
//

#import "NotificationVC.h"
#import "OrderDetailViewController.h"
#import "NotificationTableViewCell.h"
#import "MyBidsOrderDetailViewController.h"
#import "ExpiredJobViewController.h"
@implementation NotificationVC
{
    UIRefreshControl *btmefreshControl;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    _currentPage=1;
    [self setUpUIElementsValues];
}


-(void)setUpUIElementsValues{
    
    self.title = @"";

    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    _tableNotification.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    _tableNotification.rowHeight = UITableViewAutomaticDimension;
    _tableNotification.estimatedRowHeight = 100.0;
    
    btmefreshControl = [[UIRefreshControl alloc]init];
    [_tableNotification addSubview: btmefreshControl];
    _tableNotification.alwaysBounceVertical = true;
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];

}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    lastPageBool=NO;
    [self getNotificationList];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];

    if (_arrResponse.count>0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableNotification scrollToRowAtIndexPath:indexPath
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
    }
}

-(void)refresh:(id)sender{
    
    if([LSUtils isNetworkConnected]) {
        [_arrResponse removeAllObjects];
        [_tableNotification reloadData];
        _currentPage=1;
        lastPageBool= NO;
        [self getNotificationList];
    }
    else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - IBAction

-(void)backButtonClicked:(id)sender{
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[DashboardViewController class]]) {
            CATransition *animation = [CATransition animation];
            [animation setType:kCATransitionMoveIn];
            [animation setDuration:.5];
            [animation setSubtype:kCATransitionFromBottom];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            [[aViewController.view layer] addAnimation:animation forKey:nil];
            [self.navigationController popToViewController:aViewController animated:YES];
        }
    }
//    DashboardViewController *dashboard = (DashboardViewController*)[sharedUtils.mainStoryboard   instantiateViewControllerWithIdentifier: @"DashboardViewController"];
//    
//    [self.navigationController pushViewController:dashboard animated:YES];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

#pragma mark- Tableview delegates

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    activityView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tableNotification.frame.size.width, 40)];
    activityView.backgroundColor =[UIColor clearColor];
    activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activity.frame = CGRectMake(CGRectGetMidX(_tableNotification.frame) -15, CGRectGetMidY(activityView.frame), 30, 30);
    [activityView addSubview:activity];
    _tableNotification.tableFooterView= activityView;
    return activityView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrResponse.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(btmefreshControl.isRefreshing  == true){
        [btmefreshControl endRefreshing ];
    }
    
    static NSString *cellIdentifier = @"CellIdentifier";
    
    NotificationTableViewCell *cell = (NotificationTableViewCell*)[self.tableNotification dequeueReusableCellWithIdentifier:cellIdentifier];
  
    cell.labelNotification.text = [[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification"];
    
    int notType = [[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue];
    
    NSString* message;

    switch (notType) {
            
            case PICKUPDNOTI:  message = @"Order PickedUp!";
            break;
            
            case BIDNOTI:  message = @"Bid Done!";
            break;
            
            case EARNINGNOTI: message = @"Point Earned!";
            break;
            
            case COMPLETEDNOTI: message = @"Order Completed!";
            break;
            
           case UPDATEPRICE:  message = @"PriceUpdate Reminder!";
            break;
            
           case EXPIREDNOTI:  message = @"Order Expired!";
            break;
    }
   
    if ([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"isread"] isEqualToString:@"0"]) {
        cell.notificationIcon.image=[UIImage imageNamed:@"Mail_notif"];
    }else{
        cell.notificationIcon.image=[UIImage imageNamed:@"open_mail"];
    }
    cell.labelTitle.text = message;
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT];
    
    //Create the date assuming the given string is in GMT
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [df dateFromString:[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"created_on"]];
    
    //Create a date string in the local timezone
    [df setDateFormat:SHOW_YEAR_MONTH_DATE_FORMAT];
    NSString *strServerDate=[df stringFromDate:date];
    [df setDateFormat:SHOW_YEAR_MONTH_DATE_FORMAT];
    NSString *strCurrentDate=[df stringFromDate:[NSDate date]];
    
    //NSLog(@"date = %@ %@", strServerDate,strCurrentDate);
    
    if ([strCurrentDate isEqualToString:strServerDate]) {
        NSTimeZone* CurrentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSTimeZone* SystemTimeZone = [NSTimeZone systemTimeZone];
        NSInteger currentGMTOffset = [CurrentTimeZone secondsFromGMTForDate:date];
        NSInteger SystemGMTOffset = [SystemTimeZone secondsFromGMTForDate:date];
        NSTimeInterval interval = SystemGMTOffset - currentGMTOffset;
        NSDate* TodayDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:date];
        NSLog(@"Current time zone Today Date : %@", TodayDate);
        
        [df setDateFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
        cell.labelTime.text=[[df stringFromDate:TodayDate] lowercaseString];
    }
    else{
        [df setDateFormat:SHOWING_MONTH_AND_DATE_FORMAT];
        cell.labelTime.text=[df stringFromDate:date];
    }
    
    if(lastPageBool==NO)
    {
        if(indexPath.row==[_arrResponse count]-1 )
        {
            if(_currentPage <_totalPages)
            {
                _currentPage=_currentPage+1;
                [self getNotificationList];
                activity.hidden=NO;
            }
            else
            {
                lastPageBool=YES;
                //increase or decrease content size //_tableNotification setContentSiz
                activity.hidden=YES;
            }
        }
    }
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"isread"] isEqualToString:@"0"]){
            
            __weak __typeof__(self) weakSelf = self;

            [self setReadNotification:[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"id"] completionBlock:^(BOOL succeeded) {
                if(succeeded){
                    
                    NotificationTableViewCell *cell = (NotificationTableViewCell*)[weakSelf.tableNotification cellForRowAtIndexPath:indexPath];
                    cell.notificationIcon.image=[UIImage imageNamed:@"open_mail"];
                }
            }];
        }
            int k = [[UserManager getNotificationCount] intValue]-1;
            
            [UserManager saveNotificationCount:[NSString stringWithFormat:@"%d",k]];
            
            [UIApplication sharedApplication].applicationIconBadgeNumber =[[UserManager getNotificationCount] integerValue];
        });
        // If you then need to execute something making sure it's on the main thread (updating the UI for example)
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue] == COMPLETEDNOTI ){
                //Its a completed Order and Unread
                [self getRatingStatusCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
                    if(error!=nil){
                        return ;
                    }
                    if(result == nil){
                        
                    }
                    if(success){
                        
                        if( [[result objectForKey:@"code"]isEqualToString:@"200"] && [[result valueForKey:@"israted"] intValue] == 0){
                            UIViewController *rateVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"RatingViewController"];
                            [self.navigationController pushViewController:rateVC animated:YES ];
                        }
                        else   {
                            OrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
                            vc.swiftySideMenu.centerViewController = self.navigationController;
                            vc.orderId =[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"order_id"];
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                    }else{
                        OrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
                        vc.swiftySideMenu.centerViewController = self.navigationController;
                        vc.orderId =[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"order_id"];
                        [self.navigationController pushViewController:vc animated:YES];
                   }
                }];
            }
           else if([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue] == PICKUPDNOTI){
               
                OrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
                vc.swiftySideMenu.centerViewController = self.navigationController;
                vc.orderId =[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"order_id"];
                [self.navigationController pushViewController:vc animated:YES];

            }
           else if([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue] == BIDNOTI){
               
               MyBidsOrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"MyBidsOrderDetailViewController"];
               vc.swiftySideMenu.centerViewController = self.navigationController;
               vc.orderId =[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"order_id"];
               [self.navigationController pushViewController:vc animated:YES];
               
           }
           else if([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue] == EARNINGNOTI){
                
                UIViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"MyRewardListViewController"];
                vc.swiftySideMenu.centerViewController = self.navigationController;
                [self.navigationController pushViewController:vc animated:YES];
            }
           else if([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue] == UPDATEPRICE){
                
                MyBidsOrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"MyBidsOrderDetailViewController"];
                vc.orderId = [[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"order_id"];
                vc.swiftySideMenu.centerViewController = self.navigationController;
                [self.navigationController pushViewController:vc animated:YES];
            }
            
           else if([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue] == EXPIREDNOTI){
               ExpiredJobViewController*  vc = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier:@"ExpiredJobViewController"];
              vc.isFromNotificationScreen =true;
               vc.swiftySideMenu.centerViewController = self.navigationController;
               [self.navigationController pushViewController:vc animated:YES];
           }
        });
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}

#pragma  mark - WebServices

-(void)getNotificationList{

    if([LSUtils isNetworkConnected])
    {
//        _currentPage=_currentPage+1;
//        
//        if (_currentPage==1) {
//         [sharedUtils startAnimator:self];
//        }
        
        
        if(btmefreshControl.isRefreshing ==false){            
        }
        if (_currentPage==1) {
            
            [sharedUtils startAnimator:self];
        }
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@&page_no=%d",getNotificationList_Service,[UserManager getUserID],(int) _currentPage ];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [sharedUtils stopAnimator:self];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                           options:0
                                                                                                             error:NULL];
                                                  if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      if (_currentPage==1) {
                                                          _arrResponse=[[NSMutableArray alloc] initWithArray:[Response objectForKey:@"data"]];
                                                      }
                                                      else{
                                                          [_arrResponse addObjectsFromArray:[Response objectForKey:@"data"]];
                                                      }
                                                      if (_arrResponse.count>0) {
                                                          
                                                          self.totalPages  = [[Response objectForKey:@"noofpage"] integerValue];
                                                      }
                                                      
                                                      if(_arrResponse.count >0){
                                                          
                                                          UIButton* editButton = [UIButton buttonWithType:UIButtonTypeCustom];
                                                          [editButton setFrame:CGRectMake(0, 0, 100, 50)];
                                                          [editButton addTarget:self action:@selector(clearList:) forControlEvents:UIControlEventTouchUpInside];
                                                          [editButton setTitle:@"Clear all" forState:UIControlStateNormal];
                                                          [editButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Semibold" size:15]];
                                                          [editButton setTitleColor:AppDefaultBlueColor forState:UIControlStateNormal];
                                                          
                                                          UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
                                                          
                                                          negativeSpacer.width = -25;
                                                          
                                                          [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:editButton] , nil]];
                                                          
                                                      }
                                                  }
                                                  else if ([[Response  objectForKey:@"code"] isEqualToString:@"210"]) {
                                                      
                                                      if(_arrResponse.count == 0) {
                                                          
                                                          [sharedUtils showNoResultsViewWithOptionalText:NSLocalizedString(@"You do not have any notifications.", @"You do not have notifications.") xPosition:0 yPosition:0 screenWidth:0 screenHeight:64 ];
                                                          sharedUtils.noResultsView.backgroundColor = [UIColor whiteColor];
                                                          [self.tableNotification addSubview:sharedUtils.noResultsView];
                                                      }
                                                  }
                                              }
                                              else{
                                                  if(error!= nil){
                                                      
                                                      UIAlertController* controller=   [sharedUtils showAlert:@"" withMessage:error.localizedDescription ];
                                                      [self presentViewController:controller animated:true completion:nil];
                                                      
                                                  }
                                              }
                                              [_tableNotification reloadData];
                                              if(btmefreshControl.isRefreshing  == true){
                                                  [btmefreshControl endRefreshing ];
                                              }
                                              
                                          });
                                      }];
        [task resume];
    }
    else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}
-(void)setReadNotification:(NSString*)orderId  completionBlock:(void (^)(BOOL succeeded))completionBlock {
  
        if([LSUtils isNetworkConnected])
        {
            
            NSURL *url = [NSURL URLWithString:Base_URL];
            NSString *params = [NSString stringWithFormat:@"%@&notification_id=%@",setReadNotification_Service,orderId];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:60.0];
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{ if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                           options:0
                                                                                                             error:NULL];
                                                  NSLog(@"Response:%@",Response);
                                                  if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      completionBlock(true);
                                                  }
                                              }
                                              else{
                                                  //:k_Msg_Unknown_Error
                                              }
                                              });
                                          }];
            [task resume];
        }
        else{
            //no internet
        }
   
}

-(void)clearList:(id)sender{
    
//    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
//    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
//    
//    for(int i = 0 ; i<_arrResponse.count ; i++){
//        
//        NSDictionary* dict  = [_arrResponse objectAtIndex:i];
//        
//        //Read notifications
//        if([[dict objectForKey:@"isread"] isEqualToString:@"1"]){
//            
//            NSIndexPath *indexNo = [NSIndexPath indexPathForRow:i   inSection:0];
//            [tempArray addObject:indexNo];
//            [indexSet addIndex:indexNo.row];
//        }
//    }
//    [_arrResponse removeObjectsAtIndexes:indexSet];
//    [_tableNotification beginUpdates];
//    [_tableNotification deleteRowsAtIndexPaths:tempArray withRowAnimation:UITableViewRowAnimationFade];
//    [_tableNotification endUpdates];
    
    [self clearNotificationListWithCompletion:^(BOOL success) {
    }];
}


-(void)clearNotificationListWithCompletion:(void (^) (BOOL success))completion {
    
    if([LSUtils isNetworkConnected])
    {
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",clearAllNotification,[UserManager getUserID] ];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{ if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                       options:0
                                                                                                         error:NULL];
                                              [sharedUtils stopAnimator:self];
                                              
                                              if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:[Response  objectForKey:@"message"]];
                                                  
                                                  [self presentViewController:controller animated:NO completion:^{
                                                      [_arrResponse removeAllObjects];
                                                      _arrResponse = nil;
                                                      [self.tableNotification reloadData];

                                                  }];
                                              }
                                              else  {
                                                  UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:[Response  objectForKey:@"message"]];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  completion(false);
                                              }
                                          }
                                          });
                                      }];
        [task resume];
    }
    else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

//need to rate order since it is completed
-(void)getRatingStatusCompletion:(void (^) (BOOL success,NSDictionary* result,NSError* error) )completion{
    
    if([LSUtils isNetworkConnected]) {
        NSString *params = [NSString stringWithFormat:@"%@&order_id=%@",orderdetailforrating , [UserManager getUserID]];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  
                                                  if(responseDictionary == nil){
                                                      
                                                      completion(true, nil , nil);
                                                      
                                                  }
                                                  
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSMutableArray *arrayOfData;
                                                      
                                                      arrayOfData = [[responseDictionary valueForKey:@"data"] objectAtIndex:0];
                                                      
                                                      sharedUtils.dictOfOrderTosRate = [[NSMutableDictionary alloc]init];
                                                      [sharedUtils.dictOfOrderTosRate setObject:[arrayOfData valueForKey:@"orderid"] forKey:@"orderid"];
                                                      [sharedUtils.dictOfOrderTosRate setObject:[responseDictionary valueForKey:@"deliveryboyimgurl"] forKey:@"deliveryboyimgurl"];
                                                      
                                                      [sharedUtils.dictOfOrderTosRate setObject:[responseDictionary valueForKey:@"israted"] forKey:@"israted"];
                                                      
                                                      NSArray *driverArray = [arrayOfData valueForKey:@"deliveryboydetail"];
                                                      
                                                      NSMutableArray * driversArry = [[NSMutableArray alloc]init];
                                                      
                                                      for(int index =0 ; index<driverArray.count ; index++){
                                                          
                                                          NSDictionary * driverDict = [driverArray objectAtIndex:index];
                                                          
                                                          [driversArry addObject:driverDict];
                                                      }
                                                      [sharedUtils.dictOfOrderTosRate setObject:driversArry forKey:@"DriverArray"];
                                                      completion(true, responseDictionary , nil);
                                                      
                                                  }
                                                  else  if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                                      
                                                      completion(true, responseDictionary , nil);
                                                      //no date
                                                  }
                                                  
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      //error
                                                      completion(false, nil , error);
                                                      
                                                  }else{
                                                      //somting wrong
                                                      completion(false, nil , nil);
                                                      
                                                      
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
    }
}

@end

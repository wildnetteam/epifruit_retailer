//
//  NotificationVC.h
//  Dropwash
//
//  Created by Yogita on 19/07/16.
//  Copyright © 2016 Mobrill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    UIView *activityView;
    UIActivityIndicatorView *activity;
    BOOL lastPageBool;
}

typedef enum {
    BIDNOTI = 2,
    COMPLETEDNOTI = 3,
    UPDATEPRICE = 5,
    PICKUPDNOTI = 8 ,
    EARNINGNOTI = 9,
    EXPIREDNOTI = 10
}NOTIFICATIONType;


@property (retain, nonatomic)id parentObject;
@property (nonatomic,strong) NSMutableArray *arrResponse;
@property (nonatomic,strong) NSArray *arrOrderStatus;
@property (nonatomic,strong) NSArray *arrOrderStatusDetail;

@property (weak, nonatomic) IBOutlet UITableView *tableNotification;
@property (weak, nonatomic) IBOutlet UILabel *labelNotification;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;


@end

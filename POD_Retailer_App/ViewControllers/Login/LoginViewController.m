//
//  LoginViewController.m
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "LoginViewController.h"
#import "RegistrationViewController.h"
#import "AppDelegate.h"
#import "DashboardViewController.h"
#import "UserManager.h"
#import "ForgotpasswordViewController.h"
#import "NotificationManager.h"
#import "AppDelegate.h"

@interface LoginViewController ()
@end

@implementation LoginViewController
{
    UIImage *downloadedImage;
    NSString* profileWithBaseString;
    BOOL isPasswordVisible;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUIElements];
    self.view.userInteractionEnabled = true;
    
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem  = nil;
    self.username.text=@"";
    self.password.text=@"";
    [self placeholderSize:_username :@"Enter email or mobile number..."];
    [self placeholderSize:_password:@"Enter your password..."];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];

}

-(void)setUIElements {
    
    self.title=@"";
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    
    [self placeholderSize:_username :@"Enter email or mobile number..."];
    [self placeholderSize:_password:@"Enter your password..."];
    
    [_signUpButton setBackgroundColor:REGISTERBUTTON_BACKGROUND_COLOR];
    [_facebookSignInButton setBackgroundColor:REGISTERBUTTON_BACKGROUND_COLOR];
    [_googleSignInButton setBackgroundColor:REGISTERBUTTON_BACKGROUND_COLOR];
    _signUpButton.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0);
    
    //Add padding to text field
    [LSUtils addPaddingToTextField:_username];
    [LSUtils addPaddingToTextField:_password];
    
    [_loginButton.layer setShadowOffset:CGSizeMake(0, 4)];
    [_loginButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_loginButton.layer setShadowOpacity:0.1];
    
    //Change Clear Button Image
    
    if (!IS_IPHONE5) {
        self.EmailTextFieldAndPasswordLabelSpace.constant = 30;
        self.LoginButtonAndDontHaveLableSpace.constant = -20;
        self.TopImageViewAndEmailLabelSpace.constant = -50;
        self.TopLayoutAndTopImagaeViewSpace.constant = -20;
        self.textfieldForgotPassSpace.constant = 7;
        self.forgotPassLoginButtonSpace.constant = -28;
        
    }
}

-(void)placeholderSize:(UITextField*)textfield :(NSString*)text{
    UIColor *color = [UIColor whiteColor];
    textfield.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Light" size:15.0]
                                                 }
     ];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}


- (IBAction)showOrHidePassword:(id)sender {
    
    if (!isPasswordVisible) {
        
        _password.secureTextEntry=false;    //Show Password
        isPasswordVisible=true;
        
    }
    else{
        _password.secureTextEntry=true;       //Hide Password
        isPasswordVisible=false;
    }
    
}
- (IBAction)loginButtonTapped:(id)sender {

    if([self validateLoginCredentials]) {
        [self LoginApi:1 idForLogin:0 isImageAvailable:false];
    }

}

-(void)LoginApi:(int)mode idForLogin:(NSString*)u_id isImageAvailable:(BOOL)yesOrNo{
    
    if([LSUtils isNetworkConnected])
    {
        NSString* params ;
        
        if([UserManager getDeviceToken]!=nil){
            
            params = [NSString stringWithFormat:@"%@&vendorEmail=%@&password=%@&device_type=2&device_token=%@",Login_Service,_username.text,_password.text,[UserManager getDeviceToken]];
        }else{
            params =[NSString stringWithFormat:@"%@&vendorEmail=%@&password=%@",Login_Service,_username.text,_password.text];
        }
        [sharedUtils  startAnimator:self];
        _loginButton.enabled = false;
        _loginButton.userInteractionEnabled = false;
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                             
                                              [sharedUtils stopAnimator:self];
                                              _loginButton.enabled = true;
                                              _loginButton.userInteractionEnabled = true;
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  
                                                  UIAlertController *controller;
                                                  
                                                  if(responseDictionary == nil){
                                                      
                                                      controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      
                                                  }else{
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          NSDictionary *arrayOfdata;
                                                          NSArray   *details = [responseDictionary valueForKey:@"data"];
                                                          arrayOfdata = [details objectAtIndex:0];
                                                          
                                                          // Clear the credentials if any from "UserManager" class
                                                          [UserManager removeUserCredential];
                                                          
                                                          [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"id"] withvar:k_LoggedInUserID];
                                                          [UserManager saveUserCredentials:@"" withvar:k_LoginTokenID];
                                                          [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"fname"] withvar:k_UserName];
                                                          [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"vendorEmail"] withvar:k_UserEmail];
                                                          [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"vendor_mobile_no"] withvar:k_UserPhoneNumber];
                                                          [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"Refrence_code"] withvar:k_UserReferenceCode];
                                                          if([LSUtils checkNilandEmptyString:[arrayOfdata valueForKey:@"vendor_image"]]){
                                                              
                                                              [UserManager saveUserCredentials:[NSString stringWithFormat:@"%@/%@",[responseDictionary valueForKey:@"userimgurl"], [arrayOfdata valueForKey:@"vendor_image"]] withvar:k_UserImageURL];
                                                          }
                                                          
                                                          [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"ISLOGGEDIN"];
                                                          DashboardViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardViewController"];
                                                          
                                                          if(downloadedImage!=nil){
                                                              vc.pimage =downloadedImage;
                                                          }
                                                          CATransition* transition = [CATransition animation];
                                                          transition.duration = 0.5;
                                                          transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
                                                          transition.type = kCATransitionMoveIn;
                                                          [self.navigationController.view.layer addAnimation:transition forKey:nil];
                                                          [self.navigationController pushViewController:vc animated:YES];
                                                          
                                                          [AppDelegate getAppDelegate].SwiftySideMenu.centerViewController = self.navigationController;
                                                          controller = [sharedUtils showAlert:@"Successfull" withMessage:@"You have logged in successfully."];
                                                          
                                                      }    else{
                                                          controller =   [sharedUtils showAlert:@"Sorry" withMessage:[responseDictionary objectForKey:@"message"]];
                                                          
                                                      }
                                                  }
                                                  if(controller!=nil)
                                                      [self presentViewController:controller animated:YES completion:nil];
                                              }else{
                                                  UIAlertController*  controller;
                                                  if(error!=nil){
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                                                  }else{
                                                        controller = [sharedUtils showAlert:@"Sorry" withMessage:@"There is no response , please try again"];
                                                  }
                                                  [self presentViewController:controller animated:YES completion:nil];

                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (IBAction)signUpButton:(id)sender {
    
    RegistrationViewController *regVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"RegistrationViewController"];
    
    [self.navigationController pushViewController:regVC animated:YES];
}

- (IBAction)signInWithGoogle:(id)sender {
    
    [LSUtils saveToUserDefaults:@"google" keys:@"logintype"];
    
    [sharedUtils startAnimator:self];
    self.googleSignInButton.enabled =false;
    self.googleSignInButton.userInteractionEnabled =false;
    sharedUtils.appdelegateInstance.googleSignInInstance.shouldFetchBasicProfile = true;
    
    if([sharedUtils.appdelegateInstance.googleSignInInstance  hasAuthInKeychain]) {
        sharedUtils.appdelegateInstance.googleSignInInstance.uiDelegate = self;
        sharedUtils.appdelegateInstance.googleSignInInstance.delegate = self;
        [sharedUtils.appdelegateInstance.googleSignInInstance signInSilently];
        
    } else {
        sharedUtils.appdelegateInstance.googleSignInInstance.delegate = self;
        sharedUtils.appdelegateInstance.googleSignInInstance.uiDelegate = self;
        [sharedUtils.appdelegateInstance.googleSignInInstance signIn];
        
    }
}

#pragma mark - Google Sign In delegate

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    if(error==nil) {
        
        [UserManager saveUserCredentials:user.userID withvar:k_LoggedInUserID];
        [UserManager saveUserCredentials:@"" withvar:k_LoginTokenID];
        [UserManager saveUserCredentials:user.profile.name withvar:k_UserName];
        [UserManager saveUserCredentials:user.profile.givenName withvar:k_FirstName];
        [UserManager saveUserCredentials:user.profile.email withvar:k_UserEmail];
        [UserManager saveUserCredentials:user.profile.familyName withvar:k_LastName];
        [UserManager saveUserCredentials:@"2" withvar:k_LoginType];
        
        if([user.profile hasImage]) {
            
            NSURL *profile_URL = [user.profile imageURLWithDimension:200];
            
            
            NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                           downloadTaskWithURL:profile_URL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                               // 3
                                                               downloadedImage = [UIImage imageWithData:
                                                                                  [NSData dataWithContentsOfURL:location]];
                                                               
                                                               profileWithBaseString =   [[NSData dataWithContentsOfURL:location] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                                                               // Clear the credentials if any from "UserManager" class
                                                             //  [dict setObject:profile_URL forKey:k_UserImageURL];
                                                               [UserManager saveUserCredentials:[NSString stringWithFormat:@"%@",
                                                                                                 profile_URL]
                                                                                                 withvar:k_UserImageURL];

                                                               
                                                               dispatch_sync(dispatch_get_main_queue(), ^{
                                                                   
                                                                   [sharedUtils stopAnimator:self];
                                                                   self.googleSignInButton.enabled =true;
                                                                   self.googleSignInButton.userInteractionEnabled =true;
                                                                   
                                                                   RegistrationViewController *regVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"RegistrationViewController"];
                                                                   regVC.name = [NSString stringWithFormat:@"%@ %@",[UserManager getFirstName],[UserManager getLastName]];
                                                                   regVC.email = [UserManager getEmail];
                                                                   regVC.phone = [UserManager getUserPhoneNumber];
                                                                   regVC.imgUrl = [UserManager getUserImageURL];
                                                                   
                                                                   [self.navigationController pushViewController:regVC animated:YES];
                                                               });

                                                           }];
            
            // 4
            [downloadPhotoTask resume];
            
            
            
        }else
        {
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.googleSignInButton.enabled =true;
                self.googleSignInButton.userInteractionEnabled =true;
                RegistrationViewController *regVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"RegistrationViewController"];
                regVC.name = [NSString stringWithFormat:@"%@ %@",[UserManager getFirstName],[UserManager getLastName]];
                regVC.email = [UserManager getEmail];
                regVC.phone = [UserManager getUserPhoneNumber];
                
                [self.navigationController pushViewController:regVC animated:YES];
                [sharedUtils stopAnimator:self];
            });
        }
        
    }else{
        
        self.googleSignInButton.enabled =true;
        self.googleSignInButton.userInteractionEnabled =true;
        UIAlertController *controller = [sharedUtils showAlert:@"Failed" withMessage:error.localizedDescription];
        [self presentViewController:controller animated:YES completion:nil];
        [sharedUtils stopAnimator:self];
    }
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {

    self.googleSignInButton.enabled =true;
    self.googleSignInButton.userInteractionEnabled =true;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - FaceBook SignIn

- (IBAction)signInWithFacebook:(id)sender {
    
    [LSUtils saveToUserDefaults:@"facebook" keys:@"logintype"];
    
    [sharedUtils startAnimator:self];
    self.facebookSignInButton.enabled = false;

    AppDelegate* del=  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del.fbManager setLoginBehavior:FBSDKLoginBehaviorBrowser];
    
    [del.fbManager logInWithReadPermissions:@[@"email",@"user_about_me"]
                         fromViewController:self
                                    handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                        
                                        if(result.isCancelled == true){
                                            
                                            self.facebookSignInButton.enabled = true;
                                            [sharedUtils stopAnimator:self];
                                            
                                            UIAlertController *controller = [sharedUtils showAlert:@"sorry" withMessage:@"The user canceled the sign-in flow."];
                                            
                                            [self presentViewController:controller animated:YES completion:nil];
                                            return ;
                                            
                                        }
                                        
                                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email, first_name, last_name"}]
                                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                             
                                             if (!error) {
                                                 // NSString *userID = [[FBSDKAccessToken currentAccessToken] userID];
                                                 
                                                 NSString* profilePicURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=150",[result valueForKey:@"id"]];
                                                 
                                                 [self downloadProfileImage:profilePicURL];
                                                 
                                                 [UserManager saveUserCredentials:[result valueForKey:@"id"] withvar:k_LoggedInUserID];
                                                 [UserManager saveUserCredentials:@"" withvar:k_LoginTokenID];
                                                 [UserManager saveUserCredentials:[result valueForKey:@"name"] withvar:k_UserName];
                                                 [UserManager saveUserCredentials:[result valueForKey:@"first_name"] withvar:k_FirstName];
                                                 [UserManager saveUserCredentials:[result valueForKey:@"email"] withvar:k_UserEmail];
                                                 [UserManager saveUserCredentials:[result valueForKey:@"last_name"] withvar:k_LastName];
                                                 [UserManager saveUserCredentials:@"3" withvar:k_LoginType];
                                                 [UserManager saveUserCredentials:profilePicURL withvar:k_UserImageURL];
                                                 
                                             }else{
                                                 
                                                 self.facebookSignInButton.enabled = true;
                                                 [sharedUtils stopAnimator:self];
                                                 
                                                 UIAlertController *controller = [sharedUtils showAlert:@"sorry" withMessage:@"The user canceled the sign-in flow."];
                                                 [self presentViewController:controller animated:YES completion:nil];
                                             }
                                         }];
                                    }];
}

-(void)downloadProfileImage:(NSString*)picUrlString {
    
    NSURL *url = [NSURL URLWithString:picUrlString];
    
    NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                   downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                       
                                                       downloadedImage = [UIImage imageWithData:
                                                                          [NSData dataWithContentsOfURL:location]];
                                                       
                                                       profileWithBaseString =   [[NSData dataWithContentsOfURL:location] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                                                       dispatch_sync(dispatch_get_main_queue(), ^{
                                                           
                                                           self.facebookSignInButton.userInteractionEnabled = true;
                                                           self.facebookSignInButton.enabled = true;
                                                           [sharedUtils stopAnimator:self];
                                                           
                                                           RegistrationViewController *regVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"RegistrationViewController"];
                                                           
                                                           regVC.name = [NSString stringWithFormat:@"%@ %@",[UserManager getFirstName],[UserManager getLastName]];
                                                           regVC.email = [UserManager getEmail];
                                                           regVC.phone = [UserManager getUserPhoneNumber];
                                                           regVC.imgUrl = [UserManager getUserImageURL];
                                                           
                                                           [self.navigationController pushViewController:regVC animated:YES];
                                                       });

                                                   }];
    
    [downloadPhotoTask resume];
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
}

- (void)loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error {
    
    UIAlertController *controller = [sharedUtils showAlert:@"sorry" withMessage:error.localizedDescription];
    [self presentViewController:controller animated:YES completion:nil];
    
}

- (IBAction)forgotPasswordButtonClicked:(id)sender {
    
    ForgotpasswordViewController *forgotPasswordVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"ForgotpasswordViewController"];
    [self.navigationController pushViewController:forgotPasswordVC animated:YES];
    
}

#pragma mark - Validate Login credential

-(BOOL)validateLoginCredentials{
    
    if(![LSUtils checkNilandEmptyString:_username.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"User name or mobile number must be filled." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        return false;
        
    }

    if(![LSUtils checkNilandEmptyString:_password.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"password can not be blank." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        return false;
    }
    
    return true;
}

#pragma mark - UITextField Delegate Methods

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField ==_username){
        
        [_password becomeFirstResponder];
    }else if (textField == _password){
        
        [textField resignFirstResponder];
        [_loginButton becomeFirstResponder];
        
    }
    return true;
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:true];
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    AppDelegate* del=  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    del.fbManager = nil;
}


// Animate text field
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==_username) {
        
        UIButton *userClearButton = [_username valueForKey:@"_clearButton"];
        [userClearButton setImage:[UIImage imageNamed:@"cross_white"] forState:UIControlStateNormal];
        [userClearButton setImage:[UIImage imageNamed:@"cross_white"] forState:UIControlStateHighlighted];
       
        _username.placeholder = @"";
        
        if ([self.username.text length]>0) {
            self.emailImageView.hidden=true;
            _emailbackView.hidden = true;

        }
        
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - _username.frame.size.height +20, self.view.frame.size.width, self.view.frame.size.height);
        }];
        textField.returnKeyType = UIReturnKeyNext;
    } else {
        
        if (textField==_password)
        {
            UIButton *passwordClearButton = [_password valueForKey:@"_clearButton"];
            [passwordClearButton setImage:[UIImage imageNamed:@"cross_white"] forState:UIControlStateNormal];
            [passwordClearButton setImage:[UIImage imageNamed:@"cross_white"] forState:UIControlStateHighlighted];

            _password.placeholder = @"";
            
            if ([self.password.text length]>0) {
                self.passwordImageView.hidden=true;
            }
        }
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -2.5*_username.frame.size.height + 64, self.view.frame.size.width, self.view.frame.size.height);
        }];
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_username)
    {
        if(!([_username.text length]>0))
        {
            [self placeholderSize:_username :@"Enter email or mobile number..."];
        }
        self.emailImageView.hidden = false;
        _emailbackView.hidden = false;
    }
    if (textField==_password)
    {
        if(!([_password.text length]>0))
        {
            [self placeholderSize:_password :@"Enter your password..."];
            
        }
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
    self.emailImageView.hidden = false;
    self.passwordImageView.hidden = false;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    if(textField == self.username)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.emailImageView.hidden = true;
            _emailbackView.hidden = true;
        }
        if (([self.username.text length] == 1) && [string isEqualToString:@""]) {
            self.emailImageView.hidden = false;
            _emailbackView.hidden = false;
        }
    }
    if(textField == self.password)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.passwordImageView.hidden = true;
        }
        if (([self.password.text length] == 1) && [string isEqualToString:@""]) {
            self.passwordImageView.hidden = false;
        }
    }
    return true;
}


@end

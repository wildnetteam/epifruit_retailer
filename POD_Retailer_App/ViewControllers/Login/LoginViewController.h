//
//  LoginViewController.h
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DashboardViewController.h"
#import "RegistrationViewController.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SwiftySideMenuViewController.h"
#import "UIViewController+SwiftySideMenu.h"

@interface LoginViewController : UIViewController <NSURLSessionDelegate,NSURLSessionDataDelegate,  GIDSignInUIDelegate ,GIDSignInDelegate , FBSDKLoginButtonDelegate >

@property (weak, nonatomic) IBOutlet UIView *emailbackView;

@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *EmailTextFieldAndPasswordLabelSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *LoginButtonAndDontHaveLableSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopImageViewAndEmailLabelSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopLayoutAndTopImagaeViewSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textfieldForgotPassSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *forgotPassLoginButtonSpace;

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *googleSignInButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookSignInButton;
@property (weak, nonatomic) IBOutlet UILabel *DontAccLabel;

@end

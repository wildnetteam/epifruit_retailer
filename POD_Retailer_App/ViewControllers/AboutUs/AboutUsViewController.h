//
//  AboutUsViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 24/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSUtils.h"

@interface AboutUsViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

//
//  OrderDetailViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 28/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "LSUtils.h"
#import "OrderHistoryModel.h"
#import "DeliveryBoyModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AddressViewController.h"

@interface OrderDetailViewController ()
{
    NSMutableArray *imagesArray;
    NSMutableArray *deliveryBoyArray;
    NSString *delBoyImageBaseUrl;
    int center;
    OrderHistoryModel *orderDetail;
    UIRefreshControl *btmefreshControl;

}
@property (weak, nonatomic) IBOutlet UIView *dimView;

@end

@implementation OrderDetailViewController{
    
    __weak IBOutlet NSLayoutConstraint *progressTrailingConstraint;
    
    __weak IBOutlet NSLayoutConstraint *progressLeadingConstraint;
    
    __weak IBOutlet NSLayoutConstraint *progressHeightConstraint;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    orderDetail = [[OrderHistoryModel alloc]init];
    [self setUIElements];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    progressLeadingConstraint.constant =  -self.view.frame.size.width ;//- _headingLabel.frame.size.height;
    progressTrailingConstraint.constant = self.view.frame.size.width;
    imagesArray =[[NSMutableArray alloc]init];
    [_tableView addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    _pageControl.hidesForSinglePage = true;
    //Set default Image on scrollView
    UIImageView* imgView = [[UIImageView alloc] init];
    CGRect frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size = CGSizeMake(sharedUtils.screenWidth, _imageScrollView.frame.size.height +64 );
    imgView.frame = frame;
    imgView.alpha = .6;
    [imgView setBackgroundColor:[UIColor whiteColor]];
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    [_imageScrollView addSubview:imgView];
    imgView.image = [UIImage imageNamed:@"noimage"];
    
    [self getOrderDetails];
}


-(void)setProgressStatus:(int)oStatus {
    
        //    0 => pending, 1=>accepted ,  2 => ongoing , 3 =>compleated , 6 =>order canceled , 7=>order rejected by all drivers
        switch (oStatus) {
                //not yet accepted
            case PENDING:
            case CANCELED:
            case REJECTED:{
                
                progressHeightConstraint.constant = 0;
                _progressView.hidden = true;
            }
                break;
                //Accepted not yet picked up
            case ACCEPTED:{
                _circle1View.image = [UIImage imageNamed:@"greenTick.png"];
                _line1View.image = [UIImage imageNamed:@"halfGreenStrip.png"];
                [_acceptedLabel setTextColor:[UIColor blackColor]];
            }
                break;
                // picked up
            case ONGOING:{
                _circle1View.image = [UIImage imageNamed:@"greenTick.png"];
                _line1View.image = [UIImage imageNamed:@"greenStrip.png"];
                _circle2View.image = [UIImage imageNamed:@"greenTick.png"];
                _line2View.image = [UIImage imageNamed:@"halfGreenStrip.png"];
                [_acceptedLabel setTextColor:[UIColor blackColor]];
                [_pickUpLabel setTextColor:[UIColor blackColor]];
            }
                break;
                //Delivered
            case COMPLETED:{
                //Order completed,Payment done.Make all green
                _circle1View.image = [UIImage imageNamed:@"greenTick.png"];
                _line1View.image = [UIImage imageNamed:@"greenStrip.png"];
                _circle2View.image = [UIImage imageNamed:@"greenTick.png"];
                _line2View.image = [UIImage imageNamed:@"greenStrip.png"];
                _circle3View.image = [UIImage imageNamed:@"greenTick.png"];
                [_acceptedLabel setTextColor:[UIColor blackColor]];
                [_pickUpLabel setTextColor:[UIColor blackColor]];
                [_deliverylabel setTextColor:[UIColor blackColor]];

                
            }
                break;
            default:
                break;
        }
    progressLeadingConstraint.constant =  0 ;//- _headingLabel.frame.size.height;
    progressTrailingConstraint.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

-(void)setTextFields {
 
    if(btmefreshControl.isRefreshing  == true){
        [btmefreshControl endRefreshing ];
    }
    
    if(orderDetail.orderStatus != CANCELED || orderDetail.orderStatus != COMPLETED){
        
        UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelButton setFrame:CGRectMake(0, 0,80, 50)];
        [cancelButton addTarget:self action:@selector(cancelOrderr:) forControlEvents:UIControlEventTouchUpInside];
        [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    _pickUpAddresslabel.text = orderDetail.pickAdress;
    _deliveryAddressLabel.text = orderDetail.delAdress;
    _productDescriptionLabel.text = orderDetail.productDescription;
    _productTitleLabel.text = orderDetail.productTitle;
    _priceLabel.text = [NSString stringWithFormat:@"$%.02f",[orderDetail.price floatValue]];
    _pickUpTimeLabel.text= [self getFormatedDate:orderDetail.productdateAndTime];
    _orderNoLabel.text = [NSString stringWithFormat:@"Order no. %@", orderDetail.productOrderNo];
    [_progressLabel setAttributedText: [sharedUtils getOrderStatus:orderDetail.orderStatus ]];
}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"placed on%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [_tableView removeObserver:self forKeyPath:@"contentSize"];
    _progressView.hidden = true;
    [super viewWillDisappear:animated];

}

-(void)GetProductImage {
    
    for (int index =0 ; index<orderDetail.productImagesArray.count; index++) {
        [self loadImage:[NSString stringWithFormat:@"%@/%@", orderDetail.imagebaseURL, [orderDetail.productImagesArray objectAtIndex:index]]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    orderDetail =nil;
}

-(void)setUIElements{
    
    self.title = @"";
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];

    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    self.view.backgroundColor = [UIColor colorWithRed:244.0f/254 green:244.0f/254 blue:244.0f/254 alpha:1];
    btmefreshControl = [[UIRefreshControl alloc]init];
    [_scrollView addSubview: btmefreshControl];
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
}

-(void)refresh:(id)sender{

    [self getOrderDetails];
}


-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)cancelOrderr:(id)sender{
    [self cancelOrderByID];
}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    CGFloat x = _imageScrollView.contentOffset.x;
    float width  = _imageScrollView.frame.size.width;
    float value =x/width;
    self.pageControl.currentPage = round(value);
}

- (IBAction)changePage:(UIPageControl*)pageControll{
    [_imageScrollView setDelegate:nil];
    [_imageScrollView setContentOffset:CGPointMake(_imageScrollView.frame.size.width * self.pageControl.currentPage, 0) animated:YES];
    [_imageScrollView setDelegate:self];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma  setUI for image on scroll View

-(void)setImages:(NSMutableArray*)imgArray{
    
    
    for(UIImageView *imgView in _imageScrollView.subviews){
        [imgView removeFromSuperview];
    }
    _arrayOFImages = [imgArray copy];
    _imageScrollView.contentSize = CGSizeMake(_imageScrollView.frame.size.width * imgArray.count, 0);
    _pageControl.numberOfPages = [imgArray count];

    for (int i = 0; i < imgArray.count; i++) {
        CGRect frame;
        frame.origin.x = _imageScrollView.frame.size.width*i;
        frame.origin.y = 0;
        frame.size = CGSizeMake(_imageScrollView.frame.size.width, _imageScrollView.frame.size.height + 64);
        UIImageView* imgView = [[UIImageView alloc] init];
        imgView.frame = frame;
        // UIImage *newimage=  [self imageByScalingToSize:frame.size forImage: [imgArray objectAtIndex:i]];
        imgView.image = [imgArray objectAtIndex:i];
        imgView.backgroundColor = [UIColor whiteColor];
        [_imageScrollView addSubview:imgView];
    }
//    // Create the colors
//    UIColor *topColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
//    UIColor *bottomColor = [UIColor colorWithRed:56.0/255.0 green:56.0/255.0 blue:56.0/255.0 alpha:1.0];
//    
//    // Create the gradient
//    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
//    theViewGradient.colors = [NSArray arrayWithObjects: (id)topColor.CGColor, (id)bottomColor.CGColor, nil];
//    theViewGradient.frame =  CGRectMake(0, 0, _imageScrollView.contentSize.width, _imageScrollView.bounds.size.height);
//    //Add gradient to view
//    [_imageScrollView.layer insertSublayer:theViewGradient atIndex:0];
}


//to resize image
- (UIImage *) imageByScalingToSize:(CGSize)targetSize forImage:(UIImage *)sourceImage
{
    UIImage *generatedImage = nil;
    UIGraphicsBeginImageContextWithOptions(targetSize,NO,1.0);
    
    [sourceImage drawInRect:CGRectMake(0, 0,targetSize.width,targetSize.height-64)];
    generatedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return generatedImage;
}

- (UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    float ratio = newSize.width/image.size.width;
    [image drawInRect:CGRectMake(0, 0, newSize.width, ratio * image.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    NSLog(@"New Image Size : (%f, %f)", newImage.size.width, newImage.size.height);
    UIGraphicsEndImageContext();
    return newImage;
}
-(void) loadImage:(NSString *)imageLink{
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    [manager loadImageWithURL:[NSURL URLWithString:imageLink] options:SDWebImageDelayPlaceholder progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        NSLog(@"IMage :%@",image);
        [imagesArray addObject:image];
        [self setImages:imagesArray];
    }];
    
}


-(void)getDistance:(NSArray*)locationArray :(void (^) (BOOL success,NSDictionary* result,NSError* error))completion{
    
    NSString *params = [NSString stringWithFormat:@"%@&driver_lat=%@&driver_long=%@&order_lat=%@&order_long=%@",getDriverOrderDistance,[locationArray objectAtIndex:0],[locationArray objectAtIndex:1],[locationArray objectAtIndex:2],[locationArray objectAtIndex:3]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                                  if(responseDictionary == nil){
                                                      
                                                      completion(false, nil, nil);
                                                      
                                                      
                                                  }else
                                                      completion(true,responseDictionary,nil);
                                                  
                                                  
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      completion(true,nil,error);
                                                      
                                                  }else{
                                                      
                                                      completion(false,nil,nil);
                                                      
                                                  }
                                              }

                                      });
                                  }];
    [task resume];
}

-(void)getOrderDetails{
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&vendor_id=%@", MyOrderDetail,_orderId,[UserManager getUserID]];
    
    imagesArray =[[NSMutableArray alloc]init];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    if(btmefreshControl.isRefreshing ==false)[sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSDictionary * arrayOforderDetail = [[responseDictionary valueForKey:@"orderdata"] objectAtIndex:0];
                                                  //status is dummy here, Of no use
                                                  //Set Order Model
                                                  orderDetail  =
                                                  [[OrderHistoryModel alloc]initWithTitle:[arrayOforderDetail objectForKey:@"product_title"] Description:[NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_quantity"]]?[arrayOforderDetail objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_type"]]?[arrayOforderDetail objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_weight"]]?[arrayOforderDetail objectForKey:@"product_weight"]:@"" ] OrderNo:[arrayOforderDetail objectForKey:@"order_number"] Status:[[arrayOforderDetail objectForKey:@"order_status"] intValue] DateAndTime:[arrayOforderDetail objectForKey:@"created_on"]];
                                                      orderDetail.pickUpCordinate =[NSDictionary dictionaryWithObjectsAndKeys:[arrayOforderDetail valueForKey:@"address_lat"], @"pi_lat",[arrayOforderDetail valueForKey:@"address_long"], @"pi_long",nil];
                                                  if([orderDetail.productDescription containsString:@"( )"]||![LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_weight"]]){
                                                      [[orderDetail.productDescription stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                  }
                                                  if([orderDetail.productDescription isEqualToString:@""]){
                                                      orderDetail.productDescription = @"not mentioned";
                                                  }
                                                  orderDetail.price = [arrayOforderDetail valueForKey:@"accepted_final_order_price"];
                                                  orderDetail.productId = [arrayOforderDetail objectForKey:@"order_id"];
                                                  orderDetail.productdateAndTime = [arrayOforderDetail objectForKey:@"created_on"];
                                                  orderDetail.pickAdress =[arrayOforderDetail objectForKey:@"pickup_address"];
                                                  orderDetail.delAdress =[arrayOforderDetail objectForKey:@"dropoff_address"];
                                                  //for track View
                                                  orderDetail.minRate = [arrayOforderDetail objectForKey:@"min_rate"];
                                                  orderDetail.maxrate = [arrayOforderDetail objectForKey:@"max_rate"];
                                                  ////////////////////////////////////////////////////////////////////////////
                                                  NSArray *imageArray =[responseDictionary valueForKey:@"imagedata"];
                                                  NSMutableArray* imageNameArray = [[NSMutableArray alloc]init];
                                                  
                                                  for(int index = 0; index < imageArray.count; index++){
                                                      
                                                      NSDictionary *imageDict =[imageArray objectAtIndex:index];
                                                      [imageNameArray addObject:[imageDict objectForKey:@"product_image"]];
                                                  }
                                                  if(btmefreshControl.isRefreshing == false){
                                                      orderDetail.productImagesArray = imageNameArray;
                                                      orderDetail.imagebaseURL= [responseDictionary valueForKey:@"productImagepath"];
                                                      [self GetProductImage ];
                                                  }
                                                  
                                                  [self setProgressStatus:orderDetail.orderStatus];
                                                  deliveryBoyArray= [[NSMutableArray alloc]init];
                                                  /////////////////////////////////////////////////////////////////////////
                                                  NSArray *driverArray = [responseDictionary valueForKey:@"driverdata"];
                                                  if((driverArray == nil) || ([driverArray isKindOfClass:[NSNull class]])||driverArray.count == 0){
                                                      //dont set value

                                                  }else{
                                                      for(int index= 0 ; index < driverArray.count; index++){
                                                          
                                                          DeliveryBoyModel* deliveryBoy = [[DeliveryBoyModel alloc]init];
                                                          
                                                          NSDictionary * driverDictionary = [driverArray objectAtIndex:index];
                                                          if(driverDictionary!= nil){
                                                              deliveryBoy.dName = [driverDictionary objectForKey:@"deliveryboy_name"];
                                                              deliveryBoy.dProfileURL = [driverDictionary objectForKey:@"deliveryboy_image"];
                                                              deliveryBoy.dMobile = [driverDictionary objectForKey:@"deliveryboy_mobile"];
                                                              
                                                            deliveryBoy.dEmail = [LSUtils checkNilandEmptyString:[driverDictionary valueForKey:@"deliveryboy_email"]]?[driverDictionary valueForKey:@"deliveryboy_email"]:@"";
                                                              
                                                              deliveryBoy.isBooked = [[driverDictionary objectForKey:@"isbooked"] intValue];
                                                              
                                                              deliveryBoy.imageName = [driverDictionary objectForKey:@"deliveryboy_image"];
                                                              deliveryBoy.dtransportMode = [driverDictionary objectForKey:@"transportation_mode"];
                                                              deliveryBoy.dBidAmount = [driverDictionary objectForKey:@"bid_amount"];
                                                              deliveryBoy.dId = [driverDictionary objectForKey:@"deliveryBoy_id"];
                                                              deliveryBoy.dOrderBidId = [driverDictionary objectForKey:@"orderBid_id"];
                                                              
                                                              deliveryBoy.dRate = [driverDictionary objectForKey:@"total_rating"];
                                                              
                                                              deliveryBoy.isfavouite = [[driverDictionary objectForKey:@"is_favourite"] intValue];
                                                              deliveryBoy.delBoyImageBaseUrl = [responseDictionary objectForKey:@"driverImagepath"];
                                                          }
                                                          [deliveryBoyArray addObject:deliveryBoy];
                                                          
                                                          [_tableView reloadData];
                                                      }
                                                      
                                                  }
                                                  [self setTextFields];
                                              }else  {
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }
                                          else {
                                              UIViewController*  controller ;
                                              if(error != nil){
                                                  
                                                  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                              }else  controller =   [sharedUtils showAlert:@"Somthing went wrong." withMessage:error.localizedDescription];
                                              [self presentViewController:controller animated:YES completion:nil];
                                          }
                                          if(btmefreshControl.isRefreshing  == true){
                                              [btmefreshControl endRefreshing ];
                                          }
                                      });
                                  }];
    [task resume];
}

-(void)cancelOrderByID{

    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&vendor_id=%@",cancelOrder,_orderId , [UserManager getUserID]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Order canceled successfully."];
                                                  [self presentViewController:controller animated:YES completion:^{
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                      });
                                                  }];
                                              }
                                              else {
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    CGRect frame = _tableView.frame;
    frame.size = _tableView.contentSize;
    _viewHeightConstant.constant = frame.size.height;
}

#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return deliveryBoyArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderDetailViewDriverTableViewCell *cell = (OrderDetailViewDriverTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    DeliveryBoyModel *object = [deliveryBoyArray objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[OrderDetailViewDriverTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:@"CellIdentifier"];
    }
    cell.callButton.tag = (int)indexPath.row;
    cell.messageButton.tag = (int)indexPath.row;
    cell.favTopButton.tag = (int)indexPath.row;
    cell.favImageButton.tag = (int)indexPath.row;

    cell.cellDelagte = self;

    [cell setDeliverBoyDetails:object];
    
    //If order is completed No need to track/call/message driver. So disable the option
    if(orderDetail.orderStatus == COMPLETED){
        
        cell.stackView.alpha = .1;
        cell.callButton.enabled = false;
        cell.messageButton.enabled = false;
        cell.trackButton.enabled = false;
        cell.favImageButton.hidden= false;
        cell.favTopButton.enabled= true;
    }else{
        cell.stackView.alpha = 1;
        cell.callButton.enabled = true;
        cell.messageButton.enabled = true;
        cell.trackButton.enabled = true;
        cell.favImageButton.hidden= true;
        cell.favTopButton.enabled= false;
      
    }
    [self getDistanceForDriver:(int)indexPath.row :^(NSString *dis) {
        cell.phoneLabel.text = dis;
    }];
    
    [cell.driverimageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",delBoyImageBaseUrl, object.imageName]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    cell.driverimageView.layer.cornerRadius = 10;
    cell.driverimageView.layer.masksToBounds = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

//Open mapview and track all the drivers on map along with pick up address
-(void)track:(UIButton*)tracBt{
    DeliveryBoyModel *object = [deliveryBoyArray objectAtIndex:tracBt.tag];
    //Read data from firebase DB
    [self readFromDB:object];
}

-(void)message:(UIButton*)messgcBt{
    
    DeliveryBoyModel *object = [deliveryBoyArray objectAtIndex:messgcBt.tag];
    
    NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",object.dMobile];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:%@",phoneStr]]];
    
}
-(void)call:(UIButton*)callBt{
    
    DeliveryBoyModel *object = [deliveryBoyArray objectAtIndex:callBt.tag];
    
    NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",object.dMobile];
    NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
    [[UIApplication sharedApplication] openURL:phoneURL];
    
}

-(void)fav:(UIButton*)favButton {
    
    DeliveryBoyModel *object = [deliveryBoyArray objectAtIndex:favButton.tag];
    
    
    [self setDriverAsFavourite:object.dId :^(BOOL success, NSDictionary *result, NSError *error) {
        
        if (error != nil) {
            // handel error part here
            UIAlertController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            [self presentViewController:controller animated:YES completion:nil];
        }
        
        if(success){
            if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                
                //1 fav  2 = unfav
               NSString* imageName = [[result objectForKey:@"is_fav"]intValue] ==2?@"heart":@"filledHeart";
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:favButton.tag inSection:0];
                NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                
                [_tableView beginUpdates];
                  OrderDetailViewDriverTableViewCell *cell = (OrderDetailViewDriverTableViewCell*)[self.tableView cellForRowAtIndexPath:[indexPaths firstObject]];
                [ cell.favImageButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
                [_tableView endUpdates];
                
            }else{
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:[result objectForKey:@"message"]];
                [self presentViewController:controller animated:YES completion:nil];
                
            }
        }else{
            UIViewController*  controller;
            
            if(result ==nil){
                
                controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
            }else{
                NSLog(@"%@",result);
                controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
            }
            [self presentViewController:controller animated:YES completion:nil];
        }
    }];

}

-(void)getDistanceForDriver:(int)indexNumber :(void (^) (NSString* dis))completion{
   
    DeliveryBoyModel *delObject = [deliveryBoyArray objectAtIndex:indexNumber];

    AppDelegate *del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    __block NSString *lat;
    __block NSString *longi;
        __block int  count = 0 ;
    //Get driver's current location from fire base
        [[[del.rootRef child:@"User"] child:delObject.dId] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSLog(@"Count :%lu", (unsigned long)snapshot.childrenCount);
            
            if(snapshot.childrenCount == 0){
                //Either driver is not logged in or location could not be loaded right now
                 completion(@"distance could not be found");
            }
            for (FIRDataSnapshot* child in snapshot.children) {
                count++;
                NSDictionary *dict = (NSDictionary*)snapshot.value;
                if([child.key isEqualToString:@"currentLat"]){
                    lat = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                if([child.key isEqualToString:@"currentLog"]){
                    longi = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                if(count == snapshot.childrenCount){
                    
                    //Get driver current latlong and pick up latlong to see driver distance from pick up location
                    __block NSArray *allLocation = [NSArray arrayWithObjects:lat, longi ,[ orderDetail.pickUpCordinate objectForKey:@"pi_lat"],[  orderDetail.pickUpCordinate objectForKey:@"pi_long"],nil];//,@"28.535517", @"77.391029", nil];
                    
                    [self getDistance:allLocation :^(BOOL success, NSDictionary *result, NSError *error) {
                
                        if(success && error == nil){
                            if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                                
                                completion([NSString stringWithFormat:@"%.02f miles away...",[[[result objectForKey:@"data"] objectForKey:@"distance"] floatValue]]);
                            }
                        }
                    }];
                }
            }
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
}

-(void)readFromDB :(DeliveryBoyModel*)delObject1{
   
   // NSString *userID =@"4";// [FIRAuth auth].currentUser.uid;
    AppDelegate *del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *tempArray =[[NSMutableArray alloc]init];
   
    for(DeliveryBoyModel *delObject in deliveryBoyArray){
        
        __block int  count = 0 ;
        [[[del.rootRef child:@"User"] child:delObject.dId] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSLog(@"Count :%d", (int)snapshot.childrenCount);
            
            if(snapshot.childrenCount == 0){
              
                UIAlertController* alert = [sharedUtils showAlert:@"" withMessage:@"Unable to track driver's location"];
                [self presentViewController:alert animated:true completion:nil];
            }
            for (FIRDataSnapshot* child in snapshot.children) {
                NSDictionary *dict = (NSDictionary*)snapshot.value;
                if([child.key isEqualToString:@"currentLat"]){
                    delObject.dlat = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                if([child.key isEqualToString:@"currentLog"]){
                    delObject.dlon = [NSString stringWithFormat:@"%@",[dict valueForKey:child.key]];
                }
                count++;
                if(count == snapshot.childrenCount){
     
                    [tempArray addObject:delObject];

                    if(tempArray.count == deliveryBoyArray.count){
                        AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
                        
                        //To hide add driver option when bid screen
                        vc.isFromBidDetailScreen = false;
                        vc.pickUpLoctionCordinate = orderDetail.pickUpCordinate;
                        vc.locationArray = tempArray ;
                        [self.navigationController pushViewController:vc animated:true];
                    }
                }

            }
            
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
    }
}

-(void)setDriverAsFavourite:(NSString*)driverId :(void (^) (BOOL success,NSDictionary* result,NSError* error))completion{
  
    //  Performing task on backgrounf thread
  //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    [sharedUtils startAnimator:self];
 
        NSString *params = [NSString stringWithFormat:@"%@&driver_id=%@&vendor_id=%@",toggleFavouriteDriver,driverId,[UserManager getUserID]];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  
                                                  if(responseDictionary == nil){
                                                      
                                                      completion(false, nil, nil);
                                                      
                                                      
                                                  }else
                                                      completion(true,responseDictionary,nil);
                                                  
                                                  
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      completion(true,nil,error);
                                                      
                                                  }else{
                                                      
                                                      completion(false,nil,nil);
                                                      
                                                  }
                                              }
                                              
                                          });
                                      }];
        [task resume];
   // });
}

@end

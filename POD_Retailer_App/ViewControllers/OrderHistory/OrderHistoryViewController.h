//
//  OrderHistoryViewController.h
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyOrderDescriptionTableViewCell.h"


@interface OrderHistoryViewController : UIViewController  <UITableViewDelegate, UITableViewDataSource , RepostDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *myOrderSegmentContril;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic , strong)NSMutableArray *ongoinArray;
@property (nonatomic , strong)NSMutableArray *completedArray;

@property (nonatomic ) BOOL isFromNotificationScreen;
@property(nonatomic)int priviousScreen;

@end

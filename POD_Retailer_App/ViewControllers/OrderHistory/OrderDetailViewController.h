//
//  OrderDetailViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 28/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailViewDriverTableViewCell.h"

@interface OrderDetailViewController : UIViewController<UIScrollViewDelegate ,UITableViewDelegate, UITableViewDataSource, TrackMessageCallDelegate >


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleCenterConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *pickUpTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *VehicleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddresslabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property(nonatomic, retain) NSString* imagebaseURL;
@property (retain, nonatomic) NSArray* arrayOFImages;
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UIImageView* circle1View;
@property (weak, nonatomic) IBOutlet UILabel * acceptedLabel;
@property (weak, nonatomic) IBOutlet UIImageView* line1View;
@property (weak, nonatomic) IBOutlet UIImageView* circle2View;
@property (weak, nonatomic) IBOutlet UILabel * pickUpLabel;
@property (weak, nonatomic) IBOutlet UIImageView* line2View;
@property (weak, nonatomic) IBOutlet UIImageView* circle3View;
@property (weak, nonatomic) IBOutlet UILabel *deliverylabel;
@property(nonatomic, retain) NSString* orderId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstant;
@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end

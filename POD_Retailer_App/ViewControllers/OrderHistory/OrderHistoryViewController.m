//
//  OrderHistoryViewController.m
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "OrderHistoryViewController.h"
#import "OrderHistoryModel.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "OrderDetailViewController.h"
@interface OrderHistoryViewController ()
{
    int viewToAppear;
    UIRefreshControl *btmefreshControl;
}
@end

@implementation OrderHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    _isFromNotificationScreen = false;
    [self setUIElement];
    if(_priviousScreen == 0)
    [self getOngoingOrders];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getOngoingOrders) name:NSdidRecieveNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    if(_priviousScreen == 1){
        
        _myOrderSegmentContril.selectedSegmentIndex = 1;
        [self onGoingOrCompletedSegmentButonClicked:_myOrderSegmentContril];
    }
    
    if(_isFromNotificationScreen){
        
        [_ongoinArray removeAllObjects];
        [_completedArray removeAllObjects];
        [_tableView reloadData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getOngoingOrders];
        });
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}


-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)setUIElement{
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;

    _myOrderSegmentContril.selectedSegmentIndex = 0;
    _myOrderSegmentContril.layer.borderColor = [UIColor colorWithRed:0/255.0f green:208/255.0f blue:229/255.0f alpha:1.0f].CGColor;
    _tableView.backgroundColor = [UIColor clearColor];
   
    UIFont *objFont = [UIFont fontWithName:@"Lato-Regular" size:15];
    // Add font object to Dictionary
    NSDictionary *dictAttributes = [NSDictionary dictionaryWithObject:objFont forKey:NSFontAttributeName];
    // Set dictionary to the titleTextAttributes
    [_myOrderSegmentContril setTitleTextAttributes:dictAttributes forState:UIControlStateNormal];
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;
    self.title = @"";
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    if(_priviousScreen == 1){
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    }else{
        [self.navigationItem setLeftBarButtonItem:barButtonItem];
        
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;
    
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0;
    viewToAppear = 1;//1 for ongoing 2 for completed
    btmefreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview: btmefreshControl];
    _tableView.alwaysBounceVertical = true;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

-(void)refresh:(id)sender{

    if([LSUtils isNetworkConnected]){
        viewToAppear == 1?[self getOngoingOrders]:[self getCompletedOrders];
    }
    else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        }];
    }
}


#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}
- (IBAction)onGoingOrCompletedSegmentButonClicked:(id)sender {
    
    UISegmentedControl *seg = (UISegmentedControl*)sender;
    switch (seg.selectedSegmentIndex) {
        case 0:
        {
            viewToAppear = 1;
            if(_ongoinArray.count == 0){
                    [self getOngoingOrders];
            }
        }
        break;
        case 1:
        {
             viewToAppear = 2;
            if(_completedArray.count == 0)
                [self getCompletedOrders];
        }
        break;
        default:
            break;
    }
    //can not assign cgsizeZero as it will start showing refresh controller. So made 5.
    [_tableView setContentOffset:CGPointMake(0, 5) animated:YES];

    [_tableView reloadData];
}


#pragma mark - tableView Method

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (  viewToAppear == 1) {
        return _ongoinArray.count;
    }else
        return _completedArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(btmefreshControl.isRefreshing  == true){
        [btmefreshControl endRefreshing ];
    }
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier"];

    // setup the cell
    MyOrderDescriptionTableViewCell *cell = (MyOrderDescriptionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.repostDelagte = (id)self;
    cell.repostButton.tag = indexPath.row;
    OrderHistoryModel* orderObjectAtIndex = [[OrderHistoryModel alloc]init];
    
    if (  viewToAppear == 1 && indexPath.row < _ongoinArray.count) {
     orderObjectAtIndex = [_ongoinArray objectAtIndex:indexPath.row] ;
    }
    else if( viewToAppear == 2 && indexPath.row < _completedArray.count){
                orderObjectAtIndex = [_completedArray objectAtIndex:indexPath.row];
    }
    
    if(orderObjectAtIndex!=nil){
        cell.moreImagesLabel.tag = indexPath.row;
        [cell setOrderDetails:orderObjectAtIndex];
        
        cell.timeLabel.text =[self getFormatedDate:orderObjectAtIndex.productdateAndTime];
        
        [cell.imageViewForProduct sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", orderObjectAtIndex.imagebaseURL, orderObjectAtIndex.randomImageString]]
                                    placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        cell.repostButton.hidden = orderObjectAtIndex.orderStatus == CANCELED ?false:true;
    }
    cell.imageViewForProduct.layer.cornerRadius = 10;
    cell.imageViewForProduct.layer.masksToBounds = true;
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([LSUtils isNetworkConnected]){
        
        OrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
        vc.swiftySideMenu.centerViewController = self.navigationController;
        OrderHistoryModel* orderObjectAtIndex;
        if (  viewToAppear == 1 && indexPath.row < _ongoinArray.count ) {
            orderObjectAtIndex = [_ongoinArray objectAtIndex:indexPath.row] ;
        }
        else{
            
            if(indexPath.row < _completedArray.count)
                orderObjectAtIndex = [_completedArray objectAtIndex:indexPath.row];
        }
        vc.orderId =orderObjectAtIndex.productId;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - Repost Methods -

- (void)repostButtonClicked:(UIButton*)repostButton {
    
    if([LSUtils isNetworkConnected]){
        
        OrderHistoryModel* orderObjectAtIndex = [_completedArray objectAtIndex:repostButton.tag];
        
        [self repostJob:orderObjectAtIndex :(int)repostButton.tag :^(BOOL success, NSDictionary *result, NSError *error) {
            
            if(error!=nil){
                
                UIAlertController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                [self presentViewController:controller animated:YES completion:nil];
            }
            
            if(result == nil){
                
                UIAlertController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                [self presentViewController:controller animated:YES completion:nil];
            }
            if(success){
                
                if([[result objectForKey:@"code"]isEqualToString:@"200"]){
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:repostButton.tag inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [_tableView beginUpdates];
                    [_completedArray removeObjectAtIndex:indexPath.row];
                    [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                    [_tableView endUpdates];
                }
                UIAlertController*  controller =   [sharedUtils showAlert:@"" withMessage:[result objectForKey:@"message"]];
                [self presentViewController:controller animated:YES completion:nil];
            }
            
        } ];
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)repostJob:(OrderHistoryModel*)orderDetail :(int)indexOfCell :(void (^)(BOOL success, NSDictionary *result, NSError *error))completion  {
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@",repostOrder,orderDetail.productId ];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              if(responseDictionary == nil){
                                                  
                                                  completion(true, nil, nil);
                                              }
                                              else  {
                                                  completion (true,responseDictionary , nil);
                                              }
                                          }
                                          else {
                                              
                                              if(error != nil){
                                                  
                                                  completion(true, nil, error);
                                              }else
                                                  completion(false, nil, nil);
                                          }
                                      });
                                  }];
    [task resume];
    
}


#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:true];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:true];
}

-(void)dealloc{
    
    _ongoinArray=nil;
    _completedArray= nil;
}

- (void)didReceiveMemoryWarning {
    
    _ongoinArray=nil;
    _completedArray= nil;
    [super didReceiveMemoryWarning];
}

-(void)getOngoingOrders{
    
    if([LSUtils isNetworkConnected]){
        
        if(btmefreshControl.isRefreshing ==false)
            [sharedUtils startAnimator:self];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            _ongoinArray= nil;
            _ongoinArray = [[NSMutableArray alloc]init];
            
            NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",ongoingOrderList,[UserManager getUserID]];
            
            NSURL *url = [NSURL URLWithString:Base_URL];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [sharedUtils stopAnimator:self];
                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                         options:0
                                                                                                                           error:NULL];
                                                      //NSLog(@"Response:%@",responseDictionary);
                                                      
                                                      if(responseDictionary == nil){
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                          
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          return ;
                                                          
                                                      }
                                                      
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          NSMutableArray *arrayOfAddress;
                                                          arrayOfAddress = [responseDictionary valueForKey:@"data"] ;
                                                          
                                                          for(int index =0 ; index<arrayOfAddress.count ; index++){
                                                              
                                                              NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                              
                                                              //Set Order Model
                                                              OrderHistoryModel *mode =
                                                              [[OrderHistoryModel alloc]initWithTitle:[arrayOfdata valueForKey:@"product_title"] Description:[NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_quantity"]]?[arrayOfdata objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_type"]]?[arrayOfdata objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_weight"]]?[arrayOfdata objectForKey:@"product_weight"]:@"" ] OrderNo:[arrayOfdata valueForKey:@"order_number"] Status:[[arrayOfdata valueForKey:@"order_status"] intValue] DateAndTime:[arrayOfdata valueForKey:@"created_on"]];
                                                              mode.productdateAndTime = [arrayOfdata valueForKey:@"created_on"];
                                                              mode.productId = [arrayOfdata valueForKey:@"id"];
                                                              mode.imagebaseURL = [responseDictionary valueForKey:@"imagepath"];
                                                              mode.productOrderNo = [arrayOfdata valueForKey:@"order_number"];
                                                              mode.randomImageString = [arrayOfdata valueForKey:@"product_image"];
                                                              mode.imageCount = [[arrayOfdata valueForKey:@"imageCount"] intValue];
                                                              mode.maxrate = [arrayOfdata valueForKey:@"max_rate"];
                                                              mode.minRate = [arrayOfdata valueForKey:@"min_rate"];
                                                              mode.jumpPrice = [[arrayOfdata valueForKey:@"jump_price"] intValue];
                                                              
                                                              if([mode.productDescription containsString:@"( )"]||![LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_weight"]]){
                                                                  mode.productDescription = [[mode.productDescription stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                              }
                                                              
                                                              [self.ongoinArray addObject:mode];
                                                          }
                                                      }
                                                      else  {
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                  }else{
                                                      //data is nil
                                                      if(error != nil){
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }else{
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                  }
                                                  if(btmefreshControl.isRefreshing  == true){
                                                      [btmefreshControl endRefreshing ];
                                                  }
                                                  [_tableView setContentOffset:CGPointZero animated:YES];
                                                  [_tableView reloadData];
                                              });
                                          }];
            [task resume];
        });
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}
-(void)getCompletedOrders {
    
    if([LSUtils isNetworkConnected]){
        
        if(btmefreshControl.isRefreshing ==false)
            [sharedUtils startAnimator:self];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            _completedArray =nil;
            _completedArray = [[NSMutableArray alloc]init];
            
            NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",completedOrderList,[UserManager getUserID]];
            
            NSURL *url = [NSURL URLWithString:Base_URL];
            
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [sharedUtils stopAnimator:self];
                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                        options:0
                                                                                                                          error:NULL];
                                                      // NSLog(@"Response:%@",responseDictionary);
                                                      
                                                      if(responseDictionary == nil){
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                          
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          return ;
                                                          
                                                      }
                                                      
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          NSMutableArray *arrayOfAddress;
                                                          arrayOfAddress = [responseDictionary valueForKey:@"data"] ;
                                                          
                                                          for(int index =0 ; index<arrayOfAddress.count ; index++){
                                                              
                                                              NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                              
                                                              //Set Order Model
                                                              OrderHistoryModel *mode =
                                                              [[OrderHistoryModel alloc]initWithTitle:[arrayOfdata valueForKey:@"product_title"] Description:[NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_quantity"]]?[arrayOfdata objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_type"]]?[arrayOfdata objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_weight"]]?[arrayOfdata objectForKey:@"product_weight"]:@"" ] OrderNo:[arrayOfdata valueForKey:@"order_number"] Status:[[arrayOfdata valueForKey:@"order_status"] intValue] DateAndTime:[arrayOfdata valueForKey:@"created_on"]];
                                                              if([mode.productDescription containsString:@"( )"]||![LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_weight"]]){
                                                                  mode.productDescription = [[mode.productDescription stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                              }
                                                              
                                                              mode.productdateAndTime = [arrayOfdata valueForKey:@"created_on"];
                                                              mode.productId = [arrayOfdata valueForKey:@"id"];
                                                              mode.imagebaseURL = [responseDictionary valueForKey:@"productimgurl"];
                                                              
                                                              //its completed order
                                                              [self.completedArray addObject:mode];
                                                              
                                                              NSArray *imageArray =[arrayOfdata valueForKey:@"order_images"];
                                                              NSMutableArray* imageNameArray = [[NSMutableArray alloc]init];
                                                              for(int index = 0; index < imageArray.count; index++){
                                                                  
                                                                  NSDictionary *imageDict =[imageArray objectAtIndex:index];
                                                                  [imageNameArray addObject:[imageDict objectForKey:@"product_image"]];
                                                              }
                                                              mode.productImagesArray = imageNameArray;
                                                          }
                                                      }
                                                      else  if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"No data Found."];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                      
                                                  }else{
                                                      //data is nil
                                                      if(error != nil){
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }else{
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }
                                                  }
                                                  if(btmefreshControl.isRefreshing == true){
                                                      [btmefreshControl endRefreshing ];
                                                  }
                                                  [_tableView setContentOffset:CGPointZero animated:YES];
                                                  [_tableView reloadData];
                                              });
                                          }];
            [task resume];
        });
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"placed on%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}



@end

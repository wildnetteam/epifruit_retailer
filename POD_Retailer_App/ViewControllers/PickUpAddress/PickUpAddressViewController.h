//
//  PickUpAddressViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 19/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import  "NoAddressViewController.h"
@interface PickUpAddressViewController : UIViewController  <UINavigationBarDelegate>
@property (nonatomic, retain) NSMutableArray* pickUpAddressArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) int saveOrEditMode;//1 for save 2 for edit 3 for selecting address
@property (strong ,nonatomic) NSString* PickupID;
@property (strong ,nonatomic) AddressModel* modelWhenNotChanged;
@property (nonatomic) BOOL isprocessing;

@end

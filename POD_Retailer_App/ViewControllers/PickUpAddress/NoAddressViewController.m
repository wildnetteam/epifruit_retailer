//
//.m
//  PODRetailerApp
//
//  Created by Arpana on 20/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "NoAddressViewController.h"
#import "PickUpAddressPopoverView.h"
#import "PickUpAddressViewController.h"
#import "NotificationManager.h"
@interface NoAddressViewController (){
    UIButton *dimView;
    PickUpAddressPopoverView *viewPop;
}

@end

@implementation NoAddressViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.swiftySideMenu.centerViewController = self.navigationController;
    
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    [self setUpUIElement];
    
    [sharedUtils startAnimator:self];
    
    [sharedUtils getAllPickUpAddress:^(BOOL success, NSDictionary *result, NSError *error) {
        [sharedUtils stopAnimator:self];
        
        if (error != nil) {
            // handel error part here
            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            if(success){
                if([[result valueForKey:@"code"]intValue] == 200){
                    
                    if(sharedUtils.pickUpAddressArray.count>0){
                        
                        PickUpAddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"PickUpAddressViewController"];
                        vc.pickUpAddressArray = sharedUtils.pickUpAddressArray;
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                }else  if([[result valueForKey:@"code"]intValue] == 300){
                    
                    UIViewController*  controller =   [sharedUtils showAlert:@"Success" withMessage:@"No address is added yet."];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                }
                
            } else {
                UIViewController*  controller;
                if(result == nil){
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                }else{
                    NSLog(@"%@",result);
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                }
                [self presentViewController:controller animated:YES completion:nil];
            }
        }
    }];
    
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}
-(void)setUpUIElement{
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    self.title =@"";
    if(_saveOrEditMode != 3){
        
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
        barButtonItem.tintColor = [UIColor blackColor];
        
        self.navigationItem.leftBarButtonItem = barButtonItem;
        
    }else{
        
        //View controller need to have both menu and back button
        
        ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
        
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
        barButtonItem.tintColor = [UIColor whiteColor];
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
        
    }

    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    [self.addAddressButton.layer setShadowOffset:CGSizeMake(0, 4)];
    [self.addAddressButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.addAddressButton.layer setShadowOpacity:0.2];
  
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
}


-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)backButtonClicked:(id)sender{
    

    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[PlaceOrderViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }

}


- (IBAction)addAddressButtonClicked:(id)sender {
 
    if([LSUtils isNetworkConnected]){

    [dimView removeFromSuperview];
    dimView = nil;
    dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    viewPop= [[PickUpAddressPopoverView alloc] init];
       viewPop.pickUPAddressDel = self;
    viewPop.parentControllerTypePickUP = ADDAddressControllerPick;

    viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];
    [UIView commitAnimations];
    viewPop.backgroundColor=[UIColor whiteColor];
    [dimView addSubview:viewPop];
    [self.view addSubview:dimView];
    }else{
        [sharedUtils stopAnimator:self];
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected"];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)saveData:(AddressModel*)addressObject canUpdate:(BOOL)isEdited{
    
    [sharedUtils startAnimator:self];
    
    if([LSUtils isNetworkConnected]) {
        
        [sharedUtils addAddressToDataBase:addressObject Completion:^(BOOL success, NSDictionary *result, NSError *error){
            [sharedUtils stopAnimator:self];

            if (error != nil) {
                // handel error part here
                [sharedUtils stopAnimator:self];
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                [self presentViewController:controller animated:YES completion:nil];
            }else{
                
                if(success){
                    
                    if([[result valueForKey:@"code"]intValue] == 200){
                        
                        [sharedUtils getAllPickUpAddress:^(BOOL success, NSDictionary *result, NSError *error) {
                            [sharedUtils stopAnimator:self];
                            
                            if (error != nil) {
                                // handel error part here
                                UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:error.localizedDescription];
                                
                                [self presentViewController:controller animated:YES completion:nil];
                                
                            }else{
                                if(self.saveOrEditMode == 3){
                                    
                                    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                                    for (UIViewController *aViewController in allViewControllers) {
                                        if ([aViewController isKindOfClass:[PlaceOrderViewController class]]) {
                                            sharedUtils.pickupAddressModel = nil;
                                        //    sharedUtils.pickupAddressModel = addressObject;
                                            sharedUtils.pickupAddressModel = [sharedUtils.pickUpAddressArray firstObject];
                                            [self.navigationController popToViewController:aViewController animated:NO];
                                        }
                                    }
                                    
                                }else{
                                    PickUpAddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"PickUpAddressViewController"];
                                    [self.navigationController pushViewController:vc animated:YES];
                                    
                                    self.swiftySideMenu.centerViewController = self.navigationController;
                                }
                                
                            }
                        }];
                    }else   if([[result valueForKey:@"code"]intValue] == 211){
                        
                        NSMutableArray *arrayOfAddress;
                        arrayOfAddress = [result valueForKey:@"err_data"];
                        
                        if( [[arrayOfAddress valueForKey:@"copyaddresspass"]intValue]==0){
                            
                            UIAlertController *controller =  [sharedUtils showAlert:@"sorry" withMessage:@"Address already exists!"];
                            
                            [self presentViewController:controller animated:YES completion:nil];
                            
                        }
                        if( [[arrayOfAddress valueForKey:@"countaddresspass"]intValue]==0){
                            
                            UIAlertController *controller =  [sharedUtils showAlert:@"sorry" withMessage:@"You already have three address in your address book!"];
                            
                            [self presentViewController:controller animated:YES completion:nil];
                            
                        }
                        if( [[arrayOfAddress valueForKey:@"labeladdresspass"]intValue]==0){
                            
                        }
                    }
                }else{
                    
                    UIViewController*  controller ;
                    if(result ==nil){
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                    }else{
                        NSLog(@"Result5%@",result);
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                    }
                    [self presentViewController:controller animated:YES completion:nil];
                    
                }
            }
        }];
    }
    else{
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
        
        [self presentViewController:controller animated:YES completion:nil];
        [sharedUtils stopAnimator:self];
    }
}


#pragma mark - Validation method

-(BOOL)validatetextField :(AddressModel*)addresss{
    
    if(![LSUtils checkNilandEmptyString:addresss.addressTitle]){
        
        [[NotificationManager notificationManager] displayMessage:@"Label can not be blank." withType:Negative];
        
        return false;
        
    }
    
    if(![LSUtils checkNilandEmptyString:addresss.addressLine1]){
        
        [[NotificationManager notificationManager] displayMessage:@"Address Line 1 can not be blank." withType:Negative];
        
        return false;
        
    }
    
    if(![LSUtils checkNilandEmptyString:addresss.addressLine2]){
        
        [[NotificationManager notificationManager] displayMessage:@"Address Line 2 can not be blank." withType:Negative];
        
        return false;
        
    }
    
    if(![LSUtils checkNilandEmptyString:addresss.city]){
        
        [[NotificationManager notificationManager] displayMessage:@"City can not be blank." withType:Negative];
        
        return false;
        
    }
    
    if([addresss.phoneNumber length]<10){
        [[NotificationManager notificationManager] displayMessage:@"Phone Number length can not be less than 10." withType:Negative];
        return false;
        
    }
    if(![LSUtils checkNilandEmptyString:addresss.state]){
        
        [[NotificationManager notificationManager] displayMessage:@"State can not be blank." withType:Negative];
        
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:addresss.country]){
        
        [[NotificationManager notificationManager] displayMessage:@"Confirm Password can not be blank." withType:Negative];
        // [self dismissViewControllerAnimated:YES completion:nil];
        
        return false;
    }

    if(![LSUtils checkNilandEmptyString:addresss.zipCode]){
        
        [[NotificationManager notificationManager] displayMessage:@"Zip code can not be blank." withType:Negative];
        
        return false;
    }
    return true;
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
}

@end

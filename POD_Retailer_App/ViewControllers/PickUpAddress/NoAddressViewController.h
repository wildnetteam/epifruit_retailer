//
//  NoAddressViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 20/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickUpAddressPopoverView.h"
#import "PlaceOrderViewController.h"

@interface NoAddressViewController : UIViewController <pickUPAddressDelegate>
@property (weak, nonatomic) IBOutlet UIButton *addAddressButton;
@property (nonatomic) int saveOrEditMode;//1 for save 2 for edit 3 for selecting address


@end

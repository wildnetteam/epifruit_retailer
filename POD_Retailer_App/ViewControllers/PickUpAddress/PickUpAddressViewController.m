//
//  PickUpAddressViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 19/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "PickUpAddressViewController.h"
#import "PickUpAddressTableViewCell.h"
#import "UITableView+DataSourceController.h"
#import "MainDataSourceController.h"
#import "UserManager.h"

@interface PickUpAddressViewController (){
    UIButton *dimView;
    PickUpAddressPopoverView *viewPop;
}

@end

@implementation PickUpAddressViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUIElement];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    self.navigationItem.rightBarButtonItem.enabled = true;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    if(sharedUtils.pickUpAddressArray.count == 0){
        
        self.navigationItem.rightBarButtonItem.enabled = false;

        if([LSUtils isNetworkConnected]) {
            
            [sharedUtils startAnimator:self];
  
            [sharedUtils getAllPickUpAddress:^(BOOL success, NSDictionary *result, NSError *error) {
                [sharedUtils stopAnimator:self];
                
                if (error != nil) {
                    // handel error part here
                    UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:error.localizedDescription];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                    
                }
                if(success){
                    
                    if([[result valueForKey:@"code"]intValue] == 200){
                        
                        self.pickUpAddressArray = nil;
                        
                        [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
                    }
                    if(sharedUtils.pickUpAddressArray.count>=3){
                        
                        self.navigationItem.rightBarButtonItem.enabled = false;
                        self.navigationItem.rightBarButtonItem.customView.alpha = 0.1;
                    }
                    if(sharedUtils.pickUpAddressArray.count==0){
                        
                        [self goToNoAddressViewController];
                    }
                    if(sharedUtils.pickUpAddressArray.count>=3){
                        
                        self.navigationItem.rightBarButtonItem.enabled = false;
                    }
                }else{
                    UIViewController *controller;
                    if(result == nil){
                        
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                    }else{
                        NSLog(@"%@",result);
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem occured."];
                    }
                    [self presentViewController:controller animated:YES completion:nil];
                }
                
            }];
        }else{
            [sharedUtils stopAnimator:self];
            UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected"];
            [self presentViewController:controller animated:YES completion:nil];
        }
    }else{
        
        self.navigationItem.rightBarButtonItem.enabled = true;
 
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];

    UIButton* addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setFrame:CGRectMake(0, 0,50, 50)];
    [addButton addTarget:self action:@selector(addAddress) forControlEvents:UIControlEventTouchUpInside];
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:addButton]];
    
    self.navigationItem.rightBarButtonItem.customView.alpha = 1;

    if( _pickUpAddressArray.count >= 3){
        
        self.navigationItem.rightBarButtonItem.enabled = false;
        self.navigationItem.rightBarButtonItem.customView.alpha = 0.1;
        
    }
}
-(void)goToNoAddressViewController{
    
  NoAddressViewController* vc = (NoAddressViewController*)[sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NoAddressViewController"];
    vc.saveOrEditMode = _saveOrEditMode;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    
//    if(_saveOrEditMode != 3)
//   // _saveOrEditMode = 0;
    [super viewWillDisappear:animated];
 
}

+(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
-(void)setUIElement{
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    self.title =@"";
    
    if(_saveOrEditMode != 3){
       
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
        barButtonItem.tintColor = [UIColor blackColor];
        
        self.navigationItem.leftBarButtonItem = barButtonItem;
        
    }else{
        
        //View controller need to have both menu and back button
        
        ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
        
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
        barButtonItem.tintColor = [UIColor blackColor];
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
        
    }
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];

    MainDataSourceController *dataSourceCtrl = [[MainDataSourceController alloc] init];
    dataSourceCtrl.tableView = _tableView;
    dataSourceCtrl.viewController = self;
    _tableView.dataSourceController = (id) dataSourceCtrl;
    
    // load data source
    [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
    
}
// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}


-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)backButtonClicked:(id)sender{
    
    if(_isprocessing == true){
        NSLog(@"processing");
        return;
    }
    sharedUtils.pickupAddressModel = _modelWhenNotChanged;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addAddress {
    
    [dimView removeFromSuperview];
    dimView = nil;
    dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    viewPop= [[PickUpAddressPopoverView alloc] init];
    viewPop.pickUPAddressDel = (id) self;
    viewPop.parentControllerTypePickUP = ADDAddressControllerPick;

    viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
    
                         }
                         completion:^(BOOL finished){
                         }];
    [UIView commitAnimations];
    viewPop.backgroundColor=[UIColor whiteColor];
    //Arpana latest change
    if(_saveOrEditMode != 3)
    _saveOrEditMode = 0;
    [dimView addSubview:viewPop];
     [self.view addSubview:dimView];
}
-(void)saveData:(AddressModel*)addressObject canUpdate:(BOOL)isEdited{
    
    if([LSUtils isNetworkConnected]) {

    self.navigationItem.rightBarButtonItem.enabled = false;

    if(_saveOrEditMode == 2  || (_saveOrEditMode == 3 && isEdited)){
        [sharedUtils startAnimator:self];

        //update address
        [sharedUtils updatePickUpAddress:addressObject Completion:^(BOOL success, NSDictionary *result, NSError *error) {
            [sharedUtils stopAnimator:self];
                        
            self.navigationItem.rightBarButtonItem.enabled = true;

            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                [self presentViewController:controller animated:YES completion:nil];
            }
            if(success){
               // self.tableView.userInteractionEnabled = false;

                if([[result valueForKey:@"code"]intValue] == 200){
                   
                    UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Address updated successfully."];
                    [self presentViewController:controller animated:YES completion:nil];
                    
                    MBProgressHUD* progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    progressHUD.labelText=@"Updating UI...";

                    sharedUtils.pickupAddressModel = nil;
                    sharedUtils.pickupAddressModel = addressObject;
                    [sharedUtils getAllPickUpAddress:^(BOOL success, NSDictionary *result, NSError *error) {
                       // self.tableView.userInteractionEnabled = true;
                        [MBProgressHUD hideHUDForView:self.view animated:YES];

                        if (error != nil) {
                            // handel error part here

                            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                            
                            [self presentViewController:controller animated:YES completion:nil];
                        }else{
                            if(success){
                                if([[result valueForKey:@"code"]intValue] == 200){
                                    self.pickUpAddressArray = nil;
                                    [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
                                }
                            }
                            if(sharedUtils.pickUpAddressArray.count>0){
                                
                            }
                            if(sharedUtils.pickUpAddressArray.count>=3){
                                
                                self.navigationItem.rightBarButtonItem.enabled = false;
                                self.navigationItem.rightBarButtonItem.customView.alpha = 0.1;
                            }
                        }
                        
                    }];
                }
                else  if([[result valueForKey:@"code"]intValue] == 211){
                    NSMutableArray *arrayOfAddress;
                    arrayOfAddress = [result valueForKey:@"err_data"];
                    
                    if( [[arrayOfAddress valueForKey:@"copyaddresspass"]intValue]==0){
                        
                        UIAlertController *controller =  [sharedUtils showAlert:@"sorry" withMessage:@"Address already exists!"];
                        
                        [self presentViewController:controller animated:YES completion:nil];
                        
                    }
                    if( [[arrayOfAddress valueForKey:@"countaddresspass"]intValue] == 0){
                        
                        UIAlertController *controller =  [sharedUtils showAlert:@"sorry" withMessage:@"You already have three address in your address book!"];
                        
                        [self presentViewController:controller animated:YES completion:nil];
                        
                    }
                    if( [[arrayOfAddress valueForKey:@"labeladdresspass"]intValue]==0){
                        
                    }
                }
                else  if([[result valueForKey:@"code"]intValue] == 300){
                    UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Address could not be updated."];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }else{
                UIViewController*  controller;
                
                if(result == nil){
                    
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                }else{
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem occured."];
                }
                [self presentViewController:controller animated:YES completion:nil];
            }
        }];
    }

    else{
        [sharedUtils startAnimator:self];
        [sharedUtils addAddressToDataBase:addressObject Completion:^(BOOL success, NSDictionary *result, NSError *error){
            [sharedUtils stopAnimator:self];
            self.navigationItem.rightBarButtonItem.enabled = true;

            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                [self presentViewController:controller animated:YES completion:nil];
            }
            if(success){
                if([[result valueForKey:@"code"]intValue] == 200){
                    
                    UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Address added successfully."];
                    [self presentViewController:controller animated:YES completion:nil];
                    
                   MBProgressHUD* progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    progressHUD.labelText=@"Updating UI...";
                    
                    [sharedUtils getAllPickUpAddress:^(BOOL success, NSDictionary *result, NSError *error) {
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];

                        if (error != nil) {
                            // handel error part here
                            UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:error.localizedDescription];
                            
                            [self presentViewController:controller animated:YES completion:nil];
                            
                        }else{
                            
                            self.pickUpAddressArray = nil;
                            if(sharedUtils.pickUpAddressArray.count>0){
                                
                                [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
                            }
                            if(sharedUtils.pickUpAddressArray.count>=3){
                                
                                self.navigationItem.rightBarButtonItem.enabled = false;
                                self.navigationItem.rightBarButtonItem.customView.alpha = 0.1;
                            }
                        }
                    }];
                    
                }else   if([[result valueForKey:@"code"]intValue] == 211){
                    
                    NSMutableArray *arrayOfAddress;
                    arrayOfAddress = [result valueForKey:@"err_data"];
                    
                    if( [[arrayOfAddress valueForKey:@"copyaddresspass"]intValue]==0){
                        
                        UIAlertController *controller =  [sharedUtils showAlert:@"sorry" withMessage:@"Address already exists!"];
                        
                        [self presentViewController:controller animated:YES completion:nil];
                        
                    }
                    if( [[arrayOfAddress valueForKey:@"countaddresspass"]intValue]==0){
                        
                        UIAlertController *controller =  [sharedUtils showAlert:@"sorry" withMessage:@"You already have three address in your address book!"];
                        
                        [self presentViewController:controller animated:YES completion:nil];
                        
                    }
                    if( [[arrayOfAddress valueForKey:@"labeladdresspass"]intValue]==0){
                        
                    }
                }
                if(sharedUtils.pickUpAddressArray.count>=3){
                    
                    self.navigationItem.rightBarButtonItem.enabled = false;
                    self.navigationItem.rightBarButtonItem.customView.alpha = 0.1;
                }
            }
            else{
                UIViewController*  controller;
                if(result == nil){
                    
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                }else{
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem occured."];
                }
                [self presentViewController:controller animated:YES completion:nil];
            }
        }];

    }
    }else{
        [sharedUtils stopAnimator:self];
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected"];
        [self presentViewController:controller animated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

//
//  MyRewardListViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 03/08/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRewardListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *totalEarninglabel;
@property (weak, nonatomic) IBOutlet UILabel *totalpointslabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic)int priviousScreen;

@end

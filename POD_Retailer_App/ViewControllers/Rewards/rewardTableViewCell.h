//
//  rewardTableViewCell.h
//  PODRetailerApp
//
//  Created by Arpana on 03/08/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface rewardTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
@end

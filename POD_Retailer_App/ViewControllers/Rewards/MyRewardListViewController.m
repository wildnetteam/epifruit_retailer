//
//  MyRewardListViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 03/08/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "MyRewardListViewController.h"
#import "rewardTableViewCell.h"
#import "ReferralViewController.h"
@interface MyRewardListViewController ()
{
    UIRefreshControl *btmefreshControl;
    NSMutableArray *rewardArray;
}
@end

@implementation MyRewardListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIElements];
    [self getRewardListCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
        NSLog(@"%@", result);
        
        rewardArray = [[NSMutableArray alloc]init];
        if ([[result objectForKey:@"code"] isEqualToString:@"200"]) {
            
            _totalpointslabel.text = [[result valueForKey:@"data"]objectForKey:@"order_amount_point"];
            _totalEarninglabel.text = [NSString stringWithFormat:@"$%.02f",[[[result valueForKey:@"data"]objectForKey:@"total_amount"] floatValue]];
            
            NSArray* arrayOfDataForDeleveryBoy = [[result valueForKey:@"data"] valueForKey:@"delivery"];
            NSArray* arrayOfDataForVendor = [[result valueForKey:@"data"] valueForKey:@"vendor"];
            NSArray* arrayOfEarningForVendor = [[result valueForKey:@"data"] valueForKey:@"point_earning"];
            for(int index = 0 ; index< arrayOfDataForDeleveryBoy.count ; index++) {
                
                NSDictionary * dict = [arrayOfDataForDeleveryBoy objectAtIndex:index];
                [rewardArray addObject:dict];
            }
            for(int index = 0 ; index< arrayOfDataForVendor.count ; index++) {
                
                NSDictionary * dict = [arrayOfDataForVendor objectAtIndex:index];
                [rewardArray addObject:dict];
            }
            for(int index = 0 ; index< arrayOfEarningForVendor.count ; index++) {
                
                NSDictionary * dict = [arrayOfEarningForVendor objectAtIndex:index];
                [rewardArray addObject:dict];
            }
            [_tableView reloadData];
            
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void)setUIElements{
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    if(_priviousScreen == 1){
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    }else{
        [self.navigationItem setLeftBarButtonItem:barButtonItem];
        
    }
    
    UIButton* inviteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [inviteButton setFrame:CGRectMake(0, 0,150, 50)];
    [inviteButton addTarget:self action:@selector(invitebuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [inviteButton setTitle:@"Refer Others" forState:UIControlStateNormal];
    [inviteButton setTitleColor:AppDefaultBlueColor forState:UIControlStateNormal];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -25;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:inviteButton] , nil]];

    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    //Adding refresh controoler on op to referesh
    btmefreshControl = [[UIRefreshControl alloc]init];
    [self.tableView insertSubview:btmefreshControl atIndex:0];
    self.tableView.alwaysBounceVertical = true;
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
}
-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:true];
}

-(void)invitebuttonClicked:(id)sender{
    
    ReferralViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"ReferralViewController"];
    vc.swiftySideMenu.centerViewController = self.navigationController;
    
//    CATransition* transition = [CATransition animation];
//    transition.duration = 0.2;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:vc animated:true];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)refresh:(id)sender{
    
    if([LSUtils isNetworkConnected]){
        
        rewardArray = nil;
        [_tableView reloadData];
        [self getRewardListCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
            
            rewardArray = [[NSMutableArray alloc]init];
            
            if ([[result objectForKey:@"code"] isEqualToString:@"200"]) {
                
                _totalpointslabel.text = [[result valueForKey:@"data"]objectForKey:@"order_amount_point"];
                _totalEarninglabel.text = [NSString stringWithFormat:@"$%.02f",[[[result valueForKey:@"data"]objectForKey:@"total_amount"] floatValue]];
                
                NSArray* arrayOfDataForDeleveryBoy = [[result valueForKey:@"data"] valueForKey:@"delivery"];
                NSArray* arrayOfDataForVendor = [[result valueForKey:@"data"] valueForKey:@"vendor"];
                NSArray* arrayOfEarningForVendor = [[result valueForKey:@"data"] valueForKey:@"point_earning"];
                for(int index = 0 ; index< arrayOfDataForDeleveryBoy.count ; index++) {
                    
                    NSDictionary * dict = [arrayOfDataForDeleveryBoy objectAtIndex:index];
                    [rewardArray addObject:dict];
                }
                for(int index = 0 ; index< arrayOfDataForVendor.count ; index++) {
                    
                    NSDictionary * dict = [arrayOfDataForVendor objectAtIndex:index];
                    [rewardArray addObject:dict];
                }
                for(int index = 0 ; index< arrayOfEarningForVendor.count ; index++) {
                    
                    NSDictionary * dict = [arrayOfEarningForVendor objectAtIndex:index];
                    [rewardArray addObject:dict];
                }
                [_tableView reloadData];
                
            }
        }];
    }
    else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        } ];
    }
}


-(void)getRewardListCompletion:(void (^) (BOOL success,NSDictionary* result,NSError* error))completion {
    
    if([LSUtils isNetworkConnected]){
        
        if(btmefreshControl.isRefreshing ==false) [sharedUtils startAnimator:self ];
        
        NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",getUserPoints,[UserManager getUserID]];
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [sharedUtils stopAnimator:self];
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data  options:0 error:NULL];
                                                  
                                                  if(responseDictionary == nil){
                                                      
                                                      completion(false, nil, nil);
                                                      
                                                      
                                                  }else
                                                      completion(true,responseDictionary,nil);
                                                  
                                                  
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      completion(true,nil,error);
                                                      
                                                  }else{
                                                      
                                                      completion(false,nil,nil);
                                                      
                                                  }
                                              }
                                              if(btmefreshControl.isRefreshing == true){
                                                  [btmefreshControl endRefreshing ];
                                              }
                                              
                                          });
                                      }];
        if(btmefreshControl.isRefreshing == true)
            [btmefreshControl endRefreshing ];
        
        [task resume];
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  rewardArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    rewardTableViewCell *cell = (rewardTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    if(indexPath.row < rewardArray.count){

    NSDictionary *dict = [rewardArray objectAtIndex:indexPath.row];
    cell.labelName.text =[LSUtils checkNilandEmptyString:[dict objectForKey:@"fname"]]?[dict objectForKey:@"fname"]:@"";
    cell.labelAmount.text = [LSUtils checkNilandEmptyString:[dict objectForKey:@"amount"]]?[NSString stringWithFormat:@"$%.02f",[[dict objectForKey:@"amount"]floatValue]]:@"$0";
    cell.labelTitle.text = [dict objectForKey:@"title"];
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT];
    //Create the date assuming the given string is in GMT
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [df dateFromString:[dict objectForKey:@"created_on"]];
    [df setDateFormat:@"dd MMMM"];
    cell.labelTime.text=[df stringFromDate:date];
    }
    return cell;
}
@end

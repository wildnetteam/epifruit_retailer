//
//  ReferralViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 02/08/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSUtils.h"

@interface ReferralViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *referralCodeLabelView;

@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UIButton *cpRefferalCodeButton;

@property (weak, nonatomic) IBOutlet UILabel *referralCodeLabel;

@property (weak, nonatomic) IBOutlet UILabel *ReferalLabel;

@end

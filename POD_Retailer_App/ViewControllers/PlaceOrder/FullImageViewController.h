//
//  FullImageViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 25/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  DeleteProductCellDelegate <NSObject>

@required
- (void)deleteImage:(int)index;
@end

@interface FullImageViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (copy, nonatomic) UIImage *image;
@property ( nonatomic) int imageIndex;

@property (nonatomic, assign) id <DeleteProductCellDelegate> deleteDelagte;

@end

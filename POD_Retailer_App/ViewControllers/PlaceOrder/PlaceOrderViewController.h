//
//  PlaceOrderViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 25/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField_WithTitle.h"
#import "UIButton_WithTiltleNavigateIcon.h"
#import "PickUpAddressPopoverView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AppDelegate.h"
#import "PickUpAddressViewController.h"
#import "SelectedAddressView.h"
#import "OrderHistoryModel.h"
#import "PreviouOrderViewController.h"

@interface PlaceOrderViewController : UIViewController <UIImagePickerControllerDelegate  , pickUPAddressDelegate, sendSelectedOrderDetailToPreviousControllerDelegate,ChangeAndEditDelegate ,UICollectionViewDelegate,UICollectionViewDataSource , UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *deliverAddresslabel;
@property (weak, nonatomic) IBOutlet UIImageView *dropUpArrow;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryAddressViewConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *pickUpArrow;
@property (weak, nonatomic) IBOutlet UITextField *pickUpAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *delivaryAddressTextField;
@property (weak, nonatomic) IBOutlet UIView *pickUpAddressView;
@property (weak, nonatomic) IBOutlet UIView *deliVeryAddressView;
@property (weak, nonatomic) IBOutlet UIImageView *gallaryImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryAddressViewHeight;

@property (weak, nonatomic) IBOutlet UIImageView *cameraImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UITextField *previousOrdertextField;

@property (weak, nonatomic) IBOutlet UITextField *pTitleTextField;
@property (weak, nonatomic) IBOutlet UITextField * typeTextField;
@property (weak, nonatomic) IBOutlet UITextField * quantityTextField;
@property (weak, nonatomic) IBOutlet UITextField * weightTextfield;
@property (weak, nonatomic) IBOutlet UITextField *costTextField;
@property (weak, nonatomic) IBOutlet UITextField * otherTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *otherTextFieldHeightConstarint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *otherTextFieldTopConstarint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *otherTextFieldBottomConstarint;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet  UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIToolbar* actionToolbar;


//String for pickup and delivery address selected.
@property (nonatomic, retain) NSDictionary* pickUpDict;
@property (nonatomic, retain) NSDictionary* deliveryDict;

@property (nonatomic , retain) AddressModel* pickAddress;
@property (nonatomic , retain) ConsumerAddressModel* DelAddress;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) OrderHistoryModel* previousSelectedOrder;

@end

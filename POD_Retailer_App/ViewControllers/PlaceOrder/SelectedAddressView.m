//
//  SelectedAddressView.m
//  PODRetailerApp
//
//  Created by Arpana on 02/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "SelectedAddressView.h"
#import "PickUpAddressViewController.h"
@implementation SelectedAddressView

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (IBAction)changeButtonClicked:(id)sender {

    //create a delegate of orderdescription screen
    // get order id when selected
    // now pass order id to pick up address controller
    
    if (_changeOrEditDel && [_changeOrEditDel respondsToSelector:@selector(changeAddressObject:)])
        [_changeOrEditDel changeAddressObject:(int)self.tag];
    
}
- (IBAction)editButtonClicked:(id)sender {
    if (_changeOrEditDel && [_changeOrEditDel respondsToSelector:@selector(editAddressObject:)])
        [_changeOrEditDel editAddressObject:(int)self.tag];
}

@end

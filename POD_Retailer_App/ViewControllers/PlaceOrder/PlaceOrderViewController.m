//
//  PlaceOrderViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 25/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "PlaceOrderViewController.h"
#import "BidAmountViewController.h"
#import "UIImage+Scale.h"
#import "UIView+XIB.h"
#import "ProductImageCollectionViewCell.h"
#import "FullImageViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "NotificationManager.h"
#import "OrderDetailsModel.h"
#import "IQActionSheetPickerView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PlaceOrderViewController () <IQActionSheetPickerViewDelegate>

{
    NSMutableArray *imageArray;
    ProductImageCollectionViewCell * tableCell;
    BOOL imageSelectetionCompleted;
    int viewDisappearTag;
    NSArray *weighArray;
    NSMutableArray *typeArray;

    __weak IBOutlet NSLayoutConstraint *toolBarBottomConstraint;
}

@end

@implementation PlaceOrderViewController
{
    __weak IBOutlet NSLayoutConstraint *pickUpViewHeight;
    OrderDetailsModel *Finaldetail;

}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    imageSelectetionCompleted = false;
    weighArray = [[NSArray alloc]initWithObjects:@"0-5 pounds",@"6-10 pounds",@"11-15 pounds",@"16-20 pounds",@"20+ pounds",nil];

    //Get it done in background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{

        [self getproductCarrierTypeCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
            
            if(success){
                if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                    
                    typeArray = [[NSMutableArray alloc]init];
                    
                    NSMutableArray *arrayOfAddress;
                    
                    arrayOfAddress = [result valueForKey:@"data"];
                    
                    for(int index =0 ; index<arrayOfAddress.count ; index++){
                        
                        NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                        
                        [typeArray addObject:[arrayOfdata objectForKey:@"product_type"]];
                    }
                    [typeArray addObject:@"others"];
                }
            }
        }];
    });
    imageArray = [[NSMutableArray alloc]init];
    [imageArray addObject:[UIImage imageNamed:@"image_order_description"]];
    [self setUIElement];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    _pickerView.hidden = true;
    _actionToolbar.hidden = true;
    
    [self placeholderSize:_pickUpAddressTextField :@"Add pickup address..."];
    [self placeholderSize:_delivaryAddressTextField :@"Add Delivery address..."];
    [self placeholderSize:_previousOrdertextField :@"Select previous order..."];
    [self placeholderSize:_costTextField :@"Add Receipt price..."];

    if( viewDisappearTag == 99){
        
        if(sharedUtils.pickupAddressModel!= nil ){
            
            NSLog(@"%@",sharedUtils.pickupAddressModel.addressId);
            NSLog(@"%@",sharedUtils.pickupAddressModel.addressTitle);

            
            //check which is selected pick up or deliver address depending on dictinary...
            _pickUpDict = [[NSDictionary alloc]initWithObjectsAndKeys:@"PickUpAdressSelected",@"title", sharedUtils.pickupAddressModel.addressId,@"ADID", nil];
            
            
            [self fillPickUpOrDeliveryAddress];
        }else{
            if(_pickUpDict==nil){
                for (UIView *subUIView in _pickUpAddressView.subviews) {
                    if([subUIView isKindOfClass:[SelectedAddressView class]]){
                    [subUIView removeFromSuperview];
                    }
                }
                _pickUpAddressTextField.hidden = false;
                _pickUpArrow.hidden = false;
                pickUpViewHeight.constant = 43;

            }
        }
    }
    if( viewDisappearTag == 999){
        
        if(sharedUtils.consumerAddressModel!=nil ){
            
            //check which is selected pick up or deliver address depending on ...
            
            _deliveryDict = [[NSDictionary alloc]initWithObjectsAndKeys:@"DeliveryAdressSelected",@"title", sharedUtils.consumerAddressModel.addressId,@"ADID", nil];
            
            [self fillPickUpOrDeliveryAddress];
        }else{
            if(_deliveryDict==nil){
                for (UIView *subUIView in _deliVeryAddressView.subviews) {
                    if([subUIView isKindOfClass:[SelectedAddressView class]]){
                    [subUIView removeFromSuperview];
                    }
                }
                _delivaryAddressTextField.hidden = false;
                _dropUpArrow.hidden = false;
                _deliveryAddressViewConstraint.constant = 43;

            }
        }
    }
    [self.view layoutIfNeeded];
    
    viewDisappearTag = 0;
}
-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    
    if(viewDisappearTag == 99)//pickup
    {
        _pickAddress =  sharedUtils.pickupAddressModel;
        sharedUtils.pickupAddressModel = nil;
        _pickUpDict= nil;

    }
    if(viewDisappearTag == 999)//delvery
    {
        _DelAddress = sharedUtils.consumerAddressModel;
        sharedUtils.consumerAddressModel = nil;
        _deliveryDict = nil;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void)fillPickUpOrDeliveryAddress{
    
    if(_pickUpDict!=nil && viewDisappearTag == 99){
        
        // custom view
        NSString *nibName =  @"SelectedAddressView";
        SelectedAddressView *view  = [SelectedAddressView initWithNib:nibName bundle:nil];
        if([view isDescendantOfView:_pickUpAddressView]) {
            if([view isKindOfClass:[SelectedAddressView class]]){
            [view removeFromSuperview];
            }
        }
        view.changeOrEditDel =self;
        view.tag = 1001;//for pickup address
        pickUpViewHeight.constant = view.frame.size.height;

        _pickUpAddressTextField.hidden = true;
        _pickUpArrow.hidden = true;
        view.frame = CGRectMake(0,0, _pickUpAddressView.frame.size.width, _pickUpAddressView.frame.size.height);
        
        [_pickUpAddressView addSubview:view];
        [view.addressLabel setAttributedText:[sharedUtils setPickUpFields:sharedUtils.pickupAddressModel]];
        _pickAddress = nil;
        _pickAddress = sharedUtils.pickupAddressModel;
        
    }
    if( _deliveryDict !=nil && viewDisappearTag == 999){
        
        // custom view
        NSString *nibName =  @"SelectedAddressView";
        SelectedAddressView *view  ;
        view = nil;
        view = [SelectedAddressView initWithNib:nibName bundle:nil];
        if([view isDescendantOfView:_deliVeryAddressView]) {
            [view removeFromSuperview];
        }
        view.changeOrEditDel =self;
        view.tag = 1002;//for deliver address

        _deliveryAddressViewConstraint.constant = view.frame.size.height;
        
        _delivaryAddressTextField.hidden = true;
        _dropUpArrow.hidden = true;
        view.frame = CGRectMake(0,0, _deliVeryAddressView.frame.size.width, _deliVeryAddressView.frame.size.height);
        
        [_deliVeryAddressView addSubview:view];

        [view.addressLabel setAttributedText:[sharedUtils setPickUpFieldsForDeliveryAddress:sharedUtils.consumerAddressModel]];
        _DelAddress = nil ;
        _DelAddress = sharedUtils.consumerAddressModel;
    }

}

-(void)changeAddressObject:(int)viewtag {
    NSLog(@"Change button pressed");
    
  
    switch (viewtag) {
            
        case 1001:{
            
          //  [self reinitializeTransportAndDriverOnPickUpChanges];

            viewDisappearTag = 99; // fro pickup
            if(_pickUpDict!=nil){
                for (UIView *subUIView in _pickUpAddressView.subviews) {
                    if([subUIView isKindOfClass:[SelectedAddressView class]]){

                    [subUIView removeFromSuperview];
                    }
                }
            }
            PickUpAddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"PickUpAddressViewController"];
            vc.saveOrEditMode = 3;
            vc.PickupID = [_pickUpDict valueForKey:@"ADID"] ;
            vc.modelWhenNotChanged = sharedUtils.pickupAddressModel;
            [self.navigationController pushViewController:vc animated:YES];
            [AppDelegate getAppDelegate].SwiftySideMenu.centerViewController = self.navigationController;
            
            [self cancelKeyboard];
            
        }
            break;
            
        case 1002:{
         viewDisappearTag = 999; //delivery
            if(_deliveryDict!=nil){
                for (UIView *subUIView in _deliVeryAddressView.subviews) {
                    if([subUIView isKindOfClass:[SelectedAddressView class]]){
                        [subUIView removeFromSuperview];
                    }
                }
            }
            //it is for delivery address
            DeliveryAddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DeliveryAddressViewController"];
            vc.saveOrEditMode = 3;
            vc.DelevryID = [_deliveryDict valueForKey:@"ADID"] ;
            vc.modelWhenNotChanged = sharedUtils.consumerAddressModel;
            [self.navigationController pushViewController:vc animated:YES];
            
            self.swiftySideMenu.centerViewController = self.navigationController;
            
            [self cancelKeyboard];
        }
            break;
    }
}

-(void)editAddressObject:(int)viewtag{
    switch (viewtag) {
            
        case 1001:{
            
            [self reinitializeTransportAndDriverOnPickUpChanges];

            viewDisappearTag = 99;
            // it is foviewwilr pick up address
            UIView *dimView;
            dimView = nil;
            dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
           PickUpAddressPopoverView* viewPop= [[PickUpAddressPopoverView alloc] init];
            viewPop.pickUPAddressDel = self;
            viewPop.isInEditMode = true;
            viewPop.parentControllerTypePickUP = OrderDesControllerPick;
            viewPop.addressObjectDetail = sharedUtils.pickupAddressModel;
            _pickAddress = nil;
            _pickAddress = sharedUtils.pickupAddressModel;

            viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            
            [UIView animateWithDuration:0.4
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                                 
                             }
                             completion:^(BOOL finished){
                             }];
            [UIView commitAnimations];
            viewPop.backgroundColor=[UIColor whiteColor];
            [dimView addSubview:viewPop];
            [self.view addSubview:dimView];
            
        }
            break;
        case 1002:{
            //it is for delivery address
            viewDisappearTag = 999;
           UIView *dimView = nil;
            dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
           DeliveryAddressPopoverView* viewPop= [[DeliveryAddressPopoverView alloc] init];
            viewPop.deliveryAddressDel = (id)self;
            viewPop.isInEditMode = true;
            viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            viewPop.addressObjectDetail = sharedUtils.consumerAddressModel;
            _DelAddress = nil;
            _DelAddress = sharedUtils.consumerAddressModel;
            viewPop.parentControllerType = OrderDesController;
            [UIView animateWithDuration:0.4
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                                 
                             }
                             completion:^(BOOL finished){
                             }];
            [UIView commitAnimations];
            viewPop.backgroundColor=[UIColor whiteColor];
            [dimView addSubview:viewPop];
            [self.view addSubview:dimView];
        }
            break;
    }

}

-(void)updateOrderDescriptioScreen:(AddressModel*)addModel{
    
    if(_pickUpDict!=nil){
        
        for (UIView *subUIView in _pickUpAddressView.subviews) {
            if([subUIView isKindOfClass:[SelectedAddressView class]]){
            [subUIView removeFromSuperview];
            }
        }
    }
    if(sharedUtils.pickupAddressModel!=nil){
        
        //check which is selected pick up or deliver address depending on dictinary...
        _pickUpDict = [[NSDictionary alloc]initWithObjectsAndKeys:@"PickUpAdressSelected",@"title", sharedUtils.pickupAddressModel.addressId,@"ADID", nil];
        viewDisappearTag = 99;
        MBProgressHUD* progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText=@"Updating UI...";
        [self fillPickUpOrDeliveryAddress];
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    }
    [self.view layoutIfNeeded];

}
-(void)updateOrderDescriptioScreenForConsumer:(ConsumerAddressModel*)addModel{
    
    if(_deliveryDict!=nil){
        
        for (UIView *subUIView in _deliVeryAddressView.subviews) {
            if([subUIView isKindOfClass:[SelectedAddressView class]]){
            [subUIView removeFromSuperview];
            }
        }
    }
    if(sharedUtils.consumerAddressModel!=nil){
        
        //check which is selected pick up or deliver address depending on ...
        //_deliverAddressString =@"DeliveryAdressSelected";
        
        _deliveryDict = [[NSDictionary alloc]initWithObjectsAndKeys:@"DeliveryAdressSelected",@"title", sharedUtils.consumerAddressModel.addressId,@"ADID", nil];
        viewDisappearTag = 999;

        [self fillPickUpOrDeliveryAddress];
    }
    [self.view layoutIfNeeded];
}

-(void)setSelectedOrderData:(OrderDetailsModel *)orderDetail{
    
    NSLog(@"Order detail %@", orderDetail.description);
    _pTitleTextField.text = orderDetail.productTitle;
    _weightTextfield.text = orderDetail.productWeight;
    _quantityTextField.text = orderDetail.productQuantity;
    
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",orderDetail.packageType];
    NSArray *resultArray=[typeArray filteredArrayUsingPredicate:pred];
    if (resultArray.count == 0){
         //   NSLog(@"Result  %@", resultArray.description);
        [self updateTextFieldForType:@"others"];
        _otherTextField.text =  orderDetail.packageType;
        _typeTextField.text = @"others";
    }else{
       _typeTextField.text =  orderDetail.packageType;
        [self updateTextFieldForType:@""];
    }
 
    _costTextField.text = [LSUtils checkNilandEmptyString: orderDetail.productCost]? orderDetail.productCost:@"";
    
    if(_pickUpDict!=nil){
        for (UIView *subUIView in _pickUpAddressView.subviews) {
            if([subUIView isKindOfClass:[SelectedAddressView class]]){
                
                [subUIView removeFromSuperview];
            }
        }
    }
    if( _deliveryDict !=nil){
        
        // custom view
        NSString *nibName =  @"SelectedAddressView";
        SelectedAddressView *view  ;
        view = nil;
        view = [SelectedAddressView initWithNib:nibName bundle:nil];
        if([view isDescendantOfView:_deliVeryAddressView]) {
            [view removeFromSuperview];
        }
    }
    sharedUtils.pickupAddressModel = orderDetail.productPickUp;
    sharedUtils.consumerAddressModel = orderDetail.productDelivery;
    
//    //check which is selected pick up or deliver address depending on dictinary...
    _pickUpDict = [[NSDictionary alloc]initWithObjectsAndKeys:@"PickUpAdressSelected",@"title",orderDetail.productPickUp.addressId,@"ADID", nil];
    _deliveryDict = [[NSDictionary alloc]initWithObjectsAndKeys:@"DeliveryAdressSelected",@"title", orderDetail.productDelivery.addressId,@"ADID", nil];
    
    if(_pickUpDict!=nil){
        
        // custom view
        NSString *nibName =  @"SelectedAddressView";
        SelectedAddressView *view  = [SelectedAddressView initWithNib:nibName bundle:nil];
        if([view isDescendantOfView:_pickUpAddressView]) {
            if([view isKindOfClass:[SelectedAddressView class]]){
                [view removeFromSuperview];
            }
        }
        view.editButton.userInteractionEnabled = false;
        view.changeOrEditDel =self;
        view.tag = 1001;//for pickup address
        pickUpViewHeight.constant = view.frame.size.height;
        
        _pickUpAddressTextField.hidden = true;
        _pickUpArrow.hidden = true;
        view.frame = CGRectMake(0,0, _pickUpAddressView.frame.size.width, _pickUpAddressView.frame.size.height);
        
        [_pickUpAddressView addSubview:view];
        [view.addressLabel setAttributedText:[sharedUtils setPickUpFields:sharedUtils.pickupAddressModel]];
        _pickAddress = nil;
        _pickAddress = sharedUtils.pickupAddressModel;
        
    }
    if( _deliveryDict !=nil){
        
        // custom view
        NSString *nibName =  @"SelectedAddressView";
        SelectedAddressView *view  ;
        view = nil;
        view = [SelectedAddressView initWithNib:nibName bundle:nil];
        if([view isDescendantOfView:_deliVeryAddressView]) {
            [view removeFromSuperview];
        }
        view.changeOrEditDel =self;
        view.tag = 1002;//for deliver address
        view.editButton.userInteractionEnabled = false;

        _deliveryAddressViewConstraint.constant = view.frame.size.height;
        
        _delivaryAddressTextField.hidden = true;
        _dropUpArrow.hidden = true;
        view.frame = CGRectMake(0,0, _deliVeryAddressView.frame.size.width, _deliVeryAddressView.frame.size.height);
        
        [_deliVeryAddressView addSubview:view];
        
        [view.addressLabel setAttributedText:[sharedUtils setPickUpFieldsForDeliveryAddress:sharedUtils.consumerAddressModel]];
        _DelAddress = nil ;
        _DelAddress = sharedUtils.consumerAddressModel;
    }
   
    imageArray = [[NSMutableArray alloc]init];
    [imageArray addObject:[UIImage imageNamed:@"image_order_description"]];
    //To set Image
    for (int index =0 ; index<orderDetail.productImagesArray.count; index++) {
        [self loadImage:[NSString stringWithFormat:@"%@/%@", orderDetail.imagebaseURL, [orderDetail.productImagesArray objectAtIndex:index]]];
    }
}


-(void) loadImage:(NSString *)imageLink{
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    [manager loadImageWithURL:[NSURL URLWithString:imageLink] options:SDWebImageDelayPlaceholder progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
       // [imageArray addObject:image];
        [ imageArray insertObject:[image fixOrientation] atIndex:0];

        if(imageArray.count == 5){
            [imageArray removeLastObject];
            imageSelectetionCompleted = true;
        }
        if (imageArray.count == 2) {
            [imageArray removeLastObject];
            UIImage *testImage = [UIImage imageNamed:@"add_image_order_description"];
            [imageArray addObject:testImage];
        }
        [_collectionView reloadData];

    }];
}

-(void)setUIElement{
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;
    self.title = @"";
    self.view.backgroundColor = APP_BACKGROUND_COLOR;

    //Add padding to text field
    [LSUtils addPaddingToTextField:_previousOrdertextField];
    [LSUtils addPaddingToTextField:_pTitleTextField];
    [LSUtils addPaddingToTextField:_pickUpAddressTextField];
    [LSUtils addPaddingToTextField:_delivaryAddressTextField];
    [LSUtils addPaddingToTextField:_quantityTextField];
    [LSUtils addPaddingToTextField:_weightTextfield];
    [LSUtils addPaddingToTextField:_typeTextField];
    [LSUtils addPaddingToTextField:_otherTextField];
    [LSUtils addPaddingToTextField:_costTextField];

    [self placeholderSize:_previousOrdertextField :@"Select previous order..."];
    [self placeholderSize:_pTitleTextField :@"Add product title..."];
    [self placeholderSize:_pickUpAddressTextField :@"Add pickup address..."];
    [self placeholderSize:_quantityTextField :@"Add product quantity..."];
    [self placeholderSize:_weightTextfield :@"Select product weight..."];
    [self placeholderSize:_typeTextField :@"Select package type..."];
    [self placeholderSize:_delivaryAddressTextField :@"Add Delivery address..."];
    [self placeholderSize:_otherTextField :@"Add other package type..."];
    [self placeholderSize:_costTextField :@"Add Receipt price..."];

    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 8;
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;

    [_collectionView registerNib:[UINib nibWithNibName:@"ProductImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:@"All saved data will be lost"
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                                   [self.swiftySideMenu toggleSideMenu];
                                   
                                   if(self.swiftySideMenu.isLeftMenuOpened){
                                       
                                       [self.view endEditing:true];
                                       self.view.userInteractionEnabled = false;
                                       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
                                   }else{
                                       self.view.userInteractionEnabled = true;
                                       [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
                                   }
                                   [self preferredStatusBarStyle];
                                   [self setNeedsStatusBarAppearanceUpdate];
                               }];
    
    [alertController setValue:alertmessage forKey:@"attributedTitle"];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self  presentViewController:alertController animated:NO completion:Nil];

}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)backButtonClicked:(id)sender{
    
    if(Finaldetail != nil){
        
        [self showDataLostAlert];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)showDataLostAlert{
    
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:@"All saved data will be lost"
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self.navigationController popViewControllerAnimated:YES];
                                   
                               }];
    
    [alertController setValue:alertmessage forKey:@"attributedTitle"];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self  presentViewController:alertController animated:NO completion:Nil];
    
    
}
#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    Finaldetail = nil;
}

#pragma mark - collection view delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   // imageArray.count == 0?1:
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    tableCell =(ProductImageCollectionViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if(imageArray.count == 1){
        tableCell.imageViewProduct.image = [UIImage imageNamed:@"image_order_description"];
    }else{

        tableCell.imageViewProduct.image = [[imageArray objectAtIndex:indexPath.row] scaleToSize:CGSizeMake(75, 75)];
    }
    return tableCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(imageArray.count == 1){
        
        [self openImageOption];
    }
    if(imageSelectetionCompleted == true){
        
        FullImageViewController *view = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"FullImageViewController"];
        view.imageIndex =(int) indexPath.row;
        view.image = [imageArray objectAtIndex:indexPath.row];
        view.hidesBottomBarWhenPushed = YES;
        view.deleteDelagte = (id)self;
        
        [self.navigationController pushViewController:view animated:YES];
    }
   else{
            if(indexPath.row ==[NSIndexPath indexPathForRow:[imageArray count]-1 inSection:0] .row &&imageSelectetionCompleted == false){
                
                [self openImageOption];
                
            }else{

                FullImageViewController *view = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"FullImageViewController"];
                view.imageIndex =(int) indexPath.row;
                view.image = [imageArray objectAtIndex:indexPath.row];
                view.deleteDelagte = (id)self;
                [self.navigationController pushViewController:view animated:YES];

            }
        }
}

-(void)deleteImage:(int)index {
    [imageArray removeObjectAtIndex:index];
    if(imageSelectetionCompleted == true){
            [imageArray addObject:[UIImage imageNamed:@"add_image_order_description"]];
    }
    imageSelectetionCompleted = false;
    [self removeImage:[NSString stringWithFormat:@"%d",index ]];
    [_collectionView reloadData];
}

//Give user a option if he wants photo from gallery or take fro camera
-(void)openImageOption {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //open another viewcontroller
                                                       [self takePhotoFromCamera];
                                                       
                                                   }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self getPhotoLibrary];
                                                      }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert.popoverPresentationController setPermittedArrowDirections:0];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(sharedUtils.screenWidth / 2.0 + 20, sharedUtils.screenHeight / 2.0 , 1.0, 1.0);
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Photo related method

//Take from camera
- (void)takePhotoFromCamera {
    
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIViewController *controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Device has no camera"];
        [self presentViewController:controller animated:NO completion:nil];
    } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = (id)self;
        picker.navigationBar.translucent = false;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

//choose photo from gallery
-(void)getPhotoLibrary {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.allowsEditing = NO;
    imagePicker.delegate = self;
    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage ];
    imagePicker.navigationBar.translucent = false;

    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark UIImpagePicker Delegate

//this delegate is called when photo is selected from gallery
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ( [mediaType isEqualToString:@"public.image" ]) {
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
      //  _gallaryImageView.image = [image fixOrientation];
        
        UIImage *resizedImage = [image scaleToSize:CGSizeMake(750, 750)];
        [ imageArray insertObject:[resizedImage fixOrientation] atIndex:0];
        
     //  [ imageArray insertObject:[image fixOrientation] atIndex:0];
    }
    if(imageArray.count == 5){
        [imageArray removeLastObject];
        imageSelectetionCompleted = true;
    }
    if (imageArray.count == 2) {
        [imageArray removeLastObject];
        UIImage *testImage = [UIImage imageNamed:@"add_image_order_description"];
        [imageArray addObject:testImage];
        
    }
  //  picker = nil;
    
    [_collectionView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil]; // close image picker
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:true];
}


#pragma mark - TextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    NSRange spaceRange = [string rangeOfString:@" "];
    if([textField.text length] == 0){
        if (spaceRange.location != NSNotFound)
        {
            return NO;
        }
    }
    if(textField == _quantityTextField || textField == _costTextField ){
        //Only numbers are allowed
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }
    }
    return YES;
}

-(void)reinitializeTransportAndDriverOnPickUpChanges {
    
    Finaldetail.transport = nil;
    Finaldetail.driverCopleteDetails = nil;
    Finaldetail.driverCopleteDetails = [[NSMutableArray alloc]init];
    Finaldetail.transport = [[NSArray alloc]init];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    if(textField == _previousOrdertextField) {
        
        
        PreviouOrderViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"PreviouOrderViewController"];
        vc.senddataDelegate = self;
        [self.navigationController pushViewController:vc animated:YES];
        self.swiftySideMenu.centerViewController = self.navigationController;
        [self cancelKeyboard];
        return false;
    }
    
    if(textField == _pickUpAddressTextField) {
        //  [self reinitializeTransportAndDriverOnPickUpChanges];
        _pickUpAddressTextField.placeholder =@"";
        viewDisappearTag = 99;
        PickUpAddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"PickUpAddressViewController"];
        [self.navigationController pushViewController:vc animated:YES];
        vc.saveOrEditMode = 3;
        self.swiftySideMenu.centerViewController = self.navigationController;
        
        [self cancelKeyboard];
        
        return false;
        
    }
    if(textField == _delivaryAddressTextField){
        
        viewDisappearTag = 999;
        DeliveryAddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DeliveryAddressViewController"];
        vc.saveOrEditMode = 3;
        [self.navigationController pushViewController:vc animated:YES];
        
        self.swiftySideMenu.centerViewController = self.navigationController;
        
        [self cancelKeyboard];
        
        return false;
    }
    if(textField == _typeTextField || textField == _weightTextfield ){
        
        [self initializePickerView:textField];
        return false;
    }
     return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_pickUpAddressTextField)
    {
        if(!([_pickUpAddressTextField.text length]>0))
        {
            [self placeholderSize:_pickUpAddressTextField :@"Add pickup address..."];
            
        }
    }
    
    if (textField==_delivaryAddressTextField)
    {
        if(!([_delivaryAddressTextField.text length]>0))
        {
            [self placeholderSize:_delivaryAddressTextField :@"Add Delivery address..."];
            
        }
    }
    if(!([_pTitleTextField.text length]>0))
    {
        [self placeholderSize:_pTitleTextField :@"Add product title..."];
        
    }
    if(!([_quantityTextField.text length]>0)){
        
        [self placeholderSize:_quantityTextField :@"Add product quantity..."];
    }
    if(!([_costTextField.text length]>0)){
        
        [self placeholderSize:_costTextField :@"Add Receipt price..."];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIView *textFieldSuperView = textField.superview;
    UIResponder* nextResponder = [textFieldSuperView.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not
}

-(void)placeholderSize:(UITextField*)textfield :(NSString*)text{
    UIColor *color = [UIColor lightGrayColor];
    textfield.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Light" size:15.0]
                                                 }
     ];
}

-(void)cancelKeyboard {
    
    [[self view] endEditing:true];
}

-(BOOL)validateTextField {
    
//    if(imageArray.count < 2){
//        
//        [[NotificationManager notificationManager] displayMessage:@"Please select atlease one product image." withType:Negative];
//        
//        return false;
//    }
    if(![LSUtils checkNilandEmptyString:_pTitleTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Product title can not be blank." withType:Negative];
        
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_quantityTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Product quantity can not be blank." withType:Negative];
        
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_weightTextfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Product weight can not be blank." withType:Negative];
        
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_typeTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"package type can not be blank." withType:Negative];
        
        return false;
    }
    
    if( _pickUpDict == nil ){
        
        [[NotificationManager notificationManager] displayMessage:@"Pickup address can not be blank." withType:Negative];
        
        return false;
    }
    if( _deliveryDict == nil){
        
        [[NotificationManager notificationManager] displayMessage:@"Delivery address can not be blank." withType:Negative];
        
        return false;
    }
    return true;
}

- (IBAction)proceeedButtonClicked:(id)sender {
    
    if([self validateTextField]){
        //Reset tag so that pick up and delivery address should not lost
        viewDisappearTag = 0;
        Finaldetail = [[OrderDetailsModel alloc]init];
        Finaldetail.productPickUp = _pickAddress;
        Finaldetail.productDelivery = _DelAddress;
        Finaldetail.productTitle = [LSUtils removeLeadingSpacesfromString:_pTitleTextField.text];
        Finaldetail.productWeight = [LSUtils removeLeadingSpacesfromString:_weightTextfield.text];
        Finaldetail.productQuantity = [LSUtils removeLeadingSpacesfromString:_quantityTextField.text];
        Finaldetail.productCost = [LSUtils removeLeadingSpacesfromString:_costTextField.text];
        int count = 0;
        for(UIImage *image in imageArray){
            
            if ([image isEqual:[UIImage imageNamed:@"image_order_description"]]||[image isEqual:[UIImage imageNamed:@"add_image_order_description"]]) {
                
            }else{
                
                [self saveImage:[image scaleToSize:CGSizeMake(750, 750)] :[NSString stringWithFormat:@"%d",count ]];
                count++;
            }
        }
        if([[LSUtils removeLeadingSpacesfromString:_typeTextField.text] isEqualToString:@"others"]){
            Finaldetail.packageType = [LSUtils removeLeadingSpacesfromString:_otherTextField.text];
            
        }else
            Finaldetail.packageType = [LSUtils removeLeadingSpacesfromString:_typeTextField.text];
        
        BidAmountViewController *placeorderVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"BidAmountViewController"];
        placeorderVC.confirmationDetails = Finaldetail;
        [self.navigationController pushViewController:placeorderVC animated:YES ];
    }
}

#pragma mark - method related to saving and deleting image in document folder

- (void)removeImage:(NSString*)imageName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *imagesPath = [documentsDirectory stringByAppendingPathComponent:directoryImage];
    NSError* error;
    NSString *filePath = [imagesPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void)saveImage:(UIImage*)image :(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(image); //convert image into .png format.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // Get the Document directory path
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    // Create a new path for the new images folder
    NSString *imagesPath = [documentsPath stringByAppendingPathComponent:directoryImage];
    if (![fileManager fileExistsAtPath:imagesPath]) {
        [fileManager createDirectoryAtPath:imagesPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    NSLog(@"Image Path :%@", imagesPath);
    
    NSString *fullPath = [imagesPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]]; //add our image to the path
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
    
    NSLog(@"image saved");
}

#pragma mark - UIPICKER DELEGATE METHODS

-(void)initializePickerView:(UITextField*) textfield{
    
    // tag 21 for type  22 for weight
    IQActionSheetPickerView *picker ;
    if(textfield == _typeTextField){
        picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select package type" delegate:self];
        if(typeArray.count > 0 && typeArray !=nil){
        [picker setTitlesForComponents: @[typeArray]];
        }
        else{
            [sharedUtils startAnimator:self];

            [self getproductCarrierTypeCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
                
                [sharedUtils stopAnimator:self];

                if (error != nil) {
                    // handel error part here
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                    [self presentViewController:controller animated:YES completion:nil];
                }
                if(success){
                    if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                        
                        typeArray = [[NSMutableArray alloc]init];
                        
                        NSMutableArray *arrayOfAddress;
                        
                        arrayOfAddress = [result valueForKey:@"data"];
                        
                        for(int index =0 ; index<arrayOfAddress.count ; index++){
                            
                            NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                            
                            [typeArray addObject:[arrayOfdata objectForKey:@"product_type"]];
                        }
                        [typeArray addObject:@"others"];
                        [picker setTitlesForComponents: @[typeArray]];
                    }else{
                        [picker dismiss];
                    }
                    [picker reloadAllComponents];
                }else{
                    UIViewController*  controller;
                    
                    if(result ==nil){
                        
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                    }else{
                        NSLog(@"%@",result);
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                    }
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }];
        }
    } else if(textfield == _weightTextfield){
        
        picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select product weight" delegate:self];
        [picker setTitlesForComponents: @[weighArray]];
    }
    picker.titleFont = IS_IPHONE5?[UIFont fontWithName:@"Lato-Semibold" size:15.0]:[UIFont fontWithName:@"Lato-Semibold" size:20.0];
    picker.titleColor = [UIColor blackColor];
    picker.pickerComponentsFont = [UIFont fontWithName:@"Lato-Regular" size:20.0];
    
    picker.index = (int) textfield.tag;
    [picker setTag:1];
    [picker show];
    
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles :(int)arrayNo
{
    if( pickerView.index == 21){
        //type textfield
        _typeTextField.text = titles[0];
        [self updateTextFieldForType :_typeTextField.text];
        }else{
            //weight
            _weightTextfield.text = titles[0];
        }
}

-(void)updateTextFieldForType:(NSString*)value {
    
    if([value isEqualToString:@"others"]){

        _otherTextField.hidden = false;
        _otherTextFieldTopConstarint.constant = 20;
        _otherTextFieldHeightConstarint.constant = 43;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
        }];
        
    }else{
    
        _otherTextField.hidden = true;
        _otherTextField.text = @"";
        _otherTextFieldTopConstarint.constant = 0;
        _otherTextFieldHeightConstarint.constant = 0;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
        }];


    }
    [UIView commitAnimations];

}
/*
 {"methodname":"getProductType","token":"ecbcd7eaee29848978134beeecdfbc7c"}
 */
-(void)getproductCarrierTypeCompletion:(void (^) (BOOL success,NSDictionary* result,NSError* error))completion {
    
    NSString *params = [NSString stringWithFormat:@"%@",getProductType];
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data  options:0 error:NULL];
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  completion(false, nil, nil);

                                                  
                                              }else
                                                  completion(true,responseDictionary,nil);
                                          
                                              
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  completion(true,nil,error);
                                                  
                                              }else{
                                                  
                                                  completion(false,nil,nil);

                                              }
                                          }
                                      });
                                  }];
    [task resume];

}
@end

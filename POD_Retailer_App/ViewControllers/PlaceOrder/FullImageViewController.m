//
//  FullImageViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 25/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "FullImageViewController.h"
#import "Constant.h"
//#import "LSUtils.h"
#import "UIImage+Scale.h"
@interface FullImageViewController ()

@end

@implementation FullImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Photos";
    [self setLeftBarBtnItem];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    if(_image) {
        UIImage *resizedImage = [_image scaleToSize:CGSizeMake(400, 400)];

        [_selectedImageView setImage:resizedImage];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}


-(void)viewWillDisappear:(BOOL)animated{
    
    _image = nil;
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setLeftBarBtnItem
{
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_camera"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    UIButton* addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setFrame:CGRectMake(0, 0,100, 50)];
    [addButton addTarget:self action:@selector(deletebuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [addButton setTitle:@"Delete" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor colorWithRed:59.0f/255 green:135.0f/255 blue:242.0f/255 alpha:1] forState:UIControlStateNormal];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -25;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:addButton] , nil]];
    
    //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:addButton]];
    
     self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:59.0f/255 green:135.0f/255 blue:242.0f/255 alpha:1];

}

-(void)deletebuttonClicked:(id)sender{
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:@"Do you want to delete this image?"
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                       
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                                   //save the data to pick up list
                                   
                                   NSLog(@"OK action");
                                   [self.navigationController popViewControllerAnimated:YES];
                                   
                                   if([self.deleteDelagte respondsToSelector:@selector(deleteImage:)]){
                                       [self.deleteDelagte deleteImage:_imageIndex];
                                   }
                               }];
    
    [alertController setValue:alertmessage forKey:@"attributedTitle"];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self  presentViewController:alertController animated:NO completion:Nil];
    
}

-(void)backButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end

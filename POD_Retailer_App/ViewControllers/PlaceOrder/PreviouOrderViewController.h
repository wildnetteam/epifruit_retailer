//
//  PreviouOrderViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 14/07/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailsModel.h"

@protocol sendSelectedOrderDetailToPreviousControllerDelegate <NSObject>

-(void)setSelectedOrderData:(OrderDetailsModel *)orderDetail;

@end

@interface PreviouOrderViewController : UIViewController<UITableViewDelegate, UITableViewDataSource ,UISearchBarDelegate>
{
    UIView *activityView;
    UIActivityIndicatorView *activity;
    BOOL lastPageBool;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic , strong)NSMutableArray *completedArray;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic,strong) NSMutableArray *arrResponse;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, assign) id<sendSelectedOrderDetailToPreviousControllerDelegate> senddataDelegate;

@end

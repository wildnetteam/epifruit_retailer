//
//  PreviouOrderViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 14/07/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "PreviouOrderViewController.h"
#import "MyOrderDescriptionTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "OrderDetailViewController.h"

@interface PreviouOrderViewController (){
    UIRefreshControl *btmefreshControl;
    NSString *serchText;
}

@end

@implementation PreviouOrderViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    [self setUIElement];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    
    _currentPage=1;
    lastPageBool=NO;
    
    [self reloadData:@""];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (_completedArray.count>0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)setUIElement{
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;
    
    _tableView.backgroundColor = [UIColor clearColor];
  
    self.title = @"";
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 8;
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;
    
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0;
    btmefreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview: btmefreshControl];
    _tableView.alwaysBounceVertical = true;
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

-(void)refresh:(id)sender{

    if([LSUtils isNetworkConnected]) {
        [_completedArray removeAllObjects];
        _currentPage=1;
        lastPageBool= NO;
        [self reloadData:@""];
    }
    else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        }];
    }
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - tableView Method

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    activityView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 40)];
    activityView.backgroundColor =[UIColor clearColor];
    activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activity.frame = CGRectMake(CGRectGetMidX(_tableView.frame) -15, CGRectGetMidY(activityView.frame), 30, 30);
    [activityView addSubview:activity];
    _tableView.tableFooterView= activityView;
    return activityView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

        return _completedArray.count;
}

-(NSMutableAttributedString*)setPickUpFieldsForDeliveryAddress:(ConsumerAddressModel*)obj {
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3.0f;
    
    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    if([LSUtils checkNilandEmptyString:obj.addressTitle]){
        
        NSAttributedString *label = [[NSMutableAttributedString alloc]initWithString:obj.addressTitle                                                                            attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:12], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:label];
    }
    
    
//    if([LSUtils checkNilandEmptyString:obj.consumerName]){
//        
//        NSAttributedString *bussinessName = [[NSMutableAttributedString alloc] initWithString:obj.consumerName                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:11], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
//        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
//        
//        [completeTitlestring appendAttributedString:bussinessName];
//        
//    }
    if([LSUtils checkNilandEmptyString:obj.addressLine1]){
        
        NSAttributedString *addressLine1 = [[NSMutableAttributedString alloc] initWithString:obj.addressLine1                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:12], NSFontAttributeName, paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
        [completeTitlestring appendAttributedString:addressLine1];
        
    }
    
    if([LSUtils checkNilandEmptyString:obj.addressLine2]){
        NSAttributedString *addressLine2 = [[NSMutableAttributedString alloc] initWithString:obj.addressLine2                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:12], NSFontAttributeName, nil]];
        
        if([LSUtils checkNilandEmptyString:obj.consumerName]){
            
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
            
        }else
            [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        
        [completeTitlestring appendAttributedString:addressLine2];
    }
    
    if([LSUtils checkNilandEmptyString:obj.city]){
        NSAttributedString *city = [[NSMutableAttributedString alloc] initWithString:obj.city                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:12], NSFontAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
        [completeTitlestring appendAttributedString:city];
    }
    
    if([LSUtils checkNilandEmptyString:obj.state]){
        NSAttributedString *state = [[NSMutableAttributedString alloc] initWithString:obj.state                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:12], NSFontAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [completeTitlestring appendAttributedString:state];
    }
    if([LSUtils checkNilandEmptyString:obj.zipCode]){
        NSAttributedString *zip = [[NSMutableAttributedString alloc] initWithString:obj.zipCode                                                                              attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:12], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
        
        [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@" - "]];
        [completeTitlestring appendAttributedString:zip];
    }
    return completeTitlestring;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(btmefreshControl.isRefreshing  == true){
        [btmefreshControl endRefreshing ];
    }
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier"];
    
    // setup the cell
    MyOrderDescriptionTableViewCell *cell = (MyOrderDescriptionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if(_completedArray.count >0){
    OrderDetailsModel* orderObjectAtIndex =  [_completedArray objectAtIndex:indexPath.row];
    
    
    cell.moreImagesLabel.tag = indexPath.row;
    [cell setPreviousOrderDetails:orderObjectAtIndex];
    
    [cell.timeLabel setAttributedText:[self setPickUpFieldsForDeliveryAddress:orderObjectAtIndex.productDelivery]];
    
    if(orderObjectAtIndex.productImagesArray.count>0)
    [cell.imageViewForProduct sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", orderObjectAtIndex.imagebaseURL, [orderObjectAtIndex.productImagesArray objectAtIndex:0]]]
                                placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
    cell.imageViewForProduct.layer.cornerRadius = 10;
    cell.imageViewForProduct.layer.masksToBounds = true;
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    if(lastPageBool==NO)
    {
        if(indexPath.row==[_completedArray count]-1 )
        {
            if(_currentPage <_totalPages)
            {
                _currentPage=_currentPage+1;
                [self reloadData:@""];
                activity.hidden=NO;
            }
            else
            {
                lastPageBool=YES;
                //increase or decrease content size //_tableNotification setContentSiz
                activity.hidden=YES;
            }
        }
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(_completedArray.count > 0){
        
        OrderDetailsModel* orderObjectAtIndex =  [_completedArray objectAtIndex:indexPath.row];

        if ([self.senddataDelegate respondsToSelector:@selector(setSelectedOrderData:)])
        {
            [self.senddataDelegate setSelectedOrderData:orderObjectAtIndex];
        }
        [self.navigationController popViewControllerAnimated:true];
    }
}

-(void)reloadData:(NSString*)searchText{

    [self getPreviousOrders:searchText :^(BOOL success, NSDictionary *responseDictionary, NSError *error){
        
        if (error != nil) {
            // handel error part here
            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            if(success){
                
                if(responseDictionary == nil){
                    
                    UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                    return ;
                    
                }
                if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                    
                    if (_currentPage==1) {
                        _completedArray = [[NSMutableArray alloc]init];
                        _arrResponse=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"data"]];
                    }
                    else{
                        [_arrResponse addObjectsFromArray:[responseDictionary objectForKey:@"data"]];
                    }
                    if (_arrResponse.count>0) {
                        
                        self.totalPages  = [[responseDictionary objectForKey:@"noofpage"] integerValue];
                    }
                    
                    for(int index =(int)_completedArray.count ; index<_arrResponse.count ; index++){
                        
                        NSDictionary * arrayOfdata = [_arrResponse objectAtIndex:index];
                        
                        OrderDetailsModel *obj = [[OrderDetailsModel alloc]init];
                        
                        obj.productTitle = [arrayOfdata valueForKey:@"product_title"];
                        obj.productWeight = [LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_weight"]]?[arrayOfdata objectForKey:@"product_weight"]:@"" ;
                        obj.productQuantity =[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_quantity"]]?[arrayOfdata objectForKey:@"product_quantity"]:@"";
                        obj.packageType = [LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_type"]]?[arrayOfdata objectForKey:@"product_type"]:@"";
                        obj.maxAmount = [arrayOfdata objectForKey:@"max_rate"];
                        obj.minAmount = [arrayOfdata objectForKey:@"min_rate"];
                        obj.isJumpedPrice = [[arrayOfdata objectForKey:@"jump_price"] intValue];
                        obj.productCost = [arrayOfdata objectForKey:@"product_price"];
                        obj.productPickUp = [arrayOfdata objectForKey:@"pickup_address"];
                        obj.productDelivery = [arrayOfdata objectForKey:@"dropoff_address"];
                        
                        NSArray *imageArray =[arrayOfdata valueForKey:@"product_image"];
                        NSMutableArray* imageNameArray = [[NSMutableArray alloc]init];
                        for(int index = 0; index < imageArray.count; index++){
                            
                            NSDictionary *imageDict =[imageArray objectAtIndex:index];
                            [imageNameArray addObject:[imageDict objectForKey:@"product_image"]];
                        }
                        obj.productImagesArray = imageNameArray;
                        obj.imagebaseURL = [responseDictionary objectForKey:@"imagepath"];
                        obj.productdateAndTime = [arrayOfdata valueForKey:@"created_on"];
                        obj.productOrderNo =[arrayOfdata valueForKey:@"order_number"];
                        
                        //Get pickup address object
                        //Set address Model
                        NSDictionary *pickupDict =[arrayOfdata valueForKey:@"pickup_address"];
                        
                        AddressModel *addressObject = [[AddressModel alloc]initWithAddressParamsAsAddressTitle:[pickupDict valueForKey:@"address_label"]
                                                                                                  AddressLine1:[pickupDict valueForKey:@"address_field_1"]
                                                                                                  AddressLine2:[pickupDict valueForKey:@"address_field_2"]
                                                                                                       ZipCode:[pickupDict valueForKey:@"postcode"]
                                                                                                          City:[pickupDict valueForKey:@"city"]
                                                                                                         State:[pickupDict valueForKey:@"state"] Country:[pickupDict valueForKey:@"country"] PhoneNumber:[pickupDict valueForKey:@"contact_no"] Comment:[arrayOfdata valueForKey:@"comments"] BussinessName:[pickupDict valueForKey:@"bussiness_name"] Longitute:[[pickupDict valueForKey:@"address_long"]floatValue]
                                                                                                      Latitute:[[pickupDict valueForKey:@"address_lat"]floatValue] AddressId:[pickupDict valueForKey:@"id"]];
                        
                        obj.productPickUp = addressObject;
                        
                        
                        //Get delivery address object
                        //Set delivery address Model
                        NSDictionary *dropoffDict =[arrayOfdata valueForKey:@"dropoff_address"];
                        
                        ConsumerAddressModel *delAddressObject = [[ConsumerAddressModel alloc]initWithAddressParamsAsAddressTitle:[dropoffDict valueForKey:@"address_label"]
                                                                                                                     AddressLine1:[dropoffDict valueForKey:@"address_field_1"]
                                                                                                                     AddressLine2:[dropoffDict valueForKey:@"address_field_2"]
                                                                                                                          ZipCode:[dropoffDict valueForKey:@"postcode"]
                                                                                                                             City:[dropoffDict valueForKey:@"city"]
                                                                                                                            State:[dropoffDict valueForKey:@"state"] Country:[dropoffDict valueForKey:@"country"]
                                                                                                                      PhoneNumber:[dropoffDict valueForKey:@"usermobileno"] Comment:[dropoffDict valueForKey:@"comments"]
                                                                                                                         userName:[dropoffDict valueForKey:@"username"]
                                                                                                                        userEmail:[dropoffDict valueForKey:@"useremail"]
                                                                                                                        Longitute:[[dropoffDict valueForKey:@"address_long"]floatValue]
                                                                                                                         Latitute:[[dropoffDict valueForKey:@"address_lat"]floatValue] AddressId:[dropoffDict valueForKey:@"id"]];
                        obj.productDelivery = delAddressObject;
                        
                        [self.completedArray addObject:obj];
                    }
                }
                else {
                    
                    UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                    [self presentViewController:controller animated:YES completion:nil];
                }
                [_tableView reloadData];
                [_searchBar resignFirstResponder];
                [self.view endEditing:true];
                
            }else{
                UIAlertController *controller;
                if(responseDictionary ==nil){
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                }else{
                    NSLog(@"%@",responseDictionary);
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                }
                [self presentViewController:controller animated:YES completion:nil];
            }
        }
    }];
}
#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)getPreviousOrders:(NSString*)searchString :(void (^) (BOOL success,NSDictionary* result,NSError* error))completion{
    
    if(btmefreshControl.isRefreshing ==false){
        
        
    }
    if (_currentPage==1) {
        
        [sharedUtils startAnimator:self];
    }
    
    NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@&page_no=%d&search_string=%@",myPreviousOrderList,[UserManager getUserID],(int) _currentPage , searchString];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              completion(true,responseDictionary,nil);
                                          }
                                          if(error != nil){
                                              
                                              completion(true,nil,error);
                                          }
                                          if(data.length == 0) {
                                              completion(true,nil,nil);
                                          }
                                          if(btmefreshControl.isRefreshing  == true){
                                              [btmefreshControl endRefreshing ];
                                          }
                                      });
                                  }];
    [task resume];
}


#pragma mark - UISearchViewDelegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    // Do things
    [self.searchBar resignFirstResponder];
    _currentPage = 1;
    lastPageBool=YES;
    _totalPages= 0;
    [_completedArray removeAllObjects];
    [self reloadData:searchBar.text];
    [_searchBar resignFirstResponder];
    [self.view endEditing:true];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if([searchText isEqualToString:@""]){
        
        [self.searchBar resignFirstResponder];
        [self.view endEditing:YES];

        _currentPage=1;
        lastPageBool=NO;
        _totalPages=0;
        [self getPreviousOrders:@"" :^(BOOL success, NSDictionary *responseDictionary, NSError *error){
            
            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                
                [self presentViewController:controller animated:YES completion:nil];
            }else{
                if(success){
                    
                    if(responseDictionary == nil){
                        
                        UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                        
                        [self presentViewController:controller animated:YES completion:nil];
                        return ;
                        
                    }
                    if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                        
                        if (_currentPage==1) {
                            _completedArray = [[NSMutableArray alloc]init];
                            _arrResponse=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"data"]];
                        }
                        else{
                            [_arrResponse addObjectsFromArray:[responseDictionary objectForKey:@"data"]];
                        }
                        if (_arrResponse.count>0) {
                            
                            self.totalPages  = [[responseDictionary objectForKey:@"noofpage"] integerValue];
                        }
                        
                        for(int index =0 ; index<_arrResponse.count ; index++){
                            
                            NSDictionary * arrayOfdata = [_arrResponse objectAtIndex:index];
                            
                            OrderDetailsModel *obj = [[OrderDetailsModel alloc]init];
                            
                            obj.productTitle = [arrayOfdata valueForKey:@"product_title"];
                            obj.productWeight = [LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_weight"]]?[arrayOfdata objectForKey:@"product_weight"]:@"" ;
                            obj.productQuantity =[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_quantity"]]?[arrayOfdata objectForKey:@"product_quantity"]:@"";
                            obj.packageType = [LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_type"]]?[arrayOfdata objectForKey:@"product_type"]:@"";
                            obj.maxAmount = [arrayOfdata objectForKey:@"max_rate"];
                            obj.minAmount = [arrayOfdata objectForKey:@"min_rate"];
                            obj.isJumpedPrice = [[arrayOfdata objectForKey:@"jump_price"] intValue];
                            obj.productCost = [arrayOfdata objectForKey:@"product_price"];
                            obj.productPickUp = [arrayOfdata objectForKey:@"pickup_address"];
                            obj.productDelivery = [arrayOfdata objectForKey:@"dropoff_address"];
                            
                            NSArray *imageArray =[arrayOfdata valueForKey:@"product_image"];
                            NSMutableArray* imageNameArray = [[NSMutableArray alloc]init];
                            for(int index = 0; index < imageArray.count; index++){
                                
                                NSDictionary *imageDict =[imageArray objectAtIndex:index];
                                [imageNameArray addObject:[imageDict objectForKey:@"product_image"]];
                            }
                            obj.productImagesArray = imageNameArray;
                            obj.imagebaseURL = [responseDictionary objectForKey:@"imagepath"];
                            obj.productdateAndTime = [arrayOfdata valueForKey:@"created_on"];
                            obj.productOrderNo =[arrayOfdata valueForKey:@"order_number"];
                            
                            //Get pickup address object
                            //Set address Model
                            NSDictionary *pickupDict =[arrayOfdata valueForKey:@"pickup_address"];
                            
                            AddressModel *addressObject = [[AddressModel alloc]initWithAddressParamsAsAddressTitle:[pickupDict valueForKey:@"address_label"]
                                                                                                      AddressLine1:[pickupDict valueForKey:@"address_field_1"]
                                                                                                      AddressLine2:[pickupDict valueForKey:@"address_field_2"]
                                                                                                           ZipCode:[pickupDict valueForKey:@"postcode"]
                                                                                                              City:[pickupDict valueForKey:@"city"]
                                                                                                             State:[pickupDict valueForKey:@"state"] Country:[pickupDict valueForKey:@"country"] PhoneNumber:[pickupDict valueForKey:@"contact_no"] Comment:[arrayOfdata valueForKey:@"comments"] BussinessName:[pickupDict valueForKey:@"bussiness_name"] Longitute:[[pickupDict valueForKey:@"address_long"]floatValue]
                                                                                                          Latitute:[[pickupDict valueForKey:@"address_lat"]floatValue] AddressId:[pickupDict valueForKey:@"id"]];
                            
                            obj.productPickUp = addressObject;
                            
                            
                            //Get delivery address object
                            //Set delivery address Model
                            NSDictionary *dropoffDict =[arrayOfdata valueForKey:@"dropoff_address"];
                            
                            ConsumerAddressModel *delAddressObject = [[ConsumerAddressModel alloc]initWithAddressParamsAsAddressTitle:[dropoffDict valueForKey:@"address_label"]
                                                                                                                         AddressLine1:[dropoffDict valueForKey:@"address_field_1"]
                                                                                                                         AddressLine2:[dropoffDict valueForKey:@"address_field_2"]
                                                                                                                              ZipCode:[dropoffDict valueForKey:@"postcode"]
                                                                                                                                 City:[dropoffDict valueForKey:@"city"]
                                                                                                                                State:[dropoffDict valueForKey:@"state"] Country:[dropoffDict valueForKey:@"country"]
                                                                                                                          PhoneNumber:[dropoffDict valueForKey:@"usermobileno"] Comment:[dropoffDict valueForKey:@"comments"]
                                                                                                                             userName:[dropoffDict valueForKey:@"username"]
                                                                                                                            userEmail:[dropoffDict valueForKey:@"useremail"]
                                                                                                                            Longitute:[[dropoffDict valueForKey:@"address_long"]floatValue]
                                                                                                                             Latitute:[[dropoffDict valueForKey:@"address_lat"]floatValue] AddressId:[dropoffDict valueForKey:@"id"]];
                            obj.productDelivery = delAddressObject;
                            
                            [self.completedArray addObject:obj];
                        }
                    }
                    else {
                        
                        UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                        [self presentViewController:controller animated:YES completion:nil];
                    }
                    [_tableView reloadData];
                    
                }else{
                    UIAlertController * controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }
        }];
    }
}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"placed on%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}

@end


//
//  SelectedAddressView.h
//  PODRetailerApp
//
//  Created by Arpana on 02/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeAndEditDelegate <NSObject>

@required

-(void)changeAddressObject:(int)viewtag;
-(void)editAddressObject:(int)viewtag;

@end

@interface SelectedAddressView : UIView

typedef enum {
    PickUpAddress,
    DeliveryAddress
}AddressController;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIImageView *editImageView;
@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (weak, nonatomic) IBOutlet UIImageView *changeImageView;
@property (nonatomic, retain) NSString* selectedPickUpAddressString;
@property (nonatomic, retain) NSString* selectedDeliveryAddressString;
@property (nonatomic, assign) id<ChangeAndEditDelegate> changeOrEditDel;
@property (nonatomic, retain) NSString* AddressString;


@end

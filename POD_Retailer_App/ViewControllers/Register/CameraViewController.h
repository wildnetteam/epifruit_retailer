//
//  CameraViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 18/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVCamPreviewView.h"
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>

@interface CameraViewController : UIViewController {
    
    AVCaptureSession *CaptureSession;
    AVCaptureDeviceInput *VideoInputDevice;
}

@property (retain) AVCaptureVideoPreviewLayer *PreviewLayer;

@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, weak) IBOutlet AVCamPreviewView *previewView;
@property (weak, nonatomic) IBOutlet UIView *bottomBarView;

@end

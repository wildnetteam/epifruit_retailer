//
//  CameraViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 18/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "CameraViewController.h"
#import "NotificationManager.h"
#import "RegistrationViewController.h"
@interface CameraViewController ()

@end

@implementation CameraViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpCamera];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
   // sharedUtils.profileImagefromCamera = nil;
}
-(void)setUpCamera {
    
    CaptureSession = [[AVCaptureSession alloc] init];
    
    //ADD VIDEO INPUT
    AVCaptureDevice *VideoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (VideoDevice)
    {
        NSError *error;
        
        VideoInputDevice = [[AVCaptureDeviceInput alloc] initWithDevice:[self CameraWithPosition:AVCaptureDevicePositionBack] error:&error];
        
        if (!error)
        {
            if ([CaptureSession canAddInput:VideoInputDevice])
                [CaptureSession addInput:VideoInputDevice];
            else
                NSLog(@"Couldn't add video input");
        }
        else
        {
            NSLog(@"Couldn't create video input");
        }
    }
    else
    {
        [[NotificationManager notificationManager] displayMessage:@"Could not find camera." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"Couldn't create video capture device");
        return;
    }
    
    AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    
    
    if ([CaptureSession canAddOutput:stillImageOutput])
    {
        [stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
        [CaptureSession addOutput:stillImageOutput];
        [self setStillImageOutput:stillImageOutput];
    }
    NSLog(@"Adding video preview layer");
    
    if( VideoInputDevice!=nil){
        [self setPreviewLayer:[[AVCaptureVideoPreviewLayer alloc] initWithSession:CaptureSession]];
        
        [[self PreviewLayer] setVideoGravity:AVLayerVideoGravityResizeAspectFill];

        AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        if ([CaptureSession canAddOutput:stillImageOutput])
        {
            [stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
            [CaptureSession addOutput:stillImageOutput];
            [self setStillImageOutput:stillImageOutput];
        }
        
        //----- DISPLAY THE PREVIEW LAYER -----
        //Display it full screen under out view controller existing controls
        NSLog(@"Display the preview layer");
        
        CGRect layerRect = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetHeight(_bottomBarView.frame));
        
        //[[UIScreen mainScreen]bounds];//[[[self view] layer] bounds];
        [_PreviewLayer setBounds:layerRect];
        [_PreviewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),
                                               CGRectGetMidY(layerRect))];
        //[[[self view] layer] addSublayer:[[self CaptureManager] previewLayer]];
        //We use this instead so it goes on a layer behind our UI controls (avoids us having to manually bring each control to the front):
        UIView *CameraView = [[UIView alloc] init];
        [[self view] addSubview:CameraView];
        [self.view sendSubviewToBack:CameraView];
        
        [[CameraView layer] addSublayer:_PreviewLayer];
        
        //----- START THE CAPTURE SESSION RUNNING -----
        [CaptureSession startRunning];
    }
}

//********** GET CAMERA IN SPECIFIED POSITION IF IT EXISTS **********
- (AVCaptureDevice *) CameraWithPosition:(AVCaptureDevicePosition) Position
{
    NSArray *Devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *Device in Devices)
    {
        if ([Device position] == Position)
        {
            return Device;
        }
    }
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)snapStillImage
{
    
    // Update the orientation on the still image output video connection before capturing.
    [[self stillImageOutput]connectionWithMediaType:AVMediaTypeVideo];
    
    
    // Flash set to Auto for Still Capture
    [CameraViewController setFlashMode:AVCaptureFlashModeAuto forDevice:VideoInputDevice.device];
    
    // Capture a still image.
    [[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        
    }];
    
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
        {
            captureDevice = device;
            break;
        }
    }
    
    return captureDevice;
}

-(void)changeCameraPosition {

    AVCaptureDevice *currentVideoDevice = [VideoInputDevice device];
    AVCaptureDevicePosition preferredPosition = AVCaptureDevicePositionUnspecified;
    AVCaptureDevicePosition currentPosition = [currentVideoDevice position];
    
    switch (currentPosition)
    {
        case AVCaptureDevicePositionUnspecified:
            preferredPosition = AVCaptureDevicePositionBack;
            break;
        case AVCaptureDevicePositionBack:
            preferredPosition = AVCaptureDevicePositionFront;
            break;
        case AVCaptureDevicePositionFront:
            preferredPosition = AVCaptureDevicePositionBack;
            break;
    }
    
    AVCaptureDevice *videoDevice = [CameraViewController deviceWithMediaType:AVMediaTypeVideo preferringPosition:preferredPosition];
    
    AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:nil];
    
    [CaptureSession beginConfiguration];
    
    [CaptureSession removeInput:VideoInputDevice];
    if ([CaptureSession canAddInput:videoDeviceInput])
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:currentVideoDevice];
        
        [CameraViewController setFlashMode:AVCaptureFlashModeAuto forDevice:videoDevice];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:videoDevice];
        
        [CaptureSession addInput:videoDeviceInput];
        VideoInputDevice = videoDeviceInput;
    }
    else
    {
        [CaptureSession addInput:VideoInputDevice];
    }
    [CaptureSession commitConfiguration];
}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
    CGPoint devicePoint = CGPointMake(.5, .5);
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    //  dispatch_async([self sessionQueue], ^{
    AVCaptureDevice *device = [VideoInputDevice device];
    NSError *error = nil;
    if ([device lockForConfiguration:&error])
    {
        if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode])
        {
            [device setFocusMode:focusMode];
            [device setFocusPointOfInterest:point];
        }
        if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode])
        {
            [device setExposureMode:exposureMode];
            [device setExposurePointOfInterest:point];
        }
        [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
        [device unlockForConfiguration];
    }
    else
    {
        NSLog(@"%@", error);
    }

}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
    if ([device hasFlash] && [device isFlashModeSupported:flashMode])
    {
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"%@", error);
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  RegistrationViewController.m
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "RegistrationViewController.h"
#import "UIImage+Scale.h"
#import <AVFoundation/AVFoundation.h>
#import "LoginViewController.h"
#import "NotificationManager.h"
#import "RegisterOTPViewController.h"

@interface RegistrationViewController ()
{
   BOOL isProileImageSelected;
    BOOL termsAccepted;
    __weak IBOutlet UIButton *acceptTermButton;

}

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.view setBackgroundColor:APP_BACKGROUND_COLOR];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self setUpUIElementsValues];
    self.nametextField.text = self.name;
    isProileImageSelected = false;
    if(self.imgUrl)
    {
        NSURL *url = [NSURL URLWithString:self.imgUrl];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        UIImage *tmpImage = [[UIImage alloc] initWithData:data];
        
        [self setprofileImagefarme];
        self.profileImageView.image =  tmpImage;
        isProileImageSelected = true;
    }
    self.emailTextField.text = self.email;
    self.phoneTextField.text = self.phone;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    termsAccepted = false;
    [acceptTermButton setTitle:@"" forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(void)setprofileImagefarme{
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width /2.0;
    self.profileImageView.layer.masksToBounds = YES;
     _profileImageView.clipsToBounds = YES;
}

-(void)setUpUIElementsValues{
    
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    
    self.title = @"";
    
    //Add padding to text field
    [LSUtils addPaddingToTextField:_nametextField];
    [LSUtils addPaddingToTextField:_emailTextField];
    [LSUtils addPaddingToTextField:_phoneTextField];
    [LSUtils addPaddingToTextField:_passwordtextField];
    [LSUtils addPaddingToTextField:_confirmPasswordTextField];
    [LSUtils addPaddingToTextField:_refferalCodeTextField];
    
    //change clear button Image
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, sharedUtils.screenWidth, 50)];
    // numberToolbar.backgroundColor =  [UIColor colorWithHexString:@"#3fcbe4"];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    numberToolbar.barTintColor =[UIColor whiteColor];// [UIColor colorWithHexString:@"#3fcbe4"];
    _phoneTextField.inputAccessoryView = numberToolbar;
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    
    //    sharedUtils.profileImagefromCamera = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (IBAction)uploadProfileImage:(id)sender {
    
    [self openImageOption];
    
}

- (IBAction)verifyCode:(id)sender {
}

//Give user a option if he wants photo from gallery or take fro camera
-(void)openImageOption {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //open another viewcontroller
                                                       [self goToCameraView];
                                                       
                                                   }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self getPhotoLibrary];
                                                      }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert.popoverPresentationController setPermittedArrowDirections:0];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(sharedUtils.screenWidth / 2.0 + 20, sharedUtils.screenHeight / 2.0 , 1.0, 1.0);
    
    [self presentViewController:alert animated:YES completion:nil];
}

//take a snap from cemera
//go to another viewcontroller for clicking photo
-(void)goToCameraView{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIViewController *controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Device has no camera"];
        [self presentViewController:controller animated:NO completion:nil];
        
        
    } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = (id)self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}

//choose photo from gallery
-(void)getPhotoLibrary {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.allowsEditing = NO;
    imagePicker.delegate = self;
    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage ];
    imagePicker.navigationBar.translucent = false;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}

#pragma mark UIImpagePicker Delegate

//this delegate is called when photo is selected from gallery
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ( [mediaType isEqualToString:@"public.image" ]) {
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [self setprofileImagefarme];
        isProileImageSelected = true;
        UIImage *resizedImage =  [image scaleToSize:ProfileImageResizeScale];
        _profileImageView.image= [resizedImage fixOrientation];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)acceptTermsButtonClicked:(id)sender {
    
    if(termsAccepted){
        [acceptTermButton setTitle:@"" forState:UIControlStateNormal];
        termsAccepted = false;
        
    }else{
        [acceptTermButton setTitle:@"check" forState:UIControlStateNormal];
        termsAccepted = true;
        
    }
}

#pragma mark - Validation method

-(BOOL)validatetextField{
    
    if(![LSUtils checkNilandEmptyString:_nametextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Name can not be blank." withType:Negative];
        return false;
        
    }
    
    if(![LSUtils checkNilandEmptyString:_emailTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Email can not be blank." withType:Negative];
        return false;
        
    }
    
    if(![LSUtils validateEmail:_emailTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Email id is wrong." withType:Negative];
        return false;
        
    }
    
    if([LSUtils validateEmail:_emailTextField.text]){
        NSArray *Array = [_emailTextField.text componentsSeparatedByString:@"@"];
        if (Array.count>0) {
            
            NSString *localPartEmail = [Array firstObject];
            if(localPartEmail.length<3 || localPartEmail.length >28){
                
                [[NotificationManager notificationManager] displayMessage:@"Email id length must be range in 3 to 28." withType:Negative];
                return false;
            }
        }
    }
    if(![LSUtils checkNilandEmptyString:_phoneTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Phone Number can not be blank." withType:Negative];
        return false;
        
    }
    if([self getLength:_phoneTextField.text]<10){
        [[NotificationManager notificationManager] displayMessage:@"Phone Number length can not be less than 10." withType:Negative];
        return false;
    }
    if(_nametextField.text.length<2 || _nametextField.text.length >28){
        
        [[NotificationManager notificationManager] displayMessage:@"Name  must be range in 2 to 28." withType:Negative];
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_passwordtextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Password can not be blank." withType:Negative];
        return false;
    }
    if(_passwordtextField.text.length<6 || _passwordtextField.text.length >=21){
        
        [[NotificationManager notificationManager] displayMessage:@"Password must be range in 6 to 21." withType:Negative];
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_confirmPasswordTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Confirm Password can not be blank." withType:Negative];
        return false;
    }
    if(![_passwordtextField.text isEqualToString:_confirmPasswordTextField.text ]){
        
        [[NotificationManager notificationManager] displayMessage:@"Password mismatch." withType:Negative];
        return false;
    }
    if(!termsAccepted){
        [[NotificationManager notificationManager] displayMessage:@"Please accept terms and conditions." withType:Negative];
        return false;
    }
    return true;
}

- (IBAction)signUpButtonPressed:(id)sender {
    
    if([self validatetextField]) {
        
        [sharedUtils  startAnimator:self];
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSString *mobile =  [_phoneTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@"(" withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@")" withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSString *mobileNumberWithCountryCode = [@"+01" stringByAppendingString:mobile];
        
        NSString *params ;
        if(isProileImageSelected == true){
            
           NSString *imgURL =  [UIImagePNGRepresentation(_profileImageView.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
           params = [NSString stringWithFormat:@"%@&mode=%d&vendorEmail=%@&password=%@&fname=%@&mobile=%@&refrence_code=%@&vendor_image=%@",SignUp_Service,1,[LSUtils removeLeadingSpacesfromString:_emailTextField.text],[LSUtils removeLeadingSpacesfromString:_passwordtextField.text],[LSUtils removeLeadingSpacesfromString:_nametextField.text],mobileNumberWithCountryCode,[LSUtils removeLeadingSpacesfromString:_refferalCodeTextField.text],imgURL];
        }else{
            
            params = [NSString stringWithFormat:@"%@&mode=%d&vendorEmail=%@&password=%@&fname=%@&mobile=%@&refrence_code=%@",SignUp_Service,1,[LSUtils removeLeadingSpacesfromString:_emailTextField.text],[LSUtils removeLeadingSpacesfromString:_passwordtextField.text],[LSUtils removeLeadingSpacesfromString:_nametextField.text],mobileNumberWithCountryCode,[LSUtils removeLeadingSpacesfromString:_refferalCodeTextField.text]];
        }
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        params = [params stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData* data, NSURLResponse* response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              UIAlertController *controller;

                                              if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                 options:0
                                                                                                                   error:NULL];

                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"] || [[responseDictionary objectForKey:@"code"]isEqualToString:@"317"]) {
                                                  
                                                  NSMutableArray *arrayOfdata ;
                                                  
                                                  arrayOfdata = [responseDictionary valueForKey:@"data"];
//                                                  UIAlertController * alert = [UIAlertController
//                                                                               alertControllerWithTitle:@"Verify your email"
//                                                                               message:@"We just sent an email to your email id.\nWe need to verify that this email address belongs to you. Check the email and follow the instructions in the message we sent to you."
//                                                                               preferredStyle:UIAlertControllerStyleAlert];
//                                                  
//                                                UIAlertAction* okButton = [UIAlertAction
//                                                                              actionWithTitle:@"Ok"
//                                                                              style:UIAlertActionStyleDefault
//                                                                              handler:^(UIAlertAction * action) {
////                                                                                  [self.navigationController popViewControllerAnimated:true];
//                                                                                  [self goToLoginViewController];
////  
//                                                                              }];
//                                                  
//                                                  [alert addAction:okButton];
//                                                  [self presentViewController:alert animated:YES completion:nil];
                                                
                                                  
                                                  RegisterOTPViewController *otpVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"RegisterOTPViewController"];
                                                  otpVC.otp = [NSString stringWithFormat:@"%@",[[arrayOfdata objectAtIndex:0] objectForKey:@"otp_code"]];
                                                  otpVC.venderID = [[arrayOfdata objectAtIndex:0] objectForKey:@"vendor_id"];
                                                  otpVC.phoneNumber = mobileNumberWithCountryCode;

                                                  [self.navigationController pushViewController:otpVC animated:YES ];

                                              }
                                              else
                                              {
                                                  controller = [sharedUtils showAlert:@"Sorry" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }
                                              else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }                                          });
                                      }];
        [task resume];
    }
}

-(void)backButtonClicked:(id)sender{
   
    LoginViewController *login = (LoginViewController*)[sharedUtils.mainStoryboard
                                                        instantiateViewControllerWithIdentifier: @"LoginViewController"];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, 
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:login animated:NO];
    
}

-(void)goToLoginViewController{
    LoginViewController *login = (LoginViewController*)[sharedUtils.mainStoryboard
                                                        instantiateViewControllerWithIdentifier: @"LoginViewController"];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal,
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:login animated:NO];
}
#pragma mark - TextFieldDelegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==_nametextField) {
        
        UIButton *nameClearButton = [_nametextField valueForKey:@"_clearButton"];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];

        if ([self.nametextField.text length]>0) {
            self.nameImageView.hidden=true;
        }
 

    }
    
        if (textField==_emailTextField) {
            UIButton *emailClearButton = [_emailTextField valueForKey:@"_clearButton"];
            [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
            [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];

            if ([self.emailTextField.text length]>0) {
                self.emailImageView.hidden=true;
            }
            
        }
    
        if (textField==_phoneTextField) {
            
            UIButton *phoneClearButton = [_phoneTextField valueForKey:@"_clearButton"];
            [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
            [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];

            if ([self.phoneTextField.text length]>0) {
                self.phoneImageView.hidden=true;
            }
            
        }
    
        if (textField==_passwordtextField) {
            
            UIButton *passClearButton = [_passwordtextField valueForKey:@"_clearButton"];
            [passClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
            [passClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
            
            if ([self.passwordtextField.text length]>0) {
                self.passwordImageView.hidden=true;
            }
            
        }
    
        if (textField==_confirmPasswordTextField) {
            
            UIButton *confirmPassClearButton = [_confirmPasswordTextField valueForKey:@"_clearButton"];
            [confirmPassClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
            [confirmPassClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
            

            if ([self.confirmPasswordTextField.text length]>0) {
                self.confirmPasswordImageview.hidden=true;
            }
            
        }
    if (textField==_refferalCodeTextField) {
        
        UIButton *refferalCodeButton = [_refferalCodeTextField valueForKey:@"_clearButton"];
        [refferalCodeButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [refferalCodeButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.refferalCodeTextField.text length]>0) {
            self.refferalCodeImageview.hidden=true;
        }
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.nameImageView.hidden=false;
    self.emailImageView.hidden=false;
    self.phoneImageView.hidden=false;
    self.passwordImageView.hidden=false;
    self.confirmPasswordImageview.hidden=false;
    self.refferalCodeImageview.hidden=false;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if(textField == self.refferalCodeTextField){
        //check if referl code field textfield is cleared enable register button
        if ([self.refferalCodeTextField.text length] == 0){
            
            _registerButton.enabled = true;
        }else
            _registerButton.enabled = false;
    }
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIView *textFieldSuperView = textField.superview;
    UIResponder* nextResponder = [textFieldSuperView.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {

        if (([self.refferalCodeTextField.text length] == 1) && [string isEqualToString:@""]) {
            _registerButton.enabled = true;
        }
        return YES;
    }

    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    if(textField == self.nametextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.nameImageView.hidden = true;
        }
        
        if (([self.nametextField.text length] == 1) && [string isEqualToString:@""]) {
            self.nameImageView.hidden = false;
            return false;
        }
        
        if(_nametextField.text.length >= 28 ){
            
            
            [[NotificationManager notificationManager] displayMessage:@"Name must be of length 2 to 28." withType:Negative];
            
            return false;
        }
        return true;
    }
    
    if(textField == self.emailTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.emailImageView.hidden = true;
        }
        
        if (([self.emailTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.emailImageView.hidden = false;
        }
        
    }
    
    if(textField == self.refferalCodeTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.refferalCodeImageview.hidden = true;
        }
        
        if (([self.refferalCodeTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.refferalCodeImageview.hidden = false;
        }
            _registerButton.enabled = false;
    }
    
    if(textField == self.passwordtextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.passwordImageView.hidden = true;
        }
        
        if (([self.passwordtextField.text length] == 1) && [string isEqualToString:@""]) {
            self.passwordImageView.hidden = false;
        }
        
        if(_passwordtextField.text.length >= 21 ){
            
            
            [[NotificationManager notificationManager] displayMessage:@"Password must be of length 6 to 21." withType:Negative];
            
            return false;
        }

        
    }
    
    if(textField == self.confirmPasswordTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.confirmPasswordImageview.hidden = true;
        }
        
        if (([self.confirmPasswordTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.confirmPasswordImageview.hidden = false;
        }
        
    }
    
    
    if(textField == _phoneTextField ) {
        
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.phoneImageView.hidden = true;
        }
        
        if (([self.phoneTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.phoneImageView.hidden = false;
        }
        
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }

        int length = (int)[self getLength:textField.text];
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
    }
    return YES;
}



- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
    }
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];

    return length;
}


-(void)cancelNumberPad{
    [_phoneTextField resignFirstResponder];
    _phoneTextField.text = @"";
}

-(void)doneWithNumberPad{
    // NSString *numberFromTheKeyboard = _phoneTextField.text;
    [_phoneTextField resignFirstResponder];
}
// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:true];
}

- (IBAction)verifyRefferralCode:(id)sender  {
    
    [_refferalCodeTextField resignFirstResponder];
    
    if(![LSUtils checkNilandEmptyString:_refferalCodeTextField.text]){
        [[NotificationManager notificationManager] displayMessage:@"Please enter referral code." withType:Negative];
        return;
    }
    if([LSUtils isNetworkConnected])
    {
        self.view.userInteractionEnabled = NO;
        
        NSString *params = [NSString stringWithFormat:@"%@&refrence_code=%@",validateRefrenceCode,_refferalCodeTextField.text];
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              self.view.userInteractionEnabled = YES;
                                              _registerButton.enabled= true;
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];

                                                  if(responseDictionary == nil){
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  UIAlertController *controller ;
                                                  
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  else {
                                                      
                                                      controller =   [sharedUtils showAlert:@"Sorry" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}

@end

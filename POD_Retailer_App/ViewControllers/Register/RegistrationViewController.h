//
//  RegistrationViewController.h
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "UITextField_WithIcon.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UserManager.h"

@interface RegistrationViewController : UIViewController <UIActionSheetDelegate , UIImagePickerControllerDelegate , UITextFieldDelegate, UINavigationControllerDelegate>{
   
}

@property(nonatomic) NSString *name;
@property(nonatomic) NSString *email;
@property(nonatomic) NSString *phone;
@property(nonatomic) NSString *imgUrl;


@property (weak, nonatomic) IBOutlet UIImageView *nameImageView;
@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImageView;
@property (weak, nonatomic) IBOutlet UIImageView *confirmPasswordImageview;
@property (weak, nonatomic) IBOutlet UIImageView *refferalCodeImageview;

@property (weak, nonatomic) IBOutlet UILabel *registerTitleLabel;
@property (retain, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *userEmailText;
@property (weak, nonatomic) IBOutlet UIView *userFirstNameText;

@property (weak, nonatomic) IBOutlet UIView *userPasswordText;
@property (weak, nonatomic) IBOutlet UIView *userConfirmPasswordNameText;
@property (weak, nonatomic) IBOutlet UIView *userMobileNumberText;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *nametextField;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *passwordlabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordtextField;
@property (weak, nonatomic) IBOutlet UILabel *confirmpasswordLabel;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UILabel *refferalCodeLabel;
@property (weak, nonatomic) IBOutlet UITextField* refferalCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;

@property (weak, nonatomic) IBOutlet UIView *contentView;


@end

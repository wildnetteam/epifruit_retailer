//
//  RegisterOTPViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 17/07/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "RegisterOTPViewController.h"
#import "OTPViewController.h"
#import "NotificationManager.h"
#import "SettingsViewController.h"
#import "LoginViewController.h"
@interface RegisterOTPViewController ()

@end

@implementation RegisterOTPViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

-(void)setUpUIElements{
    
    self.title = @"";
    
    UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(0, 0,100, 50)];
    [cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    [self.verifyButton.layer setShadowOffset:CGSizeMake(0, 4)];
    [self.verifyButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.verifyButton.layer setShadowOpacity:0.2];
    _verifyButton.exclusiveTouch = true;
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    [[NotificationManager notificationManager] displayMessage:_otp withType:Normal andDuration:3.0];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)cancelAction:(id)sender {
    
    [[self view] endEditing:true];
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString*)getCode{
    
    NSString *code= [NSString stringWithFormat:@"%@%@%@%@",_firstNo.text,_secondNo.text,_thirdNo.text,_fourthNo.text];
    return code;
}

-(BOOL)ValidateTextField{
    
    if((![LSUtils checkNilandEmptyString:_firstNo.text]) || (![LSUtils checkNilandEmptyString:_secondNo.text]) || (![LSUtils checkNilandEmptyString:_thirdNo.text]) || (![LSUtils checkNilandEmptyString:_fourthNo.text])){
        
        [[NotificationManager notificationManager] displayMessage:@"Fill otp fields." withType:Negative];
        return false;
    }
    return true;
    
}
- (IBAction)verifyOtp:(id)sender  {
    
    if([self ValidateTextField]) {
        
        if([LSUtils isNetworkConnected])
        {
            self.view.userInteractionEnabled = NO;
            
            NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@&otp_code=%@",otpcodeverificationForRegister,_venderID,[self getCode]];
            
            [sharedUtils  startAnimator:self];
            
            NSURL *url = [NSURL URLWithString:Base_URL];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  [sharedUtils stopAnimator:self];
                                                  self.view.userInteractionEnabled = YES;
                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                        options:0
                                                                                                                          error:NULL];
                                                      
#ifdef DEBUG
                                                      NSLog(@"Response:%@",responseDictionary);
                                                      
#endif
                                                      if(responseDictionary == nil){
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                          
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }
                                                      UIAlertController *controller ;
                                                      
                                                      
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                         
                                                          UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                          
                                                          [self presentViewController:controller animated:YES completion:^{
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  
                                                                  LoginViewController *login = [sharedUtils.mainStoryboard
                                                                                                instantiateViewControllerWithIdentifier: @"LoginViewController"];
                                                                  
                                                                  CATransition *animation = [CATransition animation];
                                                                  [animation setType:kCATransitionPush];
                                                                  [animation setDuration:.8];
                                                                  [animation setSubtype:kCATransitionFromTop];
                                                                  [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
                                                                  [[login.view layer] addAnimation:animation forKey:nil];
                                                                  [self.navigationController pushViewController:login animated:true];
                                                              });
                                                          }];
                                                      }
                                                      else {
                                                          
                                                          controller =   [sharedUtils showAlert:@"Sorry" withMessage:[responseDictionary objectForKey:@"message"]];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }
                                                  }else{
                                                      //data is nil
                                                      if(error != nil){
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                          
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }else{
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                          
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                  }
                                              });
                                          }];
            [task resume];
            
        }else{
            
            UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
            
            [self presentViewController:controller animated:YES completion:nil];
            
        }
    }
}

- (IBAction)resendOtp:(id)sender{
    
    if([LSUtils isNetworkConnected])
    {
        self.view.userInteractionEnabled = NO;
        
        NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@&mobile=%@",resendotp,_venderID,_phoneNumber];
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              self.view.userInteractionEnabled = YES;
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  
#ifdef DEBUG
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  
#endif
                                                  if(responseDictionary == nil){
                                                      
                                                      UIAlertController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  UIAlertController *controller ;
                                                  
                                                  
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      [[NotificationManager notificationManager] displayMessage:[NSString stringWithFormat:@"%@",[[[responseDictionary valueForKey:@"data"] objectAtIndex:0] objectForKey:@"otp_code"] ]withType:Normal andDuration:3.0];

                                                  }
                                                  else {
                                                      
                                                      controller =   [sharedUtils showAlert:@"Sorry" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}

-(void)requestOtpClicked:(id)sender{
    
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField == _firstNo){
        [_secondNo becomeFirstResponder];
    }
    else if(textField == _secondNo){
        [_thirdNo becomeFirstResponder];
    }
    else  if(textField == _thirdNo){
        [_fourthNo becomeFirstResponder];
    }
    
    return NO;
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//
//    if(textField.text.length >= 1){
//        if(textField == _firstNo){
//            [_secondNo becomeFirstResponder];
//        }
//        else if(textField == _secondNo){
//            [_thirdNo becomeFirstResponder];
//        }
//        else  if(textField == _thirdNo){
//            [_fourthNo becomeFirstResponder];
//        }
//    }
//    return YES;
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // This allows numeric text only, but also backspace for deletes
    if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
        return NO;
    
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength >= 1) {
        if(textField == _firstNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_secondNo afterDelay:0.2];
        }
        else if(textField == _secondNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_thirdNo afterDelay:0.2];
        }
        else if (textField == _thirdNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_fourthNo afterDelay:0.2];
        }
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == _fourthNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_thirdNo afterDelay:0.1];
        }
        else if (textField == _thirdNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_secondNo afterDelay:0.1];
        }
        else if (textField == _secondNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_firstNo afterDelay:0.1];
        }
    }
    return newLength <= 1;
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}
// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}

@end


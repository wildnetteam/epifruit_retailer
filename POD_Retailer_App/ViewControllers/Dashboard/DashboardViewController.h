//
//  DashboardViewController.h
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBarButtonItem+Badge.h"

@interface DashboardViewController : UIViewController 


@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (nonatomic,strong) UIImage *pimage;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *totalOrderImageView;
@property (weak, nonatomic) IBOutlet UILabel *totalOrderlabel;
@property (weak, nonatomic) IBOutlet UIImageView *totalMoneySpentImageView;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneySpentLabel;
@property (weak, nonatomic) IBOutlet UIButton *placeOrderButton;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneySpentlabel;
@property (weak, nonatomic) IBOutlet UIButton *redirectToOrder;
@property (weak, nonatomic) IBOutlet UIButton *redirectToPoint;
@property (weak, nonatomic) IBOutlet UIView *referralCodeLabelView;

@end

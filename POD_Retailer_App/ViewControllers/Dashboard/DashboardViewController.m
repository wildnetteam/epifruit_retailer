//
//  DashboardViewController.m
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "DashboardViewController.h"
#import "LeftMenuViewController.h"
#import "TTTOrdinalNumberFormatter.h"
#import "PlaceOrderViewController.h"
#import "UserManager.h"
#import "RatingViewController.h"
#import "MyRewardListViewController.h"
#import "OrderHistoryViewController.h"

@implementation DashboardViewController
{
    NSString *todaysDateString;
    __weak IBOutlet NSLayoutConstraint *deviderLandingTopFromMoney;
    __weak IBOutlet NSLayoutConstraint *referViewBottom;
    CAShapeLayer  *border;

}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setWelcomeText];
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(viewDidAppear:) name:NSdidGetUnReadNotificationCount object:nil];
    [self getTotalOders];

}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:false];
    self.view.userInteractionEnabled = true;
    [self setUPUIElements];

    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;
    
    // check If any unrated Orders
    [sharedUtils getRatingStatusCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
        if(error!=nil){
            return ;
        }
        if(result == nil){
            
        }
        if(success){
            
            if( [[result objectForKey:@"code"]isEqualToString:@"200"]){
                
                NSLog(@"Entered into rating scree");
                RatingViewController *rateVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"RatingViewController"];
                [self.navigationController pushViewController:rateVC animated:YES ];
            }
            else   if( [[result objectForKey:@"code"]isEqualToString:@"210"]){
                
            }
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:false];
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.rightBarButtonItem.badgeValue= [UserManager getNotificationCount];
}

-(void)viewWillDisappear:(BOOL)animated {

    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:true];
}

-(void)makeIconImageRoundShape{

    _totalOrderImageView.layer.cornerRadius = _totalOrderImageView.frame.size.width /2.0;
    _totalOrderImageView.layer.masksToBounds = YES;
    _totalMoneySpentImageView.layer.cornerRadius = _totalMoneySpentImageView.frame.size.width /2.0;
    //_totalMoneySpentImageView.layer.masksToBounds = YES;
    _totalMoneySpentImageView.clipsToBounds = YES;
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [self getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}

-(void)setWelcomeText{
    
    todaysDateString= [self getFormatedDateStringFromDate:[NSDate date] inFormat:@"MMM yy, EEEE"];
    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    NSAttributedString *welcomeSubString ;//= [[NSMutableAttributedString alloc] init];
    NSAttributedString *dateSubString ;//= [[NSMutableAttributedString alloc] init];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0f;
    welcomeSubString = [[NSAttributedString alloc]
                 initWithString:@"EpiFruit"
                 attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Semibold" size:20], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
    [completeTitlestring appendAttributedString:welcomeSubString];
    
    [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@" \n"]];
    dateSubString = [[NSAttributedString alloc]
                     initWithString:todaysDateString
                     attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Light" size:16], NSFontAttributeName, nil]];
    [completeTitlestring appendAttributedString:dateSubString];
    [_welcomeLabel setAttributedText:completeTitlestring];
}

-(void)setOrderInformation :(NSString*)oNo {
    
    NSMutableAttributedString * orderString =  [[NSMutableAttributedString alloc] initWithString:@""];
    NSAttributedString *noOfOrderString ;//= [[NSMutableAttributedString alloc] init];
    NSAttributedString *orderTextString ;//= [[NSMutableAttributedString alloc] init];
    noOfOrderString = [[NSAttributedString alloc]
                        initWithString:oNo
                        attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Medium" size:12], NSFontAttributeName, nil]];
    orderTextString = [[NSAttributedString alloc]
                     initWithString:@"Total number of orders"
                     attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    [orderString appendAttributedString:orderTextString];
    [orderString appendAttributedString:[[NSAttributedString alloc] initWithString:@" \n"]];
    [orderString appendAttributedString:noOfOrderString];
    [_totalOrderlabel setAttributedText:orderTextString];
    _orderNoLabel.text =oNo;
}

-(void)setMoneySpentInformation :(NSString*)price{
    
    NSMutableAttributedString * orderString =  [[NSMutableAttributedString alloc] initWithString:@""];
    NSAttributedString *noOfOrderString;
    NSAttributedString *orderTextString ;
    noOfOrderString = [[NSAttributedString alloc]
                       initWithString:[NSString stringWithFormat:@"$%@",price]
                       attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Medium" size:12], NSFontAttributeName, nil]];
    orderTextString = [[NSAttributedString alloc]
                       initWithString:@"Total points earned"
                       attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    [orderString appendAttributedString:orderTextString];
    [orderString appendAttributedString:[[NSAttributedString alloc] initWithString:@" \n"]];
    [_totalMoneySpentLabel setAttributedText:orderTextString];
    [_moneySpentlabel setAttributedText:noOfOrderString];
}

-(void)setUPUIElements{
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    self.title =@"";
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"notification"] style:UIBarButtonItemStylePlain target:self action:@selector(notificatioBtnClick)];
    if(IS_IPHONE5){
        deviderLandingTopFromMoney.constant = 10;
        referViewBottom.constant = 5;

    }
    border = [CAShapeLayer layer];
    border.strokeColor = COLOR_Border.CGColor;//[UIColor colorWithRed:67/255.0f green:37/255.0f blue:83/255.0f alpha:1].CGColor;
    border.fillColor = nil;
    border.lineDashPattern = @[@4, @2];
    [self.referralCodeLabelView.layer addSublayer:border];
    [self.view layoutIfNeeded];

}

-(void)getTotalOders {
    
    if([LSUtils isNetworkConnected]){
        
        [sharedUtils startAnimator:self];
        
        NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",GetTodayOrder, [UserManager getUserID]];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data       options:0  error:NULL];
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSDictionary *detailDictionary;
                                                      NSArray   *details = [responseDictionary valueForKey:@"data"];
                                                      detailDictionary = [details objectAtIndex:0];
                                                      [self setOrderInformation:[detailDictionary valueForKey:@"ongoingorder"] ];
                                                      [self setMoneySpentInformation:[detailDictionary objectForKey:@"costspend"]];
                                                  }
                                              }else{
                                              }
                                          });
                                      }];
        [task resume];
    }else{
       
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}


#pragma mark - Method to handle tap

- (IBAction)rewardButtonTapped:(id)sender {
    MyRewardListViewController*  vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                             @"MyRewardListViewController"];
    vc.priviousScreen = 1;
    [self.navigationController pushViewController:vc animated:YES ];
}
- (IBAction)orderButtonTapped:(id)sender {
    
    OrderHistoryViewController *VC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"OrderHistoryViewController"];
    VC.priviousScreen = 1;
    [self.navigationController pushViewController:VC animated:YES ];
}

//for converting no to ordinal  value
-(NSString*)getDateWithOrdinalValue:(NSString*)dateString{
    
    TTTOrdinalNumberFormatter *ordinalNumberFormatter = [[TTTOrdinalNumberFormatter alloc] init];
    NSNumber *number = [NSNumber numberWithInt:[dateString intValue]];
    return [NSString stringWithFormat:@"%@", [ordinalNumberFormatter stringFromNumber:number]];
}

- (IBAction)placeOrderButtonClicked:(id)sender {
    
    [LSUtils deleteMyImagesDirectory];
    PlaceOrderViewController *placeorderVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"PlaceOrderViewController"];
    [self.navigationController pushViewController:placeorderVC animated:false ];
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)notificatioBtnClick
{
    UIViewController*  vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NotificationVC"];
    [self.navigationController pushViewController:vc animated:YES];
}


@end

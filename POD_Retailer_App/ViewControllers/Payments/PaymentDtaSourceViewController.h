//
//  PaymentDtaSourceViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 09/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailsModel.h"

@interface PaymentDtaSourceViewController : UIViewController <UITableViewDelegate, UITableViewDataSource >//, ABDataSourceController>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray* cardArray;
@property(nonatomic)int priviousScreen;
@property (nonatomic, retain) OrderDetailsModel* model;
@property (retain, nonatomic) NSMutableArray *imageArray;

@end

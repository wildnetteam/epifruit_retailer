//
//  NoCardViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 10/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "NoCardViewController.h"
#import "AddNewCardViewController.h"
#import "ConfirmOrderViewController.h"
@interface NoCardViewController ()

@end

@implementation NoCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElement];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-
(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)setUpUIElement{
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    self.title =@"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
   // barButtonItem.tintColor = [UIColor whiteColor];
    
    if(_priviousScreen == 1){
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    }else{
        [self.navigationItem setLeftBarButtonItem:barButtonItem];
        
    }
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.AddCard.layer setShadowOffset:CGSizeMake(0, 4)];
    [self.AddCard.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.AddCard.layer setShadowOpacity:0.2];
    [sharedUtils startAnimator:self];
    [sharedUtils getYourCard:^(BOOL success, NSDictionary *result, NSError *error) {
        
        [sharedUtils stopAnimator:self];
        if (error != nil) {
            // handel error part here
            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            if(success){
                if([[result valueForKey:@"code"]intValue] == 200){
                    
                    PaymentDtaSourceViewController *placeorderVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"PaymentDtaSourceViewController"];
                    placeorderVC.priviousScreen =self.priviousScreen;
                    placeorderVC.model = self.orderModel;
                    [self.navigationController pushViewController:placeorderVC animated:YES ];
                }
                
                else  if([[result valueForKey:@"code"]intValue] == 300){
                    
                    UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"No card added yet."];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }else {
                UIViewController*  controller;
                if(result == nil){
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                }else{
                    NSLog(@"%@",result);
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                }
                [self presentViewController:controller animated:YES completion:nil];
            }
        }
    }];
}

-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)backButtonClicked:(id)sender{
    [self.view endEditing:true];

    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[ConfirmOrderViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addNewcard:(id)sender {
    
    
    AddNewCardViewController* vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                                    @"AddNewCardViewController"];
    vc.priviousScreen = self.priviousScreen;
    if(_priviousScreen == 1){
        vc.carryOredrModel = self.orderModel;
    }
    [self.navigationController pushViewController:vc animated:true];
    
    
}

@end

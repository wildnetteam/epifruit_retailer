//
//  RemoveVCard.m
//  AnimatedConstraints
//
//  Created by Keith Harrison http://useyourloaf.com
//  Copyright (c) 2015 Keith Harrison. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//  contributors may be used to endorse or promote products derived from
//  this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.


#import "RemoveVCard.h"

@interface RemoveVCard ()

@property (weak, nonatomic) IBOutlet UIView *yellowView;
@property (weak, nonatomic) IBOutlet UIView *removeView;
#import "CardModel.h"
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *CardViewWidthConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TrailingBetwwenView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardViewSpacingContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *EditViewConstraint;

@end

@implementation RemoveVCard

NSString *modeUserDefaultKey = @"modeUserDefaultKey";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title =@"";
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandle:)];
    rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [rightRecognizer setNumberOfTouchesRequired:1];
    
    //add the your gestureRecognizer , where to detect the touch..
    [self.view addGestureRecognizer:rightRecognizer];
    
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandle:)];
    leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:leftRecognizer];

    [leftRecognizer setNumberOfTouchesRequired:1];
    _enableleftSwipe = true;
    _enableRightSwipe = true;
    
}

-(void)setDetails{
    
}
- (void)rightSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(_enableRightSwipe){
        NSLog(@"rightSwipeHandle");
        [UIView animateWithDuration:.5 animations:^{
            self.cardViewSpacingContraint.constant = self.cardViewSpacingContraint.constant + 100;
            _TrailingBetwwenView.constant = _TrailingBetwwenView.constant - 100 ;
            _enableleftSwipe = true;
            _enableRightSwipe = false;
            [self.view layoutIfNeeded];
        }];
    }
    
}

- (void)leftSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(_enableleftSwipe){
    NSLog(@"leftSwipeHandle");
    [UIView animateWithDuration:.5 animations:^{
        self.cardViewSpacingContraint.constant =  self.cardViewSpacingContraint.constant - 100;
        _TrailingBetwwenView.constant = _TrailingBetwwenView.constant + 100;
        _enableleftSwipe = false;
        _enableRightSwipe = true;
        [self.view layoutIfNeeded];
    }];
    
    }
}

- (IBAction)deleteCardButtonPressed:(id)sender {
    
    NSLog(@"Delete card button pressed");
}

@end

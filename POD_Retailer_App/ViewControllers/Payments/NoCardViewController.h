//
//  NoCardViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 10/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentDtaSourceViewController.h"

@interface NoCardViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *AddCard;
@property(nonatomic)int priviousScreen; //1 for placeorder 0 for sidemenu
@property (nonatomic, retain) OrderDetailsModel* orderModel;

@end

//
//  AddNewCardViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 28/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "AddNewCardViewController.h"
#import "NotificationManager.h"
#import "PaymentDtaSourceViewController.h"
@interface AddNewCardViewController (){
    
    NSString *cardNumber;
    NSString *expiryMonth;
    NSString *expiryYear;
}
@end

@implementation AddNewCardViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUIelElement];

}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [self placeholderSize:_cerditCardTextField :@"Enter your card number..."];
    [self placeholderSize:_validateDateTextField:@"MM/YYYY"];
    [self placeholderSize:_cvvTextField:@"Enter cvv"];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUIelElement {
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    _validateDateTextField.delegate = self;
    _validateDateTextField.keyboardType = UIKeyboardTypeNumberPad;
    _cerditCardTextField.delegate= self;
    _cerditCardTextField.keyboardType = UIKeyboardTypeNumberPad;
    _cvvTextField.delegate = self;
    _cvvTextField.keyboardType = UIKeyboardTypeNumberPad;
    [LSUtils addPaddingToTextField:_cerditCardTextField];
    [LSUtils addPaddingToTextField:_validateDateTextField];
    [LSUtils addPaddingToTextField:_cvvTextField];

    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)]];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor blackColor]];
}

-(void)placeholderSize:(UITextField*)textfield :(NSString*)text {
    
    UIColor *color = [UIColor lightGrayColor];
    textfield.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Light" size:15.0]
                                                 }
     ];
}

#pragma mark- CreditCard Validation

- (NSMutableArray *) toCharArray:(NSString*)str{
    
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[str length]];
    for (int i=0; i < [str length]; i++) {
        NSString *ichar = [NSString stringWithFormat:@"%c", [str characterAtIndex:i]];
        [characters addObject:ichar];
    }
    return characters;
}


-(NSString*)getFormatedDateStringFromDateUTC:(NSDate*)date inFormat:(NSString*)strFormat{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [df stringFromDate:date];
}

-(BOOL)ValidateExpiryDate{
    
    NSDateFormatter* dateFormatterHuman = [[NSDateFormatter alloc] init];
    [dateFormatterHuman setDateFormat:@"MM/yyyy"];
    //        [dictionaryToDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"];

    [dateFormatterHuman setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatterHuman setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate* enteredDate = [dateFormatterHuman dateFromString:_validateDateTextField.text ];
    if(enteredDate == nil){
        return false;
    }
    NSString* todayString = [self getFormatedDateStringFromDateUTC:[NSDate date] inFormat:@"MM/yyyy"];
    NSDate *todayDate = [dateFormatterHuman dateFromString:todayString];
    
    NSComparisonResult result = [todayDate compare:enteredDate];
    switch (result)
    {
        case NSOrderedAscending:
            return true;

            break;
        case NSOrderedDescending:
            return false;
            break;
        case NSOrderedSame:
            return true;
            break;
    }
    return true;
}

-(BOOL) luhnCheck:(NSString *)stringToTest {
    
    stringToTest = [stringToTest stringByReplacingOccurrencesOfString:@" " withString:@""];

    if(stringToTest.doubleValue==0)
        return NO;
    if(_cerditCardTextField.text.length<9 || _cerditCardTextField.text.length>21)
        return NO;
    
    
    NSMutableArray *stringAsChars =[self toCharArray:stringToTest];
    
    BOOL isOdd = YES;
    int oddSum = 0;
    int evenSum = 0;
    
    for (int i = (int) [stringToTest length] - 1; i >= 0; i--) {
        
        int digit = [(NSString *)[stringAsChars objectAtIndex:i] intValue];
        
        if (isOdd)
            oddSum += digit;
        else
            evenSum += digit/5 + (2*digit) % 10;
        
        isOdd = !isOdd;
    }
    
    return ((oddSum + evenSum) % 10 == 0);
}

- (void)CheckButtonClick {
    if(![self luhnCheck:_cerditCardTextField.text]){
        NSLog(@"Invalid credit card");
    }
    else{
        NSLog(@"Valid credit card");
    }
    
}
    
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

- (IBAction)saveCardButtonPressed:(id)sender {
    
    [self Addcard];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    if(textField == _validateDateTextField ) {
        
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }
        
        
        int length = (int)[self getLength:textField.text];
        
        if(length == 6)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 2)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@/",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:2]];
        }
    }
    if(textField == _cerditCardTextField){
        
        __block NSString *text = [textField text];
        
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *newString = @"";
        while (text.length > 0) {
            NSString *subString = [text substringToIndex:MIN(text.length, 4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@" "];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        
        if (newString.length >= 24) {
            return NO;
        }
        
        [textField setText:newString];
        
        return NO;
    }
    if(textField == _cvvTextField){
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }
        
        if([_cvvTextField.text length] >= 4){
            
            return false;
        }
        return  true;
    }
    return  true;
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"/" withString:@""];
    int length = (int)[mobileNumber length];
    if(length > 6)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-6];
        
    }
    return mobileNumber;
}

- (int)getLength:(NSString *)date
{
    date = [date stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    int length = (int)[date length];
    
    return length;
}

-(void)Addcard {
    
    if([self validateTextField]){
        
        [sharedUtils  startAnimator:self];
        
        [self.view setUserInteractionEnabled:false];
        
        NSString  *clientToken = @"sandbox_sjmsgmxt_32k9nszy5j9hr7rs";
        
        cardNumber = [_cerditCardTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *expiryDate = [_validateDateTextField.text stringByReplacingOccurrencesOfString:@"/" withString:@""];
        expiryMonth = [expiryDate substringWithRange:NSMakeRange(0, 2)];
        expiryYear = [expiryDate substringFromIndex:2];
        
        BTAPIClient *braintree = [[BTAPIClient alloc] initWithAuthorization:clientToken];
        BTCardClient *client = [[BTCardClient alloc] initWithAPIClient:braintree];
        BTCard *request = [BTCard new];
        
        request.number = cardNumber;// @"378282246310005";//;
        request.expirationMonth = expiryMonth;//@"05";//expiryMonth;
        request.expirationYear = expiryYear ;//@"2017";//expiryYear;
        request.cvv = _cvvTextField.text;
        [client tokenizeCard:request
                  completion:^(BTCardNonce *nonce, NSError *error) {
                      
                      // Communicate the nonce to your server, or handle error
                      [self postNonceToServer:nonce.nonce];
                  }];
    }
}

- (void)postNonceToServer:(NSString *)paymentMethodNonce{
    
    if([LSUtils isNetworkConnected])
    {
        
        NSString *params = [NSString stringWithFormat:@"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=setpaymentcardinfo&vendor_id=%@&card_nounce=%@",[UserManager getUserID],paymentMethodNonce];
        
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              [self.view setUserInteractionEnabled:true];

                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  
#ifdef DEBUG
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  
#endif
                                                  if([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      PaymentDtaSourceViewController *placeorderVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"PaymentDtaSourceViewController"];
                                                      placeorderVC.priviousScreen = self.priviousScreen;
                                                      placeorderVC.model = self.carryOredrModel;
                                                      [self.navigationController pushViewController:placeorderVC animated:YES ];
                                                      
                                                  }
                                                  else if([[responseDictionary objectForKey:@"code"]isEqualToString:@"81715"]) {
                                                      UIViewController* controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Credit card number is invalid."];
                                                      [self presentViewController:controller animated:YES  completion:nil];
                                                  }else if([[responseDictionary objectForKey:@"code"]isEqualToString:@"81717"]) {
                                                      UIViewController* controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Credit card number is not an accepted  number."];
                                                      [self presentViewController:controller animated:YES  completion:nil];
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"81710"]) {
                                                      UIViewController* controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Expiration date is invalid."];
                                                      [self presentViewController:controller animated:YES  completion:nil];
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"81707"]) {
                                                      UIViewController* controller =   [sharedUtils showAlert:@"Failed" withMessage:@"CVV must be 4 digits for American Express and 3 digits for other card types."];
                                                      [self presentViewController:controller animated:YES  completion:nil];
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"81711"]) {
                                                      UIViewController* controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Expiration date year is invalid. It must be between 1975 and 2200."];
                                                      [self presentViewController:controller animated:YES  completion:nil];
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"81716"]) {
                                                      UIViewController* controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Credit card number must be 12-19 digits."];
                                                      [self presentViewController:controller animated:YES  completion:nil];
                                                  }
                                                 else if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]) {
                                                     
                                                      UIViewController* controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Card could not be saved. Please try again "];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }

}
-(BOOL)validateTextField{
    
    if(![LSUtils checkNilandEmptyString:_cerditCardTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Please enter credit card number. " withType:Negative];
        return false;
        
    }
    
    if(![LSUtils checkNilandEmptyString:_validThrlabel.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Please enter card expiry date." withType:Negative];
        return false;
        
    }
    if(![self ValidateExpiryDate]){
        
        [[NotificationManager notificationManager] displayMessage:@"Please enter valid expiry date." withType:Negative];
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_cvvTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Please enter cvv." withType:Negative];
        
        return false;
        
    }

    if([_cvvTextField.text length]<3){
        [[NotificationManager notificationManager] displayMessage:@"Please enter three digit cvv number." withType:Negative];
        return false;
        
    }
    if([self getLength:_validateDateTextField.text]<6){
        [[NotificationManager notificationManager] displayMessage:@"Please enter expiry in MM/YYYY format." withType:Negative];
        
        return false;
        
    }
    return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField==_cerditCardTextField)
    {
        if(!([_cerditCardTextField.text length]>0))
        {
            [self placeholderSize:_cerditCardTextField :@"Enter your card number..."];
        }
    }
    
    if (textField==_validateDateTextField)
    {
        if(!([_validateDateTextField.text length]>0))
        {
            [self placeholderSize:_validateDateTextField:@"MM/YYYY"];
            
        }
    }
    if (textField==_cvvTextField)
    {
        if(!([_cvvTextField.text length]>0))
        {
            [self placeholderSize:_cvvTextField:@"Enter cvv"];
            
        }
    }
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
        [self.view  endEditing:true];
}

@end

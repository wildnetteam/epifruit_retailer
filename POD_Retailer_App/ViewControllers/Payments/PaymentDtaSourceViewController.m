//
//  PaymentDtaSourceViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 09/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "PaymentDtaSourceViewController.h"
#import "ABMenuTableViewCell.h"
#import "PamentmenuView.h"
#import "PaymentTableViewCell.h"
#import "ConfirmOrderViewController.h"
#import "CardModel.h"
#import "AddNewCardViewController.h"
#import "CompletOrderViewController.h"
static NSString *menuCellIdentifier = @"Default Cell";
static NSString *customCellIdentifier = @"Custom Cell";

@interface PaymentDtaSourceViewController ()

@end

@implementation PaymentDtaSourceViewController{
    
    __weak IBOutlet UIButton *proceedButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElement];
    if(_priviousScreen ==1){
        [self imagesCountOfProduct];
    }
    
    if([LSUtils isNetworkConnected]){
    if(sharedUtils.cardDetails == nil)
    {
        
        [sharedUtils startAnimator:self];
        
        [sharedUtils getYourCard:^(BOOL success, NSDictionary *result, NSError *error) {
            
            [sharedUtils stopAnimator:self];
            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                
                [self presentViewController:controller animated:YES completion:nil];
            }else{
                if(success){
                    if([[result valueForKey:@"code"]intValue] == 200){
                        
                        _cardArray = [[NSMutableArray alloc]initWithObjects:sharedUtils.cardDetails, nil];
                          [_tableView reloadData];

                    }
                    else  if([[result valueForKey:@"code"]intValue] == 300){
                        
                        UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"No card added yet."];
                        
                        [self presentViewController:controller animated:YES completion:nil];
                    }
                }
            }
        }];

    }else
    _cardArray = [[NSMutableArray alloc]initWithObjects:sharedUtils.cardDetails, nil];
    }else{
        [sharedUtils stopAnimator:self];
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
        
        [self presentViewController:controller animated:YES completion:nil];
  
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [self setNeedsStatusBarAppearanceUpdate];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification ];
    [super viewWillDisappear:animated];
}

-(void)setUpUIElement{
    
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = true;
    self.title = @"";
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];

    if(_priviousScreen == 1){
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
        proceedButton.hidden = false;
    }else{
        [self.navigationItem setLeftBarButtonItem:barButtonItem];
        proceedButton.hidden = true;

    }
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    sharedUtils.shouldDisplayBackButtonWithLeftMenu = false;

}

-(void)backButtonClicked:(id)sender{
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[ConfirmOrderViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  _cardArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ABMenuTableViewCell *cell = nil;
    
    cell =[self customCellAtIndexPath:indexPath];
    
    // custom menu view
    NSString *nibName =  @"CardDetailsRightMenu";
    PamentmenuView *menuView = [PamentmenuView initWithNib:nibName bundle:nil];
    menuView.delegate = (id)self;
    menuView.indexPath = indexPath;
    cell.rightMenuView = menuView;
    
    return cell;
}
#pragma mark Private Methods

- (void)loadDataSource {
    
    [self .tableView reloadData];
}


- (ABMenuTableViewCell*)menuCellAtIndexPath:(NSIndexPath*)indexPath {
    ABMenuTableViewCell *cell = (ABMenuTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:menuCellIdentifier];
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark ABCellMenuViewDelegate Methods

- (void)cellMenuViewDeleteBtnTapped:(PamentmenuView *)menuView {
    // update data source
    
    
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:@"Do you want to delete saved card?"
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       // make sure to reload in order to update the custom menu index path for each row
                                       NSMutableArray *rowsToReload = [NSMutableArray array];
                                       for (int i = 0; i <_cardArray.count - menuView.indexPath.row; i++) {
                                           [rowsToReload addObject:[NSIndexPath indexPathForRow:menuView.indexPath.row + i inSection:0]];
                                       }
                                       if(rowsToReload.count >0 )
                                       [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationAutomatic];
                                       
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                                   //save the data to pick up list
                                   
                                   if([LSUtils isNetworkConnected]){
                                       
                                       [self removeCardDetailCompletion:^(BOOL success) {
                                       
                                       if(success){
                                           
                                           [_cardArray removeObjectAtIndex:menuView.indexPath.row];
                                           
                                           
                                           if(_cardArray.count == 0){
                                               AddNewCardViewController* vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                                                                               @"AddNewCardViewController"];
                                               
                                               vc.priviousScreen = self.priviousScreen ;
                                               vc.carryOredrModel = self.model;
                                               [self.navigationController pushViewController:vc animated:true];
                                           }
                                           
                                       }else{
                                           UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Could not remove card details"];
                                           
                                           [self presentViewController:controller animated:YES completion:nil];
                                       }
                                   }];
                                   }else{
                                       
                                       UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
                                       
                                       [self presentViewController:controller animated:YES completion:nil];
                                   }
                                   
                               }];
    
    [alertController setValue:alertmessage forKey:@"attributedTitle"];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];

}

- (PaymentTableViewCell*)customCellAtIndexPath:(NSIndexPath*)indexPath
{
    
    // setup the cell
    PaymentTableViewCell *cell = (PaymentTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
    
    CardModel *model =  [_cardArray objectAtIndex:indexPath.row];
    cell.cardNumberLabel.text = [cell.cardNumberLabel.text stringByAppendingFormat:@" %@", [NSString stringWithFormat:@"**** **** %@",model.cardNumber]];
    cell.expiryLabel.text = [NSString stringWithFormat:@" %@/%@", model.expMonth,model.expYear];
    cell.cardTypeLabel.text = model.cardType;
    
    return cell;
}


#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}


-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)removeCardDetailCompletion:(void (^) (BOOL success))completion{
    
    NSString *params = [NSString stringWithFormat:@"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=removecardinfo&vendor_id=%@",[UserManager getUserID]];
    
    [sharedUtils  startAnimator:self];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              [sharedUtils stopAnimator:self];
#ifdef DEBUG
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              
#endif
                                              if([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  sharedUtils.cardDetails = nil;
                                                  completion(true);
                                                  
                                                 
                                              }
                                              if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]) {
                                                  
                                                  NSLog(@"Data can not be save");
                                                  completion(false);
                                              }
                                              
                                          }
                                                                                    
                                      });
                                  }];
    [task resume];
    
}

- (IBAction)proceedWithPaymentAction:(id)sender {
    
    if([LSUtils isNetworkConnected]){
        
        proceedButton.enabled = false;
        [self saveOrderDetails];
    }else{
        
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (NSString*)createParamForImage{
    
    NSMutableString *imageNames =[[NSMutableString alloc]init];

    //Get All image base 64 string
    for(int j=1;j <= _imageArray.count;j++)
    {
        NSString *imageString = [self convertImageToBase:[_imageArray objectAtIndex:j-1]];
        
        [imageNames appendString:[NSString stringWithFormat:@"&product_image_%d=%@",j,imageString]];
    }
    return [NSString stringWithString: imageNames];
}
-(void)saveOrderDetails{
    
   NSString * images = [self createParamForImage];

    NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@&product_title=%@&product_description=%@&pickup_address=%@&dropoff_address=%@&pickup_address_id=%@&dropoff_address_id=%@&min_rate=%@&max_rate=%@&jump_price=%@&redeemed_point=%@&redeemed_amount=%@&product_type=%@&product_quantity=%@&product_weight=%@&product_price=%@%@",PlaceOrder,[UserManager getUserID],_model.productTitle,_model.productDescription,[sharedUtils getPickUpAddress:_model.productPickUp],[sharedUtils getDeliveryAddress:_model.productDelivery],_model.productPickUp.addressId,_model.productDelivery.addressId,_model.minAmount,_model.maxAmount,[NSString stringWithFormat:@"%d",_model.isJumpedPrice],[NSString stringWithFormat:@"%d",_model.isRedeemPrice],_model.redeemPrice,_model.packageType,_model.productQuantity,_model.productWeight,_model.productCost, images];
    
    MBProgressHUD* progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText=@"Uploading details...";
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          proceedButton.enabled = true;
                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              if([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]){
                                                  
                                                  [LSUtils deleteMyImagesDirectory];
                                                  CompletOrderViewController* vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                                                                                  @"CompletOrderViewController"];
                                                  [self.navigationController pushViewController:vc animated:true];

                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              
                                          }else{
                                              UIViewController*  controller;
                                              
                                              if(error!= nil){
                                                  controller  =   [sharedUtils showAlert:@"Failed" withMessage:error.localizedDescription];
                                              }else
                                                  controller  =   [sharedUtils showAlert:@"Failed" withMessage:@"Server error, please try again."];
                                              
                                              [self presentViewController:controller animated:YES completion:nil];
                                          }
                                      });
                                  }];
    [task resume];
    
}

-(NSArray *)imagesCountOfProduct
{
    // Get the Document directory path
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    // Create a new path for the new images folder
    NSString *documentsDirectory = [documentsPath stringByAppendingPathComponent:directoryImage];
    
    int count;
    _imageArray  = [[NSMutableArray alloc]init];
    
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
        
        if ([[[directoryContent objectAtIndex:count] pathExtension] isEqualToString:@"png"]) {
            //This is Image File with .png Extension
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[directoryContent objectAtIndex:count]];
            [_imageArray addObject: filePath];
        }
    }
    
    return _imageArray;
}


-(NSString*)convertImageToBase:(NSString*)imageStr{
    
    UIImage *img = [UIImage imageWithContentsOfFile:imageStr] ;
    
    NSString *imgURL =  [UIImagePNGRepresentation(img) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return imgURL;
}


@end

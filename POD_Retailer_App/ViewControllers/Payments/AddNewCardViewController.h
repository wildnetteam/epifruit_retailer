//
//  AddNewCardViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 28/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BraintreeCore.h>
#import <Braintree/BTCard.h>
#import <Braintree/BTCardClient.h>
#import "OrderDetailsModel.h"

@interface AddNewCardViewController : UIViewController <UITextFieldDelegate >
@property (weak, nonatomic) IBOutlet UILabel *cvvLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberlabel;
@property (weak, nonatomic) IBOutlet UILabel *validThrlabel;
@property (weak, nonatomic) IBOutlet UITextField *cerditCardTextField;
@property (weak, nonatomic) IBOutlet UITextField *validateDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *cvvTextField;
@property(nonatomic)int priviousScreen;
@property(nonatomic, retain)OrderDetailsModel* carryOredrModel;//1 for placeorder 0 for sidemenu


@end

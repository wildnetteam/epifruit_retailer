//
//  PrivacyPoliciesViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 19/04/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "PrivacyPoliciesViewController.h"

@interface PrivacyPoliciesViewController ()

@end

@implementation PrivacyPoliciesViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // [self getAllOrders:[self getNeededDateFormat:[NSDate date]]];
    [self setUpUIElementsValues];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    [sharedUtils startAnimator:self];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:TermsAndConditionLink]]];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [sharedUtils stopAnimator:self];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [sharedUtils stopAnimator:self];
    [sharedUtils showAlert:@"Failed" withMessage:[NSString stringWithFormat:@"%@",error]];
}


-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

@end



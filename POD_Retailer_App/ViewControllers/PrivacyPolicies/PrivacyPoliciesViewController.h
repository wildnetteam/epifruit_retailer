//
//  PrivacyPoliciesViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 19/04/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSUtils.h"
#import "Constant.h"
@interface PrivacyPoliciesViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

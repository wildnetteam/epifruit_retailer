//
//  DeliveryAddressViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 28/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"DeliveryAddressDataSourceController.h"
#import "NoConsumerAddressViewController.h"
@interface DeliveryAddressViewController : UIViewController <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, retain) NSMutableArray* deliveryAddressArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) int saveOrEditMode;
@property (strong ,nonatomic) NSString* DelevryID;
@property (strong ,nonatomic) ConsumerAddressModel* modelWhenNotChanged;
@property (nonatomic) BOOL isprocessing;

@end

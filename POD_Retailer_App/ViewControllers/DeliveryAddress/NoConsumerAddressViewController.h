//
//  NoConsumerAddressViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 29/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaceOrderViewController.h"

@interface NoConsumerAddressViewController : UIViewController <DeliveryAddressDelegate>
@property (weak, nonatomic) IBOutlet UIButton *addConsumerAddressButton;
@property (nonatomic) int saveOrEditMode;//1 for save 2 for edit 3 for selecting address

@end

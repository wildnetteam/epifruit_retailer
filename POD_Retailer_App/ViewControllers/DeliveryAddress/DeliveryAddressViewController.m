//
//  DeliveryAddressViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 28/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "DeliveryAddressViewController.h"
#import "UITableView+DataSourceController.h"

@interface DeliveryAddressViewController ()
{
    UIButton *dimView;
    DeliveryAddressPopoverView *viewPop;
    NSArray *tempArrayToHoldAllDeliveryAddress;
}

@end

@implementation DeliveryAddressViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self setUIElement];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    self.navigationItem.rightBarButtonItem.enabled = true;

    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    tempArrayToHoldAllDeliveryAddress = [[NSMutableArray alloc]init];
    tempArrayToHoldAllDeliveryAddress = [sharedUtils.consumerAddressArray mutableCopy];

    if(_deliveryAddressArray.count == 0){
        
        if([LSUtils isNetworkConnected]) {
            
            [sharedUtils startAnimator:self];
            
            [sharedUtils getAllConsumerAddress:@"" :^(BOOL success, NSDictionary *result, NSError *error) {
                [sharedUtils stopAnimator:self];
                
                if (error != nil) {
                    // handel error part here
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                }else{
                    
                    if(success){
                        if([[result valueForKey:@"code"]intValue] == 200){
                            
                            if(sharedUtils.consumerAddressArray.count>0){
                                
                                tempArrayToHoldAllDeliveryAddress = [sharedUtils.consumerAddressArray mutableCopy];
 
                                // load data source
                                [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
                            }
                            
                        }else  if([[result valueForKey:@"code"]intValue] == 300){
                            
                            [self goToNoAddressViewController];
                        }
                        
                    }else{
                        UIViewController*  controller;
                        
                        if(result ==nil){
                            controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                        }else{
                            NSLog(@"%@",result);
                            controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                        }
                        [self presentViewController:controller animated:YES completion:nil];
                    }
                }
            }];
        }else{
            
            [sharedUtils stopAnimator:self];
            UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected"];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)goToNoAddressViewController{
    
    NoConsumerAddressViewController* vc = (NoConsumerAddressViewController*)[sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NoConsumerAddressViewController"];
    
    vc.saveOrEditMode = _saveOrEditMode;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    
   // if(_saveOrEditMode!=3)
 //   _saveOrEditMode = 0;
    sharedUtils.consumerAddressArray = nil;
    sharedUtils.consumerAddressArray = [tempArrayToHoldAllDeliveryAddress mutableCopy];
    tempArrayToHoldAllDeliveryAddress = nil;
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setUIElement{
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    self.title =@"";
    
    if(_saveOrEditMode != 3){
        
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
        barButtonItem.tintColor = [UIColor blackColor];
        
        self.navigationItem.leftBarButtonItem = barButtonItem;
        
    }else{
        
        //View controller need to have both menu and back button
        
        ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
        
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
        barButtonItem.tintColor = [UIColor blackColor];
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
        
    }
    
    UIButton* addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setFrame:CGRectMake(0, 0,50, 50)];
    [addButton addTarget:self action:@selector(addAddress) forControlEvents:UIControlEventTouchUpInside];
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:addButton]];
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    
    DeliveryAddressDataSourceController *dataSourceCtrl = [[DeliveryAddressDataSourceController alloc] init];
    dataSourceCtrl.tableView = _tableView;
    dataSourceCtrl.viewController = self;
    _tableView.dataSourceController = (id)dataSourceCtrl;
    [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil]; 
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

//-(void)backButtonClicked:(id)sender{
//    
//    
//    sharedUtils.consumerAddressModel = _modelWhenNotChanged;
//    [self.navigationController popViewControllerAnimated:YES];
//}

-(void)backButtonClicked:(id)sender{
    
    if(_isprocessing == true){
        NSLog(@"processing");
        return;
    }
    sharedUtils.consumerAddressModel = _modelWhenNotChanged;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addAddress {

    [dimView removeFromSuperview];
    dimView = nil;
    dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    viewPop= [[DeliveryAddressPopoverView alloc] init];
    viewPop.deliveryAddressDel = (id)self;
    viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];
    [UIView commitAnimations];
    viewPop.parentControllerType = ADDAddressController;
    //Arpana latest change
    if(_saveOrEditMode != 3)
        _saveOrEditMode = 0;
    viewPop.backgroundColor=[UIColor whiteColor];
    [dimView addSubview:viewPop];
    [self.view addSubview:dimView];
    
    
}
-(void)saveData:(ConsumerAddressModel*)addressObject canUpdate:(BOOL)isEdited{
    self.navigationItem.rightBarButtonItem.enabled = false;

    if([LSUtils isNetworkConnected]) {
        
    if(_saveOrEditMode == 2 ||(_saveOrEditMode == 3 && isEdited)){
        //update address
        [sharedUtils startAnimator:self];

        [sharedUtils updateConsumerAddress:addressObject Completion:^(BOOL success, NSDictionary *result, NSError *error) {
            [sharedUtils stopAnimator:self];
            self.navigationItem.rightBarButtonItem.enabled = true;
            if(success){
                if (error != nil) {
                    // handel error part here
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                }
                if([[result valueForKey:@"code"]intValue] == 200){
                    
                    UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Address updated successfully."];
                    [self presentViewController:controller animated:YES completion:nil];
                    
                    MBProgressHUD* progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    progressHUD.labelText=@"Updating UI...";
                    
                    [sharedUtils getAllConsumerAddress:@"" :^(BOOL success, NSDictionary *result, NSError *error) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];

                        if (error != nil) {
                            // handel error part here
                            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                            
                            [self presentViewController:controller animated:YES completion:nil];
                        }else{
                            self.deliveryAddressArray = nil;
                            if(sharedUtils.consumerAddressArray.count>0){
                                
                                [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
                            }
                        }
                    }];
                    
                }else  if([[result valueForKey:@"code"]intValue] == 211){
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Address is invalid."];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                    
                }
                else  if([[result valueForKey:@"code"]intValue] == 300){
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Address could not be updated."];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                }
                
            }else{
                UIViewController*  controller;
                if(result == nil){
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                }else{
                    NSLog(@"%@",result);
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                }
                
                
                [self presentViewController:controller animated:YES completion:nil];
            }
        }];
    }else{
        [sharedUtils startAnimator:self];
        [sharedUtils addConsumerAddressToDataBase:addressObject Completion:^(BOOL success, NSDictionary *result, NSError *error) {
            [sharedUtils stopAnimator:self];
            self.navigationItem.rightBarButtonItem.enabled = true;
            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                
                [self presentViewController:controller animated:YES completion:nil];
            }else{
 
                if(success){
                    UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Address added successfully."];
                    [self presentViewController:controller animated:YES completion:nil];
                    
                    MBProgressHUD* progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    progressHUD.labelText=@"Updating UI...";
                    
                    [sharedUtils getAllConsumerAddress:@"" :^(BOOL success, NSDictionary *result, NSError *error) {
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];

                        if (error != nil) {
                            // handel error part here
                            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                            
                            [self presentViewController:controller animated:YES completion:nil];
                        }else{
                            if(success){
                                if([[result valueForKey:@"code"]intValue] == 200){
                                    [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
                                }
                            }
                        }
                    }];
                }else {
                    UIViewController*  controller;
                    if(result == nil){
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                    }else{
                        NSLog(@"%@",result);
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem is occured."];
                    }
                    [self presentViewController:controller animated:YES completion:nil];
                }
                
            }
        }];
    }
    }else{
      
        [sharedUtils stopAnimator:self];

        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];

}

#pragma mark - UISearchViewDelegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    // Do things
    [self.searchBar resignFirstResponder];
    
    [sharedUtils getAllConsumerAddress:searchBar.text :^(BOOL success, NSDictionary *result, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                
                [self presentViewController:controller animated:YES completion:nil];
            }else{
                if(success){
                    if([[result valueForKey:@"code"]intValue] == 200){
                        
                    }else if([[result valueForKey:@"code"]intValue] == 300){
                        UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:[result valueForKey:@"message"]];
                        [self presentViewController:controller animated:YES completion:nil];
                    }
                    [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
                    
                }
            }
        });
    }];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [sharedUtils getAllConsumerAddress:@"" :^(BOOL success, NSDictionary *result, NSError *error) {
       
        dispatch_async(dispatch_get_main_queue(), ^{

        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (error != nil) {
            // handel error part here
            UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            if(success){
                if([[result valueForKey:@"code"]intValue] == 200){
                    [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
                }
            }
        }
        });
    }];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    if([searchText isEqualToString:@""]){
        sharedUtils.consumerAddressArray = [ tempArrayToHoldAllDeliveryAddress mutableCopy];
        [_tableView.dataSourceController refreshDataSourceWithCompletionHandler:nil];
        [_searchBar resignFirstResponder];
        [self.view  endEditing:true];

    }
}
@end

//
//  DeliveryAddressPopoverView.h
//  PODRetailerApp
//
//  Created by Arpana on 02/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "ConsumerAddressModel.h"
#import <GooglePlaces/GooglePlaces.h>

@protocol DeliveryAddressDelegate <NSObject>
//@property (nonatomic) int diaappearTag; // To decide if we need to call getaddressmethod  gain on viewillAppear(caused due to GMSAutoComplete view dissmiss)
@optional
-(void)saveData:(ConsumerAddressModel*)addressDetail canUpdate:(BOOL)isEdited;
-(void)cancel:(id)sender;
-(void)updateOrderDescriptioScreenForConsumer:(ConsumerAddressModel*)addModel;

@end

@interface DeliveryAddressPopoverView : UIView <UITextFieldDelegate , UITextViewDelegate, GMSAutocompleteViewControllerDelegate>


typedef enum {
    OrderDesController = 0,
    EditController,
    ADDAddressController
}ControllerType;

@property (nonatomic, strong) UILabel *Label;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UILabel *userMobileLabel;
@property (nonatomic, strong) UILabel *userEmailLabel;
@property (nonatomic, strong) UILabel *addressLine1Label;
@property (nonatomic, strong) UILabel *addressLine2Label;
@property (nonatomic, strong) UILabel *postCodelabel;
@property (nonatomic, strong) UILabel *cityLabel;
@property (nonatomic, strong) UILabel *stateLabel;
@property (nonatomic, strong) UIImageView *searchImage;
@property (nonatomic, strong) UILabel *countryLabel;
@property (nonatomic, strong) UILabel *contactNumberLabel;
@property (nonatomic, strong) UILabel *commentsLabel;

@property (nonatomic, strong) UITextField *labeltextfield;
@property (nonatomic, strong) UITextField *userNametextfield;
@property (nonatomic, strong) UITextField *userEmailtextfield;
@property (nonatomic, strong) UITextField *userMobiletextfield;
@property (nonatomic, strong) UITextField *addressLine1textfield;
@property (nonatomic, strong) UITextField *addressLine2textfield;
@property (nonatomic, strong) UITextField *postCodetextfield;
@property (nonatomic, strong) UITextField *citytextfield;
@property (nonatomic, strong) UITextField *statetextfield;
@property (nonatomic, strong) UITextField *countrytextfield;
@property (nonatomic, strong) UITextField *contactNumbertextfield;
@property (nonatomic, strong) UITextField *contactCodeNumbertextfield;

@property (nonatomic, strong) UITextView *commentstextView;
@property (nonatomic, strong) UIView *topView;

@property (strong, nonatomic) UIView *popViewTopBar; // The bar at the top of the picker view
@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) UILabel *popViewTopBarTitle;

@property (assign , nonatomic) id<DeliveryAddressDelegate> deliveryAddressDel;

@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) ConsumerAddressModel *addressObjectDetail;
@property (nonatomic) BOOL isInEditMode;

@property (nonatomic) ControllerType parentControllerType;


@end

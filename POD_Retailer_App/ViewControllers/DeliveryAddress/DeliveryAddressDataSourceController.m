//
//  DeliveryAddressDataSourceControlle
//  SampleProject
//
//  Created by Alex Bumbu on 13/07/15.
//  Copyright (c) 2015 Alex Bumbu. All rights reserved.
//

#import "DeliveryAddressDataSourceController.h"
#import "ABMenuTableViewCell.h"
#import "UserManager.h"
#import "UIImage+Scale.h"
#import "DeliveryAddressViewController.h"
#import "ABCellMenuView.h"
#import "DeliveryAddressTableViewCell.h"


static NSString *menuCellIdentifier = @"Default Cell";
static NSString *customCellIdentifier = @"Custom Cell";

@interface DeliveryAddressDataSourceController () <ABCellMenuViewDelegate>
{
    DeliveryAddressTableViewCell *customecell;
}
@end

@implementation DeliveryAddressDataSourceController {
    NSMutableArray *_dataSource;
}

@synthesize tableView = _tableView;
@synthesize viewController = _viewController;

- (void)refreshDataSourceWithCompletionHandler:(void (^)())completion {
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [self loadDataSource];
    
    if (completion)
        completion();
}

#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  sharedUtils.consumerAddressArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ABMenuTableViewCell *cell = nil;
    cell = [self customCellAtIndexPath:indexPath];
   cell.selectionStyle = UITableViewCellSelectionStyleNone;
    // custom menu view
    NSString *nibName =  @"ABCellCustomStyleMenuView";
    ABCellMenuView *menuView = [ABCellMenuView initWithNib:nibName bundle:nil];
    menuView.delegate = self;
    menuView.indexPath = indexPath;
    cell.rightMenuView = menuView;
    return cell;
}

- (ABMenuTableViewCell*)menuCellAtIndexPath:(NSIndexPath*)indexPath {
    ABMenuTableViewCell *cell = (ABMenuTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:menuCellIdentifier];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DeliveryAddressTableViewCell *cell = (DeliveryAddressTableViewCell *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
    int height;
    
    height = [self getLabelDynamicHeight:cell.bussinessLabel.text andLabel:cell.bussinessLabel];
    int height1 = [self getLabelDynamicHeight:cell.stateLabel.text andLabel:cell.stateLabel];
    int totalHeight = height +height1+ cell.titleLabel.frame.size.height +31 +2.5+4 + 16;
    //+cell.stateLabel.frame.size.height
    return totalHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

//return dynamic label size
-(int)getLabelDynamicHeight:(NSString *)yourString andLabel:(UILabel *)yourLabel
{
    //Calculate the expected size based on the font and linebreak mode of your label
    
    CGSize constraint = CGSizeMake(yourLabel.frame.size.width,9999);
    
    NSDictionary *attributes = @{NSFontAttributeName: yourLabel.font};
    
    CGRect rect = [yourString boundingRectWithSize:constraint
                                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                        attributes:attributes
                                           context:nil];
    return rect.size.height;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DeliveryAddressViewController* v = (DeliveryAddressViewController*)_viewController;
    ConsumerAddressModel *addressObject = [v.deliveryAddressArray objectAtIndex:indexPath.row];
    
    sharedUtils.consumerAddressModel = nil;
    
    NSLog(@"Delivery Edit mode: %d", v.saveOrEditMode);
    if( v.saveOrEditMode ==3 ){
        
        sharedUtils.consumerAddressModel = addressObject;
        
        [v.navigationController popViewControllerAnimated:true];
    }

}


#pragma mark ABCellMenuViewDelegate Methods

- (void)cellMenuViewFlagBtnTapped:(ABCellMenuView *)menuView {
  }

//Edit Address
- (void)cellMenuViewMoreBtnTapped:(ABCellMenuView *)menuView {
    
    ABMenuTableViewCell *cell = (ABMenuTableViewCell*)[self.tableView cellForRowAtIndexPath:menuView.indexPath];
    
    [cell updateMenuView:ABMenuUpdateHideAction animated:YES];
    
     DeliveryAddressViewController* v = (DeliveryAddressViewController*)_viewController;
    UIView *dimView;
    [dimView removeFromSuperview];
    dimView = nil;
    dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, v.view.frame.size.width, v.view.frame.size.height)];
    dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
   DeliveryAddressPopoverView*  viewPop= [[DeliveryAddressPopoverView alloc] init];
    
    viewPop.deliveryAddressDel = (id)v;
    viewPop.addressObjectDetail = [sharedUtils.consumerAddressArray objectAtIndex:menuView.indexPath.row];
    viewPop.isInEditMode = true;
    
    viewPop.parentControllerType = EditController;
    viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, dimView.frame.size.width, v.view.frame.size.height);
    
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewPop.frame = CGRectMake(0,CGRectGetHeight(v.view.frame)*.20, v.view.frame.size.width, v.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];

    viewPop.backgroundColor=[UIColor whiteColor];
    //1 for save 2 for edit
    [dimView addSubview:viewPop];
    
    if(v.saveOrEditMode !=3)
    v.saveOrEditMode = 2;
    
    [v.view addSubview:dimView];

}

- (UIViewController *)currentTopViewController {
    
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}
- (void)cellMenuViewDeleteBtnTapped:(ABCellMenuView *)menuView {
    
    DeliveryAddressViewController* v = (DeliveryAddressViewController*)_viewController;

    if([LSUtils isNetworkConnected]){
        
        NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                               initWithString:@"Do you want to delete address from address book?"
                                               attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@""
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           ABMenuTableViewCell *cell = (ABMenuTableViewCell*)[self.tableView cellForRowAtIndexPath:menuView.indexPath];
                                           
                                           [cell updateMenuView:ABMenuUpdateHideAction animated:YES];
                                       }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Before delete %@", sharedUtils.consumerAddressArray);
                                       // update data source
                                       ConsumerAddressModel *object = [sharedUtils.consumerAddressArray objectAtIndex:menuView.indexPath.row];
                                       //To restrict user to click back button while deleting procees is goin on.
                                       v.isprocessing = true;
                                       [self deletePickUpAddress:object CompletionHandler:^(BOOL success, NSError *error) {
                                           //To restrict user to click back button while deleting procees is goin on.
                                           v.isprocessing = false;
                                           if(error!= nil){
                                               //Thre is some error occured
                                               UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                               [v presentViewController:controller animated:YES completion:nil];
                                               return ;
                                           }if(success){
                                               
                                               NSLog(@"After delete %@", sharedUtils.consumerAddressArray);

                                               //deleted successfully
                                               [sharedUtils.consumerAddressArray removeObjectAtIndex:menuView.indexPath.row];
                                               
                                               if( v.saveOrEditMode == 3 ){
                                                   
                                                   if([v.DelevryID intValue] == [object.addressId intValue]){
                                                       
                                                       sharedUtils.consumerAddressModel = nil;
                                                       v.modelWhenNotChanged = nil;
                                                   }
                                               }
                                               if(sharedUtils.consumerAddressArray.count == 0){//to change mantish Bug
                                                   
                                                   NoConsumerAddressViewController* vc = (NoConsumerAddressViewController*)[sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NoConsumerAddressViewController"];
                                                   vc.saveOrEditMode = v.saveOrEditMode;
                                                   [v.navigationController pushViewController:vc animated:YES];
                                               }
                                               // update UI
                                               [self.tableView deleteRowsAtIndexPaths: @[menuView.indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                                               
                                               // make sure to reload in order to update the custom menu index path for each row
                                               NSMutableArray *rowsToReload = [NSMutableArray array];
                                               for (int i = 0; i < sharedUtils.consumerAddressArray.count - menuView.indexPath.row; i++) {
                                                   [rowsToReload addObject:[NSIndexPath indexPathForRow:menuView.indexPath.row + i inSection:0]];
                                               }
                                               if(rowsToReload != nil){
                                                   [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationAutomatic];
                                                   
                                               }else [_tableView reloadData];
                                           }else{
                                               UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Address could not be deleted"];
                                               [v presentViewController:controller animated:YES completion:nil];
                                           }
                                       }];
                                   }];
        
        [alertController setValue:alertmessage forKey:@"attributedTitle"];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        UIViewController *currentTopVC = [self currentTopViewController];
        
        if(currentTopVC!=nil)
            [currentTopVC  presentViewController:alertController animated:NO completion:Nil];
    }else{
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [v presentViewController:controller animated:YES completion:nil];
    }

}

-(void)deletePickUpAddress:(ConsumerAddressModel*)ad CompletionHandler:(void (^) (BOOL success, NSError* error))completion  {
        
        NSString*  params = [NSString stringWithFormat:@"%@&id=%@",RemoveConsumerAddress_Service,ad.addressId];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  
                                                  NSLog(@"Response:%@",responseDictionary);
                                                  

                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      completion(true,nil);
                                                  }else
                                                      completion(false,nil);

                                              }
                                              if(error!=nil)
                                                  completion(false,error);

                                          });
                                      }];
        [task resume];
        
    
}

#pragma mark Private Methods

- (void)loadDataSource {

    DeliveryAddressViewController* v = (DeliveryAddressViewController*)_viewController;
    v.deliveryAddressArray = nil;
    v.deliveryAddressArray = sharedUtils.consumerAddressArray;
    
     [self .tableView reloadData];
}

- (DeliveryAddressTableViewCell*)customCellAtIndexPath:(NSIndexPath*)indexPath
{
    
    // setup the cell
    DeliveryAddressTableViewCell *cell = (DeliveryAddressTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
    cell.contentViewBottomSpace.constant = IS_IPHONE5?15:30;
    
    [cell layoutIfNeeded];
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    DeliveryAddressViewController* v = (DeliveryAddressViewController*)_viewController;
    
    [cell setAddressFields:[v.deliveryAddressArray objectAtIndex:indexPath.row] :@"Addresslabel"];
    
    if( v.saveOrEditMode == 3 ){
        
        cell.selectOrAddButton.hidden = false;

       ConsumerAddressModel* model = [v.deliveryAddressArray objectAtIndex:indexPath.row];
        
            if([v.DelevryID intValue]  == [model.addressId intValue]){
               [cell.selectOrAddButton setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
                 v.modelWhenNotChanged = model;
            }

    }else{
        cell.selectOrAddButton.hidden = true;
    }
    return cell;
}

-(void)saveData:(id)sender{
    [self loadDataSource];
}
@end

//
//  ExpiredJobViewController.m
//  EpiFruitApp
//
//  Created by Arpana on 18/08/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "ExpiredJobViewController.h"
#import "MyBidsOrderDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ExpiredJobViewController ()
{
    UIRefreshControl *btmefreshControl;
    NSMutableArray *OrderList;

}
@end

@implementation ExpiredJobViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    [self getOrderList];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = true;
    [self setUIElement];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getOrderList) name:NSdidRecieveNotification object:nil];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)setUIElement{
    
    
    _tableView.backgroundColor = [UIColor clearColor];
    
    self.title = @"";
    
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    
    //View controller need to have both menu and back button
    
    ////////////////////////////////////////// Adding menu and back button togather////////////////////////////////
    
    if(_isFromNotificationScreen){
        
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 8;
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        
    }else{
       
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
        barButtonItem.tintColor = [UIColor blackColor];
        
        [self.navigationItem setLeftBarButtonItem:barButtonItem ];
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50.0;
    btmefreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview: btmefreshControl];
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

-(void)refresh:(id)sender{
    
    if([LSUtils isNetworkConnected]){
        [self getOrderList];
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        } ];
    }
}

#pragma mark - tableView Method

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return OrderList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(btmefreshControl.isRefreshing  == true){
        [btmefreshControl endRefreshing ];
    }
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier"];
    
    // setup the cell
    MyOrderDescriptionTableViewCell *cell = (MyOrderDescriptionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.repostDelagte = (id)self;
    cell.repostButton.tag = indexPath.row;
    if(OrderList.count > 0) {
        OrderHistoryModel* orderObjectAtIndex = [OrderList objectAtIndex:indexPath.row];
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        if(IS_IPHONE_6P){
            cell.cellShadowSeparartor.constant = -40;
            cell.titleAndDescDistance.constant = 15;
        }
        
        [cell setBIdOrderDetails:orderObjectAtIndex];
        //  cell.descriptionLabel.text = @"deliveryboy_imagevendor_4750735c2fc7587dc0c0783a91b1fec7.png";
        cell.timeLabel.text =[self getFormatedDate:orderObjectAtIndex.productdateAndTime];
        
        [cell.imageViewForProduct sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",orderObjectAtIndex.imagebaseURL, orderObjectAtIndex.randomImageString ]]  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        
        cell.imageViewForProduct.layer.cornerRadius = 10;
        cell.imageViewForProduct.layer.masksToBounds = true;
        
        [cell setNeedsLayout];
        [cell layoutIfNeeded];
    }
    return  cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier"];
    
    MyOrderDescriptionTableViewCell *cell = (MyOrderDescriptionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //    if(IS_IPHONE_6P)
    //        return  200;
    return cell.contentView.frame.size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([LSUtils isNetworkConnected]){
        OrderHistoryModel* orderObjectAtIndex = [OrderList objectAtIndex:indexPath.row];
        
        MyBidsOrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"MyBidsOrderDetailViewController"];
        vc.orderId = orderObjectAtIndex.productId;
        vc.swiftySideMenu.centerViewController = self.navigationController;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)repostButtonClicked:(UIButton*)repostButton {
    
    if([LSUtils isNetworkConnected]){
        
            OrderHistoryModel* orderObjectAtIndex = [OrderList objectAtIndex:repostButton.tag];
            
            [self repostJob:orderObjectAtIndex :(int)repostButton.tag :^(BOOL success, NSDictionary *result, NSError *error) {
                
                if(error!=nil){
                    
                    UIAlertController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                    [self presentViewController:controller animated:YES completion:nil];
                }
                
                if(result == nil){
                    
                    UIAlertController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                    [self presentViewController:controller animated:YES completion:nil];
                }
                if(success){
                    
                    if([[result objectForKey:@"code"]isEqualToString:@"200"]){
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:repostButton.tag inSection:0];
                        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                        [_tableView beginUpdates];
                        [OrderList removeObjectAtIndex:indexPath.row];
                        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                        [_tableView endUpdates];
                    }
                    UIAlertController*  controller =   [sharedUtils showAlert:@"" withMessage:[result objectForKey:@"message"]];
                    [self presentViewController:controller animated:YES completion:nil];
                }
                
            } ];
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)repostJob:(OrderHistoryModel*)orderDetail :(int)indexOfCell :(void (^)(BOOL success, NSDictionary *result, NSError *error))completion  {
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@",repostOrder,orderDetail.productId ];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              if(responseDictionary == nil){
                                                  
                                                  completion(true, nil, nil);
                                              }
                                              else  {
                                                  completion (true,responseDictionary , nil);
                                              }
                                          }
                                          else {
                                              
                                              if(error != nil){
                                                  
                                                  completion(true, nil, error);
                                              }else
                                                  completion(false, nil, nil);
                                          }
                                      });
                                  }];
    [task resume];
    
}

#pragma mark - SideMenu Methods -
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:NSdidRecieveNotification];
    //responseDict = nil;
    [super viewWillDisappear:animated];
}

-(void)getOrderList{
    
    if([LSUtils isNetworkConnected]){
        
        OrderList = [[NSMutableArray alloc]init];
        
        NSString *params = [NSString stringWithFormat:@"%@&vendor_id=%@",vendorExpiredOrderList,[UserManager getUserID]];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        if(btmefreshControl.isRefreshing ==false)[sharedUtils startAnimator:self];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data   options:0  error:NULL];
                                                  
                                                  if(responseDictionary == nil){
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      return ;
                                                      
                                                  }
                                                  
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSMutableArray *arrayOfAddress;
                                                      
                                                      arrayOfAddress = [responseDictionary valueForKey:@"data"] ;
                                                      
                                                      for(int index =0 ; index<arrayOfAddress.count ; index++){
                                                          
                                                          NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                          //Set Order Model
                                                          //////////// Set response value ///////////////////////////////////////////
                                                          OrderHistoryModel *mode =
                                                          [[OrderHistoryModel alloc]initWithTitle:[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_title"]]?[arrayOfdata objectForKey:@"product_title"]:@"" Description:[NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_quantity"]]?[arrayOfdata objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_type"]]?[arrayOfdata objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_weight"]]?[arrayOfdata objectForKey:@"product_weight"]:@"" ] OrderNo:[arrayOfdata objectForKey:@"delivery_cost"] Status:[[arrayOfdata objectForKey:@"order_status"] intValue] DateAndTime:[arrayOfdata objectForKey:@"created_on"]];
                                                          
                                                          //If product description is blank or weight is not mentioned , Remove ()
                                                          if([mode.productDescription containsString:@"( )"]||![LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_weight"]]){
                                                              mode.productDescription = [[mode.productDescription stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                          }
                                                          mode.imageCount = [[arrayOfdata objectForKey:@"imageCount"] intValue];
                                                          mode.randomImageString = [LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"product_image"]]?[arrayOfdata objectForKey:@"product_image"]:@"";
                                                          mode.productdateAndTime = [arrayOfdata objectForKey:@"created_on"];
                                                          mode.productId = [arrayOfdata objectForKey:@"id"];
                                                          mode.imagebaseURL = [responseDictionary objectForKey:@"imagepath"];
                                                          mode.driverCount = [[arrayOfdata objectForKey:@"driverCount"] intValue];
                                                          mode.productOrderNo = [arrayOfdata objectForKey:@"order_number"];
                                                          mode.minRate =[LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"min_rate"]] ?[arrayOfdata objectForKey:@"min_rate"]:@"";
                                                          mode.maxrate = [LSUtils checkNilandEmptyString:[arrayOfdata objectForKey:@"max_rate"]]?[arrayOfdata objectForKey:@"max_rate"]:@"";
                                                          mode.jumpPrice = [[arrayOfdata objectForKey:@"jump_price"] intValue];
                                                          [OrderList addObject:mode];
                                                          
                                                          ///////////////////////////////////////////////////////////////////////////////////////
                                                      }
                                                  }
                                                  else {
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                              }
                                              if(btmefreshControl.isRefreshing  == true){
                                                  [btmefreshControl endRefreshing ];
                                              }
                                              [_tableView reloadData];
                                              
                                          });
                                      }];
        [task resume];
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
        } ];
    }
}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"placed on%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}

@end



//
//  ExpiredJobViewController.h
//  EpiFruitApp
//
//  Created by Arpana on 18/08/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyOrderDescriptionTableViewCell.h"

@interface ExpiredJobViewController : UIViewController <UITableViewDelegate , UITableViewDataSource ,RepostDelegate >
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic ) BOOL isFromNotificationScreen;

@end

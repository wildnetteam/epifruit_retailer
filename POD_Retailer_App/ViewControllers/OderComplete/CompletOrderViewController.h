//
//  CompletOrderViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 01/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompletOrderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@end

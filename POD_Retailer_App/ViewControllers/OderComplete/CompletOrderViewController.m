//
//  CompletOrderViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 01/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "CompletOrderViewController.h"
#import "SwiftySideMenuViewController.h"
#import "UIViewController+SwiftySideMenu.h"
#import "LeftMenuViewController.h"
#import "AppDelegate.h"
#import "DashboardViewController.h"

@interface CompletOrderViewController ()

@end

@implementation CompletOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

-(void)setUpUIElements{
    
    self.title =@"";
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}

-(void)backButtonClicked:(id)sender{
    
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[DashboardViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)okButtonPressed:(id)sender {
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[ DashboardViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }

}
@end

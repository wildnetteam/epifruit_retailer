//
//  UITextView_WithPlaceholder.h
//  CommusoftV2
//
//  Created by Jonathan Neumann on 24/11/2015.
//  Copyright © 2015 Commusoft Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView_WithPlaceholder : UITextView

@property (strong, nonatomic) NSString *placeholder;

@end

//
//  UIButton_WithTiltleNavigateIcon.h
//  CommusoftV2
//
//  Created by Amit Shakya on 21/01/16.
//  Copyright © 2016 Commusoft Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton_WithTiltleNavigateIcon : UIView
@property (nonatomic, strong) UILabel  *titleLabel;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, strong) UILabel  *buttonTimeLabel;
@property (nonatomic) BOOL isRequired;
@property BOOL isHidden;
@property (nonatomic) BOOL isEnable;

@end

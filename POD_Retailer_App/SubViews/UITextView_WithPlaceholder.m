//
//  UITextView_WithPlaceholder.m
//  CommusoftV2
//
//  Created by Jonathan Neumann on 24/11/2015.
//  Copyright © 2015 Commusoft Inc. All rights reserved.
//

#import "UITextView_WithPlaceholder.h"

@implementation UITextView_WithPlaceholder

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        // Listen for the notification that editing has started and remove the placeholder
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removePlaceholder) name:UITextViewTextDidBeginEditingNotification object:self];
        
        // Listen for the notification
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPlaceholder) name:UITextViewTextDidEndEditingNotification object:self];
    }
    return self;
}

- (void) setText:(NSString *)text{
    
    [super setText:text];
    
    // Replace the text with the placeholder if we pass an empty string and the keyboard is not up
    if ([[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] && self.isFirstResponder == false){
        self.text = _placeholder;
    }
    
    self.textColor = [UIColor blackColor];
    if ([self.text isEqualToString:_placeholder]){
        self.textColor = [UIColor grayColor];;
    }
}

- (void) setPlaceholder:(NSString *)placeholder{
    
    _placeholder = placeholder;
    self.text = placeholder;
}

- (void) removePlaceholder{
    
    if ([self.text isEqualToString:_placeholder]){
        self.text = @"";
    }
}

- (void) addPlaceholder{
    
    if ([[self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
        self.text = _placeholder;
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

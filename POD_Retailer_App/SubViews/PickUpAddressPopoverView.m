//
//  PickUpAddressPopoverView.m
//  PODRetailerApp
//
//  Created by Arpana on 02/12/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "PickUpAddressPopoverView.h"
#import "UIView+BorderAndDesign.h"
#import "UITextView+Placeholder.h"
#import "NotificationManager.h"

@implementation PickUpAddressPopoverView
{
    NSString *placNameString;
}

///Width of textfieldcode = 37
-(id)initWithFrame:(CGRect)frame {
    
   self = [super initWithFrame:frame];

    if(self){
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
        
        _Label = [[UILabel alloc]initWithFrame:CGRectZero];
        _businessNameLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _addressLine1Label = [[UILabel alloc]initWithFrame:CGRectZero];
        _addressLine2Label = [[UILabel alloc]initWithFrame:CGRectZero];
        _postCodelabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _cityLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _stateLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _countryLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _contactNumberLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _commentsLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _labeltextfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _labeltextfield.delegate = self;
        _businessNametextfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _businessNametextfield.delegate = self;
        _addressLine1textfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _addressLine2textfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _postCodetextfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _citytextfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _statetextfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _countrytextfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _contactNumbertextfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _contactCodeNumbertextfield = [[UITextField alloc]initWithFrame:CGRectZero];
        _contactCodeNumbertextfield.text = @"+01";
        _contactCodeNumbertextfield.enabled = false;
        _contactNumbertextfield.delegate = self;
        _contactNumbertextfield.keyboardType = UIKeyboardTypePhonePad;

        _commentstextView = [[UITextView alloc]initWithFrame:CGRectZero];
        _commentstextView.delegate= self;
        _businessNametextfield.delegate = self;
        _addressLine1textfield.delegate = self;
        _addressLine2textfield.delegate= self;
        _citytextfield.delegate = self;
        _statetextfield.delegate = self;
        _countrytextfield.delegate = self;
        _postCodetextfield.delegate = self;
        
        _businessNametextfield.keyboardType = UIKeyboardTypeASCIICapable;
        _postCodetextfield.keyboardType = UIKeyboardTypeNumberPad;
        _addressLine1textfield.keyboardType = UIKeyboardTypeASCIICapable;
        _addressLine2textfield.keyboardType = UIKeyboardTypeASCIICapable;
        _citytextfield.keyboardType = UIKeyboardTypeASCIICapable;
        _statetextfield.keyboardType = UIKeyboardTypeASCIICapable;
        _countrytextfield.keyboardType = UIKeyboardTypeASCIICapable;
        _scrollView = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectZero];
        
        _searchImage = [[UIImageView alloc] init];
        [_searchImage setImage:[UIImage imageNamed:@"search"]];
        
        _topView = [[UIView alloc]init];
        _topView.backgroundColor = [UIColor whiteColor];
       
        [_scrollView addSubview:_Label];
        [_scrollView addSubview:_labeltextfield];
        [_labeltextfield addSubview:_topView];
        [_labeltextfield addSubview:_searchImage];

        
        _popViewTopBar = [[UIView alloc] init];
        _popViewTopBar.backgroundColor = [UIColor colorWithHexString:@"#3fcbe4"];
        
        _cancelButton = [[UIButton alloc] init];
        [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = FONT_LABEL_BOLD;
        
        _doneButton = [[UIButton alloc] init];
        [_doneButton setTitle:@"Save" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _doneButton.titleLabel.font = FONT_LABEL_BOLD;
        
        
        
        _popViewTopBarTitle =[[UILabel alloc]initWithFrame:CGRectZero];
        [_popViewTopBarTitle setTextColor:[UIColor whiteColor]];
        // Add the subviews to the popBar
        [_popViewTopBar addSubview:_cancelButton];
        [_popViewTopBar addSubview:_doneButton];
        [_popViewTopBar addSubview:_popViewTopBarTitle];

        [_scrollView addSubview:_businessNameLabel];
        [_scrollView addSubview:_businessNametextfield];
        [_scrollView addSubview:_addressLine1Label];
        [_scrollView addSubview:_addressLine1textfield];
        [_scrollView addSubview:_addressLine2Label];
        [_scrollView addSubview:_addressLine2textfield];
        [_scrollView addSubview:_cityLabel];
        [_scrollView addSubview:_citytextfield];
        [_scrollView addSubview:_stateLabel];
        [_scrollView addSubview:_statetextfield];
        [_scrollView addSubview:_countryLabel];
        [_scrollView addSubview:_countrytextfield];
        [_scrollView addSubview:_contactCodeNumbertextfield];
        [_scrollView addSubview:_contactNumberLabel];
        [_scrollView addSubview:_contactNumbertextfield];
        [_scrollView addSubview:_commentsLabel];
        [_scrollView addSubview:_commentstextView];
        [_scrollView addSubview:_postCodelabel];
        [_scrollView addSubview:_postCodetextfield];
        
        [self addSubview:_scrollView];
        [self addSubview:_popViewTopBar];
        [self setFrame:frame];
        
    }
    
    [self setUIElement];
    return self;
    
}

-(void)disableFieldWhileEditing {

    _addressLine1textfield.enabled = false;
    _addressLine2textfield.enabled = false;
    _citytextfield.enabled = false;
    _statetextfield.enabled =  false;
    _countrytextfield.enabled = false;
    _postCodetextfield.enabled = false;
}

-(void)enableFieldWhileEditing {
    
    _addressLine1textfield.enabled = true;
    _addressLine2textfield.enabled = true;
    _citytextfield.enabled = true;
    _statetextfield.enabled =  true;
    _countrytextfield.enabled = true;
    _postCodetextfield.enabled = true;
}

-(void)setUpFielddataForEditing {
    
    [self disableFieldWhileEditing];
    
    if([LSUtils checkNilandEmptyString:_addressObjectDetail.bussinessname]){
        
    _businessNametextfield.text = _addressObjectDetail.bussinessname;
    }
    _labeltextfield.text = [LSUtils getShortTitle:_addressObjectDetail.addressTitle length:32];
    _postCodetextfield.text = _addressObjectDetail.zipCode;
    _citytextfield.text = _addressObjectDetail.city;
    _statetextfield.text = _addressObjectDetail.state;;
    _countrytextfield.text = _addressObjectDetail.country;
    if([LSUtils checkNilandEmptyString:_addressObjectDetail.addressLine1]){
        _addressLine1textfield.text = _addressObjectDetail.addressLine1;
    }
    if([LSUtils checkNilandEmptyString:_addressObjectDetail.addressLine2]){
        _addressLine2textfield.text = _addressObjectDetail.addressLine2;
    }
    if([LSUtils checkNilandEmptyString:_addressObjectDetail.phoneNumber]){
        _contactNumbertextfield.text = _addressObjectDetail.phoneNumber;
    }
    if([LSUtils checkNilandEmptyString:_addressObjectDetail.comment]){
        _commentstextView.text = _addressObjectDetail.comment;
    }
}
-(void)viewDidLayoutSubviews {
    [super layoutSubviews];
}

-(void)setFrame:(CGRect)frame {
    
    [super setFrame:frame];
    
    int pad = 15;
    int controlHeight = 43;
    int controlSeparator = 10;
    int spaceBetwwenHeadingAndTextField = 0;
    
    [super setFrame:frame];
    
    _popViewTopBar.frame = CGRectMake(0,0, self.frame.size.width, 50);
    
    _cancelButton.frame = CGRectMake(0, 0, CGRectGetWidth(_popViewTopBar.frame)/4, 50);
    
    _doneButton.frame = CGRectMake(CGRectGetWidth(_popViewTopBar.frame) * 3/4, 0, CGRectGetWidth(_popViewTopBar.frame)/4, 50);
    
    _popViewTopBarTitle.frame = CGRectMake(CGRectGetMaxX(_cancelButton.frame) , 0, CGRectGetWidth(_popViewTopBar.frame)/2, 50);
    

    
    _scrollView.frame = CGRectMake(0, CGRectGetMaxY(_popViewTopBar.frame), self.frame.size.width, self.frame.size.height);
    
    _scrollView.backgroundColor = [UIColor whiteColor];
    
    _Label.frame = CGRectMake(pad, 0, sharedUtils.screenWidth - 2*pad, controlHeight
                              );
    _labeltextfield.frame = CGRectMake(pad, CGRectGetMaxY(_Label.frame) + spaceBetwwenHeadingAndTextField, sharedUtils.screenWidth
                                       - 2*pad, controlHeight  );
    
    _searchImage.frame = CGRectMake(CGRectGetWidth(_labeltextfield.frame)-25, CGRectGetHeight(_labeltextfield.frame)/2 - 10, 20  , 20);
    _topView.frame = CGRectMake(CGRectGetWidth(_labeltextfield.frame)-25, CGRectGetHeight(_labeltextfield.frame)/2 - 10, _searchImage.frame.size.width,_searchImage.frame.size.height);

    
    _businessNameLabel.frame = CGRectMake(pad, CGRectGetMaxY(_labeltextfield.frame) + controlSeparator, sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _businessNametextfield.frame = CGRectMake(pad, CGRectGetMaxY
                                (_businessNameLabel.frame) + spaceBetwwenHeadingAndTextField ,sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _addressLine1Label.frame = CGRectMake(pad, CGRectGetMaxY
                                          (_businessNametextfield.frame) +    controlSeparator , sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _addressLine1textfield.frame = CGRectMake(pad, CGRectGetMaxY(_addressLine1Label.frame) +
                                              spaceBetwwenHeadingAndTextField,   sharedUtils.screenWidth - 2*pad , controlHeight  );
    
    _addressLine2Label.frame = CGRectMake(pad, CGRectGetMaxY(_addressLine1textfield.frame) +
                                          controlSeparator,  sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _addressLine2textfield.frame = CGRectMake(pad, CGRectGetMaxY(_addressLine2Label.frame) +
                                              spaceBetwwenHeadingAndTextField,  sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _cityLabel.frame = CGRectMake(pad, CGRectGetMaxY(_addressLine2textfield.frame) +
                                          controlSeparator, sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _citytextfield.frame = CGRectMake(pad, CGRectGetMaxY(_cityLabel.frame) +
                                              spaceBetwwenHeadingAndTextField,  sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _stateLabel.frame = CGRectMake(pad, CGRectGetMaxY(_citytextfield.frame) +
                                          controlSeparator,  sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _statetextfield.frame = CGRectMake(pad, CGRectGetMaxY(_stateLabel.frame) +
                                              spaceBetwwenHeadingAndTextField,  sharedUtils.screenWidth - 2*pad, controlHeight  );
    _countryLabel.frame = CGRectMake(pad, CGRectGetMaxY(_statetextfield.frame) +
                                  controlSeparator, sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _countrytextfield.frame = CGRectMake(pad, CGRectGetMaxY(_countryLabel.frame) +
                                      spaceBetwwenHeadingAndTextField,  sharedUtils.screenWidth - 2*pad, controlHeight  );
    _postCodelabel.frame = CGRectMake(pad, CGRectGetMaxY(_countrytextfield.frame) +
                                      controlSeparator, sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _postCodetextfield.frame = CGRectMake(pad, CGRectGetMaxY(_postCodelabel.frame) +
                                          spaceBetwwenHeadingAndTextField,  sharedUtils.screenWidth - 2*pad, controlHeight  );
    _contactNumberLabel.frame = CGRectMake(pad, CGRectGetMaxY(_postCodetextfield.frame) +
                                     controlSeparator, sharedUtils.screenWidth - 2*pad, controlHeight  );
   
    _contactCodeNumbertextfield.frame = CGRectMake(pad, CGRectGetMaxY(_contactNumberLabel.frame) +
                                               spaceBetwwenHeadingAndTextField, 37 , controlHeight  );
    _contactNumbertextfield.frame = CGRectMake(CGRectGetMaxX(_contactCodeNumbertextfield.frame)+3, CGRectGetMaxY(_contactNumberLabel.frame) +
                                         spaceBetwwenHeadingAndTextField, sharedUtils.screenWidth - 2*pad - 40, controlHeight  );
    
    _commentsLabel.frame = CGRectMake(pad, CGRectGetMaxY(_contactNumbertextfield.frame) +
                                           controlSeparator, sharedUtils.screenWidth - 2*pad, controlHeight  );
    
    _commentstextView.frame = CGRectMake(pad, CGRectGetMaxY(_commentsLabel.frame) +
                                               spaceBetwwenHeadingAndTextField,  sharedUtils.screenWidth - 2*pad, controlHeight +30 );
    
     _scrollView.contentSize = CGSizeMake(sharedUtils.screenWidth, CGRectGetMaxY(_commentstextView.frame)  +CGRectGetHeight(_popViewTopBar.frame)+CGRectGetHeight(self.frame)*.20 +20);
    
    if(_addressObjectDetail!=nil){
        placNameString = _labeltextfield.text;
        [self setUpFielddataForEditing];
    }
    
}

-(void)setUIElement {
    
    _scrollView.showsHorizontalScrollIndicator =false;
    _scrollView.directionalLockEnabled = true;
    _popViewTopBarTitle.textAlignment = NSTextAlignmentCenter;
    
    _labeltextfield.borderStyle =  UITextBorderStyleLine;
    _labeltextfield.borderColor = COLOR_Border;
    _labeltextfield.textColor = COLOR_Title_Label;
    _labeltextfield.borderWidth = 1.0f;
    _labeltextfield.font= FONT_TextField;
    [LSUtils addPaddingToTextField:_labeltextfield];

    _businessNametextfield.borderStyle =  UITextBorderStyleLine;
    _businessNametextfield.borderColor = COLOR_Border;
    _businessNametextfield.textColor = COLOR_Title_Label;
    _businessNametextfield.borderWidth = 1.0f;
    _businessNametextfield.font= FONT_TextField;
    [LSUtils addPaddingToTextField:_businessNametextfield];

    
    _addressLine1textfield.borderStyle =  UITextBorderStyleLine;
    _addressLine1textfield.borderColor = COLOR_Border;
    _addressLine1textfield.textColor = COLOR_Title_Label;
    _addressLine1textfield.borderWidth = 1.0f;
    _addressLine1textfield.font= FONT_TextField;
    [LSUtils addPaddingToTextField:_addressLine1textfield];

    
    _addressLine2textfield.borderStyle =  UITextBorderStyleLine;
    _addressLine2textfield.borderColor = COLOR_Border;
    _addressLine2textfield.textColor = COLOR_Title_Label;
   // _addressLine1textfield.font = []
    [LSUtils addPaddingToTextField:_addressLine2textfield];

    _addressLine2textfield.borderWidth = 1.0f;
    _addressLine2textfield.font= FONT_TextField;
    
    _postCodetextfield.borderStyle =  UITextBorderStyleLine;
    _postCodetextfield.borderColor = COLOR_Border;
    _postCodetextfield.textColor = COLOR_Title_Label;
    _postCodetextfield.borderWidth = 1.0f;
    _postCodetextfield.font= FONT_TextField;
    [LSUtils addPaddingToTextField:_postCodetextfield];
    
    _citytextfield.borderStyle =  UITextBorderStyleLine;
    _citytextfield.borderColor = COLOR_Border;
    _citytextfield.textColor = COLOR_Title_Label;
    [LSUtils addPaddingToTextField:_citytextfield];

    _citytextfield.borderWidth = 1.0f;
    _citytextfield.font= FONT_TextField;

    _statetextfield.borderStyle =  UITextBorderStyleLine;
    _statetextfield.borderColor = COLOR_Border;
    _statetextfield.textColor = COLOR_Title_Label;
    _statetextfield.borderWidth = 1.0f;
    _statetextfield.font= FONT_TextField;
    [LSUtils addPaddingToTextField:_statetextfield];
    
    _countrytextfield.borderStyle =  UITextBorderStyleLine;
    _countrytextfield.borderColor = COLOR_Border;
    _countrytextfield.textColor = COLOR_Title_Label;
    _countrytextfield.borderWidth = 1.0f;
    _countrytextfield.font= FONT_TextField;
    [LSUtils addPaddingToTextField:_countrytextfield];
    
    _contactNumbertextfield.borderStyle =  UITextBorderStyleLine;
    _contactNumbertextfield.borderColor = COLOR_Border;
    _contactNumbertextfield.textColor = COLOR_Title_Label;
    _contactNumbertextfield.borderWidth = 1.0f;
    _contactNumbertextfield.font= FONT_TextField;
    [LSUtils addPaddingToTextField:_contactNumbertextfield];
    
    _contactCodeNumbertextfield.borderStyle =  UITextBorderStyleLine;
    _contactCodeNumbertextfield.borderColor = COLOR_Border;
    _contactCodeNumbertextfield.textColor = COLOR_Title_Label;
    _contactCodeNumbertextfield.borderWidth = 1.0f;
    _contactCodeNumbertextfield.font= FONT_TextField;
    _contactCodeNumbertextfield.textAlignment = NSTextAlignmentCenter;
    
  //  _commentstextView.borderStyle =  UITextBorderStyleLine;
    _commentstextView.borderColor = COLOR_Border;
    _commentstextView.textColor = COLOR_Title_Label;
    _commentstextView.borderWidth = 1.0f;
    _commentstextView.font= FONT_TextField;
    _commentstextView.textContainerInset = UIEdgeInsetsMake(5, 3, 0, 0);

    [LSUtils addPaddingToTextField:_contactNumbertextfield];

    _popViewTopBarTitle.text = @"Pickup address";
    _Label.text = @"Search nearby place ";
    _Label.textColor = COLOR_Title_Label;
    _labeltextfield.placeholder =@"Search...";
    _businessNameLabel.text =@"Business name";
    _businessNameLabel.textColor = COLOR_Title_Label;
    _businessNametextfield.placeholder =@"Enter business name...";
    _addressLine1Label.text =@"Address line 1";
    _addressLine1Label.textColor = COLOR_Title_Label;
    _addressLine2Label.textColor = COLOR_Title_Label;
    _addressLine2Label.text =@"Address line 2";
    _addressLine1textfield.placeholder =@"Enter address line 1...";
    _addressLine2textfield.placeholder =@"Enter address line 2...";
    _postCodelabel.text =@"Zip Code";
    _postCodelabel.textColor = COLOR_Title_Label;
    _cityLabel.text =@"City";
    _cityLabel.textColor = COLOR_Title_Label;
    _citytextfield.placeholder =@"Enter your city name...";
    _stateLabel.text =@"State";
    _stateLabel.textColor = COLOR_Title_Label;
    _statetextfield.placeholder =@"Enter your state name...";
    _countryLabel.text =@"Country";
    _countryLabel.textColor = COLOR_Title_Label;
    
    _contactNumberLabel.text =@"Contact Number";
    _contactNumberLabel.textColor = COLOR_Title_Label;
    _contactNumbertextfield.placeholder =@"Enter your contact number...";
    
    _commentstextView.textColor = COLOR_Title_Label;
    [_commentstextView.placeholderLabel setValue:[UIFont fontWithName:@"Lato-Light" size:15.0] forKeyPath:@"font"];
    UIColor *color = [UIColor grayColor];
    _commentstextView.placeholderLabel.textColor = color;
    _commentstextView.placeholder = @"Enter any comments...";
    
    _commentsLabel.text = @"Comments";
    _commentsLabel.textColor = COLOR_Title_Label;

    [sharedUtils placeholderSize:_countrytextfield :@"Enter your country name..."];
    [sharedUtils placeholderSize:_labeltextfield :@"Search..."];

    [sharedUtils placeholderSize:_businessNametextfield :@"Enter business name..."];
    [sharedUtils placeholderSize:_addressLine1textfield :@"Enter address line 1..."];
    [sharedUtils placeholderSize:_addressLine2textfield :@"Enter address line 2..."];
    [sharedUtils placeholderSize:_countrytextfield :@"Enter your country name..."];
    [sharedUtils placeholderSize:_citytextfield :@"Enter your city name..."];
    [sharedUtils placeholderSize:_postCodetextfield :@"Enter your zip code..."];
    [sharedUtils placeholderSize:_statetextfield :@"Enter your state name..."];
    [sharedUtils placeholderSize:_contactNumbertextfield :@"Enter your contact number..."];

 //   [self placeholderSize:_password:@"Enter your password..."];
    
    
    
    _Label.font= FONT_Title_Label;
    _businessNameLabel.font= FONT_Title_Label;
    _addressLine1Label.font= FONT_Title_Label;
    _addressLine2Label.font= FONT_Title_Label;
    _postCodelabel.font= FONT_Title_Label;
    _cityLabel.font= FONT_Title_Label;
    _stateLabel.font= FONT_Title_Label;
    _countryLabel.font= FONT_Title_Label;
    _contactNumberLabel.font= FONT_Title_Label;
    _commentsLabel.font= FONT_Title_Label;

    [_cancelButton addTarget:self action:@selector(popDown) forControlEvents:UIControlEventTouchUpInside];
    [_doneButton addTarget:self action:@selector(saveAndPopDown) forControlEvents:UIControlEventTouchUpInside];
}

-(void)searchAddress:(id)sender{
    
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    UIViewController *currentTopVC = [self currentTopViewController];
   [ currentTopVC presentViewController:acController animated:YES completion:nil];
      //  [self presentViewController:acController animated:YES completion:nil];
}

- (UIViewController *)currentTopViewController {
    
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}


#pragma mark - GMSAutocompleteViewControllerDeleget method
    
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    
    [self enableFieldWhileEditing];
    [self resetTextField];
    // The user has selected a place.
    [self getAddressFromLatituteLongitute:place.coordinate :place.name];
    
    UIViewController *currentTopVC = [self currentTopViewController];
    [currentTopVC dismissViewControllerAnimated:YES completion:nil];
    
}
    
-(void)getAddressFromLatituteLongitute:(CLLocationCoordinate2D)cordinate :(NSString*)placeName {
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame =CGRectMake(0,0, 100, 100);
    [self addSubview:spinner];
    [spinner startAnimating];
    
    [spinner setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:cordinate completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
        NSLog(@"reverse geocoding results:");
        GMSAddress *addressObj = [response firstResult];
        NSLog(@"coordinate.latitude=%f", addressObj.coordinate.latitude);
        NSLog(@"coordinate.longitude=%f", addressObj.coordinate.longitude);
        NSLog(@"thoroughfare=%@", addressObj.thoroughfare);
        NSLog(@"locality=%@", addressObj.locality);
        NSLog(@"subLocality=%@", addressObj.subLocality);
        NSLog(@"administrativeArea=%@", addressObj.administrativeArea);
        NSLog(@"postalCode=%@", addressObj.postalCode);
        NSLog(@"country=%@", addressObj.country);
        NSLog(@"lines=%@", addressObj.lines);
        [self settextFieldData:addressObj :placeName];
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        
    }];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self resetTextField];
    NSLog(@"Error: %@" , error);
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithError:(NSError *)error {
    
    UIViewController *currentTopVC = [self currentTopViewController];
    
    [currentTopVC dismissViewControllerAnimated:YES completion:nil];
    
}

    // User pressed cancel button.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    
    if(_isInEditMode){
        if(_addressObjectDetail!=nil)
            //To manage place order when edit there clicked
            //and search label clicked pickup model lost
            //going back to place order selcted address lost
            sharedUtils.pickupAddressModel = _addressObjectDetail;
    }
        UIViewController *currentTopVC = [self currentTopViewController];

        [currentTopVC dismissViewControllerAnimated:YES completion:nil];
}
-(void)resetTextField{
    
    _labeltextfield.text = @"";
    _postCodetextfield.text = @"";
    _citytextfield.text = @"";
    _statetextfield.text = @"";
    _countrytextfield.text = @"";
    _addressLine1textfield.text = @"";
    _addressLine2textfield.text = @"";
    _contactNumbertextfield.text = @"";
    
    
}

-(void)settextFieldData :(GMSAddress*)addressObj :(NSString*)placeName{
    
    if(addressObj.lines.count > 0){
        
         NSArray* formattedAddrray= [[addressObj.lines firstObject] componentsSeparatedByString:@","];
        
        if(formattedAddrray.count > 0){
            
        _addressLine1textfield.text = [formattedAddrray firstObject];
        }
        if(formattedAddrray.count > 1){
            _addressLine2textfield.text = [formattedAddrray objectAtIndex:1];
        }else
            _addressLine2textfield.text = addressObj.locality;
    }
    placNameString =@"";
    placNameString = placeName;
    _labeltextfield.text = [LSUtils getShortTitle:placeName length:32];
    _postCodetextfield.text = addressObj.postalCode;
    _citytextfield.text = addressObj.locality;
    _statetextfield.text = addressObj.administrativeArea;
    _countrytextfield.text = addressObj.country;
    
    if(_isInEditMode){
        
        _addressObjectDetail =[[AddressModel alloc]initWithAddressParamsAsAddressTitle:placeName AddressLine1:_addressLine1textfield.text AddressLine2:_addressLine2textfield.text ZipCode:_postCodetextfield.text City:_citytextfield.text State:_statetextfield.text Country:_countrytextfield.text PhoneNumber: [self getPhoneNumber:_contactNumbertextfield.text] Comment:_commentstextView.text BussinessName:_businessNametextfield.text Longitute:addressObj.coordinate.longitude Latitute:addressObj.coordinate.latitude AddressId:_addressObjectDetail.addressId];
    }else
    _addressObjectDetail =[[AddressModel alloc]initWithAddressParamsAsAddressTitle:placeName AddressLine1:_addressLine1textfield.text AddressLine2:_addressLine2textfield.text ZipCode:_postCodetextfield.text City:_citytextfield.text State:_statetextfield.text Country:_countrytextfield.text PhoneNumber:[self getPhoneNumber:_contactNumbertextfield.text] Comment:_commentstextView.text BussinessName:_businessNametextfield.text Longitute:addressObj.coordinate.longitude Latitute:addressObj.coordinate.latitude AddressId:@""];
    
}

-(AddressModel*)setField{
    
    return [[AddressModel alloc]initWithAddressParamsAsAddressTitle:placNameString AddressLine1:_addressLine1textfield.text AddressLine2:_addressLine2textfield.text ZipCode:_postCodetextfield.text City:_citytextfield.text State:_statetextfield.text Country:_countrytextfield.text PhoneNumber:[self getPhoneNumber:_contactNumbertextfield.text] Comment:_commentstextView.text BussinessName:_businessNametextfield.text Longitute:_addressObjectDetail.longitute Latitute:_addressObjectDetail.latitue AddressId:@""];
}

-(AddressModel*)setFieldsWhenEditingAddress {
    
    AddressModel *newaddress;
    
    newaddress  =[[AddressModel alloc]initWithAddressParamsAsAddressTitle:placNameString AddressLine1:_addressLine1textfield.text AddressLine2:_addressLine2textfield.text ZipCode:_postCodetextfield.text City:_citytextfield.text State:_statetextfield.text Country:_countrytextfield.text PhoneNumber:[self getPhoneNumber:_contactNumbertextfield.text] Comment:_commentstextView.text BussinessName:_businessNametextfield.text Longitute:_addressObjectDetail.longitute Latitute:_addressObjectDetail.latitue AddressId:_addressObjectDetail.addressId];
    
    return newaddress;
}

// Called by Cancel
- (void) popDown{
    
    [self endEditing:true];
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    _isInEditMode = false;
    
    [UIView animateWithDuration:1.0   delay:0.0
                        options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         
                         self.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + self.frame.size.height, self.frame.size.width, self.frame.size.height);
                         self.superview.backgroundColor = [UIColor clearColor];

                     }
                     completion:^(BOOL finished){
                         if(finished){
                             [self.superview removeFromSuperview];

                         }
                     }];
}
- (void) saveAndPopDown{
    
    switch (_parentControllerTypePickUP) {
        case 0:{
//            
//            //Order Description screen
            placNameString = self.labeltextfield.text;
            AddressModel *editedAddressDetail = [self setFieldsWhenEditingAddress];
            _addressObjectDetail = nil;
            _addressObjectDetail = editedAddressDetail;;
            if([self validatetextField:_addressObjectDetail]){
                 placNameString = _labeltextfield.text;
                [self saveData:_addressObjectDetail :_isInEditMode];
            }
           
        }
            break;
        case 1:{  //From Edit screen
            
            AddressModel *editedAddressDetail = [self setFieldsWhenEditingAddress];
            _addressObjectDetail = nil;
            _addressObjectDetail = editedAddressDetail;
        }
            
            break;
            
        case 2:{  //From Add screen
            
            AddressModel *currentAddressObject = [self setField];
            _addressObjectDetail = nil;
            _addressObjectDetail = currentAddressObject;
            
        }
    }
   
    if([self validatetextField:_addressObjectDetail] && (_parentControllerTypePickUP == 1 ||_parentControllerTypePickUP == 2)) {
        
        if([self.pickUPAddressDel respondsToSelector:@selector(saveData: canUpdate:)]){
            
            [self.pickUPAddressDel saveData:_addressObjectDetail canUpdate:_isInEditMode];
            _addressObjectDetail = nil;
        }
                    [self popDown];
    }
    
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        NSLog(@"Unreachable");
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(BOOL)validatetextField :(AddressModel*)addresss{
    
    if(![LSUtils checkNilandEmptyString:_labeltextfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Label can not be blank." withType:Negative];
        return false;
    }
    
    if([_businessNametextfield.text length] > 50){
        
        [[NotificationManager notificationManager] displayMessage:@"Bussiness name should be within 50 charecters." withType:Negative];
        return false;
        
    }
    
    if(![LSUtils checkNilandEmptyString:_addressLine1textfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Address Line 1 can not be blank." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_addressLine2textfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Address Line 2 can not be blank. " withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_citytextfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"City can not be blank." withType:Negative];
        return false;
    }

    if(![LSUtils checkNilandEmptyString:_statetextfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"State can not be blank." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_countrytextfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Country can not be blank." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_postCodetextfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Zip code can not be blank." withType:Negative];
        return false;
    }
    
    if(![LSUtils validatePostalCodeWithString:_postCodetextfield.text]){
        
         [[NotificationManager notificationManager] displayMessage:@"Invalid zip code." withType:Negative];
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_contactNumbertextfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Phone Number can not be blank." withType:Negative];
        return false;
        
    }
    if([self getLength:_contactNumbertextfield.text ]<10){
        [[NotificationManager notificationManager] displayMessage:@"Phone Number length can not be less than 10." withType:Negative];
        return false;
        
    }
    if(addresss.latitue == 0  || addresss.longitute == 0){
        
        [[NotificationManager notificationManager] displayMessage:@"Address is invalid." withType:Negative];
        return false;
    }
    return true;
}


#pragma mark - UITextField Delegate Methods

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == _labeltextfield){
        
        GMSAutocompleteViewController *acController =   [[GMSAutocompleteViewController alloc] init];
        acController.delegate = self;
        UIViewController *currentTopVC = [self currentTopViewController];
        [ currentTopVC presentViewController:acController animated:YES completion:nil];
        [textField resignFirstResponder];
        return false;
        
    }
    return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_countrytextfield)
    {
        if(!([_countrytextfield.text length]>0))
        {
            [sharedUtils placeholderSize:_countrytextfield :@"Enter your country name..."];
        }
    }

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if(textField == _businessNametextfield){
        [_addressLine1textfield becomeFirstResponder];
    }
    if(textField == _addressLine1textfield){
        [_addressLine2textfield becomeFirstResponder];
    }
    if(textField == _addressLine2textfield){
        [_citytextfield becomeFirstResponder];
    }
    if(textField == _citytextfield){
        [_statetextfield becomeFirstResponder];
    }
    if(textField == _statetextfield){
        [_countrytextfield becomeFirstResponder];
    }
    if(textField == _countrytextfield){
        [_postCodetextfield becomeFirstResponder];
    }
    if(textField == _postCodetextfield){
        [_contactNumbertextfield becomeFirstResponder];
    }
    if(textField == _contactNumbertextfield){
        [_commentstextView becomeFirstResponder];
    }
//    if(textField == _commentstextView){
//        [_commentstextView resignFirstResponder];
//    }
    return true;
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    //Always allow back space
    if ([text isEqualToString:@""]) {
        return YES;
    }
    // Disable emoji input
    if (![text canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    NSRange spaceRange = [text rangeOfString:@" "];
    if([textView.text length] == 0){
        if (spaceRange.location != NSNotFound)
        {
            return NO;
        } else {
            return YES;
        }
    }
    if (textView == _commentstextView) {
        
        if ([textView.text length] >=250 ) {
            return false;
        }
    }
    return true;

}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    NSRange spaceRange = [string rangeOfString:@" "];
    
    if([textField.text length] == 0){
        if (spaceRange.location != NSNotFound)
        {
            return NO;
        }
    }

    if(textField == _contactNumbertextfield ) {
        
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }
        
        
        int length = (int)[self getLength:textField.text];
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
    }
    if(textField == _addressLine1textfield || textField == _addressLine2textfield) {
        if([textField.text length]>=60){
            return false;
        }
        return true;
    }
    if(textField == _businessNametextfield ) {
        if([textField.text length]>=50){
            return false;
        }
        return true;
    }

    return  true;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if (textView == _commentstextView)
    {
        if([_commentstextView.text length] == 0){
            UIColor *color = [UIColor grayColor];
            _commentstextView.placeholderLabel.textColor = color;
            
            [_commentstextView.placeholderLabel setValue:[UIFont fontWithName:@"Lato-Light" size:15.0] forKeyPath:@"font"];
            
            _commentstextView.placeholder =@"Enter any comments...";
        }
    }
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    _commentstextView.placeholder =@"";
    [_commentstextView setValue:[UIFont fontWithName:@"Lato-Regular" size:16.0] forKeyPath:@"font"];
    
    return true;
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        
    }
    
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}

- (NSString*)getPhoneNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSString *mobileNumberWithCountryCode = [@"+01" stringByAppendingString:mobileNumber];
    return mobileNumberWithCountryCode;
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self  endEditing:true];
}


-(void)saveData:(AddressModel*)addressObject :(BOOL)isEdited{
    
    if([LSUtils isNetworkConnected]) {
        
        [MBProgressHUD showHUDAddedTo:self animated:YES];
        
        //update address
        [sharedUtils updatePickUpAddress:addressObject Completion:^(BOOL success, NSDictionary *result, NSError *error) {
            [MBProgressHUD hideHUDForView:self animated:YES];
            
            NSLog(@"%@", addressObject.addressTitle);
            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                UIViewController *currentTopVC = [self currentTopViewController];
                [currentTopVC presentViewController:controller animated:YES completion:nil];
            }
            if(success){
                
                if([[result valueForKey:@"code"]intValue] == 200){
                    
                    sharedUtils.pickupAddressModel = nil;
                    sharedUtils.pickupAddressModel = addressObject;
                    [sharedUtils getAllPickUpAddress:^(BOOL success, NSDictionary *result, NSError *error) {
                        if(success){
                            if([[result valueForKey:@"code"]intValue] == 200){
                                if([self.pickUPAddressDel respondsToSelector:@selector(updateOrderDescriptioScreen:)]){
                                    [self.pickUPAddressDel  updateOrderDescriptioScreen:addressObject];
                                }
                            }
                        }
                    }];
                }
                else  if([[result valueForKey:@"code"]intValue] == 211){
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Address is invalid."];
                    
                    UIViewController *currentTopVC = [self currentTopViewController];
                    [currentTopVC presentViewController:controller animated:YES completion:nil];
                    
                }
                else  if([[result valueForKey:@"code"]intValue] == 300){
                    UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Address could not be updated."];
                    
                    UIViewController *currentTopVC = [self currentTopViewController];
                    [currentTopVC presentViewController:controller animated:YES completion:nil];
                }
                
                [self popDown];
                
            }else{
                UIViewController*  controller;
                
                if(result == nil){
                    
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                }else{
                    NSLog(@"Result2%@",result);
                    controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Some problem occured."];
                }
                UIViewController *currentTopVC = [self currentTopViewController];
                [currentTopVC presentViewController:controller animated:YES completion:nil];
                [self popDown];
                
            }
        }];
    }else{
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
        UIViewController *currentTopVC = [self currentTopViewController];
        [currentTopVC presentViewController:controller animated:YES completion:nil];
    }
}



@end

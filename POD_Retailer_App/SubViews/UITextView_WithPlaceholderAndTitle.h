//
//  UITextView_WithPlaceholderAndTitle.h
//  CommusoftV2
//
//  Created by Jonathan Neumann on 24/11/2015.
//  Copyright © 2015 Commusoft Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UILabel_RedRequiredStar.h"
#import "UITextView_WithPlaceholder.h"

@interface UITextView_WithPlaceholderAndTitle : UIView

@property BOOL isHidden;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UITextView_WithPlaceholder *textView;
@property (nonatomic, strong, getter=getPlaceholder) NSString *placeholder;
@property (nonatomic) BOOL isRequired;
@property (nonatomic, strong, getter=getText) NSString *text; // Have to define the getter method or it doesn't get called (unlike the setter...)
@property (nonatomic, assign) id delegate;

-(void)textfieldEditable;

@end

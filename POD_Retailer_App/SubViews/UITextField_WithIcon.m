//
//  UITextField_WithIcon.m
//  PODApplication
//
//  Created by Arpana on 24/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "UITextField_WithIcon.h"

@implementation UITextField_WithIcon

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
       // [self layoutIfNeeded];

        self.clipsToBounds = true;
        self.backgroundColor = [UIColor whiteColor];
        
        _textfield = [[UITextField alloc] init];
        _textfield.font = FONT_TextField;

        _textFieldTitlelabel = [[UILabel alloc]init];
        _textFieldTitlelabel.font = FONT_Title_Label;
        
        _iconImage = [[UIImageView alloc]init];
        _viewForTitle = [[UIView alloc]init];
        _viewForTextFieldAndIcon = [[UIView alloc]init];

        _viewForTitle.backgroundColor = APP_BACKGROUND_COLOR;
        
        _viewForTextFieldAndIcon.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _viewForTextFieldAndIcon.layer.borderWidth = 1;
        

        [self addSubview:_viewForTitle];
        [self addSubview:_viewForTextFieldAndIcon];

        [_viewForTextFieldAndIcon addSubview:_textfield];
        [_viewForTextFieldAndIcon addSubview:_iconImage];
        [_viewForTitle addSubview:_textFieldTitlelabel];

        [self setFrame:self.frame];
        
    }
    return self;

}
-(void)setFrame:(CGRect)frame{
    
    [super setFrame:CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), CGRectGetHeight(frame))];
    
    _viewForTitle.frame = CGRectMake(0,0,CGRectGetWidth(frame),CGRectGetHeight(frame)/2 - 2);
    
    _viewForTextFieldAndIcon.frame = CGRectMake(0,CGRectGetMaxY( _viewForTitle.frame),CGRectGetWidth(frame),CGRectGetHeight(frame)- CGRectGetHeight(_viewForTitle.frame));


    _textFieldTitlelabel.frame = CGRectMake(0,0,CGRectGetWidth(_viewForTitle.frame),CGRectGetHeight(_viewForTextFieldAndIcon.frame));
    
    _textfield.frame = CGRectMake(10,0, CGRectGetWidth(frame)-40, CGRectGetHeight(_viewForTextFieldAndIcon.frame));
    _viewForTitle.backgroundColor = [UIColor redColor];
    
    _iconImage.frame = CGRectMake(CGRectGetMaxX(_textfield.frame),CGRectGetMinY(_textfield.frame), CGRectGetWidth(frame)-CGRectGetWidth(_textfield.frame)- 12,CGRectGetHeight(_textfield.frame));
}

@end

//
//  UITextField_WithTitle.m
//  CommusoftV2
//
//  Created by Jonathan Neumann on 05/11/2015.
//  Copyright © 2015 Commusoft Inc. All rights reserved.
//

#import "UITextField_WithTitle.h"

@implementation UITextField_WithTitle

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        // Prevent overflow to be displayed if the frame is 0 high for example
        self.clipsToBounds = true;
        _isHidden = false;
        _isRequired = false;
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = FONT_LABEL_BOLD;
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = [UIColor redColor ];
        
        _textfield = [[UITextField alloc] init];
        _textfield.font = FONT_LABEL_REGULAR;
        //_textfield.placeholder = NSLocalizedString(@"Tap to enter", @"");
        _textfield.delegate = self.delegate;
        _textfield.backgroundColor = [UIColor whiteColor];
        [self addSubview:_titleLabel];
        [self addSubview:_textfield];
        
        [self setFrame:self.frame];

        
    }
    return self;
}


- (NSString *) getText{
    return [_textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void) setText:(NSString *)text{
    _textfield.text = text;
}

- (void) setDelegate:(id)delegate{
    _textfield.delegate = delegate;
}

// The frame is 65pt high for iPhone = 15pt top padding + 20pt title label + 30pt UITextField and 85pt for iPad = 15pt top padding + 30pt title label + 40pt UITextField
- (void) setFrame:(CGRect)frame{
    
    _titleLabel.frame = CGRectMake(0, 0, CGRectGetWidth(frame) - 20, 30);
//    [_titleLabel sizeToFit];
//    if(_titleLabel.text.length!=0){
//        CGSize size=[LSUtils textString:_titleLabel.text sizeWithFont:_titleLabel.font constrainedToSize:CGSizeMake(CGRectGetWidth(self.frame) - 20, MAXFLOAT)];
//       _titleLabel.frame = CGRectMake(10, 15, CGRectGetWidth(self.frame) - 20, size.height);
//    }
//    else{
        _titleLabel.frame = CGRectMake(10, 15, CGRectGetWidth(self.frame) - 20, 20);
   // }
    _textfield.frame = CGRectMake(10, CGRectGetMaxY(_titleLabel.frame), CGRectGetWidth(self.frame) - 20, 30);
    
    CGFloat totalHeight = _isHidden ? 0 : (CGRectGetMaxY(_textfield.frame));
    
    [super setFrame:CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), totalHeight)];
}

- (void) setTitle:(NSString *)title{
    _title = title;
    
//    if (_isRequired){
//        NSAttributedString *textWithRedStar = [Functions getStringWithRedRequiredStar:_title];
//        [_titleLabel setAttributedText:textWithRedStar];
//    }
//    else{
        [_titleLabel setText:_title];
   // }
}

- (NSString *) getTitle{
    
    if (_isRequired){
        return [_titleLabel.attributedText.string stringByReplacingOccurrencesOfString:@" *" withString:@""];
    }
    else{
        return _titleLabel.text;
    }
}

- (void) setIsRequired:(BOOL)isRequired{
    
    _isRequired = isRequired;
    [self setTitle:_title];
    
    [self setFrame:self.frame];
}

@end

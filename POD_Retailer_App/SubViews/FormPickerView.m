
//
//  FormPickerView.m
//  CommusoftV2
//
//  Created by Jonathan Neumann on 27/08/2015.
//  Copyright (c) 2015 Commusoft Inc. All rights reserved.
//

#import "FormPickerView.h"

@implementation FormPickerView

- (instancetype)init{
    
    self = [super init];
    if (self) {
        
        _constantsSingleton =  [LSUtils sharedUtitlities];

        
        // TESTING - Create some bogus data
        _pickerDataArray = @[@"Item 1", @"Item 2", @"Item 3"];

        // Create the UI
        [self createUI];
        
    }
    return self;
}

- (void) createUI{
    
    // Background that turns to transparent black when the view appears
    _backgroundCoverView = [[UIView alloc] init];
    _backgroundCoverView.backgroundColor = [UIColor blackColor];
    _backgroundCoverView.alpha = 0;
    
    _popView = [[UIView alloc] init];
    _popView.backgroundColor = ColourConstants_LightBackground;
    
    _popViewTopBar = [[UIView alloc] init];
    _popViewTopBar.backgroundColor = [UIColor whiteColor];
    
    _cancelButton = [[UIButton alloc] init];
    [_cancelButton setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor colorWithRed:59.0f/255 green:135.0f/255 blue:242.0f/255 alpha:1] forState:UIControlStateNormal];
    _cancelButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    
    _doneButton = [[UIButton alloc] init];
    [_doneButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    [_doneButton setTitleColor:[UIColor colorWithRed:59.0f/255 green:135.0f/255 blue:242.0f/255 alpha:1] forState:UIControlStateNormal];
    _doneButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    
    _popViewPickerView = [[UIPickerView alloc] init];
    
    // Add the subviews to the popView
    [_popView addSubview:_popViewTopBar];
    [_popView addSubview:_popViewPickerView];
    
    _popViewPickerView.delegate = self;
    _popViewPickerView.dataSource = self;
    
    // Add the subviews to the view
    [self addSubview:_backgroundCoverView];
    
    
    [_cancelButton addTarget:self action:@selector(popDown) forControlEvents:UIControlEventTouchUpInside];
    [_doneButton addTarget:self action:@selector(saveAndPopDown) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Add the subviews to the popBar
    [_popViewTopBar addSubview:_cancelButton];
    [_popViewTopBar addSubview:_doneButton];
    
    // Add the subviews to the popView
    [_popView addSubview:_popViewTopBar];
    [self addSubview:_popView];
    [self layoutSubviews];
    [self popUp];
    
}

// Called by Cancel
- (void) popDown{
    // Animate backwards
    [UIView animateWithDuration:0.4 animations:^{
        _backgroundCoverView.alpha = 0;
        _popView.frame = CGRectMake(0, CGRectGetMaxY(_popView.frame) + CGRectGetHeight(_popView.frame), CGRectGetWidth(_popView.frame), CGRectGetHeight(_popView.frame));
    } completion:^(BOOL finished) {
        _isAppeared = false;
        
    }];
    
    if([self.custompickerDel respondsToSelector:@selector(removeNewEventView)]){
        [self.custompickerDel removeNewEventView];
    }
}

// Called by Done
- (void) saveAndPopDown{
    if([self.custompickerDel respondsToSelector:@selector(getSelectedindexNumber:)]){
        [self.custompickerDel getSelectedindexNumber:_indexNo];
    }
    [self popDown];
}


- (void) layoutSubviews{
    [super layoutSubviews];
    
    // Background view
    _backgroundCoverView.frame = CGRectMake(0, 0, _constantsSingleton.screenWidth, _constantsSingleton.screenHeight);
    
    // Pop view
    _popView.frame = CGRectMake(CGRectGetMinX(_popView.frame), CGRectGetMinY(_popView.frame), _constantsSingleton.screenWidth, 200);
    
    // Pop view Top Bar
    _popViewTopBar.frame = CGRectMake(0, 0, _constantsSingleton.screenWidth, 50);
    
    // Cancel Button
    _cancelButton.frame = CGRectMake(0, 0, 100, CGRectGetHeight(_popViewTopBar.frame));
    
    // Done button
    _doneButton.frame = CGRectMake(CGRectGetMaxX(_popViewTopBar.frame) - 100, 0, 100, CGRectGetHeight(_popViewTopBar.frame));
    
    // Picker view
    _popViewPickerView.frame = CGRectMake(0, CGRectGetMaxY(_popViewTopBar.frame), _constantsSingleton.screenWidth, CGRectGetHeight(_popView.frame) - CGRectGetHeight(_popViewTopBar.frame));
}

- (void) popUp{
    
    // Get the top view controller and add our elements to it
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootViewController.view addSubview:_backgroundCoverView];
    [rootViewController.view addSubview:_popView];
    
    // Set our elements in their original positions
    _backgroundCoverView.frame = CGRectMake(0, 0, _constantsSingleton.screenWidth, _constantsSingleton.screenHeight);
    _popView.frame = CGRectMake(0, _constantsSingleton.screenHeight, CGRectGetWidth(_popView.frame), CGRectGetHeight(_popView.frame));
    
    // Animate in place
    [UIView animateWithDuration:0.3 animations:^{
        //
        _backgroundCoverView.alpha = 0.5;
        _popView.frame = CGRectMake(0, _constantsSingleton.screenHeight - CGRectGetHeight(_popView.frame), CGRectGetWidth(_popView.frame), CGRectGetHeight(_popView.frame));
        
    } completion:^(BOOL finished) {
        _isAppeared = true;
    }];
    
}

#pragma mark - UIPICKER DELEGATE METHODS
// The number of columns of data
- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

// The number of rows of data
- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return _pickerDataArray.count;
}

// The title to display in that specific row and column

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return _pickerDataArray[row];
}

// The index of the row and column selected
- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"row %ld and column %ld were selected", (long)row, (long)component);
}

@end

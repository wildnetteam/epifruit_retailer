//
//  PickUpAddressPopoverView.h
//  PODRetailerApp
//
//  Created by Arpana on 02/12/16.
//  Copyright © 2  016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "AddressModel.h"

@protocol pickUPAddressDelegate <NSObject>
@optional
-(void)saveData:(AddressModel*)addressDetail canUpdate:(BOOL)isEdited;
-(void)cancel:(id)sender;
-(void)removePopedView;
-(void)updateOrderDescriptioScreen:(AddressModel*)addModel;
@end

@interface PickUpAddressPopoverView : UIView <UITextFieldDelegate ,UITextViewDelegate, GMSAutocompleteViewControllerDelegate>

typedef enum {
    OrderDesControllerPick = 0,
    EditControllerPick,
    ADDAddressControllerPick
}ControllerTypeForPickUP;

@property (nonatomic, strong) UILabel *Label;
@property (nonatomic, strong) UILabel *businessNameLabel;
@property (nonatomic, strong) UILabel *addressLine1Label;
@property (nonatomic, strong) UILabel *addressLine2Label;
@property (nonatomic, strong) UILabel *postCodelabel;
@property (nonatomic, strong) UILabel *cityLabel;
@property (nonatomic, strong) UILabel *stateLabel;
@property (nonatomic, strong) UIImageView *searchImage;
@property (nonatomic, strong) UILabel *countryLabel;
@property (nonatomic, strong) UILabel *contactNumberLabel;
@property (nonatomic, strong) UILabel *commentsLabel;

@property (nonatomic, strong) UITextField *labeltextfield;
@property (nonatomic, strong) UITextField *businessNametextfield;
@property (nonatomic, strong) UITextField *addressLine1textfield;
@property (nonatomic, strong) UITextField *addressLine2textfield;
@property (nonatomic, strong) UITextField *postCodetextfield;
@property (nonatomic, strong) UITextField *citytextfield;
@property (nonatomic, strong) UITextField *statetextfield;
@property (nonatomic, strong) UITextField *countrytextfield;
@property (nonatomic, strong) UITextField *contactNumbertextfield;
@property (nonatomic, strong) UITextField *contactCodeNumbertextfield;
@property (nonatomic, strong) UITextView *commentstextView;
@property (nonatomic, strong) UIView *topView;

@property (strong, nonatomic) UIView *popViewTopBar; // The bar at the top of the picker view
@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) UILabel *popViewTopBarTitle;

@property (assign , nonatomic) id<pickUPAddressDelegate> pickUPAddressDel;

@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) AddressModel *addressObjectDetail;
@property (nonatomic) BOOL isInEditMode;
@property (nonatomic) ControllerTypeForPickUP parentControllerTypePickUP;



@end

//
//  UIButton_WithTiltleNavigateIcon.m
//  CommusoftV2
//
//  Created by Amit Shakya on 21/01/16.
//  Copyright © 2016 Commusoft Inc. All rights reserved.
//

#import "UIButton_WithTiltleNavigateIcon.h"

@implementation UIButton_WithTiltleNavigateIcon

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
       
        // Prevent overflow to be displayed if the frame is 0 high for example
        self.clipsToBounds = true;
        self.backgroundColor = APP_BACKGROUND_COLOR;
        _isHidden = false;
        _isRequired = false;
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = FONT_LABEL_BOLD;
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.backgroundColor = [UIColor clearColor];
        
        _button = [[UIButton alloc] init];
        [_button setTitle:NSLocalizedString(@"Tap to select", @"") forState:UIControlStateNormal];
        [_button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        _button.titleLabel.font = FONT_LABEL_REGULAR;
        [_button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_button setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [_button setBackgroundColor:[UIColor whiteColor]];
        _buttonTimeLabel = [[UILabel alloc] init];
      //  _buttonTimeLabel.font = FONT_SSSTANDARD_ARROW;
        _buttonTimeLabel.text = @"navigateright";
        _buttonTimeLabel.textColor = [UIColor grayColor];
        
        [self addSubview:_titleLabel];
        [self addSubview:_button];
        [self addSubview:_buttonTimeLabel];
        
        [self setFrame:self.frame];
    }
    return self;
}

- (void) setTitle:(NSString *)text{
        _title = text;
        [_titleLabel setText:_title];
}

- (void) setIsRequired:(BOOL)isRequired{
    _isRequired = isRequired;
    [self setTitle:_title];
}

// The frame is 65pt high for iPhone = 15pt top padding + 20pt title label + 30pt UITextField and 85pt for iPad = 15pt top padding + 30pt title label + 40pt UITextField

-(void)layoutSubviews{
    NSLog(@"Layout ");

}

- (void) setFrame:(CGRect)frame{
    
    _titleLabel.frame = CGRectMake(10, 15, CGRectGetWidth(self.frame) - 20, 20);
    _button.frame = CGRectMake(10, CGRectGetMaxY(_titleLabel.frame), CGRectGetWidth(self.frame) - 45, 30);

    [super setFrame:CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), _isHidden ? 0 : CGRectGetMaxY(_button.frame))];

    
    [_buttonTimeLabel sizeToFit];
    _buttonTimeLabel.frame = CGRectMake(self.frame.size.width - CGRectGetWidth(_buttonTimeLabel.frame) - 10, CGRectGetMidY(_button.frame) - CGRectGetHeight(_buttonTimeLabel.frame)/2, CGRectGetWidth(_buttonTimeLabel.frame), CGRectGetHeight(_buttonTimeLabel.frame));
}

-(void)setButtonTitle:(NSString *)buttonTitle{
    
    NSLog(@"but %@ ",buttonTitle);
    
    if([buttonTitle isEqualToString:@""])
        buttonTitle=NSLocalizedString(@"Tap to select", @"");

   [_button setTitle:buttonTitle forState:UIControlStateNormal];
    
   if([buttonTitle isEqualToString:@"Tap to select"] || [buttonTitle isEqualToString:@""]){
       [_button setTitle:NSLocalizedString(@"Tap to select", @"") forState:UIControlStateNormal];
       [_button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
   }
   else
       [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

}

-(void)setIsEnable:(BOOL)isEnable{
    if(isEnable){
        _buttonTimeLabel.hidden=false;
        _button.userInteractionEnabled=true;
    }
    else{
        _buttonTimeLabel.hidden=true;
        _button.userInteractionEnabled=false;
    }
}

@end

//
//  FormPickerView.h
//  CommusoftV2
//
//  Created by Jonathan Neumann on 27/08/2015.
//  Copyright (c) 2015 Commusoft Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol selectDelegate <NSObject>
@optional
-(void)getSelectedindexNumber:(int)_number;
-(void)removeNewEventView;
@end

@interface FormPickerView : UIView <UIPickerViewDelegate, UIPickerViewDataSource>

// iVars
@property (strong, nonatomic) LSUtils *constantsSingleton;
@property (strong, nonatomic) NSArray *pickerDataArray; // The array holding the data displayed by the picker

//UI
@property (strong, nonatomic) UIView *backgroundCoverView; // The black transparent view that covers the current screen
@property (strong, nonatomic) UIView *popView; // The view that hold the bar and the UIPickerView
@property (strong, nonatomic) UIView *popViewTopBar; // The bar at the top of the picker view
@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) UIPickerView *popViewPickerView; // The picker view itself
@property (assign , nonatomic) id<selectDelegate> custompickerDel;
@property (nonatomic) int indexNo;
@property (nonatomic) BOOL isAppeared;

- (void) popUp;

@end

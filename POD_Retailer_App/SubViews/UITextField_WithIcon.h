//
//  UITextField_WithIcon.h
//  PODApplication
//
//  Created by Arpana on 24/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField_WithIcon : UIView

//UI
@property (nonatomic, strong) UITextField *textfield;
//@property (nonatomic, strong) UILabel *iconLabel;
@property (nonatomic, strong) UILabel *textFieldTitlelabel;
@property (nonatomic, strong) UIImageView *iconImage;
//@property (nonatomic, strong) UIView *separatorLine;
@property (nonatomic, strong) UIView *viewForTitle;
@property (nonatomic, strong) UIView *viewForTextFieldAndIcon;

-(void)setFrame:(CGRect)frame;

@end

//
//  UITextField_WithTitle.h
//  CommusoftV2
//
//  Created by Jonathan Neumann on 05/11/2015.
//  Copyright © 2015 Commusoft Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField_WithTitle : UIView

@property BOOL isHidden;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UITextField *textfield;
@property (nonatomic, strong, getter=getText) NSString *text; // Have to define the getter method or it doesn't get called (unlike the setter...)
@property (nonatomic) BOOL isRequired;
@property (nonatomic, assign) id delegate;

@end
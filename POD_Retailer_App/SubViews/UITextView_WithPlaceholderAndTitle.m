//
//  UITextView_WithPlaceholderAndTitle.m
//  CommusoftV2
//
//  Created by Jonathan Neumann on 24/11/2015.
//  Copyright © 2015 Commusoft Inc. All rights reserved.
//

#import "UITextView_WithPlaceholderAndTitle.h"

@implementation UITextView_WithPlaceholderAndTitle

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        // Prevent overflow to be displayed if the frame is 0 high for example
        self.clipsToBounds = true;
        _isHidden = false;
        _isRequired = false;
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = FONT_LABEL_BOLD;
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.numberOfLines = 0;
        
        _textView = [[UITextView_WithPlaceholder alloc] init];
        _textView.font = FONT_LABEL_REGULAR;
        _textView.textContainer.lineFragmentPadding = 0;
        _textView.textContainerInset = UIEdgeInsetsZero;
        _textView.backgroundColor =[UIColor whiteColor];
        [self addSubview:_titleLabel];
        [self addSubview:_textView];
        [self setFrame:self.frame];
    }
    return self;
}

- (void) setTitle:(NSString *)title{
    _title = title;
    self.titleLabel.text = title;
}

- (NSString *) getText{
    return _textView.text;
}

- (void) setText:(NSString *)text{
    _textView.text = text;
}

- (void) setPlaceholder:(NSString *)placeholder{
    _textView.placeholder = placeholder;
}

- (NSString *)getPlaceholder{
    return _textView.placeholder;
}

- (void) setIsRequired:(BOOL)isRequired{
    _isRequired = isRequired;
    
    CGRect titleLabelFrame = self.titleLabel.frame;
    [self.titleLabel removeFromSuperview];
    
    self.titleLabel = [[UILabel alloc] init];
    
    self.titleLabel.text = _title;
    self.titleLabel.font = FONT_LABEL_BOLD;
    self.titleLabel.frame = titleLabelFrame;
    self.titleLabel.numberOfLines = 0;

    [self addSubview:self.titleLabel];
    
    [self setFrame:self.frame];
}

- (void) setDelegate:(id)delegate{
    _textView.delegate = delegate;
}

// The frame is 185pt high for iPhone = 15pt top padding + 20pt title label + 115pt UITextField and 85pt for iPad = 15pt top padding + 30pt title label + 135pt UITextField
- (void) setFrame:(CGRect)frame{
    
    _titleLabel.frame = CGRectMake(0, 0, CGRectGetWidth(frame) - 20, 0);
    
    _titleLabel.frame = CGRectMake(10, 15, CGRectGetWidth(self.frame) - 20, 20);
    
    _textView.frame = CGRectMake(10, CGRectGetMaxY(_titleLabel.frame) + 10, CGRectGetWidth(self.frame) - 20,  110);
    
    CGFloat totalHeight = _isHidden ? 0 : (CGRectGetMaxY(_textView.frame));
    [super setFrame:CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), totalHeight)];
}
-(void)textfieldEditable
{
    _textView.editable=NO;
}

@end
